﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaMedio.DAO
    {
  public  class DAO__MENU
        {
        public void Actualizar(_Menu pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    _Menu aux = (from A in db._Menu where A.MenuId == pObj.MenuId select A).SingleOrDefault();
                    aux.MenuId = pObj.MenuId;
                    aux.Descripcion = pObj.Descripcion;
                    aux.PadreId = pObj.PadreId;
                    aux.Habilitado = pObj.Habilitado;
                    aux.Url = pObj.Url;
                    aux.UsuarioCreacion = pObj.UsuarioCreacion;
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Insertar(_Menu pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {

                    db._Menu.InsertOnSubmit(pObj);
                    db.SubmitChanges();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(_Menu pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {                   
                    db.usp__MenuDelete(pObj.MenuId);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(int p_MenuId)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp__MenuDelete(p_MenuId);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public _Menu get_Menu(int p_MenuId)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                _Menu aux = (from A in db._Menu where A.MenuId == p_MenuId select A).SingleOrDefault();
                return aux;
                }
            }
        }
    }
