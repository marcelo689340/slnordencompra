﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace CapaMedio.DAO
{
    public class DAO_SECTOR
    {
        public void Actualizar_Sector_Compra(List<Sector_Compra> p_lst)
        {
            using (TransactionScope trans = new TransactionScope())
            {
                try
                {
                    OrdenDataContext db = CBaseDato.getConexionDirecta();
                    bool borrar;
                    foreach (Sector_Compra fila in p_lst)
                    {
                        borrar = false;
                        if (fila.estado == false)
                        {
                            borrar = true;
                        }
                        var aux = (from A in db.Sector_Compra where A.id_sector == fila.id_sector & A.id_sector_compra == fila.id_sector_compra select A).SingleOrDefault();
                        if (aux == null)
                        {
                            if (!borrar)
                            {
                                db.Sector_Compra.InsertOnSubmit(fila);
                                db.SubmitChanges();
                            }
                        }
                        else
                        {
                            if (!borrar)
                            {
                                Sector_Compra_Actualizar(fila);
                            }
                            else
                            {
                                db.Sector_Compra.DeleteOnSubmit(aux);
                                db.SubmitChanges();
                            }
                        }
                    }
                    trans.Complete();
                }
                catch (TransactionAbortedException ex)
                {
                    // writer.WriteLine("TransactionAbortedException Message: {0}", ex.Message);
                    CLogging.Log(ex.Message);
                    throw ex;
                }
                catch (ApplicationException ex)
                {
                    //writer.WriteLine("ApplicationException Message: {0}", ex.Message);
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }

        private void Sector_Compra_Actualizar(Sector_Compra fila)
        {
            OrdenDataContext db = CBaseDato.getConexionDirecta();
            var aux = (from A in db.Sector_Compra where A.id_sector == fila.id_sector & A.id_sector_compra == fila.id_sector_compra select A).SingleOrDefault();
            aux.estado = fila.estado;
            db.SubmitChanges();
            db.Dispose();

        }
        public void Actualizar(Sector pObj)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {

                    Sector Aux = (from A in db.Sector where A.id_sector == pObj.id_sector select A).SingleOrDefault();
                    Aux.descripcion = pObj.descripcion;
                    Aux.Limite = pObj.Limite;
                    Aux.estado = pObj.estado;
                    Aux.add_usuario = pObj.add_usuario;

                    //Sector pObjNuevo = new Sector();
                    //pObjNuevo = setSector(pObj.id_sector);

                    //pObjNuevo.id_sector = pObj.id_sector;
                    //db.Sector.Attach(pObjNuevo, pObj);
                    //db.Refresh(System.Data.Linq.RefreshMode.KeepChanges, pObj);
                    db.SubmitChanges();
                    db.Dispose();


                    //Sector Aux = (from A in db.Sector where A.id_sector == pObj.id_sector select A).SingleOrDefault();
                    //Aux.descripcion = pObj.descripcion;
                    //Aux.estado = pObj.estado;
                    //Aux.fecha_ult_mod = pObj.fecha_ult_mod;
                    //Aux.add_usuario = pObj.add_usuario;
                    //db.SubmitChanges();
                    //db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Insertar(Sector pObj)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.Sector.InsertOnSubmit(pObj);
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Borrar(Sector pObj)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.usp_SectorDelete(pObj.id_sector, pObj.add_usuario);
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Borrar(int p_id_sector, int p_add_usuario)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.usp_UsuarioDelete(p_id_sector, p_add_usuario);
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public Sector setSector(int p_id_sector)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                Sector Aux = (from A in db.Sector where A.id_sector == p_id_sector select A).SingleOrDefault();
                return Aux;
            }
        }
        public List<Sector> setAllSector(int p_id_sector)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                List<Sector> Aux = (from A in db.Sector where A.id_sector == p_id_sector select A).ToList<Sector>();
                return Aux;
            }
        }

    }
}
