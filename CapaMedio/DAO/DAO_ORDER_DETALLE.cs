﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaMedio.DAO
    {
    public class DAO_ORDER_DETALLE
        {
        public void Actualizar(Orden_Detalle pOrder_Detalle)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    Orden_Detalle Aux = (from A in db.Orden_Detalle where A.id_orden == pOrder_Detalle.id_orden && A.item == pOrder_Detalle.item select A).SingleOrDefault();
                    Aux.cantidad = pOrder_Detalle.cantidad;
                    Aux.descripcion = pOrder_Detalle.descripcion;
                    Aux.precio = pOrder_Detalle.precio;
                    Aux.total = pOrder_Detalle.total;
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Actualizar(List<Orden_Detalle> pOrder_Detalle)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                var lqs = from A in db.Orden_Detalle where A.id_orden == pOrder_Detalle[0].id_orden select A;
                foreach (Orden_Detalle ord in lqs)
                    {
                    Orden_Detalle aux = (from A in pOrder_Detalle where A.id_orden == ord.id_orden && A.item == ord.item select A).SingleOrDefault();
                    ord.cantidad = aux.cantidad;
                    ord.descripcion = aux.descripcion;
                    ord.precio = aux.precio;
                    ord.total = aux.total;
                    }
                try
                    {
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }

                }
            }
        public void Insertar(Orden_Detalle pOrden_Detalle)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.Orden_Detalle.InsertOnSubmit(pOrden_Detalle);
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Insertar(List<Orden_Detalle> pOrder_Detalle)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.Orden_Detalle.InsertAllOnSubmit(pOrder_Detalle);
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(Int64 p_id_orden)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    List<Orden_Detalle> lqs = (from A in db.Orden_Detalle where A.id_orden == p_id_orden select A).ToList<Orden_Detalle>();
                    db.Orden_Detalle.DeleteAllOnSubmit(lqs);
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(Int64 p_id_orden, int p_item)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    Orden_Detalle lqs = (from A in db.Orden_Detalle where A.id_orden == p_id_orden && A.item == p_item select A).SingleOrDefault();
                    db.Orden_Detalle.DeleteOnSubmit(lqs);
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(Orden_Detalle pOrden_Detalle)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.Orden_Detalle.DeleteOnSubmit(pOrden_Detalle);
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(List<Orden_Detalle> pOrder_Detalle)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.Orden_Detalle.DeleteAllOnSubmit(pOrder_Detalle);
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public List<Orden_Detalle> GetAllOrder_Detalle(Int64 p_id_orden)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                 List<Orden_Detalle> lts = (from A in db.Orden_Detalle where A.id_orden == p_id_orden select A).ToList<Orden_Detalle>();
                 return lts;
                }  
            }
        public Orden_Detalle GetOrder_Detalle(Int64 p_id_orden, int p_item)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                Orden_Detalle lqs = (from A in db.Orden_Detalle where A.id_orden == p_id_orden && A.item == p_item select A).SingleOrDefault();
                return lqs;
                }



            }

        }
    }
