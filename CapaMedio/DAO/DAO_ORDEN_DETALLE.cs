﻿using CapaMedio.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaMedio.DAO
    {
    public class DAO_ORDEN_DETALLE
        {
        public void Actualizar(Orden_Detalle pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    Orden_Detalle Aux = (from A in db.Orden_Detalle where A.id_orden == pObj.id_orden && A.item == pObj.item  select A).SingleOrDefault();
                    Aux.id_sector = pObj.id_sector;
                    Aux.item = pObj.item;
                    Aux.cantidad = pObj.cantidad;
                    Aux.descripcion = pObj.descripcion;
                    Aux.precio = pObj.precio;
                    Aux.total = pObj.total;
                    Aux.id_sector = pObj.id_sector;
                    Aux.add_usuario = pObj.add_usuario;
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Insertar(Orden_Detalle pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.Orden_Detalle.InsertOnSubmit(pObj);
                    db.SubmitChanges();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Insertar(List<Orden_Detalle> pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.Orden_Detalle.InsertAllOnSubmit(pObj);
                    db.SubmitChanges();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(Orden_Detalle pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.Orden_Detalle.DeleteOnSubmit(pObj);
                    db.SubmitChanges();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(Int64 p_id_orden, int p_item, int p_add_usuario)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp_Orden_DetalleDelete(p_id_orden, p_item, p_add_usuario);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }

        public void Set_CDetalle_Orden(List<CDetalle_Orden> lst, long p_id_orden, List<MontoSector> lstMonto)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {

                    foreach (MontoSector row in lstMonto)
                    {
                        var Monto = (from X in db.Orden_Detalle where X.id_orden == p_id_orden & X.id_sector == row.id_sector select X.total).Sum();
                        var MontoActual = (from Y in lst where Y.Id_sector == row.id_sector select Y.Total).Sum();
                        if (((row.monto_orden - Monto) + MontoActual) > row.monto_habilitado) 
                        {
                            var descSector = (from Z in db.Sector where Z.id_sector == row.id_sector select Z).SingleOrDefault();
                            throw new System.ArgumentException("EL SECTOR " + descSector.descripcion + " HA SUPERADO EL MONTO MENSUAL");
                        }
                                                                                                                





                    }







                    var det = (from A in db.Orden_Detalle where A.id_orden == p_id_orden select A);
                    db.Orden_Detalle.DeleteAllOnSubmit(det);
                    Orden_Detalle item = new Orden_Detalle();
                    List<Orden_Detalle> detalle = new List<Orden_Detalle>();
                    foreach (CDetalle_Orden fila in lst)
                    {
                        item.id_orden = p_id_orden;
                        item.item = fila.Item;
                        item.cantidad = fila.Cantidad;
                        item.descripcion = fila.Descripcion;
                        item.precio = fila.Precio;
                        item.total = fila.Total;
                        item.id_sector = fila.Id_sector;
                        item.add_usuario = fila.Add_usuario;
                        detalle.Add(item);
                        item = new Orden_Detalle();
                    }
                    db.Orden_Detalle.InsertAllOnSubmit(detalle);
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Set_CDetalle_Orden(List<CDetalle_Orden> lst, long p_id_orden)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    var det = (from A in db.Orden_Detalle where A.id_orden == p_id_orden select A);                                        
                    db.Orden_Detalle.DeleteAllOnSubmit(det);
                    Orden_Detalle item = new Orden_Detalle();
                    List<Orden_Detalle> detalle = new List<Orden_Detalle>();
                    foreach (CDetalle_Orden fila in lst)
                        {
                        item.id_orden =p_id_orden;
                        item.item = fila.Item;
                        item.cantidad = fila.Cantidad;
                        item.descripcion = fila.Descripcion;
                        item.precio = fila.Precio;
                        item.total = fila.Total;
                        item.id_sector = fila.Id_sector;
                        item.add_usuario = fila.Add_usuario;
                        detalle.Add(item);
                        item = new Orden_Detalle();
                        }
                    db.Orden_Detalle.InsertAllOnSubmit(detalle);
                    db.SubmitChanges();                    
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        }
    }
