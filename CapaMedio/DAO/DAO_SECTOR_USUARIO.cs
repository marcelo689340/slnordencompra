﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace CapaMedio.DAO
    {
    public class DAO_SECTOR_USUARIO
        {


        public void Actualizar(List<Sector_Usuario> pObj)
            {
            using (TransactionScope trans = new TransactionScope())
                {
                try
                    {
                    OrdenDataContext db = CBaseDato.getConexionDirecta();
                    bool borrar;
                    foreach (Sector_Usuario fila in pObj)
                        {
                        borrar = false;
                        if (fila.estado == false)
                            {
                            borrar = true;
                            }

                        var Aux = (from A in db.Sector_Usuario where A.id_usuario == fila.id_usuario && A.id_sector == fila.id_sector select A).SingleOrDefault();
                        if (Aux == null)
                            {
                            if (!borrar)
                                {
                                //Insertar(fila);
                                db.Sector_Usuario.InsertOnSubmit(fila);
                                db.SubmitChanges();
                                }
                            }
                        else
                            {
                            if (!borrar)
                                {
                                Actualizar(fila);
                                }
                            else
                                {
                                db.Sector_Usuario.DeleteOnSubmit(Aux);
                                db.SubmitChanges();
                                }
                            }

                        }
                    trans.Complete();
                    }
                catch (TransactionAbortedException ex)
                    {
                    // writer.WriteLine("TransactionAbortedException Message: {0}", ex.Message);
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                catch (ApplicationException ex)
                    {
                    //writer.WriteLine("ApplicationException Message: {0}", ex.Message);
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }

        //public void Actualizar(List<Sector_Usuario> pObj)
        //    {
        //    using (OrdenDataContext db = CBaseDato.getConexionDirecta())
        //        {
        //        try
        //            {
        //            foreach (Sector_Usuario fila in pObj)
        //                {
        //                var Aux = (from A in db.Sector_Usuario where A.id_usuario == fila.id_usuario && A.id_sector == fila.id_sector select A).SingleOrDefault();
        //                if (Aux == null)
        //                    {
        //                    Insertar(fila);
        //                    }
        //                else
        //                    {
        //                    Actualizar(fila);
        //                    }

        //                }
        //            }
        //        catch (Exception ex)
        //            {
        //            CLogging.Log(ex.Message);
        //            throw ex;
        //            }
        //        }
        //    }
        public void Actualizar(Sector_Usuario pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    Sector_Usuario Aux = (from A in db.Sector_Usuario where A.id_usuario == pObj.id_usuario && A.id_sector == pObj.id_sector select A).SingleOrDefault();
                    Aux.id_usuario = pObj.id_usuario;
                    Aux.id_sector = pObj.id_sector;
                    Aux.estado = pObj.estado;
                    Aux.fecha_ult_mod = pObj.fecha_ult_mod;
                    Aux.add_usuario = pObj.add_usuario;
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Insertar(Sector_Usuario pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                // db.DeferredLoadingEnabled = false;
                try
                    {
                    db.Sector_Usuario.InsertOnSubmit(pObj);
                    db.SubmitChanges();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(Sector_Usuario pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp_Sector_UsuarioDelete(pObj.id_usuario, pObj.id_sector, pObj.add_usuario);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(int p_id_usuario, int p_id_sector, int p_add_usuario)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp_Sector_UsuarioDelete(p_id_usuario, p_id_sector, p_add_usuario);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public Sector_Usuario setSector_Usuario(int p_id_usuario, int p_id_sector)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                Sector_Usuario Aux = (from A in db.Sector_Usuario where A.id_usuario == p_id_usuario && A.id_sector == p_id_sector select A).SingleOrDefault();
                return Aux;
                }
            }
        //public List<Sector_Usuario> setAllSector_Usuario(int p_id_sector)
        //    {
        //    using (OrdenDataContext db = CBaseDato.getConexionDirecta())
        //        {
        //        List<Sector> Aux = (from A in db.Sector where A.id_sector == p_id_sector select A).ToList<Sector>();
        //        return Aux;
        //        }
        //    }
        }
    }
