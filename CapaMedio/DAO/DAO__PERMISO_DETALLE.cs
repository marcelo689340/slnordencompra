﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace CapaMedio.DAO
    {
    public class DAO__PERMISO_DETALLE
        {
        public void Actualizar(_Permiso_Detalle pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    _Permiso_Detalle Aux = (from A in db._Permiso_Detalle where A.id_permiso == pObj.id_permiso && A.MenuId == pObj.MenuId select A).SingleOrDefault();
                    Aux.Consultar = pObj.Consultar;
                    Aux.Eliminar = pObj.Eliminar;
                    Aux.Insertar = pObj.Insertar;
                    Aux.Modificar = pObj.Modificar;
                    Aux.add_usuario = pObj.add_usuario;
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Actualizar(List<_Permiso_Detalle> pObj)
            {
            using (TransactionScope trans = new TransactionScope())
                {
                try
                    {
                    OrdenDataContext db = CBaseDato.getConexionDirecta();
                    bool borrar;
                        foreach (_Permiso_Detalle fila in pObj)
                            {
                            borrar = false;
                            if (fila.Consultar == false && fila.Eliminar == false && fila.Insertar == false && fila.Modificar == false)
                                {
                                borrar = true;
                                }
                            var Aux = (from A in db._Permiso_Detalle where A.id_permiso == fila.id_permiso && A.MenuId == fila.MenuId select A).SingleOrDefault();
                            if (Aux == null)
                                {
                                if (!borrar)
                                    {
                                    db._Permiso_Detalle.InsertOnSubmit(fila);
                                    db.SubmitChanges();
                                    }
                                }
                            else
                                {
                                if (!borrar)
                                    {

                                    Actualizar(fila);
                                    }
                                else
                                    {
                                    db._Permiso_Detalle.DeleteOnSubmit(Aux);
                                    db.SubmitChanges();
                                    }
                                }
                            
                              
                            }
                        trans.Complete();
                        }
                   // }
                catch (TransactionAbortedException ex)
                    {
                    // writer.WriteLine("TransactionAbortedException Message: {0}", ex.Message);
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                catch (ApplicationException ex)
                    {
                    //writer.WriteLine("ApplicationException Message: {0}", ex.Message);
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }  
        public void Insertar(_Permiso_Detalle pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db._Permiso_Detalle.InsertOnSubmit(pObj);
                    db.SubmitChanges();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(_Permiso_Detalle pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp_Permiso_DetalleDelete(pObj.id_permiso, pObj.MenuId, pObj.add_usuario);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(int p_id_permiso, int p_MenuId, int p_add_usuario)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp_Permiso_DetalleDelete(p_id_permiso, p_MenuId, p_add_usuario);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(int p_id_permiso, int p_add_usuario)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp_Permiso_DetalleDeleteAll(p_id_permiso, p_add_usuario);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public _Permiso_Detalle set_Permiso_Detalle(int p_id_permiso, int p_MenuId)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                _Permiso_Detalle Aux = (from A in db._Permiso_Detalle where A.id_permiso == p_id_permiso && A.MenuId == p_MenuId select A).SingleOrDefault();
                return Aux;
                }
            }
        }

    //public void Actualizar(List<_Permiso_Detalle> pObj)
    //    {
    //    using (OrdenDataContext db = CBaseDato.getConexionDirecta())
    //        {
    //        try
    //            {

    //            foreach (_Permiso_Detalle fila in pObj)
    //                {
    //                //_Permiso_Detalle Aux = (from A in db._Permiso_Detalle where A.id_permiso == fila.id_permiso && A.MenuId == fila.MenuId select A).SingleOrDefault();
    //                var Aux = (from A in db._Permiso_Detalle where A.id_permiso == fila.id_permiso && A.MenuId == fila.MenuId select A).SingleOrDefault();
    //                if (Aux == null)
    //                    {
    //                    Insertar(fila);
    //                    }
    //                else
    //                    {
    //                    Actualizar(fila);
    //                    }
    //                }



    //            //Aux.Consultar = pObj.Consultar;
    //            //Aux.Eliminar = pObj.Eliminar;
    //            //Aux.Insertar = pObj.id_permiso;
    //            //Aux.Modificar = pObj.Modificar;
    //            //Aux.add_usuario = pObj.add_usuario;
    //            //db.SubmitChanges();
    //            //db.Dispose();
    //            }
    //        catch (Exception ex)
    //            {
    //            CLogging.Log(ex.Message);
    //            throw ex;
    //            }
    //        }
    //    }
    }
