﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Authentication;
using System.Text;


namespace CapaMedio.DAO
{
    public class DAO_USUARIO
    {
        public string GetMail(int p_id_usuario)
        {
            string direccion = "";
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                var lqs = (from A in db.Usuario where A.id_usuario == p_id_usuario select A).SingleOrDefault();
                if (lqs != null)
                {
                    direccion = lqs.email;
                }
            }
            return direccion;
        }
        public void Actualizar(Usuario pObj)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    Usuario Aux = (from A in db.Usuario where A.id_usuario == pObj.id_usuario select A).SingleOrDefault();
                    Aux.id_usuario = pObj.id_usuario;
                    Aux.log_usuario = pObj.log_usuario;
                    Aux.contrasenha = pObj.contrasenha;
                    Aux.nombre = pObj.nombre;
                    Aux.apellido = pObj.apellido;
                    Aux.email = pObj.email;
                    Aux.celular = pObj.celular;
                    Aux.estado = pObj.estado;
                    Aux.add_usuario = pObj.add_usuario;
                    db.SubmitChanges();
                    db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Insertar(Usuario pObj)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.Usuario.InsertOnSubmit(pObj);
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Borrar(Usuario pObj)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.usp_UsuarioDelete(pObj.id_usuario, pObj.add_usuario);
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Borrar(int p_id_usuario, int p_add_usuario)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.usp_UsuarioDelete(p_id_usuario, p_add_usuario);
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public Usuario GetUsuario(int p_id_usuario)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                Usuario Aux = (from A in db.Usuario where A.id_usuario == p_id_usuario select A).SingleOrDefault();
                return Aux;
            }
        }
        //public List<Usuario> ListAllUsuario(int p_id_usuario)
        //    {
        //    using (OrdenDataContext db = CBaseDato.getConexionDirecta())
        //        {
        //        List<Usuario> Aux = (from A in db.Usuario where A.id_usuario == p_id_usuario select A).ToList<Usuario>();
        //        return Aux;
        //        }
        //    }
        public List<Usuario> ListAllUsuario()
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                List<Usuario> Aux = (from A in db.Usuario select A).ToList<Usuario>();
                return Aux;
            }
        }
        public int Validar_Usuario(string p_log_usuario, string p_contrasebha)
        {
            int id_user = 0;
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                //Usuario Aux = (from A in db.Usuario where A.log_usuario == p_log_usuario && A.contrasenha == p_contrasebha select A).SingleOrDefault();
                try
                {
                    id_user = db.proc_Verifica_Contrasenha(p_log_usuario, p_contrasebha);
                    //id_user = Aux.id_usuario;
                }
                catch (Exception ex)
                {
                    string x = ex.Message.ToString();
                    string mens = "Usuario o password incorrecto";
                    throw new AuthenticationException(mens);
                }
                return id_user;
            }


        }

        public List<Sector> ListSectorUsuario(int p_id_usuario)
        {
            List<Sector> lst = new List<Sector>();
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    lst = (from A in db.Sector join o in db.Sector_Usuario on A.id_sector equals o.id_sector where o.id_usuario == p_id_usuario && A.estado == true select A).ToList<Sector>();

                }
                catch (Exception ex)
                {
                    string x = ex.Message.ToString();
                    string mens = "No existe Sector Asignado";
                    // CLogging.Log(mens);
                    throw new AuthenticationException(mens);
                }
            }
            return lst;
        }

        public List<usp_rolResult> ListValidar_Usuario(int p_id_usuario, string p_pagina)
        {
            List<usp_rolResult> lst = new List<usp_rolResult>();
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    lst = (from A in db.usp_rol(p_id_usuario, p_pagina) select A).ToList<usp_rolResult>();
                }
                catch (Exception ex)
                {

                    string x = ex.Message.ToString();
                    string mens = "No existe Registro";
                    // CLogging.Log(mens);
                    throw new AuthenticationException(mens);
                }
            }
            return lst;

        }
        public usp_rolResult Validar_Usuario(int p_id_usuario, string p_pagina)
        {
            usp_rolResult aux = new usp_rolResult();
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    // var c = db.usp_rol(p_id_usuario, p_pagina).SingleOrDefault();
                    //usp_rolResult aux1 = (from A in db.usp_rol(p_id_usuario, p_pagina) select A).SingleOrDefault();
                    //    aux = (from A in db.usp_rol(p_id_usuario, p_pagina) select A).SingleOrDefault();
                    if (p_id_usuario != 1)
                    {
                        aux = db.usp_rol(p_id_usuario, p_pagina).SingleOrDefault();
                    }
                    else
                    {
                        aux.menu_descripcion = "SUPER";
                        aux.Consultar = true;
                        aux.Insertar = true;
                        aux.Eliminar = true;
                        aux.Modificar = true;
                        aux.estado_permiso = true;
                        aux.estado_usuario = true;
                        aux.estado_usuario_permiso = true;
                        aux.Habilitado = 1;
                        aux.Url = "xxxx";
                    }
                }
                catch (Exception ex)
                {
                    string x = ex.Message.ToString();
                    string mens = "No existe Registro";
                    // CLogging.Log(mens);
                    throw new AuthenticationException(mens);
                }
            }
            return aux;

        }

        public void Cambiar_Password(int p_id_usuario, string p_pass_ant, string p_pass_new)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.Cambiar_Password(p_id_usuario, p_pass_ant, p_pass_new);
                    db.SubmitChanges();
                    db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }

    }
}
