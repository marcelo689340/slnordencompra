﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaMedio.DAO
    {
    public class DAO__PERMISO
        {
        public void Actualizar(_Permiso pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    _Permiso Aux = (from A in db._Permiso where A.id_permiso == pObj.id_permiso select A).SingleOrDefault();
                    Aux.id_permiso = pObj.id_permiso;
                    Aux.descripcion = pObj.descripcion;
                    Aux.estado = pObj.estado;
                    Aux.add_usuario = pObj.add_usuario;
                    db.SubmitChanges();
                    db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Insertar(_Permiso pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db._Permiso.InsertOnSubmit(pObj);
                    db.SubmitChanges();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(_Permiso pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp_PermisoDelete(pObj.id_permiso, pObj.add_usuario);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(int p_id_permiso, int p_add_usuario)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp_PermisoDelete(p_id_permiso, p_add_usuario);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public _Permiso set_Permiso(int p_id_permiso)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                _Permiso Aux = (from A in db._Permiso where A.id_permiso == p_id_permiso select A).SingleOrDefault();
                return Aux;
                }
            }
        }
    }
