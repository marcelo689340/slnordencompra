﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace CapaMedio.DAO
    {
    public class DAO_USUARIO_PERMISO
        {

        public void Actualizar(List<Usuario_Permiso> pObj)
            {
            using (TransactionScope trans = new TransactionScope())
                {
                try
                    {
                    OrdenDataContext db = CBaseDato.getConexionDirecta();
                    bool borrar;
                    foreach (Usuario_Permiso x in pObj)
                        {
                        borrar = false;
                        if (x.estado == false)
                            {
                            borrar = true;
                            }

                        var lqs = (from A in db.Usuario_Permiso where A.id_permiso == x.id_permiso && A.id_usuario == x.id_usuario select A).SingleOrDefault();
                        if (lqs != null)
                            {
                            if (!borrar)
                                {
                                lqs.estado = x.estado;
                                lqs.add_usuario = x.add_usuario;
                                }
                            else
                                {
                                db.Usuario_Permiso.DeleteOnSubmit(lqs);
                                }
                            db.SubmitChanges();
                            }
                        else
                            {
                            if (!borrar)
                                {
                                db.Usuario_Permiso.InsertOnSubmit(x);
                                db.SubmitChanges();                           }
                            }
                        }
                    trans.Complete();
                    }
                catch (TransactionAbortedException ex)
                    {
                    // writer.WriteLine("TransactionAbortedException Message: {0}", ex.Message);
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                catch (ApplicationException ex)
                    {
                    //writer.WriteLine("ApplicationException Message: {0}", ex.Message);
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }

        //public void Actualizar(List<Usuario_Permiso> pObj)
        //    {
        //    using (OrdenDataContext db = CBaseDato.getConexionDirecta())
        //        {
        //        try
        //            {
        //            foreach (Usuario_Permiso x in pObj)
        //                {
        //                var lqs = (from A in db.Usuario_Permiso where A.id_permiso == x.id_permiso && A.id_usuario == x.id_usuario select A).SingleOrDefault();
        //                if (lqs != null)
        //                    {
        //                    lqs.estado = x.estado;
        //                    lqs.add_usuario = x.add_usuario;
        //                    db.SubmitChanges();
        //                    }
        //                else
        //                    {
        //                    db.Usuario_Permiso.InsertOnSubmit(x);
        //                    db.SubmitChanges();
        //                    }

        //                }
        //            db.Dispose();
        //            }
        //        catch (Exception ex)
        //            {
        //            CLogging.Log(ex.Message);
        //            throw ex;
        //            }
        //        }
        //    }
        public void Actualizar(Usuario_Permiso pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {

                    Usuario_Permiso pObjNuevo = new Usuario_Permiso();
                    pObjNuevo.id_permiso = pObj.id_permiso;
                    pObjNuevo.id_usuario = pObj.id_usuario;
                    db.Usuario_Permiso.Attach(pObjNuevo, pObj);
                    db.Refresh(System.Data.Linq.RefreshMode.KeepChanges, pObj);
                    db.SubmitChanges();
                    db.Dispose();
                    //Usuario_Permiso Aux = (from A in db.Usuario_Permiso where A.id_permiso == pObj.id_permiso && A.id_usuario == pObj.id_usuario select A).SingleOrDefault();
                    //Aux.estado = pObj.estado;
                    //Aux.add_usuario = pObj.add_usuario;
                    //db.SubmitChanges();
                    //db.Dispose();

                    //Usuario_Permiso Aux = (from A in db.Usuario_Permiso where A.id_Usuario_Permiso == pObj.id_Usuario_Permiso select A).SingleOrDefault();
                    //Aux.descripcion = pObj.descripcion;
                    //Aux.estado = pObj.estado;
                    //Aux.fecha_ult_mod = pObj.fecha_ult_mod;
                    //Aux.add_usuario = pObj.add_usuario;
                    //db.SubmitChanges();
                    //db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Actualizar_Usuario(Usuario_Permiso pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {

                    //Usuario_Permiso pObjNuevo = new Usuario_Permiso();
                    //pObjNuevo.id_permiso = pObj.id_permiso;
                    //pObjNuevo.id_usuario = pObj.id_usuario;
                    //db.Usuario_Permiso.Attach(pObjNuevo, pObj);
                    //db.Refresh(System.Data.Linq.RefreshMode.KeepChanges, pObj);
                    //db.SubmitChanges();
                    //db.Dispose();
                    Usuario_Permiso Aux = (from A in db.Usuario_Permiso where A.id_usuario == pObj.id_usuario select A).SingleOrDefault();
                    Aux.estado = pObj.estado;
                    Aux.id_permiso = pObj.id_permiso;
                    Aux.add_usuario = pObj.add_usuario;
                    db.SubmitChanges();
                    db.Dispose();

                    //Usuario_Permiso Aux = (from A in db.Usuario_Permiso where A.id_Usuario_Permiso == pObj.id_Usuario_Permiso select A).SingleOrDefault();
                    //Aux.descripcion = pObj.descripcion;
                    //Aux.estado = pObj.estado;
                    //Aux.fecha_ult_mod = pObj.fecha_ult_mod;
                    //Aux.add_usuario = pObj.add_usuario;
                    //db.SubmitChanges();
                    //db.Dispose();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Insertar(Usuario_Permiso pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.Usuario_Permiso.InsertOnSubmit(pObj);
                    db.SubmitChanges();
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(Usuario_Permiso pObj)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp_Usuario_PermisoDelete(pObj.id_permiso, pObj.id_usuario, pObj.add_usuario);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public void Borrar(int p_id_Permiso, int p_id_usuario, int p_add_usuario)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                try
                    {
                    db.usp_Usuario_PermisoDelete(p_id_Permiso, p_id_usuario, p_add_usuario);
                    }
                catch (Exception ex)
                    {
                    CLogging.Log(ex.Message);
                    throw ex;
                    }
                }
            }
        public Usuario_Permiso setUsuario_Permiso(int p_id_Permiso, int p_id_usuario)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                Usuario_Permiso Aux = (from A in db.Usuario_Permiso where A.id_permiso == p_id_Permiso && A.id_usuario == p_id_usuario select A).SingleOrDefault();
                return Aux;
                }
            }


        public Usuario_Permiso setUsuario_Permiso(int p_id_usuario)
            {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
                {
                Usuario_Permiso Aux = (from A in db.Usuario_Permiso where A.id_usuario == p_id_usuario select A).SingleOrDefault();
                return Aux;
                }
            }
        //public List<Usuario_Permiso> setAllUsuario_Permiso(int p_id_Usuario_Permiso)
        //    {
        //    using (OrdenDataContext db = CBaseDato.getConexionDirecta())
        //        {
        //        List<Usuario_Permiso> Aux = (from A in db.Usuario_Permiso where A.id_Usuario_Permiso == p_id_Usuario_Permiso select A).ToList<Usuario_Permiso>();
        //        return Aux;
        //        }
        //    }
        }
    }
