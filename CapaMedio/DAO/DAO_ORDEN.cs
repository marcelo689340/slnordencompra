﻿using CapaMedio.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CapaMedio.DAO
{
    public class DAO_ORDEN
    {
        List<MontoSector> SectorMonto;
        string sInSql;
        private decimal monto_orden = 0;
        private decimal monto_habilitado = 0;
        public DataTable dtPresupuesto(long p_id_orden, int p_item)
        {
            DataTable dt;
            sInSql = "Select * from viewPresupuesto where id_orden = {0} and item = {1} ";
            sInSql = string.Format(sInSql, p_id_orden, p_item);
            dt = CBaseDato.DevuelveDatos(sInSql);
            return dt;
        }
        public viewPresupuesto Presupuesto(long p_id_orden, int p_item)
        {
            viewPresupuesto pre = new viewPresupuesto();
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                pre = (from A in db.viewPresupuesto where A.id_orden == p_id_orden & A.item == p_item select A).SingleOrDefault();
            }
            return pre;
        }
        public viewGridConsultaOrden getOrdenCompra(long p_id_orden)
        {
            viewGridConsultaOrden vieworden = new viewGridConsultaOrden();
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                vieworden = (from A in db.viewGridConsultaOrden where A.id_orden == p_id_orden select A).SingleOrDefault();
            }
            return vieworden;
        }
        public void Insertar_Presupuesto(long p_id_orden, string p_nombre_archivo, string p_obs, int p_id_usuario, byte[] p_presupuesto)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.usp_PresupuestoInsert(p_id_orden, p_nombre_archivo, p_obs, p_presupuesto, p_id_usuario);
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }

        private void Obtener_montos(List<CDetalle_Orden> p_det)
        {
            SectorMonto = new List<MontoSector>();
            MontoSector item = new MontoSector();
            var idSector = (from A in p_det orderby A.Id_sector select A.Id_sector).Distinct();
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                foreach (Int16 id in idSector)
                {
                    item.id_sector = id;
                    try
                    {
                        var lqs = (from A in db.CONSULTAR_MONTO_ORDEN_MES(id) select A).SingleOrDefault();
                        item.monto_orden = decimal.Parse(lqs.MONTO_USADO.ToString());
                        item.monto_habilitado = decimal.Parse(lqs.MONTO_HAB.ToString());
                    }
                    catch (Exception ex)
                    {
                        item.monto_orden = 0;
                        item.monto_habilitado = 0;
                    }
                    SectorMonto.Add(item);
                    item = new MontoSector();
                }
            }            
        }
        private void Obtener_montos(Int32 id_sector)
        {
            monto_orden = 0;
            monto_habilitado = 0;

            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    var lqs = (from A in db.CONSULTAR_MONTO_ORDEN_MES(id_sector) select A).SingleOrDefault();
                    monto_orden = decimal.Parse(lqs.MONTO_USADO.ToString());
                    monto_habilitado = decimal.Parse(lqs.MONTO_HAB.ToString());
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Actualizar(Orden pObj)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {

                    Orden pObjNuevo = new Orden();
                    pObjNuevo.id_orden = pObj.id_orden;
                    db.Orden.Attach(pObjNuevo, pObj);
                    db.Refresh(System.Data.Linq.RefreshMode.KeepChanges, pObj);
                    db.SubmitChanges();
                    db.Dispose();
                    //Orden Aux = (from A in db.Orden where A.id_orden == pObj.id_orden select A).SingleOrDefault();
                    //Aux.id_orden = pObj.id_orden;
                    //Aux.fecha = pObj.fecha;
                    //Aux.id_sector = pObj.id_sector;
                    //Aux.id_usuario = pObj.id_usuario;
                    //Aux.monto = pObj.monto;
                    //Aux.nro_factura = pObj.nro_factura;
                    //Aux.id_usuario_autorizacion = pObj.id_usuario_autorizacion;
                    //Aux.fecha_autorizacion = pObj.fecha_autorizacion;
                    //Aux.id_usuario_procesado = pObj.id_usuario_procesado;
                    //Aux.fecha_procesado = pObj.fecha_procesado;
                    //Aux.id_usuario_rechazado = pObj.id_usuario_rechazado;
                    //Aux.fecha_rechazado = pObj.fecha_rechazado;
                    //Aux.pendiente = pObj.pendiente;
                    //Aux.add_usuario = pObj.add_usuario;
                    //db.SubmitChanges();
                    //db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Insertar(Orden pObj)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.Orden.InsertOnSubmit(pObj);
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Borrar(Orden pObj)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.Orden.DeleteOnSubmit(pObj);
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void Borrar(Int64 p_id_orden, int p_add_usuario)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.usp_OrdenDelete(p_id_orden, p_add_usuario);
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public Int64 GuardarOrden(Int64 p_id_orden, DateTime p_fecha, Int32 p_id_sector, Int32 p_id_usuario, string obs, string p_ruc, List<CDetalle_Orden> det)
        {
            long result = 0;
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    this.Obtener_montos(det);
                    //
                    this.Obtener_montos(p_id_sector);

                    var monto_actual = (from A in det select A.Total).Sum();
                    //
                    if (p_id_orden == 0)
                    {
                        result = db.New_Orden_return(p_fecha, p_id_sector, obs, p_id_usuario, p_ruc);


                        //if ((monto_orden + monto_actual) <= monto_habilitado)
                        //{
                        //    result = db.New_Orden_return(p_fecha, p_id_sector, obs, p_id_usuario, p_ruc);
                        //}
                        //else
                        //{
                        //    throw new Exception("HA SUPERADO EL MONTO HABILITADO PARA COMPRA.... PARA ESTE MES.. POR FAVOR SOLICITE UNA EXTENSION..");
                        //}
                    }
                    else
                    {
                        //decimal monto_anterior = 0;
                        result = p_id_orden;
                        Orden Aux = (from A in db.Orden where A.id_orden == p_id_orden select A).SingleOrDefault();

                        var MontoTotal = (from D in det select D.Total).Sum();

                       // monto_anterior = decimal.Parse(Aux.monto.ToString());

                        //if (((monto_orden - monto_anterior) + monto_actual) <= monto_habilitado)
                        //{
                            Aux.fecha = p_fecha;
                            Aux.id_sector = p_id_sector;
                            // Aux.ruc_proveedor = p_ruc;
                            //Aux.id_usuario = p_id_usuario;
                            Aux.monto = MontoTotal;
                            Aux.observacion = obs;
                            Aux.add_usuario = p_id_usuario;
                            db.SubmitChanges();
                        //}
                        //else
                        //{
                        //    throw new Exception("HA SUPERADO EL MONTO HABILITADO PARA COMPRA PARA EL MES.. POR FAVOR SOLICITE UNA EXTENSION..");
                        //}
                    }

                    DAO_ORDEN_DETALLE dao = new DAO_ORDEN_DETALLE();
                    dao.Set_CDetalle_Orden(det, result, SectorMonto);
                    //dao.Set_CDetalle_Orden(det, result);
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
                return result;
            }
        }
        //public Int64 GuardarOrden(Int64 p_id_orden, DateTime p_fecha, Int32 p_id_sector, Int32 p_id_usuario, string obs, string p_ruc, List<CDetalle_Orden> det)
        //{
        //    long result = 0;
        //    using (OrdenDataContext db = CBaseDato.getConexionDirecta())
        //    {
        //        try
        //        {

        //            //
        //            this.Obtener_montos(p_id_sector);

        //            var monto_actual = (from A in det select A.Total).Sum();
        //            //



        //            if (p_id_orden == 0)
        //            {
        //                if ((monto_orden + monto_actual) <= monto_habilitado)
        //                {
        //                    result = db.New_Orden_return(p_fecha, p_id_sector, obs, p_id_usuario, p_ruc);
        //                }
        //                else
        //                {
        //                    throw new Exception("HA SUPERADO EL MONTO HABILITADO PARA COMPRA.... PARA ESTE MES.. POR FAVOR SOLICITE UNA EXTENSION..");
        //                }
        //            }
        //            else
        //            {

        //                decimal monto_anterior = 0;

        //                result = p_id_orden;
        //                Orden Aux = (from A in db.Orden where A.id_orden == p_id_orden select A).SingleOrDefault();
        //                monto_anterior = decimal.Parse(Aux.monto.ToString());


        //                if (((monto_orden - monto_anterior) + monto_actual) <= monto_habilitado)
        //                {
        //                    Aux.fecha = p_fecha;
        //                    Aux.id_sector = p_id_sector;
        //                    // Aux.ruc_proveedor = p_ruc;
        //                    //Aux.id_usuario = p_id_usuario;
        //                    Aux.observacion = obs;
        //                    Aux.add_usuario = p_id_usuario;
        //                    db.SubmitChanges();
        //                }
        //                else
        //                {
        //                    throw new Exception("HA SUPERADO EL MONTO HABILITADO PARA COMPRA PARA EL MES.. POR FAVOR SOLICITE UNA EXTENSION..");
        //                }




        //            }

        //            DAO_ORDEN_DETALLE dao = new DAO_ORDEN_DETALLE();
        //            dao.Set_CDetalle_Orden(det, result);


        //            //db.New_Orden(p_id_orden, p_fecha, p_id_sector, p_id_usuario);

        //            // db.New_Orden(ref AA, p_fecha, p_id_sector, p_id_usuario, p_add_usuario);

        //        }
        //        catch (Exception ex)
        //        {
        //            CLogging.Log(ex.Message);
        //            throw ex;
        //        }
        //        return result;
        //    }
        //}
        public void ChequearOrden(long p_id_orden, int p_id_usuario, bool p_estado)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    //Usuario Aux = (from A in db.Usuario where A.id_usuario == pObj.id_usuario select A).SingleOrDefault();
                    if (p_estado == true)
                    {
                        db.Check_Orden(p_id_orden, "SI", p_id_usuario);

                    }
                    else
                        db.Cancelar_Check_Orden(p_id_orden);

                    Notificar_Via_Mail_Grupo(p_id_orden, enumEstado.CHEQUEADO, p_estado);

                    db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void AutorizarOrden(long p_id_orden, int p_id_usuario, bool p_estado)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    //Usuario Aux = (from A in db.Usuario where A.id_usuario == pObj.id_usuario select A).SingleOrDefault();
                    if (p_estado == true)
                    {
                        db.Autorizar_Orden(p_id_orden, p_id_usuario);

                    }
                    else
                    {
                        db.Cancelar_Autorizacion_Orden(p_id_orden, p_id_usuario);

                    }
                    Notificar_Via_Mail_Grupo(p_id_orden, enumEstado.AUTORIZADO, p_estado);
                    db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void RechazarOrden(long p_id_orden, int p_id_usuario, string p_obs, bool p_estado)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    //Usuario Aux = (from A in db.Usuario where A.id_usuario == pObj.id_usuario select A).SingleOrDefault();
                    if (p_estado == true)
                    {
                        db.Rechazado_Orden(p_id_orden, p_id_usuario, p_obs);

                    }
                    else
                        db.Cancelar_Rechazado_Orden(p_id_orden, p_id_usuario);

                    Notificar_Via_Mail_Grupo(p_id_orden, enumEstado.RECHAZADO, p_estado);
                    db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void PendienteOrden(long p_id_orden, int p_id_usuario, string p_obs)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.Pendiente_Orden(p_id_orden, p_id_usuario, p_obs);
                    Notificar_Via_Mail_Grupo(p_id_orden, enumEstado.PENDIENTE, true);
                    db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void ProcesarOrden(long p_id_orden, int p_id_usuario, string p_nro_factura, DateTime p_fecha_pago, bool p_estado)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.Procesado_Orden(p_id_orden, p_id_usuario, p_fecha_pago);
                    Notificar_Via_Mail_Grupo(p_id_orden, enumEstado.PROCESADO, p_estado);
                    //Usuario Aux = (from A in db.Usuario where A.id_usuario == pObj.id_usuario select A).SingleOrDefault();
                    //if (p_estado == true)
                    //    db.Procesado_Orden(p_id_orden, p_id_usuario, p_nro_factura, p_fecha_pago);
                    //else
                    //    db.Cancelar_procesado_Orden(p_id_orden, p_id_usuario, p_nro_factura);

                    db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void SetDatoFacturaOrden(long p_id_orden, int p_id_usuario, string p_nro_factura, string p_ruc_proveedor)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {

                    db.DatoFactura_Orden(p_id_orden, p_id_usuario, p_nro_factura, p_ruc_proveedor);
                    //Usuario Aux = (from A in db.Usuario where A.id_usuario == pObj.id_usuario select A).SingleOrDefault();
                    //if (p_estado == true)
                    //    db.DatoFactura_Orden(p_id_orden, p_id_usuario, p_nro_factura, p_ruc_proveedor);
                    //else
                    //    db.Cancelar_procesado_Orden(p_id_orden, p_id_usuario, p_nro_factura);

                    db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void FechaPagoOrden(long p_id_orden, int p_id_usuario, DateTime p_fecha)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    db.fechaPago_Orden(p_id_orden, p_id_usuario, p_fecha);
                    db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public void OrdenCancelarAccion(long p_id_orden, int p_id_usuario)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                try
                {
                    //Usuario Aux = (from A in db.Usuario where A.id_usuario == pObj.id_usuario select A).SingleOrDefault();

                    db.Cancelar_Accion_Orden(p_id_orden, p_id_usuario);

                    db.Dispose();
                }
                catch (Exception ex)
                {
                    CLogging.Log(ex.Message);
                    throw ex;
                }
            }
        }
        public Orden getOrden(long p_id_orden)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                var lqs = (from A in db.Orden where A.id_orden == p_id_orden select A).SingleOrDefault();
                return lqs;
            }
        }
        public List<viewGridOrdenDetalle> GetOrden_Detalle(long p_id_orden)
        {
            List<viewGridOrdenDetalle> lst = new List<viewGridOrdenDetalle>();
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                lst = (from A in db.viewGridOrdenDetalle where A.id_orden == p_id_orden select A).ToList<viewGridOrdenDetalle>();
            }
            return lst;
        }
        public List<viewOrden_Estado> ListOrdenEstado(OrdenEstado p_estado)
        {
            List<viewOrden_Estado> lst = new List<viewOrden_Estado>();
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                switch (p_estado)
                {
                    case OrdenEstado.Autorizado:
                        lst = (from A in db.viewOrden_Estado where A.autorizado == 1 select A).ToList<viewOrden_Estado>();
                        break;
                    case OrdenEstado.Pendiente:
                        lst = (from A in db.viewOrden_Estado where A.pendiente == 1 select A).ToList<viewOrden_Estado>();
                        break;
                    case OrdenEstado.Rechazado:
                        lst = (from A in db.viewOrden_Estado where A.rechazado == 1 select A).ToList<viewOrden_Estado>();
                        break;
                    case OrdenEstado.Procesado:
                        lst = (from A in db.viewOrden_Estado where A.procesado == 1 select A).ToList<viewOrden_Estado>();
                        break;
                    case OrdenEstado.Todos:
                        lst = (from A in db.viewOrden_Estado select A).ToList<viewOrden_Estado>();
                        break;
                }
            }
            return lst;
        }
        public List<viewOrden_Estado> ListOrdenEstadoNull(OrdenEstado p_estado)
        {
            List<viewOrden_Estado> lst = new List<viewOrden_Estado>();
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                switch (p_estado)
                {
                    case OrdenEstado.Autorizado:
                        lst = (from A in db.viewOrden_Estado where A.autorizado == 0 select A).ToList<viewOrden_Estado>();
                        break;
                    case OrdenEstado.Pendiente:
                        lst = (from A in db.viewOrden_Estado where A.pendiente == 0 select A).ToList<viewOrden_Estado>();
                        break;
                    case OrdenEstado.Rechazado:
                        lst = (from A in db.viewOrden_Estado where A.rechazado == 0 select A).ToList<viewOrden_Estado>();
                        break;
                    case OrdenEstado.Procesado:
                        lst = (from A in db.viewOrden_Estado where A.procesado == 0 select A).ToList<viewOrden_Estado>();
                        break;
                }

            }
            return lst;
        }
        public DataTable repOrdenCompra(int p_id_sector, DateTime p_fecha_inicio, DateTime p_fecha_fin)
        {
            DataTable dt;
            sInSql = "EXEC REPORTE_ORDEN_COMPRA {0}, '{1}',  '{2}' ";
            sInSql = string.Format(sInSql, p_id_sector, p_fecha_inicio, p_fecha_fin);
            dt = CBaseDato.DevuelveDatos(sInSql);
            return dt;
        }
        public DataTable repOrdenCompraEstado(string p_estado, DateTime p_fecha_inicio, DateTime p_fecha_fin)
        {
            DataTable dt;
            sInSql = "EXEC REPORTE_ORDEN_COMPRA_ESTADO '{0}', '{1}',  '{2}' ";
            sInSql = string.Format(sInSql, p_estado, p_fecha_inicio, p_fecha_fin);
            dt = CBaseDato.DevuelveDatos(sInSql);
            return dt;
        }
        public DataTable getComprobanteImprimir(long p_id_orden)
        {
            DataTable dt;
            sInSql = "EXEC COMPROBANTE_ORDEN_COMPRA {0}";
            sInSql = string.Format(sInSql, p_id_orden);
            dt = CBaseDato.DevuelveDatos(sInSql);
            return dt;
        }
        public DataTable repCompraSectorX(int p_id_sector, DateTime p_fecha_inicio, DateTime p_fecha_fin)
        {
            DataTable dt;
            sInSql = "EXEC REPORTE_COMPRA_SECTOR_X {0}, '{1}',  '{2}' ";
            sInSql = string.Format(sInSql, p_id_sector, p_fecha_inicio, p_fecha_fin);
            dt = CBaseDato.DevuelveDatos(sInSql);
            return dt;
        }
        public DataTable repCompraSectorFechaPago(int p_id_sector, DateTime p_fecha_inicio, DateTime p_fecha_fin)
        {
            DataTable dt;
            sInSql = "EXEC REPORTE_ORDEN_COMPRA_FECHA_PAGO {0}, '{1}',  '{2}' ";
            sInSql = string.Format(sInSql, p_id_sector, p_fecha_inicio, p_fecha_fin);
            dt = CBaseDato.DevuelveDatos(sInSql);
            return dt;
        }
        private void Notificar_Via_Mail(long p_id_orden, enumEstado p_estado, bool estado)
        {


            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                var lqs = (from A in db.Orden where A.id_orden == p_id_orden select A).SingleOrDefault();
                if (lqs != null)
                {
                    var usuario = (from A in db.Usuario where A.id_usuario == lqs.id_usuario select A).SingleOrDefault();
                    if (Util.email_bien_escrito(usuario.email))
                    {
                        string Asunto;
                        string TextoMensaje = "";
                        Asunto = p_estado.ToString() + " - ORDEN DE COMPRA NRO " + p_id_orden.ToString();

                        switch (p_estado)
                        {
                            case enumEstado.PENDIENTE:

                                TextoMensaje = "LA ORDEN FUE PUESTO EN PENDIENTE POR EL SIGUIENTE MOTIVO; \n";
                                TextoMensaje = TextoMensaje + lqs.obs_gerencia;
                                break;
                            case enumEstado.RECHAZADO:
                                if (estado)
                                {
                                    TextoMensaje = "LA ORDEN FUE RECHADO POR EL SIGUIENTE MOTIVO; \n";
                                    TextoMensaje = TextoMensaje + lqs.obs_gerencia;
                                }
                                else
                                {
                                    TextoMensaje = "SE HA CANCELADO EL RECHAZO A LA ORDEN ; \n";
                                    // TextoMensaje = TextoMensaje + lqs.obs_gerencia;
                                }
                                break;
                            case enumEstado.AUTORIZADO:
                                if (estado)
                                {
                                    TextoMensaje = "LA ORDEN FUE AUTORIZADO POR GERENCIA; \n";
                                }
                                else
                                {
                                    TextoMensaje = "SE HA CANCELADO LA AUTORIZACION DE GERENCIA AL LA ORDEN; \n";
                                }

                                //TextoMensaje = TextoMensaje + lqs.obs_gerencia;
                                break;
                            case enumEstado.CHEQUEADO:
                                if (estado)
                                {
                                    TextoMensaje = "LA ORDEN HA PASADO EL CHEQUEO FINANCIERO DE ADMINISTRACION; \n";
                                }
                                else
                                {
                                    TextoMensaje = "SE HA CANCELADO EL CHEQUEO FINANCIERO DE ADMINISTRACION; \n";
                                }


                                //TextoMensaje = TextoMensaje + lqs.obs_gerencia;
                                break;

                        }
                        Util.EnviarCorreo(usuario.email, Asunto, TextoMensaje);
                    }
                }


            }
        }
        private void Notificar_Via_Mail_Grupo(long p_id_orden, enumEstado p_estado, bool estado)
        {
            using (OrdenDataContext db = CBaseDato.getConexionDirecta())
            {
                var lqs = (from A in db.Orden where A.id_orden == p_id_orden select A).SingleOrDefault();
                if (lqs != null)
                {
                    List<string> lstDirecciones = new List<string>();

                    var usuarios = (from A in db.Sector_Usuario where A.id_sector == lqs.id_sector select A.Usuario).ToArray<Usuario>();
                    foreach (Usuario us in usuarios)
                    {
                        if (Util.email_bien_escrito(us.email))
                        {
                            lstDirecciones.Add(us.email);
                        }
                    }

                    //var usuario = (from A in db.Usuario where A.id_usuario == lqs.id_usuario select A).SingleOrDefault();
                    //if (Util.email_bien_escrito(usuario.email))
                    if (lstDirecciones.Count > 0)
                    {
                        string Asunto;
                        string TextoMensaje = "";
                        Asunto = p_estado.ToString() + " - ORDEN DE COMPRA NRO " + p_id_orden.ToString();

                        switch (p_estado)
                        {
                            case enumEstado.PENDIENTE:

                                TextoMensaje = "LA ORDEN FUE PUESTO EN PENDIENTE POR EL SIGUIENTE MOTIVO; \n";
                                TextoMensaje = TextoMensaje + lqs.obs_gerencia;
                                break;
                            case enumEstado.RECHAZADO:
                                if (estado)
                                {
                                    TextoMensaje = "LA ORDEN FUE RECHADO POR EL SIGUIENTE MOTIVO; \n";
                                    TextoMensaje = TextoMensaje + lqs.obs_gerencia;
                                }
                                else
                                {
                                    TextoMensaje = "SE HA CANCELADO EL RECHAZO A LA ORDEN ; \n";
                                    // TextoMensaje = TextoMensaje + lqs.obs_gerencia;
                                }
                                break;
                            case enumEstado.AUTORIZADO:
                                if (estado)
                                {
                                    TextoMensaje = "LA ORDEN FUE AUTORIZADO POR GERENCIA; \n";
                                }
                                else
                                {
                                    TextoMensaje = "SE HA CANCELADO LA AUTORIZACION DE GERENCIA AL LA ORDEN; \n";
                                }

                                //TextoMensaje = TextoMensaje + lqs.obs_gerencia;
                                break;
                            case enumEstado.CHEQUEADO:
                                if (estado)
                                {
                                    TextoMensaje = "LA ORDEN HA PASADO EL CHEQUEO FINANCIERO DE ADMINISTRACION; \n";
                                }
                                else
                                {
                                    TextoMensaje = "SE HA CANCELADO EL CHEQUEO FINANCIERO DE ADMINISTRACION; \n";
                                }


                                //TextoMensaje = TextoMensaje + lqs.obs_gerencia;
                                break;

                        }
                        Util.EnviarCorreoVariosDestinatarios(lstDirecciones, Asunto, TextoMensaje);
                    }
                }


            }
        }
    }


}
