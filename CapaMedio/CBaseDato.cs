﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CapaMedio
{
    public class CBaseDato
    {

        const string sConexionBase = "Persist Security Info=False;User ID={0};Password={1};Initial Catalog={2};Server={3}";

        public static OrdenDataContext getConexionDirecta(string pUsuario, string pPassword)
        {
            string Conex = string.Format(sConexionBase, pUsuario, pPassword, ConfigurationManager.AppSettings.Get("BD"), ConfigurationManager.AppSettings.Get("SERVIDOR"));
            OrdenDataContext ConexionLqs = new OrdenDataContext(Conex);
            return ConexionLqs;
        }
        public static OrdenDataContext getConexionDirecta(string pUsuario, string pPassword, string pBase)
        {
            string Conex = string.Format(sConexionBase, pUsuario, pPassword, pBase, ConfigurationManager.AppSettings.Get("SERVIDOR"));
            OrdenDataContext ConexionLqs = new OrdenDataContext(Conex);
            return ConexionLqs;
        }
        public static OrdenDataContext getConexionDirecta()
        {
            CParametroBase db = new CParametroBase();
            db.Usuario = ConfigurationManager.AppSettings.Get("USUARIO");
            db.Password = ConfigurationManager.AppSettings.Get("PASSWORD");
            db.BD = ConfigurationManager.AppSettings.Get("BD");
            db.Servidor = ConfigurationManager.AppSettings.Get("SERVIDOR");
            return getConexionDirecta(db);
        }
        public static OrdenDataContext getConexionDirecta(CParametroBase cnn)
        {
            string Conex = string.Format(sConexionBase, cnn.Usuario, cnn.Password, cnn.BD, cnn.Servidor);
            OrdenDataContext ConexionLqs = new OrdenDataContext(Conex);
            return ConexionLqs;
        }

        private static string strConexion()
            {
            string sConn;
            string sUsuario = ConfigurationManager.AppSettings.Get("USUARIO");
            string sPassword = ConfigurationManager.AppSettings.Get("PASSWORD");
            string sBD = ConfigurationManager.AppSettings.Get("BD");
            string sServidor = ConfigurationManager.AppSettings.Get("SERVIDOR");
            sConn = string.Format(sConexionBase, sUsuario, sPassword, sBD, sServidor);
            return sConn;
            }

        public void Ejecuta_ABM(string sinsql)
            {
            SqlConnection objConexion = new SqlConnection(strConexion());
            // inciamos el objeto command
            SqlCommand objCommand = new SqlCommand(sinsql, objConexion);
            objCommand.CommandTimeout = 0;
            // abrimos la conexion
            objConexion.Open();
            objCommand.ExecuteNonQuery();
            // cerramos la conexion
            objConexion.Close();
            }

        public static DataTable DevuelveDatos(string sinsql)
            {
            SqlConnection objConexion = new SqlConnection(strConexion());
            SqlCommand objCommand = new SqlCommand(sinsql, objConexion);
            objCommand.CommandTimeout = 0;
            SqlDataAdapter objAdapter = new SqlDataAdapter();
            objAdapter.SelectCommand = objCommand;
            objConexion.Open();
            DataTable objDt = new DataTable();

            // rellenamos el datatable del dataset mediante fill
            objAdapter.Fill(objDt);
            // objDt.DataSet = objDs
            // cerramos la conexion
            objConexion.Close();

            return objDt;
            }



    }
    public class CParametroBase
    {
        private string usuario;
        private string password;
        private string db;
        private string servidor;
        public string Usuario { get { return usuario; } set { usuario = value; } }
        public string Password { get { return password; } set { password = value; } }
        public string BD { get { return db; } set { db = value; } }
        public string Servidor { get { return servidor; } set { servidor = value; } }
        public CParametroBase(string pUsuario, string pPassword, string pDB, string pServidor)
        {
            usuario = pUsuario;
            password = pPassword;
            db = pDB;
            servidor = pServidor;
        }
        public CParametroBase() { }

    }
}
