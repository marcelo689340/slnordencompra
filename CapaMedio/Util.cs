﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace CapaMedio
    {
    public class Util
        {
        public static DataTable ToDataTable(System.Data.Linq.DataContext ctx, object query)
            {
            if (query == null)
                {
                throw new ArgumentNullException("query");
                }
            IDbCommand cmd = ctx.GetCommand((IQueryable)query);
            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
            adapter.SelectCommand = (System.Data.SqlClient.SqlCommand)cmd;
            //DataTable dt = new DataTable(sNameDT);
            DataTable dt = new DataTable();
            try
                {
                cmd.Connection.Open();
                adapter.FillSchema(dt, SchemaType.Source);
                adapter.Fill(dt);
                }
            finally
                {
                cmd.Connection.Close();
                }
            return dt;
            }

        public DataTable LINQToDataTable<T>(IEnumerable<T> varlist)
            {
            DataTable dtReturn = new DataTable();

            // column names 
            PropertyInfo[] oProps = null;

            if (varlist == null) return dtReturn;

            foreach (T rec in varlist)
                {
                // Use reflection to get property names, to create table, Only first time, others  will follow 
                if (oProps == null)
                    {
                    oProps = ((Type)rec.GetType()).GetProperties();
                    foreach (PropertyInfo pi in oProps)
                        {
                        Type colType = pi.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                            {
                            colType = colType.GetGenericArguments()[0];
                            }

                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                        }
                    }

                DataRow dr = dtReturn.NewRow();

                foreach (PropertyInfo pi in oProps)
                    {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
                    (rec, null);
                    }

                dtReturn.Rows.Add(dr);
                }
            return dtReturn;
            }
                
        public static void EnviarCorreo(string Para, string Asunto, string TextoMensaje)
            {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("comprasses2018@gmail.com");
            msg.To.Add(Para);
            msg.Subject = Asunto;
            msg.Body = TextoMensaje;

            SmtpClient smtpc = new SmtpClient("smtp.gmail.com", 587);

            smtpc.UseDefaultCredentials = false;
            smtpc.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpc.Credentials = new System.Net.NetworkCredential("comprasses2018@gmail.com", "Ses2018*.");
            smtpc.Timeout = 60000;
            smtpc.EnableSsl = true;
            smtpc.Send(msg);
            }

        public static void EnviarCorreoVariosDestinatarios(List<string> Para, string Asunto, string TextoMensaje)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("comprasses2018@gmail.com");

            foreach (string destino in Para)
            {
                msg.To.Add(destino);
            }


            //msg.To.Add(Para);
            msg.Subject = Asunto;
            msg.Body = TextoMensaje;

            SmtpClient smtpc = new SmtpClient("smtp.gmail.com", 587);

            smtpc.UseDefaultCredentials = false;
            smtpc.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpc.Credentials = new System.Net.NetworkCredential("comprasses2018@gmail.com", "Ses2018*.");
            smtpc.Timeout = 60000;
            smtpc.EnableSsl = true;
            smtpc.Send(msg);
        }

        public static Boolean email_bien_escrito(String email)
            {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
                {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                    {
                    return true;
                    }
                else
                    {
                    return false;
                    }
                }
            else
                {
                return false;
                }
            }

        }

    public enum enumEstado
        {
        AUTORIZADO,
        APROBADO,
        RECHAZADO,
        ANULADO,
        PENDIENTE,
        CHEQUEADO,
        PROCESADO,


        }

   public class MontoSector
    {
        public int id_sector;
        public decimal monto_orden;
        public decimal monto_habilitado;
    }
    }
