﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaMedio.Clases
    {
    [Serializable()]
    public class CDetalle_Orden
        {

        long id_orden;
        int item;
        int cantidad;
        string descripcion;
        decimal precio;
        int id_sector;
        string sector;
        decimal total;
        int add_usuario;
        DateTime? fecha_ult_mod;
        public long Id_orden
            {
            get { return id_orden; }
            set { id_orden = value; }
            }        
        public int Item
            {
            get { return item; }
            set { item = value; }
            }       
        public int Cantidad
            {
            get { return cantidad; }
            set { cantidad = value; }
            }      
        public string Descripcion
            {
            get { return descripcion; }
            set { descripcion = value; }
            }        
        public decimal Precio
            {
            get { return precio; }
            set { precio = value; }
            }       
        public int Id_sector
            {
            get { return id_sector; }
            set { id_sector = value; }
            }      
        public string Sector
            {
            get { return sector; }
            set { sector = value; }
            }       
        public decimal Total
            {
            get { return total; }
            set { total = value; }
            }       
        public DateTime? Fecha_ult_mod
            {
            get { return fecha_ult_mod; }
            set {  
                fecha_ult_mod = value; 
                }
            }        
        public int Add_usuario
            {
            get { return add_usuario; }
            set { add_usuario = value; }
            }
        }
    }
