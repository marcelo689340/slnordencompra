﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaMedio.Clases
    {
    public class CMenu
        {
        int id_menu;
        string descripcion;

        public CMenu() { }
        public CMenu(int p_id_menu, string p_descripcion)
            {
            this.id_menu = p_id_menu;
            this.descripcion = p_descripcion;
            }
        public int Id_menu
            {
            get { return id_menu; }
            set { id_menu = value; }
            }
        public string Descripcion
            {
            get { return descripcion; }
            set { descripcion = value; }
            }
        }

    public class CCategoriaMenu
        {
        int id_cat_menu;
        string cat_menu_descripcion;
        List<CMenu> menu = new List<CMenu>();

        public int Id_cat_menu
            {
            get { return id_cat_menu; }
            set { id_cat_menu = value; }
            }        
        public string Cat_menu_descripcion
            {
            get { return cat_menu_descripcion; }
            set { cat_menu_descripcion = value; }
            }     
        public List<CMenu> Menu
            {
            get { return menu; }
            set { menu = value; }
            }

        //public List<CCategoriaMenu> GetTreeMenu()
        //    {
        //    List<CCategoriaMenu> lst = new List<CCategoriaMenu>();
        //    int i = -1; 
        //    using (OrdenDataContext db = CBaseDato.getConexionDirecta())
        //       {

        //       CCategoriaMenu Menu;
        //        var lqs = (from A in db.view_tree_Menu where A.Habilitado == 1 &&  select A).ToList<view_tree_Menu>();

        //        foreach (view_tree_Menu row in lqs)
        //            {                    
        //            Menu = new CCategoriaMenu();
        //            Menu.id_cat_menu = row.MenuId;
        //            Menu.cat_menu_descripcion = row.Descripcion;
        //            if (!CategoriaMenuExiste(Menu.id_cat_menu, lst))
        //                {
        //                lst.Add(Menu);

        //                i++;
        //                //lst[i].menu.Add(new CMenu(row.))



        //                }

        //            }


        //        }
        //    }


        private bool CategoriaMenuExiste(int p_id_cat, List<CCategoriaMenu> p_lst)
            {
            bool result = false;
            foreach (CCategoriaMenu fila in p_lst)
                {
                if (fila.id_cat_menu == p_id_cat)
                    {
                    result = true;
                    break;
                    }
                }
            return result;
            }

        }

    }

    
