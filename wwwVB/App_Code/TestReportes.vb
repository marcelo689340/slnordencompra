﻿
Imports System.Collections
Imports System.Collections.Generic
Imports System.Text
Imports CrystalDecisions
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports System


Public Class ReportFactory
    Protected Shared reportQueue As New Queue()

    Protected Shared Function CreateReport(ByVal reportClass As Type) As ReportDocument
        Dim report As Object = Activator.CreateInstance(reportClass)
        reportQueue.Enqueue(report)
        Return DirectCast(report, ReportDocument)
    End Function

    Public Shared Function GetReport(ByVal reportClass As Type) As ReportDocument
        '75 is my print job limit. 
        If reportQueue.Count > 5 Then
            DirectCast(reportQueue.Dequeue(), ReportDocument).Dispose()
        End If
        Return CreateReport(reportClass)
    End Function
End Class

