﻿Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Imports System.Globalization
Imports System.Text
Imports System.Net
Imports System.Net.Mail
Imports CapaMedio
Imports CapaMedio.DAO
Imports System.Web.UI.UserControl
Imports System.Data.DataTable
Imports System.Data.SqlClient
Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports Microsoft.VisualBasic
'Imports CapaMedio


Public Class PaginaBase
    ' TODO: *** Comprobar si es Inherits o Implements ***
    Inherits System.Web.UI.Page



    Public db As OrdenDataContext = CBaseDato.getConexionDirecta()

    ''Public dbTablas As dbTablasDataContext = Conexion.getConnectionTablas

    Protected Overrides Sub InitializeCulture()
        'ReDim System.Threading.Thread.CurrentThread.CurrentUICulture(0 To "MiCultura" - 1)
        System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES")
        System.Threading.Thread.CurrentThread.CurrentUICulture = New CultureInfo("es-ES")

        MyBase.InitializeCulture()
    End Sub
    Public Shared Sub EnviarCorreo(Para As String, Asunto As String, TextoMesaje As String)

        Dim msg As New MailMessage()
        msg.From = New MailAddress("comprasses2018@gmail.com")
        msg.To.Add(Para)
        msg.Subject = Asunto
        msg.Body = TextoMesaje
        ' msg.IsBodyHtml 

        Dim smtpc As SmtpClient = New SmtpClient("smtp.gmail.com", 587)

        smtpc.UseDefaultCredentials = False
        smtpc.DeliveryMethod = SmtpDeliveryMethod.Network
        smtpc.Credentials = New System.Net.NetworkCredential("comprasses2018@gmail.com", "Ses2018*.")
        smtpc.Timeout = 60000
        smtpc.EnableSsl = True
        smtpc.Send(msg)

    End Sub
    Protected Sub ControlFoco(ByRef page As Page, ByRef Control As Control)

        Dim sm As ScriptManager
        sm = ScriptManager.GetCurrent(Me.Page)
        sm.SetFocus(Control.ClientID)

    End Sub
    Public Sub MensajeTiempo(ByVal pagina As Page, ByVal sMensaje As String, tiempo As Integer)


        Dim sScript As String
        sMensaje = Replace(sMensaje, "'", "''")
        sMensaje = Replace(sMensaje, Chr(39), " ")
        sMensaje = Replace(sMensaje, "(", "")
        sMensaje = Replace(sMensaje, ")", "")

        If tiempo = 0 Then
            tiempo = 4000
        End If

        'sScript = "Materialize.toast('<div class='row col s12'>" + sMensaje + "</div>', " & tiempo & ");"
        sScript = "Materialize.toast('" + sMensaje + "', " & tiempo & ");"


        ScriptManager.RegisterStartupScript(pagina, GetType(String), "Success", sScript, True)
    End Sub

    Public Function Obtenert_Valor(ByRef sCadena As String) As String
        Dim i As Integer
        Dim sRetornar As String = ""
        i = InStr(1, sCadena, "|")
        If i <> 0 Then
            sRetornar = Left(sCadena, i - 1)
            sCadena = Mid(sCadena, i + 1, Len(sCadena) - i)
        Else
            If Len(sCadena) > 0 Then
                sRetornar = sCadena
                sCadena = ""
            End If
        End If
        Return sRetornar
    End Function
    'Public Shared SubExportToPDF(v, e As EventArgs, ByVal gv As GridView)
    '      Using sw As New StringWriter()
    '          Using hw As New HtmlTextWriter(sw)
    '  'To Export all pages
    '              gv.AllowPaging = False
    '              Me.BindGrid()

    '              GridView1.RenderControl(hw)
    '  Dim sr As New StringReader(sw.ToString())
    '  Dim pdfDoc As New Document(PageSize.A2, 10.0F, 10.0F, 10.0F, 0.0F)
    '  Dim htmlparser As New HTMLWorker(pdfDoc)
    '              PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
    '              pdfDoc.Open()
    '              htmlparser.Parse(sr)
    '              pdfDoc.Close()

    '              Response.ContentType = "application/pdf"
    '              Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf")
    '              Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '              Response.Write(pdfDoc)
    '              Response.[End]()
    '          End Using
    '      End Using
    '  End Sub

    Public Sub FechaBox(ByRef fecha_inicio As String, ByRef fecha_fin As String)
        Dim lqs = (From A In db.FECHA_INICIO_FIN Select A).SingleOrDefault
        fecha_inicio = lqs.PRI_DIA_MES_ACTUAL
        fecha_fin = lqs.ULT_DIA_MES_ACTUAL
    End Sub
    Public Sub Fecha(ByRef fecha_inicio As String, ByRef fecha_fin As String)
        Dim lqs = (From A In db.FECHA_INICIO_FIN Select A).SingleOrDefault
        fecha_inicio = lqs.PRI_DIA_MES_ACTUAL
        fecha_fin = lqs.ULT_DIA_MES_ACTUAL
    End Sub
    Public Shared Sub ExportarToGrillaPDF(ByVal fileName As String, ByVal gv As GridView, ByVal sTitulo As String)
        Dim sb As New StringBuilder()
        Dim sw As New StringWriter(sb)
        Dim htw As New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form As New HtmlForm

        gv.Caption = sTitulo
        ' gv.EnableViewState = False
        gv.AllowPaging = False
        gv.Font.Size = 5
        gv.DataBind()
        If gv.Columns(0).HeaderText = "" Then
            gv.Columns(0).Visible = False
        End If
        If gv.Columns(1).HeaderText = "" Then
            gv.Columns(1).Visible = False
        End If

        HttpContext.Current.Response.ContentType = "application/pdf"
        HttpContext.Current.Response.AddHeader("content-disposition",
        "attachment;filename=GridViewExport.pdf")
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(gv)
        pagina.RenderControl(htw)
        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New Document(PageSize.A3)
        Dim htmlparser As New HTMLWorker(pdfDoc)
        PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        HttpContext.Current.Response.Write(pdfDoc)
        HttpContext.Current.Response.End()
    End Sub

    Public Shared Sub ExportarTodaGrilla(ByVal fileName As String, ByVal gv As GridView, ByVal sTitulo As String)
        Dim sb As New StringBuilder()
        Dim sw As New StringWriter(sb)
        Dim htw As New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form As New HtmlForm
        'gv.HeaderRow.BackColor = Drawing.Color.Blue
        'gv.FooterRow.BackColor = Drawing.Color.Blue
        'gv.HeaderRow.ForeColor = Drawing.Color.Black
        'gv.FooterRow.ForeColor = Drawing.Color.Black
        'gv.HeaderRow.Style.Add("background-color", "#F08080")
        gv.Caption = sTitulo
        gv.EnableViewState = False
        gv.AllowPaging = False

        gv.DataBind()
        If gv.Columns(0).HeaderText = "" Then
            gv.Columns(0).Visible = False
        End If
        If gv.Columns(1).HeaderText = "" Then
            gv.Columns(1).Visible = False
        End If

        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(gv)
        pagina.RenderControl(htw)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Buffer = True
        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}", fileName))
        HttpContext.Current.Response.Charset = "UTF-8"
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.[Default]
        HttpContext.Current.Response.Write(sb.ToString())
        HttpContext.Current.Response.End()

    End Sub

    Public Function ConvertirNumero(ByVal monto As String) As Double
        'Dim numero As Double = Convert.ToDecimal(monto.Replace(".", "").Replace(",", "."))
        Dim numero As Double = monto.Replace(".", ",")
        Return numero
    End Function

    'Public Shared Sub ExportarTodaGrilla(ByVal fileName As String, ByVal gv As GridView)



    '    Dim sb As New StringBuilder()
    '    Dim sw As New IO.StringWriter(sb)
    '    Dim htw As New HtmlTextWriter(sw)
    '    Dim pagina As Page = New Page
    '    Dim form As New HtmlForm

    '    gv.EnableViewState = False
    '    gv.AllowPaging = False
    '    gv.DataBind()
    '    pagina.EnableEventValidation = False
    '    pagina.DesignerInitialize()
    '    pagina.Controls.Add(form)
    '    form.Controls.Add(gv)
    '    pagina.RenderControl(htw)
    '    HttpContext.Current.Response.Clear()
    '    HttpContext.Current.Response.Buffer = True
    '    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
    '    HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}", fileName))
    '    HttpContext.Current.Response.Charset = "UTF-8"
    '    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.[Default]
    '    HttpContext.Current.Response.Write(sb.ToString())
    '    HttpContext.Current.Response.End()

    'End Sub

    Public Function LinqToDataTable(query As Object) As DataTable
        If query Is Nothing Then
            Throw New ArgumentNullException("query")
        End If

        Dim cmd As IDbCommand = db.GetCommand(TryCast(query, IQueryable))
        Dim adapter As New SqlDataAdapter()
        adapter.SelectCommand = DirectCast(cmd, SqlCommand)
        Dim dt As New DataTable("sd")

        Try
            cmd.Connection.Open()
            adapter.FillSchema(dt, SchemaType.Source)
            adapter.Fill(dt)
        Finally
            cmd.Connection.Close()
        End Try
        Return dt
    End Function

    'Public Function Validar_Ingreso(ByVal sPagina As String) As String
    '    Dim sMensaje As String = ""
    '    Try
    '        Dim lqsPermiso = (From P In db._Permiso_Detalle _
    '                          Join M In db._Menu On P.MenuId Equals M.MenuId _
    '                          Join US In db.Sucursal_Usuario On P.id_permiso Equals US.id_permiso _
    '                          Where US.id_sucursal_usuario = CInt(Request.Cookies("idSucUsuario").Value _
    '                              ) _
    '                          And M.Url = sPagina _
    '                          Select P.Consultar, P.Insertar, P.Modificar, P.Eliminar, M.Habilitado).Single

    '        If lqsPermiso.Habilitado = True Then
    '            If lqsPermiso.Consultar = False Then
    '                sMensaje = "No posee permisos sobre la pagina"
    '            End If
    '        Else
    '            sMensaje = "La pagina esta Desactivada"
    '        End If

    '    Catch ex As Exception
    '        sMensaje = "No posee permisos sobre la pagina o esta Desactiva"
    '    End Try
    '    Return sMensaje
    'End Function

    Public Function Validar_Ingreso(ByVal sPagina As String, ByRef cmdNuevo As Button, ByRef grvDatos As GridView) As String
        Dim sMensaje As String = ""
        Try
            Dim obj As usp_rolResult = New usp_rolResult()
            Dim dao As DAO_USUARIO = New DAO_USUARIO()
            obj = dao.Validar_Usuario(CInt(Request.Cookies("idUsuario").Value), sPagina)
            If (obj.Habilitado = False) Then
                sMensaje = "La pagina se encuentra temporalmente Desactivada"
            Else
                If (obj.Consultar = False) Then
                    sMensaje = "NO POSEE PERMISO SOBRE ESTA PAGINA"
                Else
                    grvDatos.Columns(0).Visible = obj.Modificar
                    grvDatos.Columns(1).Visible = obj.Eliminar
                    cmdNuevo.Visible = obj.Insertar
                End If
            End If
            Return sMensaje
        Catch ex As Exception
            sMensaje = "No posee permisos sobre la pagina o esta Desactiva"
            Return sMensaje
        End Try
        'Return sMensaje
    End Function

    Public Function Validar_Ingreso(ByVal sPagina As String) As String
        Dim sMensaje As String = ""
        Try
            Dim obj As usp_rolResult = New usp_rolResult()
            Dim dao As DAO_USUARIO = New DAO_USUARIO()
            obj = dao.Validar_Usuario(CInt(Request.Cookies("idUsuario").Value), sPagina)
            If (obj.Habilitado = False) Then
                sMensaje = "La pagina se encuentra temporalmente Desactivada"
            Else
                If (obj.Consultar = False) Then
                    sMensaje = "NO POSEE PERMISO SOBRE ESTA PAGINA"
                Else
                    'grvDatos.Columns(0).Visible = obj.Modificar
                    'grvDatos.Columns(1).Visible = obj.Eliminar
                    'cmdNuevo.Visible = obj.Insertar
                End If
            End If
            Return sMensaje
        Catch ex As Exception
            sMensaje = "No posee permisos sobre la pagina o esta Desactiva"
            Return sMensaje
        End Try
        'Return sMensaje
    End Function


    Public Shared Function GridviewToDataTable(gv As GridView) As DataTable

        Dim dt As New DataTable
        gv.AllowPaging = False
        gv.DataBind()

        For Each col As DataControlField In gv.Columns
            dt.Columns.Add(col.HeaderText)
        Next

        For Each row As GridViewRow In gv.Rows
            Dim nrow As DataRow = dt.NewRow
            Dim z As Integer = 0
            For Each col As DataControlField In gv.Columns
                If row.Cells(z).Text = "" Then
                    Try
                        Dim chk_Publicar As CheckBox = DirectCast(row.Cells(z).Controls(0), CheckBox)
                        nrow(z) = chk_Publicar.Checked
                    Catch ex As Exception

                    End Try
                Else
                    nrow(z) = row.Cells(z).Text.Replace("&nbsp;", "")
                End If
                ' z += 1
                z = z + 1
            Next
            dt.Rows.Add(nrow)
        Next
        gv.AllowPaging = True
        Return dt

    End Function

    'Public Property Filtro_Buscar_Cookie(ByVal sPagina As String) As String

    '    Get
    '        Dim i As Integer = InStr(sPagina, "/")
    '        sPagina = sPagina.Substring(1, 1)

    '        sPagina = sPagina.Substring(1, i)
    '        sPagina = sPagina.Replace(".aspx", "")
    '        Dim sText As String = ""
    '        Dim cooFiltro As HttpCookie = Request.Cookies(sPagina)
    '        If Not cooFiltro Is Nothing Then
    '            sText = cooFiltro.Value.ToString
    '        End If
    '        Return sText
    '    End Get
    '    Set(value As String)
    '        Response.Cookies(sPagina).Value = value
    '    End Set
    'End Property
    Public Property Filtro_FechaIni_Cookie() As String
        Get
            Dim sText As String = ""
            Dim cooFiltro As HttpCookie = Request.Cookies("FechaIni")
            If Not cooFiltro Is Nothing Then
                sText = cooFiltro.Value.ToString
            End If
            Return sText
        End Get
        Set(value As String)
            Response.Cookies("FechaIni").Value = value
        End Set
    End Property
    Public Property Filtro_FechaFin_Cookie() As String
        Get
            Dim sText As String = ""
            Dim cooFiltro As HttpCookie = Request.Cookies("FechaFin")
            If Not cooFiltro Is Nothing Then
                sText = cooFiltro.Value.ToString
            End If
            Return sText
        End Get
        Set(value As String)
            Response.Cookies("FechaFin").Value = value
        End Set
    End Property
    'Public Property Filtro_FechaFin_Cookie(ByVal sPagina As String) As String
    '    Get
    '        Dim sText As String = ""
    '        If (Request.Cookies("FechaFin") IsNot Nothing) Then
    '            If (Request.Cookies("FechaFin")(sPagina) IsNot Nothing) Then
    '                sText = Request.Cookies("FechaFin")(sPagina)
    '            End If
    '        End If
    '        Return sText
    '    End Get
    '    Set(value As String)
    '        Dim myCookie As HttpCookie = New HttpCookie("FechaFin")
    '        myCookie(sPagina) = value
    '        myCookie.Expires = Now.AddMinutes(30)
    '    End Set
    'End Property

#Region "Cookies"
    Public Property Cookies(sCookies As String) As String
        Get
            Dim sText As String = ""
            Dim cooDato As HttpCookie
            cooDato = Request.Cookies(sCookies)
            If cooDato Is Nothing Then
                Return Nothing
            Else
                sText = cooDato.Values(sCookies)
                Return sText
            End If
        End Get
        Set(value As String)
            Dim cooDato As HttpCookie
            cooDato = Request.Cookies(sCookies)
            If cooDato Is Nothing Then
                cooDato = New HttpCookie(sCookies)
                cooDato.Value = value
                cooDato.Expires = DateTime.MaxValue 'Nunca caduca
                Response.AppendCookie(cooDato)
            Else
                cooDato.Values.Set(sCookies, value)
                cooDato.Expires = DateTime.MaxValue 'Nunca caduca
                Response.Cookies.Set(cooDato)
            End If
        End Set
    End Property

    Public Property Cookies(sClass As Object, sCookies As String) As String
        Get
            Dim sText As String = ""
            Dim sPagina As String = sClass.GetType.Name
            Dim cooDato As HttpCookie
            cooDato = Request.Cookies(sPagina)
            If Request.Cookies(sPagina) IsNot Nothing Then
                If Request.Cookies(sPagina)(sCookies) IsNot Nothing Then
                    sText = Request.Cookies(sPagina)(sCookies)
                End If
            End If
            If sText = "" Then
                Return Nothing
            Else
                Return sText
            End If
        End Get
        Set(value As String)
            Dim sPagina As String = sClass.GetType.Name
            Dim cooDato As HttpCookie

            If Request.Cookies(sPagina) IsNot Nothing Then
                cooDato = Request.Cookies(sPagina)
                If Request.Cookies(sPagina)(sCookies) IsNot Nothing Then
                    cooDato.Values.Set(sCookies, value)
                Else
                    cooDato.Values.Add(sCookies, value)
                End If
            Else
                cooDato = New HttpCookie(sPagina)
                cooDato.Values.Add(sCookies, value)
            End If
            cooDato.Expires = DateTime.MaxValue
            Response.AppendCookie(cooDato)

        End Set
    End Property
#End Region
End Class

