﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.SqlClient
Imports System.Configuration
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic


' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la siguiente línea.
<System.Web.Script.Services.ScriptService()>
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
Public Class webserviceDatos
    Inherits System.Web.Services.WebService
    '<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _

    ' Get the connectionStrings.
    Dim connectionStrings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings
    Private strConexion As String = connectionStrings("bd_Orden").ToString()
    Dim sInSQL As String



    <WebMethod()>
    Public Function BuscarDatos(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As List(Of String)
        Try

            Dim encrip As New Encriptar
            Dim sCadena As String = System.Configuration.ConfigurationManager.AppSettings("Versiontexto").ToString()
            contextKey = encrip.AES_Decrypt(contextKey, sCadena)


            If Len(Trim(contextKey)) = 0 Then
                Return Nothing
                Exit Function
            End If

            Dim sTexto As String : Dim sConexion As String : Dim sinSQL As String
            sTexto = contextKey

            Dim sCodigo As String() = sTexto.Split(New Char() {"//"})
            If sCodigo.Length > 1 Then
                sinSQL = sCodigo(0)
                sConexion = sCodigo(sCodigo.Length - 1)
            Else
                sinSQL = sTexto
                sConexion = "bd_Orden"
            End If


            Dim conn As SqlConnection = New SqlConnection
            conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings(sConexion).ConnectionString
            Dim cmd As SqlCommand = New SqlCommand
            ' cmd.CommandText = "select soc_Nombre +' ' + soc_Apellido + '     ' + SOC_CEDULA as Socio from socio where" & _
            '" Soc_Nombre + soc_Apellido like '%' + REPLACE(@filtro, ' ', '%') + '%' or soc_cedula like @filtro"

            If Not UCase(sinSQL) Like "SELECT TOP*" And Not UCase(sinSQL) Like "EXEC*" Then
                sinSQL = "select top 30 " + Right(sinSQL, Len(sinSQL) - 7)
            End If

            cmd.CommandText = Replace(sinSQL, "@%@", "'%'")
            cmd.Parameters.AddWithValue("@filtro", prefixText)


            cmd.Connection = conn
            conn.Open()
            Dim customers As List(Of String) = New List(Of String)
            Dim sdr As SqlDataReader = cmd.ExecuteReader
            Dim iCant As Integer
            iCant = 0
            While sdr.Read
                iCant = iCant + 1
                If iCant > 30 Then
                    Exit While
                End If
                'customers.Add(sdr(0).ToString)
                customers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(String.Format("{0}", sdr(1).ToString), sdr(0).ToString + ";" + sdr(1).ToString))
            End While
            conn.Close()
            Return customers
        Catch ex As Exception
            Dim cus As List(Of String) = New List(Of String)
            cus.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("Error de consulta", ""))
            Return cus
        End Try
    End Function

End Class