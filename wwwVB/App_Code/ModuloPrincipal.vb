﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Net.Mail
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports System.Drawing
Imports System.Configuration
Imports System
Imports System.Collections.Specialized
Imports System.Web
Imports System.Web.UI.WebControls

Module Modulo
    Public sUsuario As String

End Module


Public Class Correo
    Private p_smtp As SmtpClient
    Private p_msg As MailMessage

    Public Property Msg() As MailMessage
        Get
            Return p_msg
        End Get
        Set(value As MailMessage)
            p_msg = value
        End Set
    End Property




    Public Property Smtp() As SmtpClient
        Get
            p_smtp = New SmtpClient("smtp.gmail.com", 587)
            p_smtp.UseDefaultCredentials = False
            p_smtp.DeliveryMethod = SmtpDeliveryMethod.Network
            p_smtp.Credentials = New System.Net.NetworkCredential("ctbmsistemas@gmail.com", "benitez.christian")
            p_smtp.Timeout = 60000
            p_smtp.EnableSsl = True
            Return p_smtp
        End Get
        Set(value As SmtpClient)
            p_smtp = value
        End Set
    End Property


    Public Sub New()

    End Sub
    'Public ReadOnly Property getSmtp() As SmtpClient()
    '    Get
    '        smtp = New SmtpClient("smtp.gmail.com", 587)

    '        'With smtp
    '        smtp.UseDefaultCredentials = False
    '        smtp.DeliveryMethod = SmtpDeliveryMethod.Network
    '        smtp.Credentials = New System.Net.NetworkCredential("ctbmsistemas@gmail.com", "benitez.christian")
    '        smtp.Timeout = 60000
    '        smtp.EnableSsl = True
    '        ' End With

    '        Return smtp
    '    End Get
    'End Property
End Class

Public Class ObtieneDataTable
    '"bd_Orden"
    Public Function DevuelveDatos(ByVal sinsql As String, ByVal sConn As String) As Data.DataTable
        'Try
        Dim strConexion As String
        ' Get the connectionStrings.
        Dim connectionStrings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings

        strConexion = connectionStrings(sConn).ToString()

        Dim objConexion As New SqlConnection(strConexion)

        'inciamos el objeto command
        Dim objCommand As New SqlCommand(sinsql, objConexion)

        objCommand.CommandTimeout = 0

        'creamos el objeto dataAdapter
        Dim objAdapter As New SqlDataAdapter

        'configuramos la propiedad select
        objAdapter.SelectCommand = objCommand

        'abrimos la conexion
        objConexion.Open()

        'le decimos que dataset almacenar
        Dim objDt As New DataTable

        'rellenamos el datatable del dataset mediante fill
        objAdapter.Fill(objDt)
        'objDt.DataSet = objDs
        'cerramos la conexion
        objConexion.Close()

        Return objDt
        'Catch
        '    'FormsAuthentication.SignOut()

        'End Try

    End Function

    Public Sub Ejecuta_ABM(ByVal sinsql As String, ByVal sConn As String)

        Dim strConexion As String

        ' Get the connectionStrings.
        Dim connectionStrings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings

        strConexion = connectionStrings(sConn).ToString()

        Dim objConexion As New SqlConnection(strConexion)

        'inciamos el objeto command
        Dim objCommand As New SqlCommand(sinsql, objConexion)

        objCommand.CommandTimeout = 0

        'abrimos la conexion
        objConexion.Open()

        objCommand.ExecuteNonQuery()

        'cerramos la conexion
        objConexion.Close()

    End Sub

    Public Function BuildPopUpScript(ByVal Page As String, _
        Optional ByVal dialogWidth As Int32 = 255, _
        Optional ByVal dialogHeight As Int32 = 255, _
        Optional ByVal modal As Boolean = True) As String
        Try
            Dim jsmodal As String
            jsmodal = ""
            If modal Then
                jsmodal &= "if (window.showModalDialog)"
                jsmodal &= "{window.showModalDialog('" & Page.Trim & "','name',"
                jsmodal &= "'dialogWidth:" & dialogWidth & "px;dialogHeight:" & dialogHeight & "px;status:no;help:no;scroll:no');}"

                jsmodal &= "else{"
                jsmodal &= "window.open('" & Page.Trim & "','name','height=" & dialogHeight & ","
                jsmodal &= "width=" & dialogWidth & ",toolbar=no,directories=no,status=no,"
                jsmodal &= "menubar=no,scrollbars=no,resizable=no,modal=yes');"
                jsmodal &= "}"
            Else
                jsmodal &= "window.open('" & Page.Trim & "','name','height=" & dialogHeight & ","
                jsmodal &= "width=" & dialogWidth & ",toolbar=no,directories=no,status=no,"
                jsmodal &= "menubar=no,scrollbars=no,resizable=no,modal=no');"
            End If
            Return jsmodal
        Catch ex As Exception
            Return "alert(" & ex.Message & ");"
        End Try
    End Function

    'Function Enviar_Correo_Adjunto(replacements As ListDictionary,
    '                               report As ReportDocument,
    '                               NombreCuerpoHTML As String,
    '                               Asunto As String,
    '                               destinatario As String) As String
    '    Try


    '        'enviar correo
    '        Dim md As New MailDefinition()
    '        Dim Para As String : Dim sNombreArchivo As String
    '        Para = destinatario
    '        sNombreArchivo = "Archivo_" + String.Format("{0:yyyyMMdd_HHmmss}", Date.Now) + ".pdf"
    '        md.IsBodyHtml = True
    '        md.BodyFileName = NombreCuerpoHTML

    '        Dim sDireccion As String = "marcelo689340@gmail.com"
    '        Dim sContrasenha As String = "762515274"


    '        ' Dim perso As New Personalizacion
    '        'Dim db As New ModeloDataContext
    '        'db.Connection.ConnectionString = ConfigurationManager.ConnectionStrings("cnnGestion").ToString
    '        'Dim q = From da In db.DATOSEMPRESA Select da.CorreoAutomatico, da.ContrasenhaCorreoAuto
    '        'If q.Count > 0 Then
    '        '    If Not IsNothing(q.First.CorreoAutomatico) Then sDireccion = q.First.CorreoAutomatico
    '        '    If Not IsNothing(q.First.ContrasenhaCorreoAuto) Then sContrasenha = perso.UnEncryptStr(q.First.ContrasenhaCorreoAuto, "p")
    '        'End If

    '        If Trim(sDireccion) = "" Or Trim(sContrasenha) = "" Then
    '            Return "Error, no configuro correo"
    '            Exit Function
    '        End If

    '        md.Priority = MailPriority.High
    '        Dim msg As MailMessage = md.CreateMailMessage(Para, replacements, New Panel())
    '        msg.From = New MailAddress(sDireccion)

    '        msg.IsBodyHtml = True
    '        msg.Priority = MailPriority.High
    '        msg.Subject = Asunto
    '        msg.Attachments.Add(New Attachment(report.ExportToStream(ExportFormatType.PortableDocFormat), sNombreArchivo))

    '        Dim smtpc As SmtpClient = New SmtpClient("smtp.gmail.com", 587)
    '        'Dim smtpc As SmtpClient = New SmtpClient("mail2.casinodeasuncion.com.py", 465)
    '        smtpc.UseDefaultCredentials = False
    '        smtpc.DeliveryMethod = SmtpDeliveryMethod.Network
    '        smtpc.Credentials = New System.Net.NetworkCredential(sDireccion, sContrasenha)

    '        smtpc.Timeout = 60000
    '        smtpc.EnableSsl = True
    '        smtpc.Send(msg)

    '        msg.Dispose()
    '        report.Dispose()

    '        Return "ok"
    '    Catch ex As Exception
    '        Dim sMensaje As String

    '        If Not ex.Message.ToString Like "*anulado*" Then
    '            sMensaje = ex.Message.ToString & "    Error. tratando de enviar correo a: " & destinatario
    '            Return ex.Message.ToString
    '        End If
    '        Return ex.Message.ToString
    '    End Try
    'End Function


End Class

Public Class ImagenClase
    Function ImagenCambiarTamanho(imagen As Byte(), anchooriginal As Integer) As Byte()

        Try

            Dim ImagenS As New MemoryStream
            Dim Width As Integer
            Dim Height As Integer

            '  Dim Bitmap As System.Drawing.Bitmap
            '   Dim ImgFormat As System.Drawing.Imaging.ImageFormat
            Dim Img As System.Drawing.Image
            Dim baseMap As Bitmap


            Dim imageStream As New MemoryStream(imagen)
            ImagenS = imageStream

            Img = System.Drawing.Image.FromStream(ImagenS)

            Width = Img.Width
            Height = Img.Height

            Dim relacion As Double, ancho As Integer
            If anchooriginal < Width Then

                ancho = anchooriginal
                relacion = ancho * 100 / Width
                Width = anchooriginal
                Height = Height * relacion / 100

            End If

            baseMap = New Bitmap(Width, Height)
            Dim myGraphic As Graphics = Graphics.FromImage(baseMap)

            myGraphic.DrawImage(Img, 0, 0, Width, Height)

            Img = baseMap

            Dim imageConverter As New ImageConverter()
            Dim imageByte As Byte() = DirectCast(imageConverter.ConvertTo(Img, GetType(Byte())), Byte())

            Img.Dispose()
            Return imageByte

        Catch
            Return Nothing
        End Try

    End Function

    Function RecortarImagen(imagen As Byte(), top As Integer, left As Integer, ancho As Integer, alto As Integer) As Byte()
        Try

            Dim ImagenS As New MemoryStream

            ' Dim Bitmap As System.Drawing.Bitmap
            ' Dim ImgFormat As System.Drawing.Imaging.ImageFormat
            Dim Img As System.Drawing.Image
            Dim baseMap As Bitmap

            Dim imageStream As New MemoryStream(imagen)
            ImagenS = imageStream

            Img = System.Drawing.Image.FromStream(ImagenS)

            baseMap = New Bitmap(ancho, alto)
            Dim myGraphic As Graphics = Graphics.FromImage(baseMap)

            myGraphic.DrawImage(Img, New Rectangle(0, 0, ancho, alto), top, left, ancho, alto, GraphicsUnit.Pixel)

            Img = baseMap

            Dim imageConverter As New ImageConverter()
            Dim imageByte As Byte() = DirectCast(imageConverter.ConvertTo(Img, GetType(Byte())), Byte())

            Img.Dispose()

            Return imageByte

        Catch

            Return Nothing

        End Try

    End Function

End Class


Public Class ModuloPrincipal

    Public Function DevuelveDatos(ByVal sinsql As String, ByVal sConn As String) As Data.DataTable
        Dim strConexion As String
        ' Get the connectionStrings.
        Dim connectionStrings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings

        strConexion = connectionStrings(sConn).ToString()

        Dim objConexion As New SqlConnection(strConexion)

        'inciamos el objeto command
        Dim objCommand As New SqlCommand(sinsql, objConexion)

        objCommand.CommandTimeout = 0

        'creamos el objeto dataAdapter
        Dim objAdapter As New SqlDataAdapter

        'configuramos la propiedad select
        objAdapter.SelectCommand = objCommand


        'abrimos la conexion
        objConexion.Open()

        'le decimos que dataset almacenar
        Dim objDt As New DataTable

        'rellenamos el datatable del dataset mediante fill
        objAdapter.Fill(objDt)
        'objDt.DataSet = objDs
        'cerramos la conexion
        objConexion.Close()

        Return objDt

    End Function

    Public Sub Ejecuta_ABM(ByVal sinsql As String, ByVal sConn As String)

        Dim strConexion As String

        ' Get the connectionStrings.
        Dim connectionStrings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings

        strConexion = connectionStrings(sConn).ToString()

        Dim objConexion As New SqlConnection(strConexion)

        'inciamos el objeto command
        Dim objCommand As New SqlCommand(sinsql, objConexion)

        objCommand.CommandTimeout = 0

        'abrimos la conexion
        objConexion.Open()

        objCommand.ExecuteNonQuery()

        'cerramos la conexion
        objConexion.Close()

    End Sub

    Public Function Obtiene_Permisos(ByVal sTabla As String) As Data.DataTable
        Dim sinSQL As String
        Dim dt As New Data.DataTable
        Dim obj As New ModuloPrincipal

        sinSQL = "SELECT     _Permiso_Usuario_Sistema.pus_insertar, "
        sinSQL = sinSQL + " _Permiso_Usuario_Sistema.pus_eliminar, "
        sinSQL = sinSQL + " _Permiso_Usuario_Sistema.pus_modificar"
        sinSQL = sinSQL + " FROM         _Menu INNER JOIN"
        sinSQL = sinSQL + " _Permiso_Usuario_Sistema ON _Menu.MenuId = _Permiso_Usuario_Sistema.pus_id_Menu"
        sinSQL = sinSQL + " INNER JOIN Cooperativa..Usuario"
        sinSQL = sinSQL + " ON Usuario.codUSuario = _Permiso_Usuario_Sistema.pus_usuario"
        sinSQL = sinSQL + " WHERE     (Cooperativa..usuario.usuario = '" + HttpContext.Current.User.Identity.Name
        sinSQL = sinSQL + " ') AND "
        sinSQL = sinSQL + "   (_Menu.Url like '%" + sTabla + "%')"
        sinSQL = sinSQL + " AND isnumeric(_Permiso_Usuario_Sistema.pus_usuario) = 1"
        dt = obj.DevuelveDatos(sinSQL, "cnnPermisos")

        Return dt

    End Function

    Public Function Obtiene_Permisos_Usuario(ByVal sTabla As String) As String
        Dim sinSQL As String
        Dim dt As New Data.DataTable
        Dim obj As New ModuloPrincipal
        Dim sPermisos As String

        sinSQL = "SELECT     _Permiso_Usuario_Sistema.pus_insertar, "
        sinSQL = sinSQL + " _Permiso_Usuario_Sistema.pus_eliminar, "
        sinSQL = sinSQL + " _Permiso_Usuario_Sistema.pus_modificar"
        sinSQL = sinSQL + " FROM         _Menu INNER JOIN"
        sinSQL = sinSQL + " _Permiso_Usuario_Sistema ON _Menu.MenuId = _Permiso_Usuario_Sistema.pus_id_Menu"
        sinSQL = sinSQL + " INNER JOIN Cooperativa..Usuario"
        sinSQL = sinSQL + " ON Cooperativa..Usuario.codUSuario = _Permiso_Usuario_Sistema.pus_usuario"
        sinSQL = sinSQL + " WHERE     (Cooperativa..Usuario.Usuario = '" + HttpContext.Current.User.Identity.Name
        sinSQL = sinSQL + " ') AND "
        sinSQL = sinSQL + "   (_Menu.Url like '%" + sTabla + "%')"
        sinSQL = sinSQL + " and ISNUMERIC(_Permiso_Usuario_Sistema.pus_usuario) =1 "
        dt = obj.DevuelveDatos(sinSQL, "cnnPermisos")

        sPermisos = ""

        If dt.Rows.Count > 0 And Not IsNothing(dt) Then
            sPermisos = "H"
            If dt.Rows(0)("pus_insertar") = "S" Then
                sPermisos = sPermisos + "A"
            End If
            If dt.Rows(0)("pus_eliminar") = "S" Then
                sPermisos = sPermisos + "B"
            End If
            If dt.Rows(0)("pus_Modificar") = "S" Then
                sPermisos = sPermisos + "M"
            End If
        End If

        Return sPermisos

    End Function

    Public Function Obtiene_Permisos_ValoresTablas(ByVal sTabla As String) As String
        Dim sinSQL As String
        Dim dt As New Data.DataTable
        Dim obj As New ModuloPrincipal
        Dim sPermisos As String

        sinSQL = "SELECT     Insertar, Eliminar, Modificar, CodTipo"
        sinSQL = sinSQL + " FROM _Permiso_Valores_Tabla"
        sinSQL = sinSQL + " INNER JOIN Cooperativa..USuario"
        sinSQL = sinSQL + " ON Cooperativa..Usuario.codUsuario = _Permiso_Valores_tabla.codUsuario"
        sinSQL = sinSQL + " WHERE     (Usuario = '" + HttpContext.Current.User.Identity.Name + "')"
        sinSQL = sinSQL + " and codTipo = " + sTabla
        dt = obj.DevuelveDatos(sinSQL, "cnnPermisos")

        sPermisos = ""

        If dt.Rows.Count > 0 And Not IsNothing(dt) Then
            sPermisos = "H"
            If dt.Rows(0)("insertar") = "S" Then
                sPermisos = sPermisos + "A"
            End If
            If dt.Rows(0)("eliminar") = "S" Then
                sPermisos = sPermisos + "B"
            End If
            If dt.Rows(0)("Modificar") = "S" Then
                sPermisos = sPermisos + "M"
            End If
        End If

        Return sPermisos

    End Function

    Public Function BuildPopUpScript(ByVal Page As String, _
        Optional ByVal dialogWidth As Int32 = 255, _
        Optional ByVal dialogHeight As Int32 = 255, _
        Optional ByVal modal As Boolean = True) As String
        Try
            Dim jsmodal As String
            jsmodal = ""
            If modal Then
                jsmodal &= "if (window.showModalDialog)"
                jsmodal &= "{window.showModalDialog('" & Page.Trim & "','name',"
                jsmodal &= "'dialogWidth:" & dialogWidth & "px;dialogHeight:" & dialogHeight & "px;status:no;help:no;scroll:no');}"

                jsmodal &= "else{"
                jsmodal &= "window.open('" & Page.Trim & "','name','height=" & dialogHeight & ","
                jsmodal &= "width=" & dialogWidth & ",toolbar=no,directories=no,status=no,"
                jsmodal &= "menubar=no,scrollbars=no,resizable=no,modal=yes');"
                jsmodal &= "}"
            Else
                jsmodal &= "window.open('" & Page.Trim & "','name','height=" & dialogHeight & ","
                jsmodal &= "width=" & dialogWidth & ",toolbar=no,directories=no,status=no,"
                jsmodal &= "menubar=no,scrollbars=no,resizable=no,modal=no');"
            End If
            Return jsmodal
        Catch ex As Exception
            Return "alert(" & ex.Message & ");"
        End Try

    End Function

    Public Function Obtiene_CodUsuario() As Integer
        Dim sInSQL As String
        Dim dt As New DataTable
        Dim obj As New ModuloPrincipal
        Dim iUsuario As Integer

        'asigna usuario a la session
        sInSQL = "select CodUsuario from Usuario"
        sInSQL = sInSQL + " where usuario = '" + HttpContext.Current.User.Identity.Name.ToString + "'"
        dt = obj.DevuelveDatos(sInSQL, "cnnGestion")

        If dt.Rows.Count > 0 And Not IsNothing(dt) Then
            iUsuario = dt.Rows(0)(0).ToString
        End If

        Return iUsuario

    End Function



    Public Function ReporteSufijo(sReporte As String) As String
        Dim dt As New Data.DataTable
        Dim sinSQL As String
        Dim obj As New ModuloPrincipal
        Dim sSufijo As String, sExtension As String, sReporteSufijo As String, ipos As Integer
        Dim i As Integer
        For i = 1 To Len(sReporte) - 1
            If Mid(sReporte, i, 1) = "." Then
                ipos = i
            End If
        Next
        sExtension = Right(sReporte, Len(sReporte) - (ipos - 1))

        sinSQL = "select top 1 isnull(CarpetaReportes, '') from datosEmpresa"
        dt = obj.DevuelveDatos(sinSQL, "cnnGestion")

        If dt.Rows.Count > 0 And Not IsNothing(dt) Then
            sSufijo = dt.Rows(0)(0).ToString

            '            sReporte = "~/plantillas/extractogeneral.html"
            sReporte = Replace(sReporte, "/", "\")

            sReporteSufijo = Replace(sReporte, sExtension, "_" + sSufijo + sExtension)

            If System.IO.File.Exists(HttpContext.Current.Server.MapPath(sReporteSufijo)) Then
                sReporte = sReporteSufijo
            End If
        Else
            sSufijo = ""
        End If

        Return sReporte
    End Function
End Class