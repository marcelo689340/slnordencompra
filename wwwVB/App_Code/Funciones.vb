Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Security
Imports System.Reflection
Imports System.Data.SqlClient
Imports System.Configuration
Imports System
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Web.UI.WebControls
Imports System.Text

Public Class Funciones
    Public Function LimpiarTexto(ByVal sTexto As String) As String
        sTexto = Replace(sTexto, "--", "")
        sTexto = Replace(sTexto, "'", "''")
        Return sTexto
    End Function


    Function calcularDigitoRUC(numero As String, basemax As Integer) As String
        Dim codigo As Long
        Dim numero_al As String = ""
        Dim i As Integer
        For i = 1 To Len(numero)
            Dim c As Integer
            c = Mid$(numero, i, 1)
            codigo = Asc(UCase(c))
            If Not (codigo >= 48 And codigo <= 57) Then
                numero_al = numero_al & codigo
            Else
                numero_al = numero_al & c
            End If
        Next
        Dim k As Integer : Dim total As Integer
        k = 2
        total = 0
        For i = Len(numero_al) To 1 Step -1
            If (k > basemax) Then k = 2
            Dim numero_aux As String = ""
            numero_aux = Val(Mid(numero_al, i, 1))
            total = total + (numero_aux * k)
            k = k + 1
        Next
        Dim resto As Integer : Dim digito As Integer
        resto = total Mod 11
        If (resto > 1) Then
            digito = 11 - resto
        Else
            digito = 0
        End If
        calcularDigitoRUC = digito
    End Function


    'Function Enviar_Correo_Adjunto(replacements As ListDictionary,
    '                               report As ReportDocument,
    '                               NombreCuerpoHTML As String,
    '                               Asunto As String,
    '                               destinatario As String) As String
    '    Try


    '        'enviar correo
    '        Dim md As New MailDefinition()
    '        Dim Para As String : Dim sNombreArchivo As String
    '        Para = destinatario
    '        sNombreArchivo = "Archivo_" + String.Format("{0:yyyyMMdd_HHmmss}", Date.Now) + ".pdf"
    '        md.IsBodyHtml = True
    '        md.BodyFileName = NombreCuerpoHTML

    '        ' Dim sDireccion As String ', perso As New Personalizacion
    '        '  Dim sContrasenha As String
    '        'Dim db As New ModeloDataContext
    '        'db.Connection.ConnectionString = ConfigurationManager.ConnectionStrings("cnnGestion").ToString
    '        'Dim q = From da In db.DATOSEMPRESA Select da.CorreoAutomatico, da.ContrasenhaCorreoAuto
    '        'If q.Count > 0 Then
    '        '    If Not IsNothing(q.First.CorreoAutomatico) Then sDireccion = q.First.CorreoAutomatico
    '        '    If Not IsNothing(q.First.ContrasenhaCorreoAuto) Then sContrasenha = perso.UnEncryptStr(q.First.ContrasenhaCorreoAuto, "p")
    '        'End If

    '        'If Trim(sDireccion) = "" Or Trim(sContrasenha) = "" Then
    '        '    Return "Error, no configuro correo"
    '        '    Exit Function
    '        'End If

    '        md.Priority = MailPriority.High
    '        Dim msg As MailMessage = md.CreateMailMessage(Para, replacements, New Panel())
    '        '  msg.From = New MailAddress(sDireccion)

    '        msg.IsBodyHtml = True
    '        msg.Priority = MailPriority.High
    '        msg.Subject = Asunto
    '        msg.Attachments.Add(New Attachment(report.ExportToStream(ExportFormatType.PortableDocFormat), sNombreArchivo))

    '        Dim smtpc As SmtpClient = New SmtpClient("smtp.gmail.com", 587)

    '        smtpc.UseDefaultCredentials = False
    '        smtpc.DeliveryMethod = SmtpDeliveryMethod.Network
    '        'smtpc.Credentials = New System.Net.NetworkCredential(sDireccion, sContrasenha)

    '        smtpc.Timeout = 60000
    '        smtpc.EnableSsl = True
    '        smtpc.Send(msg)

    '        msg.Dispose()
    '        report.Dispose()

    '        Return "ok"
    '    Catch ex As Exception
    '        Dim sMensaje As String

    '        If Not ex.Message.ToString Like "*anulado*" Then
    '            sMensaje = ex.Message.ToString & "    Error. tratando de enviar correo a: " & destinatario
    '            Return ex.Message.ToString
    '        End If
    '        Return ex.Message.ToString
    '    End Try
    'End Function

    Public Shared Function ConvertirListToDataTable(ByVal genericList As Object) As Data.DataTable
        'convertir lista a datatable.....

        Dim dataTable As Data.DataTable = Nothing
        Dim listType As Type = genericList.GetType()

        If (listType.IsGenericType AndAlso (genericList IsNot Nothing)) Then

            '//determine the underlying type the List&lt;> contains
            Dim elementType As Type = listType.GetGenericArguments()(0)

            '//create empty table -- give it a name in case
            '//it needs to be serialized
            dataTable = New Data.DataTable(elementType.Name + "List")

            '//define the table -- add a column for each public
            '//property or field
            Dim memberInfos As MemberInfo() = elementType.GetMembers(BindingFlags.Public Or BindingFlags.Instance)
            For Each memberInfo As MemberInfo In memberInfos

                If (memberInfo.MemberType = MemberTypes.Property) Then

                    Dim propertyInfo As PropertyInfo = CType(memberInfo, PropertyInfo)

                    If IsNullableType(propertyInfo.PropertyType) Then
                        dataTable.Columns.Add(propertyInfo.Name, New ComponentModel.NullableConverter(propertyInfo.PropertyType).UnderlyingType)
                    Else
                        dataTable.Columns.Add(propertyInfo.Name, propertyInfo.PropertyType)
                    End If
                ElseIf (memberInfo.MemberType = MemberTypes.Field) Then
                    Dim fieldInfo As FieldInfo = CType(memberInfo, FieldInfo)
                    dataTable.Columns.Add(fieldInfo.Name, fieldInfo.FieldType)
                End If
            Next

            '//populate the table
            Dim listData As IList = CType(genericList, IList)
            For Each record As Object In listData
                Dim index As Integer = 0
                Dim fieldValues(dataTable.Columns.Count - 1) As Object ' = CType(New Object(), Object())
                For Each columnData As Data.DataColumn In dataTable.Columns
                    Dim memberInfo As MemberInfo = elementType.GetMember(columnData.ColumnName)(0)
                    If memberInfo.MemberType = MemberTypes.Property Then
                        Dim propertyInfo As PropertyInfo = CType(memberInfo, PropertyInfo)
                        fieldValues(index) = propertyInfo.GetValue(record, Nothing)
                    ElseIf memberInfo.MemberType = MemberTypes.Field Then
                        Dim fieldInfo As FieldInfo = CType(memberInfo, FieldInfo)
                        fieldValues(index) = fieldInfo.GetValue(record)
                    End If
                    index += 1
                Next
                dataTable.Rows.Add(fieldValues)
            Next

        End If

        Return dataTable
    End Function


    Private Shared Function IsNullableType(ByVal propertyType As Type) As Boolean

        Return (propertyType.IsGenericType) AndAlso (propertyType.GetGenericTypeDefinition() Is GetType(Nullable(Of )))

    End Function

    Public Shared Sub EnviarCorreoNotificacion(Para As String, Asunto As String, Texto As String)
        Dim correo As New System.Net.Mail.MailMessage
        correo = New System.Net.Mail.MailMessage()
        correo.From = New System.Net.Mail.MailAddress(ConfigurationManager.AppSettings.Get("MAIL_CREDENCIAL"))
        correo.To.Add(Para)
        correo.Subject = Asunto
        correo.Body = Texto
        correo.IsBodyHtml = False
        correo.Priority = System.Net.Mail.MailPriority.High


        'Dim md As New MailDefinition()
        'md.BodyFileName = Archivo
        'md.IsBodyHtml = True
        'md.Priority = MailPriority.High
        'Dim msg As MailMessage = md.CreateMailMessage(Para, replacements, New Panel())
        'msg.From = New MailAddress("ctbmsistemas@gmail.com")
        'msg.IsBodyHtml = True
        'msg.Priority = MailPriority.High
        'msg.Subject = Asunto


        Dim smtpc As SmtpClient = New SmtpClient(ConfigurationManager.AppSettings.Get("MAIL_SMTP"), Convert.ToInt16(ConfigurationManager.AppSettings.Get("MAIL_SMTP_PORT")))

        smtpc.UseDefaultCredentials = False
        smtpc.DeliveryMethod = SmtpDeliveryMethod.Network
        smtpc.Credentials = New System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("MAIL_CREDENCIAL"), ConfigurationManager.AppSettings.Get("MAIL_PASS"))
        smtpc.Timeout = 60000
        smtpc.EnableSsl = True
        smtpc.Send(correo)

    End Sub

End Class

Public Class OperacionCajaCabecera

    Private _sFecha As String
    Private _sCodSocio As String
    Private _iNroSucursal As Integer
    Private _iNroCaja As Integer
    Private _sUSuario As String
    Private _sModificacion As String
    Private _iCajaId As Integer
    Private _sTipoMovimiento As String
    Private _iNroDocumento As Integer
    Private _iNroMovimiento As Integer
    Private _iTipoDocumento As Integer
    Private _sNroBoleta As String
    Private _iCodCliente As Integer
    Private _sRUC As String
    Private _sCliente As String
    Private _iOrdenPago As Integer
    Private _PuntoExpedicion As String
    Private _Timbrado As String
    Private _VtoTimbrado As String

    Public Property Fecha() As String
        Get
            Return _sFecha
        End Get
        Set(ByVal Value As String)
            _sFecha = Value
        End Set
    End Property

    Public Property CodSocio() As String
        Get
            Return _sCodSocio
        End Get
        Set(ByVal Value As String)
            _sCodSocio = Value
        End Set
    End Property

    Public Property NroSucursal() As Integer
        Get
            Return _iNroSucursal
        End Get
        Set(ByVal Value As Integer)
            _iNroSucursal = Value
        End Set
    End Property

    Public Property NroCaja() As Integer
        Get
            Return _iNroCaja
        End Get
        Set(ByVal Value As Integer)
            _iNroCaja = Value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return _sUSuario
        End Get
        Set(ByVal Value As String)
            _sUSuario = Value
        End Set
    End Property

    Public Property Modificacion() As String
        Get
            Return _sModificacion
        End Get
        Set(ByVal Value As String)
            _sModificacion = Value
        End Set
    End Property

    Public Property CajaId() As Integer
        Get
            Return _iCajaId
        End Get
        Set(ByVal Value As Integer)
            _iCajaId = Value
        End Set
    End Property

    Public Property TipoMovimiento() As String
        Get
            Return _sTipoMovimiento
        End Get
        Set(ByVal Value As String)
            _sTipoMovimiento = Value
        End Set
    End Property

    Public Property NroDocumento() As Integer
        Get
            Return _iNroDocumento
        End Get
        Set(ByVal Value As Integer)
            _iNroDocumento = Value
        End Set
    End Property

    Public Property NroMovimiento() As Integer
        Get
            Return _iNroMovimiento
        End Get
        Set(ByVal Value As Integer)
            _iNroMovimiento = Value
        End Set
    End Property

    Public Property TipoDocumento() As Integer
        Get
            Return _iTipoDocumento
        End Get
        Set(ByVal Value As Integer)
            _iTipoDocumento = Value
        End Set
    End Property

    Public Property NroBoleta() As String
        Get
            Return _sNroBoleta
        End Get
        Set(ByVal Value As String)
            _sNroBoleta = Value
        End Set
    End Property

    Public Property CodCliente() As Integer
        Get
            Return _iCodCliente
        End Get
        Set(ByVal Value As Integer)
            _iCodCliente = Value
        End Set
    End Property

    Public Property RUC() As String
        Get
            Return _sRUC
        End Get
        Set(ByVal Value As String)
            _sRUC = Value
        End Set
    End Property

    Public Property Cliente() As String
        Get
            Return _sCliente
        End Get
        Set(ByVal Value As String)
            _sCliente = Value
        End Set
    End Property

    Public Property OrdenPago() As Integer
        Get
            Return _iOrdenPago
        End Get
        Set(ByVal Value As Integer)
            _iOrdenPago = Value
        End Set
    End Property

    Public Property PuntoExpedicion() As String
        Get
            Return _PuntoExpedicion
        End Get
        Set(value As String)
            _PuntoExpedicion = value
        End Set
    End Property

    Public Property Timbrado() As String
        Get
            Return _Timbrado
        End Get
        Set(value As String)
            _Timbrado = value
        End Set
    End Property

    Public Property VtoTimbrado() As String
        Get
            Return _VtoTimbrado
        End Get
        Set(value As String)
            _VtoTimbrado = value
        End Set
    End Property



End Class



Public Class Conexion
    Public Sub New()
        '
        ' TODO: Add constructor logic here
        '

    End Sub

    Public Shared Sub EnviarCorreoNotificacion(Para As String, Asunto As String, Texto As String)
        Dim correo As New System.Net.Mail.MailMessage
        correo = New System.Net.Mail.MailMessage()
        correo.From = New System.Net.Mail.MailAddress("marcelo689340@gmail.com")
        correo.To.Add(Para)
        correo.Subject = Asunto
        correo.Body = Texto
        correo.IsBodyHtml = False
        correo.Priority = System.Net.Mail.MailPriority.High


        'Dim md As New MailDefinition()
        'md.BodyFileName = Archivo
        'md.IsBodyHtml = True
        'md.Priority = MailPriority.High
        'Dim msg As MailMessage = md.CreateMailMessage(Para, replacements, New Panel())
        'msg.From = New MailAddress("ctbmsistemas@gmail.com")
        'msg.IsBodyHtml = True
        'msg.Priority = MailPriority.High
        'msg.Subject = Asunto


        Dim smtpc As SmtpClient = New SmtpClient("smtp.gmail.com", 587)

        smtpc.UseDefaultCredentials = False
        smtpc.DeliveryMethod = SmtpDeliveryMethod.Network
        smtpc.Credentials = New System.Net.NetworkCredential("ctbmsistemas@gmail.com", "benitez.christian")
        smtpc.Timeout = 60000
        smtpc.EnableSsl = True
        smtpc.Send(correo)

    End Sub

    Public Shared Sub EnviarCorreo(replacements As ListDictionary, Para As String, Asunto As String, Archivo As String)
        Dim md As New MailDefinition()
        md.BodyFileName = Archivo
        md.IsBodyHtml = True
        md.Priority = MailPriority.High
        Dim msg As MailMessage = md.CreateMailMessage(Para, replacements, New Panel())
        msg.From = New MailAddress("ctbmsistemas@gmail.com")
        msg.IsBodyHtml = True
        msg.Priority = MailPriority.High
        msg.Subject = Asunto


        Dim smtpc As SmtpClient = New SmtpClient("smtp.gmail.com", 587)

        smtpc.UseDefaultCredentials = False
        smtpc.DeliveryMethod = SmtpDeliveryMethod.Network
        smtpc.Credentials = New System.Net.NetworkCredential("ctbmsistemas@gmail.com", "benitez.christian")
        smtpc.Timeout = 60000
        smtpc.EnableSsl = True
        smtpc.Send(msg)

    End Sub


End Class

Public Class Permiso

    Private _Usuario As String
    Private _Pantalla As String

    Public Property Pantalla() As String
        Get
            Return _Pantalla
        End Get
        Set(ByVal Value As String)
            _Pantalla = Value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return _Usuario
        End Get
        Set(ByVal Value As String)
            _Usuario = Value
        End Set
    End Property
    Public Sub New()

    End Sub

End Class

Public Class Caja_Impresion

    Private ds_ As New Data.DataSet
    Private dt_ As New Data.DataTable
    Private url_ As String
    Private comprobantetexto_ As String
    Private tipoimpresion_ As String
    Private impresora_ As String
    Private matricial_ As String

    Public Property tipoimpresion As String
        Get
            Return tipoimpresion_
        End Get
        Set(value As String)
            tipoimpresion_ = value
        End Set
    End Property

    Public Property url As String
        Get
            Return url_
        End Get
        Set(value As String)
            url_ = value
        End Set
    End Property

    Public Property comprobantetexto As String
        Get
            Return comprobantetexto_
        End Get
        Set(value As String)
            comprobantetexto_ = value
        End Set
    End Property

    Public Property DatosDT As Data.DataTable
        Get
            Return dt_
        End Get
        Set(value As Data.DataTable)
            dt_ = value
        End Set
    End Property

    Public Property DatosDS As Data.DataSet
        Get
            Return ds_
        End Get
        Set(value As Data.DataSet)
            ds_ = value
        End Set
    End Property

    Public Property impresora As String
        Get
            Return impresora_
        End Get
        Set(value As String)
            impresora_ = value
        End Set
    End Property

    Public Property matricial As String
        Get
            Return matricial_
        End Get
        Set(value As String)
            matricial_ = value
        End Set
    End Property


End Class

Public Class Encriptar
    Public Function AES_Encrypt(ByVal input As String, ByVal pass As String) As String
        Dim AES As New Cryptography.RijndaelManaged
        Dim Hash_AES As New Cryptography.MD5CryptoServiceProvider
        Dim encrypted As String = ""
        Try
            Dim hash(31) As Byte
            Dim temp As Byte() = Hash_AES.ComputeHash(ASCIIEncoding.ASCII.GetBytes(pass))
            Array.Copy(temp, 0, hash, 0, 16)
            Array.Copy(temp, 0, hash, 15, 16)
            AES.Key = hash
            AES.Mode = Cryptography.CipherMode.ECB
            Dim DESEncrypter As Cryptography.ICryptoTransform = AES.CreateEncryptor
            Dim Buffer As Byte() = ASCIIEncoding.ASCII.GetBytes(input)
            encrypted = Convert.ToBase64String(DESEncrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
            Return encrypted
        Catch ex As Exception
        End Try

        Return encrypted
    End Function

    Public Function AES_Decrypt(ByVal input As String, ByVal pass As String) As String
        Dim AES As New Cryptography.RijndaelManaged
        Dim Hash_AES As New Cryptography.MD5CryptoServiceProvider
        Dim decrypted As String = ""
        Try
            Dim hash(31) As Byte
            Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass))
            Array.Copy(temp, 0, hash, 0, 16)
            Array.Copy(temp, 0, hash, 15, 16)
            AES.Key = hash
            AES.Mode = Cryptography.CipherMode.ECB
            Dim DESDecrypter As Cryptography.ICryptoTransform = AES.CreateDecryptor
            Dim Buffer As Byte() = Convert.FromBase64String(input)
            decrypted = ASCIIEncoding.ASCII.GetString(DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
            Return decrypted
        Catch ex As Exception
        End Try

        Return decrypted
    End Function
End Class




