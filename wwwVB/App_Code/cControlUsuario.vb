﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Collections
Imports System.Collections.Generic

Public Class cControlUsuario
    Inherits EventArgs
End Class

Public Class cParametro
    Inherits EventArgs
    Dim pspParametro As List(Of String)
    Public Sub New()
    End Sub
    Public Sub New(ByVal sParametro As List(Of String))
        _spParametro = String.Join("|", sParametro)

    End Sub
    Public Property spParametro As String
    Public Overrides Function ToString() As String
        Dim infoUsuario As New StringBuilder()

        'Dim iColumna As Integer = 0
        'While iColumna <= icant
        infoUsuario.Append("Parametro: " & spParametro.ToString)
        'End While
        Return infoUsuario.ToString
    End Function
End Class

Public Class cctlArticuloPrecio
    Inherits EventArgs
    Public Sub New()

    End Sub
    Public Sub New(ByVal sId_Articulo As String, ByVal sDescripcion As String, ByVal iId_Lista_Precio As Integer, ByVal sListaPrecio As String, ByVal dPrecio As Double, ByVal sImpuesto As String)
        _spId_Articulo = sId_Articulo
        _spDescipcion = sDescripcion
        _spId_ListaPrecio = iId_Lista_Precio
        _spListaPrecio = sListaPrecio
        _spPrecio = dPrecio
        _spImpuesto = sImpuesto
    End Sub


    Public Property spId_Articulo As String
    Public Property spDescipcion As String
    Public Property spId_ListaPrecio As Integer
    Public Property spListaPrecio As String
    Public Property spPrecio As Double
    Public Property spImpuesto As String

    Public Overrides Function ToString() As String
        Dim infoUsuario As New StringBuilder()
        infoUsuario.Append("Id_Articulo: " & spId_Articulo & "<br/>")
        infoUsuario.Append("Descripcion: " & spDescipcion & "<br/>")
        infoUsuario.Append("Id_Lista_Precio: " & spId_ListaPrecio & "<br/>")
        infoUsuario.Append("Lista_Precio: " & spListaPrecio & "<br/>")
        infoUsuario.Append("Precio: " & spPrecio & "<br/>")
        infoUsuario.Append("Impuesto: " & spImpuesto & "<br/>")
        Return infoUsuario.ToString()
    End Function
End Class
Public Class cctlCotizacion
    Inherits EventArgs
    Public Sub New()

    End Sub
    Public Sub New(ByVal dFecha As Date, ByVal iIdMoneda As Integer, ByVal dMonto As Double)
        _spFecha = dFecha
        _spIdMoneda = iIdMoneda
        _spMonto = dMonto
    End Sub
    Public Property spFecha As Date
    Public Property spIdMoneda As Integer
    Public Property spMonto As Double
    Public Overrides Function ToString() As String
        Dim infoUsuario As New StringBuilder()
        infoUsuario.Append("Fecha : " & spFecha & "<br/>")
        infoUsuario.Append("Moneda: " & spIdMoneda & "<br/>")
        infoUsuario.Append("Monto: " & spMonto & "<br/>")
        Return infoUsuario.ToString()
    End Function
End Class

