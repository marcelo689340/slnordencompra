﻿
Imports System.Web.Optimization
'Imports AspNet


Public Module BundleConfig
    ' For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkID=303951
    'Public Sub RegisterBundles(bundles As BundleCollection)
    '    'bundles.Add(New ScriptBundle("~/bundles/WebFormsJs").Include(
    '    '    "~/Scripts/WebForms/WebForms.js",
    '    '    "~/Scripts/WebForms/WebUIValidation.js",
    '    '    "~/Scripts/WebForms/MenuStandards.js",
    '    '    "~/Scripts/WebForms/Focus.js",
    '    '    "~/Scripts/WebForms/GridView.js",
    '    '    "~/Scripts/WebForms/DetailsView.js",
    '    '    "~/Scripts/WebForms/TreeView.js",
    '    '    "~/Scripts/WebForms/WebParts.js"))

    '    ''' Order is very important for these files to work, they have explicit dependencies
    '    'bundles.Add(New ScriptBundle("~/bundles/MsAjaxJs").Include(
    '    '    "~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
    '    '    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
    '    '    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
    '    '    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"))

    '    ' Use the Development version of Modernizr to develop with and learn from. Then, when you’re
    '    ' ready for production, use the build tool at http://modernizr.com to pick only the tests you need
    '    bundles.Add(New ScriptBundle("~/bundles/modernizr").Include(
    '        "~/App_Themes/bootstrap/js/modernizr-2.6.2-respond-1.1.0.min.js"))


    '    bundles.Add(New ScriptBundle("~/bundles/funciones").Include(
    '    "~/App_Themes/bootstrap/js/jquery-1.9.1.min.js",
    '    "~/App_Themes/bootstrap/js/bootstrap.min.js",
    '    "~/App_Themes/bootstrap/js/jquery.easy-pie-chart.js",
    '    "~/App_Themes/bootstrap/js/metisMenu.js",
    '    "~/App_Themes/bootstrap/js/metisMenu.min.js",
    '    "~/App_Themes/bootstrap/js/scripts.js"))

    '    'bundles.Add(New ScriptBundle("~/bundles/funciones").Include(
    '    '"~/App_Themes/bootstrap/js/jquery.min.js",
    '    '"~/App_Themes/bootstrap/js/jquery.number.js",
    '    '"~/App_Themes/bootstrap/js/bootstrap.min.js",
    '    '"~/App_Themes/bootstrap/js/metisMenu.min.js",
    '    '"~/App_Themes/sb-admin/vendor/raphael/raphael.min.js",
    '    '"~/App_Themes/sb-admin/vendor/morrisjs/morris.min.js",
    '    '"~/App_Themes/sb-admin/data/morris-data.js",
    '    '    "~/App_Themes/sb-admin/dist/js/bootstrap-select.js",
    '    '    "~/App_Themes/sb-admin/dist/js/toastr.min.js",
    '    '    "~/App_Themes/sb-admin/dist/js/bootstrap-multiselect.js"))



    '    ScriptManager.ScriptResourceMapping.AddDefinition("respond", New ScriptResourceDefinition() With {
    '        .Path = "~/App_Themes/bootstrap/js/respond.min.js",
    '        .DebugPath = "~/App_Themes/bootstrap/js/respond.js"})

    'End Sub

    Public Sub RegisterBundles(bundles As BundleCollection)
        bundles.Add(New ScriptBundle("~/bundles/WebFormsJs").Include(
            "~/Scripts/WebForms/WebForms.js",
            "~/Scripts/WebForms/WebUIValidation.js",
            "~/Scripts/WebForms/MenuStandards.js",
            "~/Scripts/WebForms/Focus.js",
            "~/Scripts/WebForms/GridView.js",
            "~/Scripts/WebForms/DetailsView.js",
            "~/Scripts/WebForms/TreeView.js",
            "~/Scripts/WebForms/WebParts.js"))

        '' Order is very important for these files to work, they have explicit dependencies
        bundles.Add(New ScriptBundle("~/bundles/MsAjaxJs").Include(
            "~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
            "~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
            "~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
            "~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"))

        ' Use the Development version of Modernizr to develop with and learn from. Then, when you’re
        ' ready for production, use the build tool at http://modernizr.com to pick only the tests you need
        bundles.Add(New ScriptBundle("~/bundles/modernizr").Include(
            "~/Scripts/modernizr-*"))

        bundles.Add(New ScriptBundle("~/bundles/funciones").Include(
            "~/Scripts/jquery-1.11.2.min.js",
            "~/Scripts/jquery.number.js",
            "~/Scripts/bootstrap-select.js",
            "~/Scripts/funciones.js",
            "~/Scripts/toastr.min.js",
            "~/Scripts/bootstrap-multiselect.js"))

        ScriptManager.ScriptResourceMapping.AddDefinition("respond", New ScriptResourceDefinition() With {
            .Path = "~/Scripts/respond.min.js",
            .DebugPath = "~/Scripts/respond.js"})

    End Sub
End Module
