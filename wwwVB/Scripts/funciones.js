﻿// JScript File


function expandcollapse(obj, row) {
    var div = document.getElementById(obj);
    var img = document.getElementById('img' + obj);

    if (div.style.display == "none") {
        div.style.display = "block";
        if (row == 'alt') {
            img.src = "../img/cerrar_detalle.png";
        }
        else {
            img.src = "../img/cerrar_detalle.png";
        }
        img.alt = "Ocultar detalles";
    }
    else {
        div.style.display = "none";
        if (row == 'alt') {
            img.src = "";
        }
        else {
            img.src = "../img/abrir_detalle.png";
        }
        img.alt = "Mostrar detalles";
    }
}

function puntitos(donde, caracter, dec) {
    var decimales = false
    /*        campo = eval("donde.form." + campo)
    for (d = 0; d < campo.length; d++) {
    if (campo[d].checked == true) {
    dec = new Number(campo[d].value)
    break;
    }
    }

    */
    if (dec != 0)
    { decimales = true }

    pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
    valor = donde.value
    largo = valor.length
    crtr = true
    if (isNaN(caracter) || pat.test(caracter) == true) {
        if (pat.test(caracter) == true)
        { caracter = "\\" + caracter }
        carcter = new RegExp(caracter, "g")
        valor = valor.replace(carcter, "")
        donde.value = valor
        crtr = false
    }
    else {
        var nums = new Array()
        cont = 0
        for (m = 0; m < largo; m++) {
            if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
            { continue; }
            else {
                nums[cont] = valor.charAt(m)
                cont++
            }

        }
    }

    if (decimales == true) {
        ctdd = eval(1 + dec);
        nmrs = 1
    }
    else {
        ctdd = 1; nmrs = 3
    }
    var cad1 = "", cad2 = "", cad3 = "", tres = 0
    if (largo > nmrs && crtr == true) {
        for (k = nums.length - ctdd; k >= 0; k--) {
            cad1 = nums[k]
            cad2 = cad1 + cad2
            tres++
            if ((tres % 3) == 0) {
                if (k != 0) {
                    cad2 = "." + cad2
                }
            }
        }

        for (dd = dec; dd > 0; dd--)
        { cad3 += nums[nums.length - dd] }
        if (decimales == true)
        { cad2 += "," + cad3 }
        donde.value = cad2
    }
    donde.focus()

}
 
function puntos(donde, decimales) {
    $(donde).number(true, decimales, ',', '.')
}

function desabilitaboton(ctrl)
{
    ctrl.value = 'Espere....';
    ctrl.className = 'ctbmbotonDeshabilitado';
}

function numeroPierdeFoco(donde) {
    if ($(donde).val() == '') {
        $(donde).val('0');
    }
}


function Guardar_Abiboton(id, vali) { if (typeof (Page_ClientValidate) == 'function') { if (Page_ClientValidate(vali) == true) { document.getElementById(id).className = 'ctbmbotonDeshabilitado'; document.getElementById(id).value = 'Espere...'; return true; } else { return false; } } else { document.getElementById(id).className = 'ctbmbotonDeshabilitado'; document.getElementById(id).value = 'Espere...'; return true; } }
function Guardar_AbibotonPregunta(id, vali, pregunta) { if (confirm(pregunta)) { if (typeof (Page_ClientValidate) == 'function') { if (Page_ClientValidate(vali) == true) { document.getElementById(id).className =  ' ctbmbotonDeshabilitado'; document.getElementById(id).value = 'Espere...'; return true; } else { return false; } } else { document.getElementById(id).className = ' ctbmbotonDeshabilitado'; document.getElementById(id).value = 'Espere...'; return true; } } else { return false; } }
function checkMaxLength(obj) { var id = obj.id; var maxLength = obj.getAttribute('maxlength'); var currentLength = obj.value.length; var span = document.getElementById('span' + id); if (currentLength >= maxLength) { obj.value = obj.value.slice(0, maxLength); currentLength = obj.value.length; }; span.firstChild.nodeValue = currentLength; }

function Guardar_btnLucas(id, vali) { if (typeof (Page_ClientValidate) == 'function') { if (Page_ClientValidate(vali) == true) { document.getElementById(id).className = document.getElementById(id).className + ' ctbmbotonDeshabilitado';  document.getElementById(id).innerHTML = 'Espere...'; return true; } else { return false; } } else { document.getElementById(id).className = document.getElementById(id).className + ' ctbmbotonDeshabilitado'; document.getElementById(id).innerHTML = 'Espere...'; return true; } }
function Guardar_btnLucasPregunta(id, vali, pregunta) { if (confirm(pregunta)) { if (typeof (Page_ClientValidate) == 'function') { if (Page_ClientValidate(vali) == true) { document.getElementById(id).className = document.getElementById(id).className + ' ctbmbotonDeshabilitado'; document.getElementById(id).innerHTML = 'Espere...'; return true; } else { return false; } } else { document.getElementById(id).className = document.getElementById(id).className + ' ctbmbotonDeshabilitado'; document.getElementById(id).innerHTML = 'Espere...'; return true; } } else { return false; } }


function validarFormatoFecha(campo) {
    var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
    if ((campo.match(RegExPattern)) && (campo != '')) {
        return true;
    } else {
        return false;
    }
}


function existeFecha(fecha) {
    var fechaf = fecha.split("/");
    var day = fechaf[0];
    var month = fechaf[1];
    var year = fechaf[2];
    var date = new Date(year, month, '0');
    if ((day - 0) > (date.getDate() - 0)) {
        return false;
    }
    return true;
}

//suma o resta fechas para javascript
function editar_fecha(fecha, intervalo, dma, simbolo) {

    var simbolo = simbolo || "-";
    var arrayFecha = fecha.split(simbolo);
    var dia = arrayFecha[0];
    var mes = arrayFecha[1];
    var anio = arrayFecha[2];

    var fechaInicial = new Date(anio, mes - 1, dia);
    var fechaFinal = fechaInicial;
    if (dma == "m" || dma == "M") {
        fechaFinal.setMonth(fechaInicial.getMonth() + parseInt(intervalo));
    } else if (dma == "y" || dma == "Y") {
        fechaFinal.setFullYear(fechaInicial.getFullYear() + parseInt(intervalo));
    } else if (dma == "d" || dma == "D") {
        fechaFinal.setDate(fechaInicial.getDate() + parseInt(intervalo));
    } else {
        return fecha;
    }
    dia = fechaFinal.getDate();
    mes = fechaFinal.getMonth() + 1;
    anio = fechaFinal.getFullYear();

    dia = (dia.toString().length == 1) ? "0" + dia.toString() : dia;
    mes = (mes.toString().length == 1) ? "0" + mes.toString() : mes;

    return dia + "/" + mes + "/" + anio;
}
