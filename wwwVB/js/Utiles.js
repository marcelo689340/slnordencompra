﻿function PageInit() {
    $dialog = $('<div></div>')
         .dialog({
             autoOpen: false,
             title: 'Dialogo Básico',
             modal: true
         });

    function MostrarMensajeModal(mensajeTexto) {
        $dialog.text(mensajeTexto);
        $dialog.dialog('open');
    }
}