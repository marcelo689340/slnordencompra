﻿Imports CapaMedio.DAO
Imports CapaMedio
Imports System.IO
Imports System.Data.Linq
Imports System.Data

Partial Class Operacion_VerPresupuesto
    Inherits PaginaBase
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                If (Request.UrlReferrer Is Nothing) Then
                    Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
                End If
                Dim saveUcnow As DateTime = DateTime.Now
                CType(Master.FindControl("lblTitulo"), Label).Text = "VER PRESUPUESTO"

                ''Accion_View(0)

                Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
                If Request.QueryString("ID") <> "0" Then
                    Datos_bd(Request.QueryString("ID"))
                    Accion_View(0)
                    'CType(Master.FindControl("lblTitulo"), Label).Text = "MODIFICAR ORDEN DE COMPRA : " + Request.QueryString("ID").ToString()
                    'Cargar_Datos(Convert.ToInt16(Request.QueryString("ID")))
                Else
                    ' cmdGuardar.Visible = False
                    'CType(Master.FindControl("lblTitulo"), Label).Text = "NUEVA ORDEN DE COMPRA"
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub


#Region "Metodos"
    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox1.tipoMensaje = 2
        cuMensajeBox1.Mensaje = sMensaje
        cuMensajeBox1.show()
    End Sub
    Private Sub Datos_bd(ByVal p_id_orden As Long)
        Dim Dao As DAO_ORDEN = New DAO_ORDEN()
        Dim ord = Dao.getOrdenCompra(p_id_orden)
        If IsNothing(ord) = False Then
            lblIdOrden.Text = ord.id_orden
            lblFecha.Text = String.Format("{0:d}", ord.fecha)
            lblSector.Text = ord.descripcion
            lblMonto.Text = String.Format("{0:N0}", ord.monto)
        End If

    End Sub
    Sub Limpiar_Foto()
        Session("imagen") = Nothing
        'Foto.ImageUrl = ""
    End Sub
    Sub DownloadPdf(dt As DataTable)
        '  Dim bytes As Byte() = DirectCast(dt.Rows(0)("presupuesto"), Byte())
        Dim bytes As Byte() = CType(dt.Rows(0)("presupuesto"), Byte())
        Response.Clear()
        Response.Buffer = True
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.ContentType = "application/pdf"
        'Response.AddHeader("content-disposition", "attachment;filename=MyPDF.pdf")
        Response.AddHeader("content-disposition", "attachment;filename=" + dt.Rows(0)("archivo_nombre"))
        '  Response.OutputStream.Write(bytes, 0, bytes.Length)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()




        ' = (Byte())
    End Sub
#End Region

    Private Sub Accion_View(ByVal index As Integer)
        MultiView1.ActiveViewIndex = index
    End Sub

    Protected Sub grillaFirmas_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grillaFirmas.RowCommand
        Dim item As Integer = Convert.ToInt16(e.CommandArgument)
        If e.CommandName = "Select" Then

            Dim id_orden As Long = Convert.ToInt64(Request.QueryString("ID"))

            Dim dao As DAO_ORDEN = New DAO_ORDEN()

            Dim dt As DataTable = New DataTable()
            dt = dao.dtPresupuesto(id_orden, item)
            DownloadPdf(dt)



        End If
    End Sub
End Class
