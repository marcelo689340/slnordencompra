﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="orden_autorizar.aspx.vb" Inherits="Operacion_orden_autorizar" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<%@ Register Src="../control/wucReporte.ascx" TagName="wucReporte" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 68px;
        }

        .auto-style3 {
        }

        .auto-style4 {
            height: 20px;
        }

        .auto-style5 {
            height: 23px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <table class="auto-style1">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                            <asp:View ID="View1" runat="server">

                                <table class="auto-style1">
                                    <tr>
                                        <td class="auto-style2">Sector</td>
                                        <td>
                                            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSector" DataTextField="descripcion" DataValueField="id_sector" Height="24px" Width="272px">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlDataSector" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="	SELECT [id_sector]
		  ,[descripcion]
	FROM Sector
	WHERE estado = 1"></asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style5" colspan="2">
                                            <div class ="cuadroTitulo">
                                                SALDO DEL MES ACTUAL
                                            </div> 
                                            <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSaldo" Width="452px">
                                                <EditItemTemplate>
                                                    id_sector:
                                                <asp:TextBox ID="id_sectorTextBox" runat="server" Text='<%# Bind("id_sector") %>' />
                                                    <br />
                                                    descripcion:
                                                <asp:TextBox ID="descripcionTextBox" runat="server" Text='<%# Bind("descripcion") %>' />
                                                    <br />
                                                    limite:
                                                <asp:TextBox ID="limiteTextBox" runat="server" Text='<%# Bind("limite") %>' />
                                                    <br />
                                                    extension:
                                                <asp:TextBox ID="extensionTextBox" runat="server" Text='<%# Bind("extension") %>' />
                                                    <br />
                                                    monto_autorizado:
                                                <asp:TextBox ID="monto_autorizadoTextBox" runat="server" Text='<%# Bind("monto_autorizado") %>' />
                                                    <br />
                                                    monto_pendiente:
                                                <asp:TextBox ID="monto_pendienteTextBox" runat="server" Text='<%# Bind("monto_pendiente") %>' />
                                                    <br />
                                                    saldo:
                                                <asp:TextBox ID="saldoTextBox" runat="server" Text='<%# Bind("saldo") %>' />
                                                    <br />
                                                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Actualizar" />
                                                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    id_sector:
                                                <asp:TextBox ID="id_sectorTextBox" runat="server" Text='<%# Bind("id_sector") %>' />
                                                    <br />
                                                    descripcion:
                                                <asp:TextBox ID="descripcionTextBox" runat="server" Text='<%# Bind("descripcion") %>' />
                                                    <br />
                                                    limite:
                                                <asp:TextBox ID="limiteTextBox" runat="server" Text='<%# Bind("limite") %>' />
                                                    <br />
                                                    extension:
                                                <asp:TextBox ID="extensionTextBox" runat="server" Text='<%# Bind("extension") %>' />
                                                    <br />
                                                    monto_autorizado:
                                                <asp:TextBox ID="monto_autorizadoTextBox" runat="server" Text='<%# Bind("monto_autorizado") %>' />
                                                    <br />
                                                    monto_pendiente:
                                                <asp:TextBox ID="monto_pendienteTextBox" runat="server" Text='<%# Bind("monto_pendiente") %>' />
                                                    <br />
                                                    saldo:
                                                <asp:TextBox ID="saldoTextBox" runat="server" Text='<%# Bind("saldo") %>' />
                                                    <br />
                                                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insertar" />
                                                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <table class="auto-style1">
                                                        <tr>
                                                            <td>LIMITE</td>
                                                            <td>
                                                                <asp:Label ID="limiteLabel" runat="server" Text='<%# Bind("limite", "{0:N0}") %>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>EXTENSION</td>
                                                            <td>
                                                                <asp:Label ID="extensionLabel" runat="server" Text='<%# Bind("extension", "{0:N0}") %>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>MONTO AUTORIZADO</td>
                                                            <td>
                                                                <asp:Label ID="monto_autorizadoLabel" runat="server" Text='<%# Bind("monto_autorizado", "{0:N0}") %>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>MONTO PENDIENTE DE AUTORIZACION</td>
                                                            <td>
                                                                <asp:Label ID="monto_pendienteLabel" runat="server" Text='<%# Bind("monto_pendiente", "{0:N0}") %>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>SALDO</td>
                                                            <td>
                                                                <asp:Label ID="saldoLabel" runat="server" ForeColor="Red" Text='<%# Bind("saldo", "{0:N0}") %>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                </ItemTemplate>
                                            </asp:FormView>
                                            <asp:SqlDataSource ID="SqlDataSaldo" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CONSULTAR_SALDO_MES_ID_SECTOR" SelectCommandType="StoredProcedure">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="DropDownList1" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <asp:GridView ID="GrillaDatos" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_orden,id_sector" DataSourceID="SqlDataSource1" Width="1016px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Visualizar">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnVisualizar" runat="server" CommandArgument='<%# Eval("id_orden")%>' CommandName="Visualizar" ImageUrl="~/App_Themes/img/estandar/buscar03.png" ToolTip="Visualizar Orden" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ver Presupuesto">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="btnPresupuesto" runat="server" ImageUrl="~/App_Themes/css/img/standar/descargar.png" NavigateUrl='<%# Eval("id_orden", "VerPresupuesto.aspx?ID={0}")%>' ToolTip="Descargar Presupuesto"></asp:HyperLink>
                                                <%-- <asp:ImageButton ID="btnPresupuesto" runat="server" CommandName="Adjuntar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/css/img/standar/agregar.png" ToolTip="Adjuntar Presupuesto" />--%>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" />
                                        <asp:BoundField DataField="fecha" DataFormatString="{0:d}" HeaderText="fecha" SortExpression="fecha" />
                                        <asp:BoundField DataField="id_sector" HeaderText="id_sector" ReadOnly="True" SortExpression="id_sector" />
                                        <asp:BoundField DataField="sector" HeaderText="sector" SortExpression="sector" />
                                        <asp:BoundField DataField="observacion" HeaderText="observacion" SortExpression="observacion" />
                                        <asp:BoundField DataField="monto" DataFormatString="{0:N0}" HeaderText="monto" SortExpression="monto">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="saldo_actual" DataFormatString="{0:N0}" HeaderText="saldo_actual" SortExpression="saldo_actual" Visible="False">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="Estado" />
                                        <asp:TemplateField HeaderText="Autorizar" SortExpression="Autorizar">
                                            <EditItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Accion")%>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnAutorizar" runat="server" CommandArgument='<%# Eval("id_orden")%>' CommandName="Autorizar" ImageUrl="~/App_Themes/css/img/standar/Seleccionar.png" OnClientClick="return confirm('Confirmar que desea Autorizar esta orden!!!?');" ToolTip="Autorizar" Visible='<%# Eval("Accion")%>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rechazar" SortExpression="Rechazar">
                                            <EditItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Accion")%>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnRechazar" runat="server" CommandArgument='<%#Container.DataItemIndex%>' CommandName="Rechazar" ImageUrl="~/App_Themes/img/estandar/eliminar01.png" OnClientClick="return confirm('Confirmar que desea Rechazar esta orden!!!?');" ToolTip="Rechazar" Visible='<%# Eval("Accion")%>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pendiente" SortExpression="Pendiente">
                                            <EditItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Pendiente") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnPendiente" runat="server" CommandArgument='<%#Container.DataItemIndex%>' CommandName="Pendiente" ImageUrl="~/App_Themes/img/estandar/bloquear02.png" OnClientClick="return confirm('Confirmar que desea poner Pendiente esta orden!!!?');" ToolTip="Pendiente" Visible='<%# Eval("Accion")%>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cancelar" SortExpression="Cancelar" Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Cancelar") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnCancelar" runat="server" CommandArgument='<%# Eval("id_orden")%>' CommandName="Cancelar" ImageUrl="~/App_Themes/img/estandar/actualizar02.png" ToolTip="Cancelar Accion" Visible='<%# Eval("Cancelar") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        SELECCIONE OTRO SECTOR.....<br />
                                        &nbsp;<br />
                                        NO EXISTE ORDEN DE COMPRA....<br />
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="GRID_AUTORIZAR_ORDEN" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DropDownList1" Name="id_sector" PropertyName="SelectedValue" Type="Int32" DefaultValue="" />
                                    </SelectParameters>
                                </asp:SqlDataSource>


                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <table class="auto-style1">
                                    <tr>
                                        <td>
                                            <table class="auto-style1">
                                                <tr>
                                                    <td class="auto-style3" colspan="2">
                                                        <div class="cuadroTitulo">
                                                            <asp:Literal ID="litTitulo" runat="server"></asp:Literal>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style3">ID_ORDEN</td>
                                                    <td>
                                                        <asp:Label ID="lblIdOrden" runat="server" Text="lblIdOrden"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style3">FECHA</td>
                                                    <td>
                                                        <asp:Label ID="lblFecha" runat="server" Text="lblFecha"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style3">SECTOR</td>
                                                    <td>
                                                        <asp:Label ID="lblSector" runat="server" Text="lblSector"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style4">OBSERVACION</td>
                                                    <td class="auto-style4">
                                                        <asp:Label ID="lblObs" runat="server" Text="lblObs"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style3">MONTO</td>
                                                    <td>
                                                        <asp:Label ID="lblMonto" runat="server" Text="lblMonto"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style3">MOTIVO</td>
                                                    <td>
                                                        <asp:TextBox ID="txtObs_gerencia" runat="server" MaxLength="150" Width="442px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtObs_gerencia" CssClass="rfv" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style4">
                                            <asp:Button ID="cmdGuardar" runat="server" CssClass="boton" Text="Guardar" Width="118px" />
                                            &nbsp;<asp:Button ID="cmdCancelar" runat="server" CausesValidation="False" CssClass="boton" Text="Cancelar" Width="118px" />
                                        </td>
                                        <td class="auto-style4"></td>
                                    </tr>
                                </table>
                                <br />
                            </asp:View>
                            <br />
                        </asp:MultiView>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <uc2:wucReporte ID="wucReporte1" runat="server" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:cuMensajeBox ID="cuMensajeBox1" runat="server" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

