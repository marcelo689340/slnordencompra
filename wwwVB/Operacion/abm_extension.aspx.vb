﻿
Partial Class Operacion_abm_extension
    Inherits PaginaBase

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            Cargar_Fecha()
            Dim sPagina As String = "/Operacion/abm_extension.aspx"
            Dim sMens As String = Validar_Ingreso(sPagina)
            If sMens = "" Then

                'Usuario.Value = Request.Cookies("idUsuario").Value
                Response.Cookies("PaginaAnterior").Value = "~" + sPagina
                CType(Master.FindControl("lblTitulo"), Label).Text = "MANTENIMIENTO DE ORDEN"
            Else
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
            End If
        End If
    End Sub
    Private Sub Cargar_Fecha()
        Dim fecha_inicio As String = ""
        Dim fecha_fin As String = ""
        Fecha(fecha_inicio, fecha_fin)
        txtFechaInicio.Text = fecha_inicio
        txtFechaFin.Text = fecha_fin
    End Sub
    Protected Sub cmdNuevo_Click(sender As Object, e As EventArgs) Handles cmdNuevo.Click
        frvDatos.ChangeMode(FormViewMode.Insert)
    End Sub

    Protected Sub SqlDataSource2_Deleted(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataSource2.Deleted
        If Not (e.Exception Is Nothing) Then
            Dim sMensaje = e.Exception.Message.ToString
            MensajeError(sMensaje)
            e.ExceptionHandled = True
        End If

    End Sub

    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox1.tipoMensaje = 2
        cuMensajeBox1.Mensaje = sMensaje
        cuMensajeBox1.show()
    End Sub

    Protected Sub SqlDataSource3_Inserted(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataSource3.Inserted
        GridView1.DataBind()
    End Sub
End Class
