﻿
Imports System.Data
Imports CapaMedio.DAO

Partial Class Operacion_ConsultarOrden : Inherits PaginaBase
    'Inherits System.Web.UI.Page
    ''Inherits PaginaBase
    Dim dt As DataTable
    Protected Sub Visualizar_Comprobante(ByVal dt As Data.DataTable)
        Dim ds As New Data.DataSet
        ds.Tables.Add(dt)
        ds.Tables(0).TableName = "COMPROBANTE_ORDEN_COMPRA"
        Session("rs") = ds
        Dim sReporte As String
        sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompra.rpt"
        wucReporte1.Show(sReporte)
    End Sub
    Private Sub Cargar_Fecha()
        Dim fecha_inicio As String = ""
        Dim fecha_fin As String = ""
        Fecha(fecha_inicio, fecha_fin)
        txtFechaInicio.Text = fecha_inicio
        txtFechaFin.Text = fecha_fin
    End Sub


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            Dim sPagina As String = "/Operacion/ConsultarOrden.aspx"
            Dim sMens As String = Validar_Ingreso(sPagina, cmdNuevo, grvDatos)
            If sMens = "" Then
                Cargar_Fecha()
                Usuario.Value = Request.Cookies("idUsuario").Value
                Response.Cookies("PaginaAnterior").Value = "~" + sPagina
                CType(Master.FindControl("lblTitulo"), Label).Text = "MANTENIMIENTO DE ORDEN"
            Else
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
            End If
        End If
    End Sub


    Private Sub Cargar_Grilla()
        ' pnlGrilla.Visible = True
    End Sub
    Protected Sub cmdEnviarExcel_Click(sender As Object, e As EventArgs) Handles cmdEnviarExcel.Click
        ExportarTodaGrilla("Orden.xls", grvDatos, "LISTADO DE ORDEN")
    End Sub

    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox.tipoMensaje = 2
        cuMensajeBox.Mensaje = sMensaje
        cuMensajeBox.show()
    End Sub

    Protected Sub grvDatos_DataBound(sender As Object, e As EventArgs) Handles grvDatos.DataBound
        If grvDatos.Rows.Count > 0 Then
            Activar_Boton(True)
        Else
            Activar_Boton(False)
        End If
    End Sub
    Private Sub Activar_Boton(ByVal estado As Boolean)
        cmdEnviarExcel.Visible = estado
        cmdEnviarPdf.Visible = estado
    End Sub
    Protected Sub SqlDataGrilla_Deleted(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataGrilla.Deleted
        Dim sMensaje As String
        If Not (e.Exception Is Nothing) Then
            sMensaje = e.Exception.Message.ToString
            MensajeError(sMensaje)
            e.ExceptionHandled = True
            Me.hdfError.Value = sMensaje
        End If
    End Sub

    Protected Sub grvDatos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grvDatos.RowCommand, GridView2.RowCommand, GridView3.RowCommand
        Dim Items As Long = e.CommandArgument
        Dim dao As DAO_ORDEN = New DAO_ORDEN()
        Dim dt As DataTable
        If e.CommandName = "Visualizar" Then
            dt = dao.getComprobanteImprimir(Items)
            Visualizar_Comprobante(dt)
        End If
    End Sub

    Protected Sub GridView1_DataBound(sender As Object, e As EventArgs) Handles GridView1.DataBound
        If GridView1.Rows.Count > 0 Then
            GridView1.SelectRow(0)
        End If
    End Sub

    Protected Sub cmdEnviarPdf_Click(sender As Object, e As EventArgs) Handles cmdEnviarPdf.Click

        Funciones.EnviarCorreoNotificacion("moviedo@casinodeasuncion.com.py", "prueba", "jajajajaa")

    End Sub
End Class
