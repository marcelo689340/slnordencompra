﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="ConsultaOrdenCompraImprimir.aspx.vb" Inherits="Operacion_ConsultaOrdenCompraImprimir" %>

<%@ Register Src="~/control/Calendario.ascx" TagName="Calendario" TagPrefix="uc1" %>

<%@ Register Src="~/control/wucReporte.ascx" TagName="wucReporte" TagPrefix="uc2" %>
<%@ Register Src="~/control/Calendario.ascx" TagPrefix="uc2" TagName="Calendario" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style3 {
        }

        .auto-style4 {
            width: 98px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>--%>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="auto-style1">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style4">Sector</td>
                                <td>
                                    <asp:DropDownList ID="cboSector" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="descripcion" DataValueField="id_sector" Height="27px" Width="323px">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT
S.id_sector,
S.descripcion

FROM
dbo.Sector AS S
INNER JOIN dbo.Sector_Usuario AS SU ON SU.id_sector = S.id_sector
WHERE
S.estado = 1 AND
SU.estado = 1 AND
SU.id_usuario = @id_usuario">
                                        <SelectParameters>
                                            <asp:CookieParameter CookieName="idUsuario" Name="id_usuario" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style4">Fecha Inicio</td>
                                <td>
                                    <uc2:Calendario runat="server" id="txtFechaInicio" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style4">Fecha Fin</td>
                                <td>
                                    <uc1:Calendario ID="txtFechaFin" runat="server" Requerido="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style4">&nbsp;</td>
                                <td>
                                    <asp:Button ID="cmdNuevo" runat="server" CssClass="boton" Height="22px" Text="Mostrar" Width="100px" />
                                    &nbsp;<asp:Button ID="cmdEnviarExcel" runat="server" CssClass="boton" Height="22px" Text="Enviar Excel" Visible="False" Width="100px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style3" colspan="2">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="id_orden,id_sector" DataSourceID="SqlDataSource2" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Visualizar">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnVisualizar" runat="server" CommandName="Visualizar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/css/img/standar/impresora.png" ToolTip="Visualizar Orden" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" />
                                <asp:BoundField DataField="fecha" DataFormatString="{0:d}" HeaderText="fecha" SortExpression="fecha" />
                                <asp:BoundField DataField="id_sector" HeaderText="id_sector" ReadOnly="True" SortExpression="id_sector" />
                                <asp:BoundField DataField="sector" HeaderText="sector" SortExpression="sector" />
                                <asp:BoundField DataField="observacion" HeaderText="observacion" SortExpression="observacion" />
                                <asp:BoundField DataField="monto" DataFormatString="{0:N0}" HeaderText="monto" SortExpression="monto">
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="fecha_check" HeaderText="fecha_check" SortExpression="fecha_check" />
                                <asp:BoundField DataField="fecha_autorizacion" HeaderText="fecha_autorizacion" SortExpression="fecha_autorizacion" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="IMPRESION_ORDEN_COMPRA" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="cboSector" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                                <asp:ControlParameter ControlID="txtFechaInicio" DbType="Date" Name="fecha_inicio" PropertyName="Text" />
                                <asp:ControlParameter ControlID="txtFechaFin" DbType="Date" Name="fecha_fin" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <uc2:wucReporte ID="wucReporte1" runat="server" />
                        <br />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdEnviarExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

