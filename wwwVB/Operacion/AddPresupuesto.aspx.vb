﻿Imports CapaMedio.DAO
Imports CapaMedio
Imports System.IO
Imports System.Data.Linq
Imports System.Data

Partial Class Operacion_AddPresupuesto
    Inherits PaginaBase
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Request.Form("__eventtarget") = "fileUpload1" Then
                If FileUpload1.HasFile Then
                    If FileUpload1.FileBytes.Length > 5000000 Then
                        MensajeError("El tamaño del archivo, supera el maximo permitido")
                        ''MensajeTiempo(Page, "El tamaño del archivo, supera el maximo permitido", 4000, OpcionMensaje.vbError)
                        Exit Sub
                    End If
                    Dim path As String = Server.MapPath("~/temporales/")
                    path = path + "borrar_" + HttpContext.Current.User.Identity.Name.ToString + ".jpg"
                    Dim arrImage As Byte()
                    arrImage = FileUpload1.FileBytes
                    Dim binaryObj As New Binary(arrImage), Foto As Byte()
                    Dim redime As New ImagenClase
                    Foto = DirectCast(binaryObj.ToArray, Byte())
                    If arrImage.Length > 1000000 Then
                        Foto = redime.ImagenCambiarTamanho(Foto, 800)
                    End If
                    File.WriteAllBytes(path, Foto)
                    Session("imagen") = Foto
                    If Not IsNothing(Session("imagen")) Then
                        lblDatos.Text = FileUpload1.FileName
                        FileUpload1.Visible = False
                    End If
                End If
            End If

            If Page.IsPostBack = False Then
                Page.Form.Attributes.Add("enctype", "multipart/form-data")
                FileUpload1.Visible = False

                ' ScriptManager1.RegisterPostBackControl(grillaFirmas)
                If (Request.UrlReferrer Is Nothing) Then
                    Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
                End If
                Dim saveUcnow As DateTime = DateTime.Now
                CType(Master.FindControl("lblTitulo"), Label).Text = "ADJUNTAR PRESUPUESTO"

                Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
                If Request.QueryString("ID") <> "0" Then
                    Datos_bd(Request.QueryString("ID"))
                    Accion_View(1)
                    CType(Master.FindControl("lblTitulo"), Label).Text = "ADJUNTAR PRESUPUESTO " ' + Request.QueryString("ID").ToString()
                    'Cargar_Datos(Convert.ToInt16(Request.QueryString("ID")))
                Else
                    ' cmdGuardar.Visible = False
                    CType(Master.FindControl("lblTitulo"), Label).Text = "NUEVA ORDEN DE COMPRA"
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
#Region "Metodos"
    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox1.tipoMensaje = 2
        cuMensajeBox1.Mensaje = sMensaje
        cuMensajeBox1.show()
    End Sub
    Private Sub Datos_bd(ByVal p_id_orden As Long)
        Dim Dao As DAO_ORDEN = New DAO_ORDEN()
        Dim ord = Dao.getOrdenCompra(p_id_orden)
        If IsNothing(ord) = False Then
            lblIdOrden.Text = ord.id_orden
            lblFecha.Text = String.Format("{0:d}", ord.fecha)
            lblSector.Text = ord.descripcion
            lblMonto.Text = String.Format("{0:N0}", ord.monto)

        End If

    End Sub
    Sub Limpiar_Foto()
        Session("imagen") = Nothing
        'Foto.ImageUrl = ""
    End Sub
    Sub DownloadPdf(dt As DataTable)


        '  Dim bytes As Byte() = DirectCast(dt.Rows(0)("presupuesto"), Byte())
        Dim bytes As Byte() = CType(dt.Rows(0)("presupuesto"), Byte())
        Response.Clear()
        Response.Buffer = True
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.ContentType = "application/pdf"
        'Response.AddHeader("content-disposition", "attachment;filename=MyPDF.pdf")
        Response.AddHeader("content-disposition", "attachment;filename=" + dt.Rows(0)("archivo_nombre"))
        '  Response.OutputStream.Write(bytes, 0, bytes.Length)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()




        ' = (Byte())
    End Sub
#End Region
    Protected Sub FileUpload1_Init(sender As Object, e As EventArgs) Handles FileUpload1.Init
        FileUpload1.Attributes.Add("onchange", "fileUpload1()")
    End Sub
    Protected Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Accion_View(1)
    End Sub
    Protected Sub cmdAdjuntar_Click(sender As Object, e As EventArgs) Handles cmdAdjuntar.Click
        Accion_View(0)
        'MultiView1.ActiveViewIndex = 0
    End Sub
    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Try
            Dim imagen As Byte()

            If IsNothing(Session("imagen")) Then
                ' MensajeTiempo(Page, "No existe imagen cargada", 3000, 0)
                Exit Sub
            End If

            imagen = Session("imagen")
            Dim binaryObj As New Binary(imagen)
            Dim sArchivo As String = lblDatos.Text
            Dim dao As DAO_ORDEN = New DAO_ORDEN()
            dao.Insertar_Presupuesto(Request.QueryString("ID"), lblDatos.Text, txtObs.Text, 1, binaryObj.ToArray())
            Accion_View(1)
            Limpiar_Foto()
            ' MultiView1.ActiveViewIndex = -1
        Catch ex As Exception
            ' MensajeTiempo(Page, ex.Message.ToString, 3000, OpcionMensaje.vbError)
        End Try
    End Sub
    Private Sub Accion_View(ByVal index As Integer)
        Select Case index
            Case -1
                MultiView1.ActiveViewIndex = -1
                Session("imagen") = Nothing
                FileUpload1.Visible = True
                SqlDatosFirmas.DataBind()
                grillaFirmas.DataBind()
            Case 0
                Session("imagen") = Nothing
                MultiView1.ActiveViewIndex = 0
                txtObs.Text = ""
                ' FileUpload1 = Nothing
                FileUpload1.Visible = True
                lblDatos.Text = ""
            Case 1

                MultiView1.ActiveViewIndex = 1
                grillaFirmas.DataBind()

        End Select
    End Sub
    Protected Sub grillaFirmas_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grillaFirmas.RowCommand
        Dim item As Integer = Convert.ToInt16(e.CommandArgument)
        If e.CommandName = "Select" Then

            Dim id_orden As Long = Convert.ToInt64(Request.QueryString("ID"))

            Dim dao As DAO_ORDEN = New DAO_ORDEN()

            Dim dt As DataTable = New DataTable()
            dt = dao.dtPresupuesto(id_orden, item)
            DownloadPdf(dt)


            'Dim pre = dao.Presupuesto(id_orden, item)
            ''Session("imagen") = pre.presupuesto
            'If (pre.archivo_nombre.ToString Like "*.pdf*") Then
            '    Dim dt As DataTable = New DataTable()
            '    dt = dao.dtPresupuesto(id_orden, item)
            '    DownloadPdf(dt)
            '    '   Foto.ImageUrl = ""
            '    ' Foto.DataBind()
            'Else
            '    Foto.ImageUrl = "~/MuestraImagenDocumentoPresupuesto.aspx?id=" & id_orden & "&item=" & item
            '    Foto.DataBind()
            'End If



        End If
    End Sub
End Class
