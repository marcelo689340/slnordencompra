﻿Imports CapaMedio.DAO

Partial Class Operacion_OrdenCompraFactura
    Inherits PaginaBase


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If
            Dim saveUcnow As DateTime = DateTime.Now
            CType(Master.FindControl("lblTitulo"), Label).Text = "DATOS DE FACTURA A LA ORDEN DE COMPRA"

            Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            If Request.QueryString("ID") <> "0" Then
                Datos_bd(Request.QueryString("ID"))

                CType(Master.FindControl("lblTitulo"), Label).Text = "MODIFICAR ORDEN DE COMPRA : " + Request.QueryString("ID").ToString()
                'Cargar_Datos(Convert.ToInt16(Request.QueryString("ID")))
            Else
                ' cmdGuardar.Visible = False
                CType(Master.FindControl("lblTitulo"), Label).Text = "NUEVA ORDEN DE COMPRA"
            End If
        End If
    End Sub

    Private Sub Datos_bd(p_id_orden As Long)

        Dim Dao As DAO_ORDEN = New DAO_ORDEN()
        Dim ord = Dao.getOrdenCompra(p_id_orden)
        If IsNothing(ord) = False Then
            lblIdOrden.Text = ord.id_orden
            lblFecha.Text = String.Format("{0:d}", ord.fecha)
            lblSector.Text = ord.descripcion
            lblMonto.Text = String.Format("{0:N0}", ord.monto)
            txtRuc.DataText = ord.ruc_proveedor
            txtNroFactura.Text = ord.nro_factura
            lblObs.Text = ord.observacion
        End If
        ' Throw New NotImplementedException
    End Sub
    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Try
            Dim Dao As DAO_ORDEN = New DAO_ORDEN()
            Dao.SetDatoFacturaOrden(Request.QueryString("ID"), Request.Cookies("idUsuario").Value, txtNroFactura.Text, txtRuc.DataText)
            Response.Redirect(cmdVolver.PostBackUrl)
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
        End Try

    End Sub
End Class
