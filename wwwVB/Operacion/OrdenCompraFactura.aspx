﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="OrdenCompraFactura.aspx.vb" Inherits="Operacion_OrdenCompraFactura" %>

<%@ Register src="../control/BuscarTablaNew.ascx" tagname="BuscarTablaNew" tagprefix="uc7" %>

<%@ Register src="../control/cuMensajeBox.ascx" tagname="cuMensajeBox" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 228%;
        }
        .auto-style2 {
    }
        .auto-style3 {
            width: 227px;
            height: 46px;
        }
        .auto-style4 {
            height: 46px;
        }
        .auto-style7 {
            width: 227px;
            height: 11px;
        }
        .auto-style8 {
            height: 11px;
        }
        .auto-style11 {
            width: 227px;
            height: 23px;
        }
        .auto-style12 {
            height: 23px;
        }
        .auto-style13 {
            width: 227px;
            height: 21px;
        }
        .auto-style14 {
            height: 21px;
        }
        .auto-style15 {
            width: 227px;
            height: 19px;
        }
        .auto-style16 {
            height: 19px;
        }
        .auto-style17 {
            width: 227px;
            height: 28px;
        }
        .auto-style18 {
            height: 28px;
        }
        .auto-style19 {
            height: 237px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="auto-style1">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">

                                <table class="auto-style1">
                                    <tr>
                                        <td class="auto-style19">
                                            <table class="auto-style1">
                                                <tr>
                                                    <td class="auto-style3" colspan="2">
                                                        <div class="cuadroTitulo">
                                                            <asp:Literal ID="litTitulo" runat="server"></asp:Literal>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2">ID_ORDEN</td>
                                                    <td>
                                                        <asp:Label ID="lblIdOrden" runat="server" Text="lblIdOrden"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style7">FECHA</td>
                                                    <td class="auto-style8">
                                                        <asp:Label ID="lblFecha" runat="server" Text="lblFecha"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style13">SECTOR</td>
                                                    <td class="auto-style14">
                                                        <asp:Label ID="lblSector" runat="server" Text="lblSector"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style11">OBSERVACION</td>
                                                    <td class="auto-style12">
                                                        <asp:Label ID="lblObs" runat="server" Text="lblObs"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style15">MONTO</td>
                                                    <td class="auto-style16">
                                                        <asp:Label ID="lblMonto" runat="server" Text="lblMonto"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style17">PROVEEDOR</td>
                                                    <td class="auto-style18">
                                                        <uc7:BuscarTablaNew ID="txtRuc" runat="server" EnviaParametro="True" Grilla_Select="SELECT distinct PrvRuc, PrvNom FROM viewProveedor where PrvRuc like '%' + @filtro + '%' or PrvNom like '%' + @filtro + '%'" ImagenURLBotonAgregar="~/App_Themes/css/img/standar/cliente.png" Mostrar_Descripcion="True" Mostrar_Select="SELECT distinct PrvRuc, PrvNom FROM viewProveedor where PrvRuc like  @filtro " MostrarBotonAgregar="False" ValidationGroup="Cabecera" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2">NRO FACTURA</td>
                                                    <td>
                                                        <asp:TextBox ID="txtNroFactura" runat="server" Width="177px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNroFactura" CssClass="rfv" ErrorMessage="*" ValidationGroup="Cabecera"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2">&nbsp;</td>
                                                    <td>
                                                        <asp:Button ID="cmdGuardar" runat="server" CssClass="boton" Text="Guardar" ValidationGroup="Cabecera" Width="118px" />
                                                        <asp:Button ID="cmdVolver" runat="server" CausesValidation="False" CssClass="boton" Text="Cancelar" Width="118px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2" colspan="2">
                                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="527px">
                                                            <Columns>
                                                                <asp:BoundField DataField="cantidad" HeaderText="cantidad" SortExpression="cantidad" />
                                                                <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion">
                                                                <ItemStyle Width="300px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="precio" DataFormatString="{0:N0}" HeaderText="precio" SortExpression="precio">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="total" DataFormatString="{0:N0}" HeaderText="total" SortExpression="total">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT 
      [cantidad]
      ,[descripcion]
      ,[precio]
      ,[total]
  FROM [bd_Orden].[dbo].[viewOrden_Detalle_2]
  where id_orden =  @id_orden
  order by item desc">
                                                            <SelectParameters>
                                                                <asp:QueryStringParameter Name="id_orden" QueryStringField="ID" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="auto-style19"></td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style4">
                                            &nbsp;<br />
                                            <br />
                                            <uc1:cuMensajeBox ID="cuMensajeBox1" runat="server" />
                                        </td>
                                        <td class="auto-style4"></td>
                                    </tr>
                                </table>

                        <br />
                        <br />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

