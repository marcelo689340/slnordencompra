﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="abm_orden.aspx.vb" Inherits="Operacion_abm_orden" %>

<%@ Register Src="../control/Calendario.ascx" TagName="Calendario" TagPrefix="uc1" %>

<%@ Register Src="../control/textNumero.ascx" TagName="textNumero" TagPrefix="uc2" %>

<%@ Register src="../control/cuMensajeBox.ascx" tagname="cuMensajeBox" tagprefix="uc3" %>

<%@ Register src="../control/wucReporte.ascx" tagname="wucReporte" tagprefix="uc4" %>

<%@ Register src="../control/BuscarenTabla.ascx" tagname="BuscarenTabla" tagprefix="uc5" %>

<%@ Register src="../control/BuscarDatos.ascx" tagname="BuscarDatos" tagprefix="uc6" %>

<%@ Register src="../control/BuscarTablaNew.ascx" tagname="BuscarTablaNew" tagprefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style4 {
            width: 81px;
        }

        .auto-style6 {
        }

        .auto-style7 {
            width: 125px;
        }
        .auto-style8 {
            height: 48px;
            width: 256px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
<img alt="" class="style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="cuadroTitulo">
                <asp:Literal ID="Literal2" runat="server" Text="CABECERA"></asp:Literal>
            </div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style8" colspan="2">Sector :
                    </td>
                    <td>
                        <asp:DropDownList ID="cboSector" runat="server" DataSourceID="SqlDataSource1" DataTextField="descripcion" DataValueField="id_sector" Height="27px" Width="323px" AutoPostBack="True" CausesValidation="True" ValidationGroup="Cabecera">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT
S.id_sector,
S.descripcion

FROM
dbo.Sector AS S
INNER JOIN dbo.Sector_Usuario AS SU ON SU.id_sector = S.id_sector
WHERE
S.estado = 1 AND
SU.estado = 1 AND
SU.id_usuario = @id_usuario">
                            <SelectParameters>
                                <asp:CookieParameter CookieName="idUsuario" Name="id_usuario" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">Fecha :</td>
                    <td class="auto-style6" colspan="2">
                        <uc1:Calendario ID="txtFecha" runat="server" ValidationGroup="Cabecera" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">Observacion</td>
                    <td class="auto-style6" colspan="2">
                        <asp:TextBox ID="txtObservacion" runat="server" Height="56px" Width="678px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">&nbsp;</td>
                    <td class="auto-style6" colspan="2">
                        <asp:Button ID="cmdVolver" runat="server" CausesValidation="False" Text="Volver" Width="118px" CssClass="boton" />
                        <asp:Button ID="cmdGuardar" runat="server"  Text="Guardar" ValidationGroup="Cabecera" Width="111px" CssClass="boton" />
                    </td>
                </tr>
            </table>
            <div class="cuadroTitulo">
                <asp:Literal ID="ltDatos" runat="server" Text="AGREGAR ITEMS"></asp:Literal>
            </div>
            <table class="auto-style1">
                <tr>
                    <td>
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style7">Descripcion</td>
                                <td>
                                    <asp:TextBox ID="txtDescripcion" runat="server" MaxLength="300" Width="772px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescripcion" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style7">Cantidad</td>
                                <td>
                                    <uc2:textNumero ID="txtCantidad" runat="server" MaxValue="999999" MinValue="1" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style7">Precio</td>
                                <td>
                                    <uc2:textNumero ID="txtPrecio" runat="server" MaxValue="99999999999" MinValue="1" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style7">Sector</td>
                                <td>
                                    <asp:DropDownList ID="cboSectorItem" runat="server" DataSourceID="SqlDataSource3" DataTextField="descripcion" DataValueField="id_sector" Height="20px" Width="273px">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT [id_sector]
      ,[descripcion]
  FROM Sector	
  where estado = 1 and 
id_sector in(select id_sector_compra  from Sector_Compra where id_sector = @id_sector)	">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="cboSector" Name="id_sector" PropertyName="SelectedValue" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="Add Item" CssClass="boton"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <asp:GridView ID="GrillaDetalle" runat="server" AutoGenerateColumns="False" DataKeyNames="id_orden,item" Height="40px" Width="1126px" ShowFooter="True">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnSeleccionar" runat="server" CausesValidation="False" CommandName="Eliminar" CommandArgument='<%# Eval("item")%>' ImageUrl="~/App_Themes/css/img/standar/eliminar02.png"  ToolTip="Elimiar Registro" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" />
                        </ItemTemplate>
                        <ItemStyle Width="20px" />
                    </asp:TemplateField>
                   <%-- <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" />--%>
                    <asp:BoundField DataField="item" HeaderText="item" ReadOnly="True" SortExpression="item" >
                    <ItemStyle HorizontalAlign="Right" Width="30px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="cantidad" HeaderText="cantidad" SortExpression="cantidad" DataFormatString="{0:N0}" >
                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion" >
                    <ItemStyle Width="400px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="sector" HeaderText="sector" SortExpression="sector" >     
                    <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="precio" HeaderText="precio" SortExpression="precio" DataFormatString="{0:N0}" >
                    <ItemStyle HorizontalAlign="Right" Width="80px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="total" SortExpression="total">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("total") %>'></asp:TextBox>
                        </EditItemTemplate>
                         <FooterTemplate>
                             <asp:Label ID="lblTotalGen" runat="server" Text="Label"></asp:Label>
                        </FooterTemplate>
                         <ItemTemplate>
                             <asp:Label ID="Label1" runat="server" Text='<%# Bind("total", "{0:N0}") %>'></asp:Label>
                        </ItemTemplate>
<%--                        <FooterTemplate>
                            <asp:Label ID="lblTotalGen"2 runat="server"></asp:Label>
                        </FooterTemplate>--%>
                        <ItemStyle HorizontalAlign="Right" Width="100px" />
                    </asp:TemplateField>
                                 
                </Columns>
            </asp:GridView>
            <uc4:wucReporte ID="wucReporte1" runat="server" />
            <uc3:cuMensajeBox ID="cuMensajeBox1" runat="server" />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

