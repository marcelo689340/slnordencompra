﻿Imports CapaMedio.DAO
Imports System.Data

Partial Class Operacion_ConsultaOrdenCompraImprimir
    Inherits PaginaBase

    Private Sub Cargar_Fecha()
        Dim fecha_inicio As String = ""
        Dim fecha_fin As String = ""
        Fecha(fecha_inicio, fecha_fin)
        txtFechaInicio.Text = fecha_inicio
        txtFechaFin.Text = fecha_fin
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            Dim sPagina As String = "/Operacion/ConsultaOrdenCompraImprimir.aspx"
            Dim sMens As String = Validar_Ingreso(sPagina)
            If sMens = "" Then
                Cargar_Fecha()
                'Usuario.Value = Request.Cookies("idUsuario").Value
                Response.Cookies("PaginaAnterior").Value = "~" + sPagina
                CType(Master.FindControl("lblTitulo"), Label).Text = "CONSULTAR ORDEN COMPRA ESTADO"
            Else
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
            End If
        End If
    End Sub

    Protected Sub Visualizar_Comprobante(ByVal dt As Data.DataTable)
        Dim ds As New Data.DataSet
        ds.Tables.Add(dt)
        ds.Tables(0).TableName = "COMPROBANTE_ORDEN_COMPRA"
        Session("rs") = ds
        Dim sReporte As String
        sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompra.rpt"
        wucReporte1.Show(sReporte)
    End Sub

    Protected Sub GridView1_DataBound(sender As Object, e As EventArgs) Handles GridView1.DataBound
        If gridview1.Rows.Count > 0 Then
            Activar_Boton(True)
        Else
            Activar_Boton(False)
        End If
    End Sub
    Private Sub Activar_Boton(ByVal estado As Boolean)
        cmdEnviarExcel.Visible = estado
    End Sub

    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim Items As Long = e.CommandArgument
        Dim dao As DAO_ORDEN = New DAO_ORDEN()
        Dim dt As DataTable
        If e.CommandName = "Visualizar" Then
            dt = dao.getComprobanteImprimir(Items)
            Visualizar_Comprobante(dt)
        End If
    End Sub

    Protected Sub cmdEnviarExcel_Click(sender As Object, e As EventArgs) Handles cmdEnviarExcel.Click
        ExportarTodaGrilla("Orden.xls", GridView1, "LISTADO DE ORDEN")
    End Sub
End Class
