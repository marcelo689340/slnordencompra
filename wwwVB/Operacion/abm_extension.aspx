﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="abm_extension.aspx.vb" Inherits="Operacion_abm_extension" %>

<%@ Register Src="../control/Calendario.ascx" TagName="Calendario" TagPrefix="uc1" %>

<%@ Register Src="../control/textNumero.ascx" TagName="textNumero" TagPrefix="uc2" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 116px;
        }
        .auto-style3 {
            width: 128px;
            height: 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
<img alt="" class="auto-style3" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="auto-style1">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style2">FECHA INICIO</td>
                                <td>
                                    <uc1:Calendario ID="txtFechaInicio" runat="server" Requerido="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2">FECHA FIN</td>
                                <td>
                                    <uc1:Calendario ID="txtFechaFin" runat="server" Requerido="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2">SECTOR</td>
                                <td>
                                    <asp:DropDownList ID="cboSector" runat="server" DataSourceID="SqlDataSource1" DataTextField="descripcion" DataValueField="id_sector" Height="27px" Width="323px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CBO_SECTOR" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2"></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style2">
                                    <asp:Button ID="cmdNuevo" runat="server" CssClass="boton" Height="22px" Text="Nuevo" Width="100px" />
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style2">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="id_extension" DataSourceID="SqlDataSource2" Width="853px" AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminr" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument='<%# Eval("id_extension")%>' ImageUrl="~/App_Themes/css/img/standar/recicle.png" Text="Eliminar" ToolTip="Elimiar Registro" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" Visible='<%# Eval("boton") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="id_extension" HeaderText="id_extension" ReadOnly="True" SortExpression="id_extension" />
                                <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion" />
                                <asp:BoundField DataField="fecha" HeaderText="fecha" SortExpression="fecha" />
                                <asp:BoundField DataField="monto" HeaderText="monto" SortExpression="monto" DataFormatString="{0:N0}">
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="monto_utilizado" HeaderText="monto_utilizado" SortExpression="monto_utilizado" DataFormatString="{0:N0}">
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="GRID_EXTENSION" DeleteCommand="ANULAR_EXTENSION" DeleteCommandType="StoredProcedure" SelectCommandType="StoredProcedure">
                            <DeleteParameters>
                                <asp:Parameter Name="id_extension" Type="Decimal" />
                                <asp:CookieParameter CookieName="idUsuario" Name="id_usuario" Type="Int32" />
                            </DeleteParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="cboSector" Name="id_sector" PropertyName="SelectedValue" />
                                <asp:ControlParameter ControlID="txtFechaInicio" DbType="Date" Name="fecha_ini" PropertyName="Text" />
                                <asp:ControlParameter ControlID="txtFechaFin" DbType="Date" Name="fecha_fin" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:FormView ID="frvDatos" runat="server" DataKeyNames="id_extension" DataSourceID="SqlDataSource3" Width="446px">
                            <EditItemTemplate>
                                &nbsp;id_extension:
                                <asp:Label ID="id_extensionLabel1" runat="server" Text='<%# Eval("id_extension") %>' />
                                <br />
                                id_sector:
                                <asp:TextBox ID="id_sectorTextBox" runat="server" Text='<%# Bind("id_sector") %>' />
                                <br />
                                monto:
                                <asp:TextBox ID="montoTextBox" runat="server" Text='<%# Bind("monto") %>' />
                                <br />
                                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Actualizar" />
                                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <table class="auto-style1">
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <asp:TextBox ID="id_extensionTextBox" runat="server" Text='<%# Bind("id_extension") %>' Enabled="False" Visible="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>SECTOR</td>
                                        <td>
                                            <asp:DropDownList ID="cboSector" runat="server" DataSourceID="SqlDataSource1" DataTextField="descripcion" DataValueField="id_sector" Height="27px" SelectedValue='<%# Bind("id_sector") %>' Width="323px">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT id_sector
      ,descripcion
  FROM   Sector 
  where estado = 1"></asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MONTO</td>
                                        <td>
                                            <uc2:textNumero ID="txtMonto" runat="server" MaxValue="999999999999" MinValue="1" Text='<%# Bind("monto") %>' Decimales="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <asp:Button ID="cmdGuardar" runat="server" CommandName="Insert" CssClass="boton" Height="22px" Text="Guardar" Width="100px" />
                                            &nbsp;<asp:Button ID="cmdCancelar" runat="server" CommandName="Cancel" CssClass="boton" Height="22px" Text="Cancelar" Width="100px" CausesValidation="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                                &nbsp;
                            </InsertItemTemplate>
                            <ItemTemplate>
                                id_extension:
                                <asp:Label ID="id_extensionLabel" runat="server" Text='<%# Eval("id_extension") %>' />
                                <br />
                                id_sector:
                                <asp:Label ID="id_sectorLabel" runat="server" Text='<%# Bind("id_sector") %>' />
                                <br />
                                monto:
                                <asp:Label ID="montoLabel" runat="server" Text='<%# Bind("monto") %>' />
                                <br />
                                <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="Nuevo" />
                            </ItemTemplate>
                        </asp:FormView>
                        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" InsertCommand="usp_ExtensionInsert" InsertCommandType="StoredProcedure" SelectCommand="SELECT id_extension
      ,id_sector
      ,monto
  FROM Extension
where  id_extension = @id_sector">
                            <InsertParameters>
                                <asp:Parameter Name="id_extension" Type="Decimal" />
                                <asp:Parameter Name="id_sector" Type="Int32" />
                                <asp:Parameter Name="monto" Type="Decimal" />
                                <asp:CookieParameter CookieName="idUsuario" Name="add_usuario" Type="Decimal" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:Parameter DefaultValue="0" Name="id_sector" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <uc3:cuMensajeBox ID="cuMensajeBox1" runat="server" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

