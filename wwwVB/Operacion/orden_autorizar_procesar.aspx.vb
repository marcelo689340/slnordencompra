﻿Imports CapaMedio.DAO
Imports System.Data


Partial Class Operacion_orden_autorizar_procesar
    Inherits PaginaBase

    Protected Sub Visualizar_Comprobante(ByVal dt As DataTable)
        Dim ds As New DataSet
        ds.Tables.Add(dt)
        ds.Tables(0).TableName = "COMPROBANTE_ORDEN_COMPRA"
        Session("rs") = ds
        Dim sReporte As String
        sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompra.rpt"
        wucReporte1.Show(sReporte)
    End Sub
    Protected Sub GrillaDatos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaDatos.RowCommand
        Dim Items As Long = e.CommandArgument
        Dim dao As DAO_ORDEN = New DAO_ORDEN()
        Dim dt As DataTable
        Try
            Select Case e.CommandName
                Case "Visualizar"
                    dt = dao.getComprobanteImprimir(Items)
                    Visualizar_Comprobante(dt)
                Case "Autorizar"
                    dao.AutorizarOrden(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value), True)
                Case "Rechazar"
                    '  dao.RechazarOrden(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value), True)
                    'Case "Pendiente"
                    '    dao.PendienteOrden(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value))
                Case "Cancelar"
                    dao.OrdenCancelarAccion(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value))
                Case "Procesar"
                    HiddenField1.Value = Items
                    Accion_View(1)
            End Select
            GrillaDatos.DataBind()
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
        End Try

    End Sub
    Private Sub Accion_View(ByVal index As Integer)
        Select Case index
            Case 0 'Grilla
                HiddenField1.Value = 0
                GrillaDatos.DataBind()
            Case 1 'Carga
                'txtObs_gerencia.Text = ""

        End Select
        MultiView1.ActiveViewIndex = index
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim sPagina As String = "/Operacion/orden_autorizar_procesar.aspx"
            Dim sMens As String = Validar_Ingreso(sPagina)
            If sMens = "" Then
                Dim cooBuscar As HttpCookie = Request.Cookies("Buscar")
                If Not cooBuscar Is Nothing Then
                    'txtBuscar.Text = cooBuscar.Value.ToString
                    'Cargar_Grilla()
                End If
                ' Usuario.Value = Request.Cookies("idUsuario").Value
                Response.Cookies("PaginaAnterior").Value = "~" + sPagina
                CType(Master.FindControl("lblTitulo"), Label).Text = "PROCESAR ORDEN DE COMPRA"
                GetCookies()
            Else
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
            End If
        End If
    End Sub

    Private Sub SetCookies()
        Cookies(Me, Me.DropDownList1.GetType.Name) = DropDownList1.SelectedValue
    End Sub

    Private Sub GetCookies()
        If Cookies(Me, Me.DropDownList1.GetType.Name) IsNot Nothing Then
            DropDownList1.SelectedValue = Cookies(Me, Me.DropDownList1.GetType.Name)
        End If
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
        SetCookies()
    End Sub

    Protected Sub SqlDataSource2_Updated(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataSource2.Updated
        If Not (e.Exception Is Nothing) Then
            Dim sMensaje As String = e.Exception.Message.ToString()
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
            e.ExceptionHandled = True
        Else
            Accion_View(0) ' GrillaDatos.DataBind()
        End If
    End Sub

    Protected Sub FormView1_ModeChanging(sender As Object, e As FormViewModeEventArgs) Handles FormView1.ModeChanging

    End Sub

    Protected Sub FormView1_ItemCommand(sender As Object, e As FormViewCommandEventArgs) Handles FormView1.ItemCommand
        If e.CommandName = "Cancel" Then
            Accion_View(0)
            ' GrillaDatos.DataBind()
        End If
    End Sub
    Protected Sub ControlarError_Guardado(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs)
        Dim cs As New CodigoScript
        Dim sMensaje As String
        If Not (e.Exception Is Nothing) Then
            sMensaje = ""
            sMensaje = sMensaje & "SE HA PRODUCIDO UN ERROR;  Descripcion :  "
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()

            e.ExceptionHandled = False
            '' Me.hdfError.Value = sMensaje
        Else
            Accion_View(0)
            '' hdfCodigo.Value = ""
            sMensaje = ""
            ''Me.hdfError.Value = ""
        End If
    End Sub


End Class
