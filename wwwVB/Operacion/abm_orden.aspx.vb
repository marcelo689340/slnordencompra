﻿Imports System.Runtime.Serialization
Imports CapaMedio
Imports CapaMedio.DAO
Imports CapaMedio.Clases

Partial Class Operacion_abm_orden
    Inherits PaginaBase
    Private grdTotal As Double
#Region "Metodos"
    Private Property lstDetalle_Orden As List(Of CDetalle_Orden)
        Get
            'Dim c As CDetalle_Orden

            Dim lst As New List(Of CDetalle_Orden)
            If Not IsNothing(ViewState("lstDetalle_Orden")) Then
                lst = ViewState("lstDetalle_Orden")
            End If
            Return lst
        End Get
        Set(value As List(Of CDetalle_Orden))

            '   Dim serializador As XmlObjectSerializer = New XmlObjectSerializer(GetType(CDetalle_Orden))


            ViewState("lstDetalle_Orden") = value
            Refresh_Grilla(value)
            'Mostrar_DetalleFactura(lstDetalleFactura)
            'Actualizar_totales(lstDetalleFactura)
        End Set
    End Property
    Protected Function Obtener_Nro_Item() As Integer
        Dim lst As New List(Of CDetalle_Orden)
        Dim Item As Integer = 0
        lst = lstDetalle_Orden()
        If lst.Count > 0 Then
            Item = lst.Count
        End If
        Item = Item + 1
        Return Item
    End Function
    Private Function Total_Item(ByVal cantidad As String, ByVal precio As String) As Double
        Dim total As Double = 0
        total = Convert.ToDouble(cantidad) * Convert.ToDouble(precio)

        Return total
    End Function
    Private Sub Datos_bd()
        Dim dao As DAO_ORDEN = New DAO_ORDEN()
        Dim lqsCab = dao.getOrden(Request.QueryString("ID"))
        cboSector.SelectedValue = lqsCab.id_sector
        txtFecha.Text = lqsCab.fecha
        txtObservacion.Text = lqsCab.observacion
        'txtRuc.DataText = lqsCab.ruc_proveedor
        'Dim lstDet As List(Of viewGridOrdenDetalle) = New List(Of viewGridOrdenDetalle)()
        Dim lstDet = dao.GetOrden_Detalle(Request.QueryString("ID"))
        Dim lst = (From A In lstDet Select New CDetalle_Orden With {.Id_orden = A.id_orden,
                                                                 .Item = A.item,
                                                                 .Cantidad = A.cantidad,
                                                                 .Descripcion = A.descripcion,
                                                                 .Precio = A.precio,
                                                                 .Id_sector = A.id_sector,
                                                                 .Sector = A.sector,
                                                                 .Total = A.total,
                                                                 .Fecha_ult_mod = A.fecha_ult_mod,
                                                                 .Add_usuario = A.add_usuario})
        lstDetalle_Orden = lst.ToList()
    End Sub
    Private Sub Limpiar_Carga()
        txtCantidad.Text = 0
        txtDescripcion.Text = ""
        txtPrecio.Text = 0
    End Sub
    Private Sub AddItem()
        Dim lst As New List(Of CDetalle_Orden)
        lst = lstDetalle_Orden()
        Dim item As New CDetalle_Orden()
        item.id_orden = Request.QueryString("ID")
        item.item = Obtener_Nro_Item()
        item.cantidad = txtCantidad.Text
        item.descripcion = txtDescripcion.Text
        item.precio = txtPrecio.Text
        item.id_sector = cboSectorItem.SelectedValue
        item.sector = cboSectorItem.SelectedItem.Text
        item.total = Total_Item(txtCantidad.Text, txtPrecio.Text)
        item.add_usuario = Request.Cookies("idUsuario").Value
        lst.Add(item)
        lstDetalle_Orden = lst
        Limpiar_Carga()
    End Sub
    Protected Sub Eliminar_Articulo(ByVal Item As Integer)
        Dim lstDetalle As New List(Of CDetalle_Orden)
        lstDetalle = lstDetalle_Orden()
        'Eliminar Articulo y renumerar el nro de item
        Dim rsDetalle = (From D In lstDetalle Where D.Item >= Item Select D Order By D.Item)
        If rsDetalle.Count > 0 Then
            lstDetalle.Remove(rsDetalle.First)
            For Each r In rsDetalle
                r.Item = r.Item - 1
            Next
        End If
        '  Mostrar_DetalleFactura(lstDetalleFactura)
        lstDetalle_Orden = lstDetalle
    End Sub
    Private Sub Refresh_Grilla(ByVal lst As List(Of CDetalle_Orden))
        Dim q = (From A In lst Select A)
        GrillaDetalle.DataSource = q.ToList ' lst.ToList
        GrillaDetalle.DataBind()
        If GrillaDetalle.Rows.Count > 0 Then
            cmdGuardar.Visible = True
        Else
            cmdGuardar.Visible = False
        End If
    End Sub
    Protected Sub Visualizar_Report(ByVal dt As Data.DataTable)
        Dim ds As New Data.DataSet
        ds.Tables.Add(dt)
        ds.Tables(0).TableName = "COMPROBANTE_ORDEN_COMPRA"
        Session("rs") = ds
        Dim sReporte As String
        sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompra.rpt"
        wucReporte1.Show(sReporte)
    End Sub
    Private Sub Guardar_Datos()
        Dim dao As DAO_ORDEN = New DAO_ORDEN()
        Dim dt As New Data.DataTable
        Dim lst = lstDetalle_Orden()
        Dim sRuc As String = ""
        'If Request.QueryString("ID") = "0" Then
        '    Dim A = dao.NewOrden(Convert.ToInt64(Request.QueryString("ID")), txtFecha.Text, cboSector.SelectedValue, Request.Cookies("idUsuario").Value, lst)
        'End If
        Try
            Dim A = dao.GuardarOrden(Convert.ToInt64(Request.QueryString("ID")), txtFecha.Text, cboSector.SelectedValue, Request.Cookies("idUsuario").Value, txtObservacion.Text, sRuc, lst)

            ' dt = dao.getComprobanteImprimir(A)
            ' Visualizar_Report(dt)
            Response.Redirect(cmdVolver.PostBackUrl.ToString)
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
        End Try
    End Sub

#End Region
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        AddItem()
    End Sub
    Protected Sub GrillaDetalle_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaDetalle.RowCommand
        Dim Items As Integer = e.CommandArgument
        If e.CommandName = "Eliminar" Then
            Eliminar_Articulo(Items)
        End If
    End Sub
    Protected Sub GrillaDetalle_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GrillaDetalle.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowTotal As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "total"))
            grdTotal = grdTotal + rowTotal
        End If

        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lbl As Label = DirectCast(e.Row.FindControl("lblTotalGen"), Label)
            lbl.Text = grdTotal.ToString("N0")
        End If
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If
            Dim saveUcnow As DateTime = DateTime.Now
            txtFecha.Text = saveUcnow


            Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            If Request.QueryString("ID") <> "0" Then
                Datos_bd()
                CType(Master.FindControl("lblTitulo"), Label).Text = "MODIFICAR ORDEN DE COMPRA : " + Request.QueryString("ID").ToString()
                'Cargar_Datos(Convert.ToInt16(Request.QueryString("ID")))
            Else
                cmdGuardar.Visible = False
                CType(Master.FindControl("lblTitulo"), Label).Text = "NUEVA ORDEN DE COMPRA"
            End If
        End If
    End Sub
    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        If GrillaDetalle.Rows.Count > 0 Then
            Guardar_Datos()
        Else
            Dim sMensaje As String = "ERROR...  DETALLE VACIO"
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
        End If
    End Sub


    Protected Sub wucReporte1_DespuesdeMostrar(sender As Object, e As EventArgs) Handles wucReporte1.DespuesdeMostrar
        Response.Redirect(cmdVolver.PostBackUrl.ToString)
    End Sub

    Protected Sub cboSector_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSector.SelectedIndexChanged
        cboSectorItem.DataBind()
        cboSectorItem.SelectedValue = cboSector.SelectedValue
    End Sub

    Protected Sub SqlDataSource3_Load(sender As Object, e As EventArgs) Handles SqlDataSource3.Load

    End Sub

    Protected Sub cboSectorItem_DataBound(sender As Object, e As EventArgs) Handles cboSectorItem.DataBound
        cboSectorItem.SelectedValue = cboSector.SelectedValue
    End Sub
End Class
