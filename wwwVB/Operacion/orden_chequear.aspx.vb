﻿Imports CapaMedio.DAO
Imports System.Data

Partial Class Operacion_orden_chequear
    Inherits PaginaBase

    Protected Sub Visualizar_Comprobante(ByVal dt As Data.DataTable)
        Dim ds As New Data.DataSet
        ds.Tables.Add(dt)
        ds.Tables(0).TableName = "COMPROBANTE_ORDEN_COMPRA"
        Session("rs") = ds
        Dim sReporte As String
        sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompra.rpt"
        wucReporte1.Show(sReporte)
    End Sub
    Protected Sub GrillaDatos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaDatos.RowCommand, GrillaDatos0.RowCommand
        Dim Items As Long = e.CommandArgument
        Dim dao As DAO_ORDEN = New DAO_ORDEN()
        Dim dt As DataTable
        Try
            Select Case e.CommandName
                Case "Cancelar"
                    dao.ChequearOrden(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value), False)
                Case "Autorizar"
                    dao.ChequearOrden(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value), True)
                Case "Visualizar"
                    dt = dao.getComprobanteImprimir(Items)
                    Visualizar_Comprobante(dt)
            End Select
            GrillaDatos.DataBind()
            GrillaDatos0.DataBind()
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
        End Try

    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim sPagina As String = "/Operacion/orden_chequear.aspx"
            Dim sMens As String = Validar_Ingreso(sPagina)
            If sMens = "" Then
                Dim cooBuscar As HttpCookie = Request.Cookies("Buscar")
                If Not cooBuscar Is Nothing Then
                    'txtBuscar.Text = cooBuscar.Value.ToString
                    'Cargar_Grilla()
                End If
                ' Usuario.Value = Request.Cookies("idUsuario").Value
                Response.Cookies("PaginaAnterior").Value = "~" + sPagina
                CType(Master.FindControl("lblTitulo"), Label).Text = "CHEQUEAR ORDEN DE COMPRA"
                GetCookies()
            Else
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
            End If
        End If
    End Sub
    Private Sub SetCookies()
        Cookies(Me, Me.DropDownList1.GetType.Name) = DropDownList1.SelectedValue
    End Sub
    Private Sub GetCookies()
        If Cookies(Me, Me.DropDownList1.GetType.Name) IsNot Nothing Then
            DropDownList1.SelectedValue = Cookies(Me, Me.DropDownList1.GetType.Name)
        End If
    End Sub
    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
        SetCookies()
    End Sub

    'Protected Sub GrillaDatos0_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaDatos0.RowCommand
    '    Dim Items As Long = e.CommandArgument
    '    Dim dao As DAO_ORDEN = New DAO_ORDEN()
    '    Dim dt As DataTable
    '    Try
    '        Select Case e.CommandName
    '            Case "Cancelar"
    '                dao.ChequearOrden(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value), False)
    '            Case "Visualizar"
    '                dt = dao.getComprobanteImprimir(Items)
    '                Visualizar_Comprobante(dt)
    '        End Select
    '        GrillaDatos.DataBind()
    '    Catch ex As Exception
    '        Dim sMensaje As String = ex.Message.ToString
    '        cuMensajeBox1.tipoMensaje = 2
    '        cuMensajeBox1.Mensaje = sMensaje
    '        cuMensajeBox1.show()
    '    End Try
    'End Sub
End Class
