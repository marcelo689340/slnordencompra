﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="VerPresupuesto.aspx.vb" Inherits="Operacion_VerPresupuesto" %>

<%@ Register Src="../control/NewMsgBox.ascx" TagName="NewMsgBox" TagPrefix="uc1" %>
<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 128px;
            height: 15px;
        }

        .auto-style2 {
            width: 100%;
        }

        .auto-style3 {
            width: 79px;
        }

        .auto-style4 {
            width: 79px;
            height: 20px;
        }

        .auto-style5 {
            height: 20px;
        }

        .auto-style7 {
        }

        .auto-style8 {
            height: 55px;
        }

        .auto-style9 {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        //$(document).ready(function () {

        //});

        function fileUpload1() {
            __doPostBack('fileUpload1', '');
            return (true);
        }
    </script>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="auto-style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <table class="auto-style2">
                <tr>
                    <td class="auto-style4">NRO ORDEN</td>
                    <td class="auto-style5">
                        <asp:Label ID="lblIdOrden" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">FECHA</td>
                    <td>
                        <asp:Label ID="lblFecha" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">SECTOR</td>
                    <td>
                        <asp:Label ID="lblSector" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">MONTO</td>
                    <td>
                        <asp:Label ID="lblMonto" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>
                        <asp:Button ID="cmdVolver" runat="server" CausesValidation="False" CssClass="boton" Text="Volver" Width="118px" />
                    </td>
                </tr>
            </table>
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <table class="auto-style2">
                        <tr>
                            <td class="auto-style8" colspan="2">
                                <asp:GridView ID="grillaFirmas" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_orden,item" DataSourceID="SqlDatosFirmas" Width="450px">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnSeleccionar" runat="server" CausesValidation="False" CommandArgument='<%# Eval("item")%>' CommandName="Select" ImageUrl="~/App_Themes/css/img/standar/descargar.png" ToolTip="Descargar Presupuesto" />
                                                <%-- <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandArgument='<%# Eval("item")%>' CommandName="Select" Text="Seleccionar"></asp:LinkButton>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" />
                                        <asp:BoundField DataField="item" HeaderText="item" ReadOnly="True" SortExpression="item" />
                                        <asp:BoundField DataField="observacion" HeaderText="observacion" SortExpression="observacion" />
                                        <asp:BoundField DataField="archivo_nombre" HeaderText="archivo_nombre" SortExpression="archivo_nombre" />
                                        <asp:BoundField DataField="fecha_ult_mod" HeaderText="fecha_ult_mod" SortExpression="fecha_ult_mod" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <%--<uc2:CuSinInformacion ID="CuSinInformacion1" runat="server" mensajeError="No existen detalles cargados" requerido="False" />--%>NO EXISTE PRESUPUESTO
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDatosFirmas" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" DeleteCommand="usp_PresupuestoDelete" DeleteCommandType="StoredProcedure" SelectCommand="SELECT [id_orden]
      ,[item]
      ,observacion
      ,archivo_nombre
      ,[fecha_ult_mod]
  FROM [bd_Orden_Imagen].[dbo].[Presupuesto]
where id_orden = @ID
">
                                    <DeleteParameters>
                                        <asp:Parameter Name="id_orden" Type="Int64" />
                                        <asp:Parameter Name="item" Type="Int32" />
                                    </DeleteParameters>
                                    <SelectParameters>
                                        <asp:QueryStringParameter Name="ID" QueryStringField="ID" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                </td>
                        </tr>
                        <tr>
                            <td class="auto-style9" colspan="2"><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style9">
                                &nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style9">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style9">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <br />
                </asp:View>
            </asp:MultiView>
            <table class="auto-style2">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <table class="auto-style2">
                            <tr>
                                <td class="auto-style7">
                                    &nbsp;</td>
                            </tr>
                        </table>

                        <br />
                    </td>
                </tr>
            </table>
            <uc2:cuMensajeBox ID="cuMensajeBox1" runat="server" />
            <br />
            <br />

        </ContentTemplate>

        <Triggers>
            <%--<asp:PostBackTrigger ControlID="cmdGuardar0" />--%>
            <asp:PostBackTrigger ControlID="grillaFirmas" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

