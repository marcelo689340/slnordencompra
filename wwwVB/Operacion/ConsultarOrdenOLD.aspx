﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.master" Culture="Auto" UICulture ="Auto" AutoEventWireup="false" CodeFile="ConsultarOrdenOLD.aspx.vb" Inherits="Operacion_ConsultarOrdenOLD" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style4 {
            width: 121px;
        }
        .auto-style6 {
            width: 191px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlBusquedas" runat="server" DefaultButton="cmdBuscar" CssClass="conPadding05">
                <div style="padding-top: 5px; width: 500px">
                    <asp:Button ID="cmdNuevo" runat="server" Text="Nuevo" Height="22px" Width="100px" CssClass="boton" PostBackUrl="~/Operacion/abm_orden.aspx?ID=0" />
                    &nbsp;<asp:Button ID="cmdEnviarExcel" runat="server" Height="22px" Text="Enviar Excel" Width="100px" CssClass="boton" Visible="False" />
                    &nbsp;<asp:Button ID="cmdEnviarPdf" runat="server" Text="Enviar PDF" Height="22px" Width="100px" CssClass="boton" Visible="False" />
                    &nbsp;
                </div>
                <table class="auto-style1" width="500px">
                    <tr>
                        <td class="auto-style6">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style6">Orden </td>
                        <td>
                            <asp:TextBox ID="txtBuscar" runat="server" Height="20px" Width="275px"></asp:TextBox>
                            &nbsp;<asp:Button ID="cmdBuscar" runat="server" Text="Buscar" CssClass="boton" />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style6">Incluir Autorizado </td>
                        <td>
                            <asp:CheckBox ID="chkAutorizado" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style6">Incluir Procesado</td>
                        <td>
                            <asp:CheckBox ID="chkProcesado" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style6">Incluir Rechazado</td>
                        <td>
                            <asp:CheckBox ID="chkRechazado" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style6">Incluir Pendiente</td>
                        <td>
                            <asp:CheckBox ID="chkPendiente" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div style="padding-top: 5px; padding-bottom: 5px">
                <asp:Panel ID="pnlGrilla" runat="server" Visible="false" Width="500px">
                    <div>
                        <table class="auto-style1">
                            <tr>
                                <%--                                <asp:Panel ID="pntOpcionGrilla" runat="server" Height="16px">
                                    <asp:Literal ID="ltlListado" runat="server" Text="Listado de Usuarios"></asp:Literal>
                                    <asp:Literal ID="ltlInformacion" runat="server"></asp:Literal>
                                    <asp:RequiredFieldValidator ID="rfvCantFila" runat="server" ControlToValidate="txtCantFila" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtCantFila" runat="server" Height="16px" Width="20px"></asp:TextBox>
                                    <asp:Button ID="Button1" runat="server" Height="24px" Text="Button" Width="31px" />
                                </asp:Panel>--%>
                            </tr>
                            <tr>
                                <td class="auto-style4">
                                    <asp:GridView ID="grvDatos" runat="server" AutoGenerateColumns="False" DataKeyNames="id_orden" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataGrilla" Width="1300px">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="btnEditar" runat="server" ToolTip="Editar Registro" ImageUrl="~/App_Themes/css/img/standar/update.png" NavigateUrl='<%# Eval("id_orden", "abm_orden.aspx?ID={0}") %>' Text="mmm" Visible='<%# Eval("Modificar") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle Width="70px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnEliminr"  runat="server" CausesValidation="False" CommandName="Delete" CommandArgument='<%# Eval("id_orden") %>' ImageUrl="~/App_Themes/css/img/standar/recicle.png" Text="Eliminar" ToolTip="Elimiar Registro" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" Visible='<%# Eval("Eliminar") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
<%--                                            <asp:TemplateField HeaderText="Modificar" SortExpression="Modificar">
                                                <EditItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Modificar") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Modificar") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Eliminar" SortExpression="Eliminar">
                                                <EditItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Eliminar") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Eliminar") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:BoundField DataField="id_orden" SortExpression="id_orden" HeaderText="id_orden" ReadOnly="True">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="fecha" HeaderText="fecha" SortExpression="fecha" DataFormatString="{0:d}" />
                                            <asp:BoundField DataField="id_sector" HeaderText="id_sector" SortExpression="id_sector" />
                                            <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion" />
                                            <asp:BoundField DataField="monto" HeaderText="monto" SortExpression="monto" DataFormatString="{0:N0}" >
                                            <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="fecha_autorizacion" HeaderText="fecha_autorizacion" SortExpression="fecha_autorizacion" DataFormatString="{0:d}" />
                                            <asp:BoundField DataField="fecha_procesado" HeaderText="fecha_procesado" SortExpression="fecha_procesado" DataFormatString="{0:d}" />
                                            <asp:BoundField DataField="fecha_rechazado" HeaderText="fecha_rechazado" SortExpression="fecha_rechazado" DataFormatString="{0:d}" />
                                            <asp:CheckBoxField DataField="pendiente" HeaderText="pendiente" SortExpression="pendiente" />

                                            <asp:BoundField DataField="id_usuario" HeaderText="id_usuario" SortExpression="id_usuario" />

                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataGrilla" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CONSULTA_ORDEN_GRID" DeleteCommand="usp_OrdenDelete" DeleteCommandType="StoredProcedure" SelectCommandType="StoredProcedure">
                                        <DeleteParameters>
                                            <asp:Parameter Name="id_orden" Type="Int64" />
                                            <asp:CookieParameter CookieName="IdUsuario" Name="add_usuario" Type="Int32" />
                                        </DeleteParameters>
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="txtBuscar" Name="FILTRO" PropertyName="Text" Type="String" />
                                            <asp:ControlParameter ControlID="chkAutorizado" Name="autorizacion" PropertyName="Checked" Type="Boolean" />
                                            <asp:ControlParameter ControlID="chkProcesado" Name="procesado" PropertyName="Checked" Type="Boolean" />
                                            <asp:ControlParameter ControlID="chkRechazado" Name="rechazado" PropertyName="Checked" Type="Boolean" />
                                            <asp:ControlParameter ControlID="chkPendiente" Name="pendiente" PropertyName="Checked" Type="Boolean" />
                                            <asp:CookieParameter CookieName="IdUsuario" Name="id_usuario" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                    <asp:HiddenField ID="hdfError" runat="server" />
                                    <asp:HiddenField ID="Usuario" runat="server" />
                                    <uc1:cuMensajeBox ID="cuMensajeBox" runat="server" />
                                </td>

                            </tr>
                        </table>
                    </div>

                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdEnviarExcel" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

