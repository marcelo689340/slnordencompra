﻿Imports CapaMedio.DAO
Imports System.Data

Partial Class Operacion_orden_autorizar_pendiente
    Inherits PaginaBase
    Protected Sub Visualizar_Comprobante(ByVal dt As Data.DataTable)
        Dim ds As New Data.DataSet
        ds.Tables.Add(dt)
        ds.Tables(0).TableName = "COMPROBANTE_ORDEN_COMPRA"
        Session("rs") = ds
        Dim sReporte As String
        sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompra.rpt"
        wucReporte1.Show(sReporte)
    End Sub
    Protected Sub GrillaDatos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaDatos.RowCommand
        Dim Items As Long = e.CommandArgument
        Dim dao As DAO_ORDEN = New DAO_ORDEN()
        Dim dt As DataTable
        Try
            Select Case e.CommandName
                Case "Visualizar"
                    dt = dao.getComprobanteImprimir(Items)
                    Visualizar_Comprobante(dt)
                Case "Autorizar"
                    dao.AutorizarOrden(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value), True)
                Case "Rechazar"
                    Dim fila As GridViewRow = GrillaDatos.Rows(Items)
                    cmdGuardar.CommandArgument = e.CommandName
                    Cargar_Datos_fila(fila)
                    Accion_View(1)
                    '  dao.RechazarOrden(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value), True)
                    'Case "Pendiente"
                    '    dao.PendienteOrden(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value))
                Case "Cancelar"
                    dao.OrdenCancelarAccion(Items, Convert.ToInt32(Request.Cookies("idUsuario").Value))


            End Select
            GrillaDatos.DataBind()
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
        End Try

    End Sub

    Private Sub Accion_View(ByVal index As Integer)
        Select Case index
            Case 0 'Grilla
                '  GrillaDatos.DataBind()
            Case 1 'Carga
                txtObs_gerencia.Text = ""

        End Select
        MultiView1.ActiveViewIndex = index
    End Sub

    Private Sub Cargar_Datos_fila(ByVal fila As GridViewRow)
        lblIdOrden.Text = fila.Cells(1).Text
        lblFecha.Text = fila.Cells(2).Text
        lblSector.Text = fila.Cells(4).Text
        lblObs.Text = fila.Cells(5).Text
        lblMonto.Text = fila.Cells(6).Text
        If (cmdGuardar.CommandArgument = "Rechazar") Then
            litTitulo.Text = "RECHAZAR ORDEN"
        Else
            litTitulo.Text = "PONER ORDEN COMO PENDIENTE"
        End If

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim sPagina As String = "/Operacion/orden_autorizar_pendiente.aspx"
            Dim sMens As String = Validar_Ingreso(sPagina)
            If sMens = "" Then
                Dim cooBuscar As HttpCookie = Request.Cookies("Buscar")
                If Not cooBuscar Is Nothing Then
                    'txtBuscar.Text = cooBuscar.Value.ToString
                    'Cargar_Grilla()
                End If
                ' Usuario.Value = Request.Cookies("idUsuario").Value
                Response.Cookies("PaginaAnterior").Value = "~" + sPagina
                CType(Master.FindControl("lblTitulo"), Label).Text = "AUTORIZAR ORDEN DE PAGO PENDIENTE"
                ' GetCookies()
                Accion_View(0)
                ' GrillaDatos.DataBind()
                GetCookies()

            Else
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
            End If
        End If
    End Sub

    Private Sub SetCookies()
        Cookies(Me, Me.DropDownList1.GetType.Name) = DropDownList1.SelectedValue
    End Sub

    Private Sub GetCookies()
        If Cookies(Me, Me.DropDownList1.GetType.Name) IsNot Nothing Then
            DropDownList1.SelectedValue = Cookies(Me, Me.DropDownList1.GetType.Name)
        End If
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
        SetCookies()
    End Sub

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Dim dao As DAO_ORDEN = New DAO_ORDEN()
        Try
            If (cmdGuardar.CommandArgument = "Rechazar") Then
                dao.RechazarOrden(Convert.ToInt64(lblIdOrden.Text), Convert.ToInt32(Request.Cookies("idUsuario").Value), txtObs_gerencia.Text, True)

            Else
                dao.PendienteOrden(Convert.ToInt64(lblIdOrden.Text), Convert.ToInt32(Request.Cookies("idUsuario").Value), txtObs_gerencia.Text)
            End If
            Accion_View(0)

        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString

            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
        End Try
    End Sub

    Protected Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Accion_View(0)
    End Sub

End Class
