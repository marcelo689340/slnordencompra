﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="orden_autorizar_procesar.aspx.vb" Inherits="Operacion_orden_autorizar_procesar" %>

<%@ Register src="../control/cuMensajeBox.ascx" tagname="cuMensajeBox" tagprefix="uc1" %>
<%@ Register src="../control/wucReporte.ascx" tagname="wucReporte" tagprefix="uc2" %>

<%@ Register src="../control/Calendario.ascx" tagname="Calendario" tagprefix="uc3" %>

<%@ Register src="../control/BuscarTablaNew.ascx" tagname="BuscarTablaNew" tagprefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 68px;
        }

        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
<img alt="" class="style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="auto-style1">
                <tr>
                    <td>
                        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                            <asp:View ID="View1" runat="server">
                                <table class="auto-style1">
                                    <tr>
                                        <td class="auto-style2">Sector</td>
                                        <td>
                                            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSector" DataTextField="descripcion" DataValueField="id_sector" Height="24px" Width="272px">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlDataSector" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CBO_SECTOR" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                        </td>
                                    </tr>
                                </table>
                                                        <asp:GridView ID="GrillaDatos" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_orden,id_sector" DataSourceID="SqlDataSource1" Width="1016px">
                            <Columns>
                                <asp:TemplateField HeaderText="Visualizar">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnVisualizar" runat="server" CommandName="Visualizar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/img/estandar/buscar03.png" ToolTip="Visualizar Orden" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Procesar" CommandArgument='<%# Eval("id_orden") %>' Text="Procesar"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" />
                                <asp:BoundField DataField="fecha" HeaderText="fecha" SortExpression="fecha" />
                                <asp:BoundField DataField="id_sector" HeaderText="id_sector" ReadOnly="True" SortExpression="id_sector" />
                                <asp:BoundField DataField="sector" HeaderText="sector" SortExpression="sector" />
                                <asp:BoundField DataField="observacion" HeaderText="observacion" SortExpression="observacion" />
                                <asp:BoundField DataField="monto" HeaderText="monto" SortExpression="monto" DataFormatString="{0:N0}">
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="fecha_autorizacion" HeaderText="fecha_autorizacion" SortExpression="fecha_autorizacion"></asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                SELECCIONE OTRO SECTOR.....<br />
                                &nbsp;<br />
                                NO EXISTE ORDEN DE COMPRA PARA PROCESAR....<br />
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="GRID_PROCESAR_ORDEN" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="DropDownList1" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                                <br />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                                                    <asp:FormView ID="FormView1" runat="server" DataKeyNames="id_orden" DataSourceID="SqlDataSource2" Width="353px">
                                        <EditItemTemplate>
                                            <table class="auto-style1">
                                                <tr>
                                                    <td>ID ORDEN</td>
                                                    <td>
                                                        <asp:Label ID="id_ordenLabel1" runat="server" Text='<%# Eval("id_orden") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>FECHA</td>
                                                    <td>
                                                        <asp:TextBox ID="fechaTextBox" runat="server" Enabled="False" Text='<%# Bind("fecha", "{0:d}") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>FECHA AUTORZACION</td>
                                                    <td>
                                                        <asp:TextBox ID="fecha_autorizacionTextBox" runat="server" Enabled="False" Text='<%# Bind("fecha_autorizacion", "{0:d}") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>SECTOR</td>
                                                    <td>
                                                        <asp:TextBox ID="descripcionTextBox" runat="server" Enabled="False" Text='<%# Bind("descripcion") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>FECHA PAGO</td>
                                                    <td>
                                                        <uc3:Calendario ID="Calendario1" runat="server" Text='<%# Bind("fecha_pago") %>' Requerido="True" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>PROVEEDOR</td>
                                                    <td>
                                                        <uc7:BuscarTablaNew ID="txtRuc" runat="server" DataText='<%# Bind("ruc_proveedor") %>' EnviaParametro="True" Grilla_Select="SELECT distinct PrvRuc, PrvNom FROM viewProveedor where PrvRuc like '%' + @filtro + '%' or PrvNom like '%' + @filtro + '%'" ImagenURLBotonAgregar="~/App_Themes/css/img/standar/cliente.png" Mostrar_Descripcion="True" Mostrar_Select="SELECT distinct PrvRuc, PrvNom FROM viewProveedor where PrvRuc like  @filtro " MostrarBotonAgregar="False" ValidationGroup="Cabecera"  />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>NRO DE FACTURA</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("nro_factura") %>'></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Button ID="Button1" runat="server" CommandName="Update" CssClass="boton" Text="Guardar" />
                                            <asp:Button ID="Button2" runat="server" CausesValidation="False" CommandName="Cancel" CssClass="boton" Text="Cancelar" />
                                            <br />
                                            &nbsp;
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            id_orden:
                                            <asp:TextBox ID="id_ordenTextBox" runat="server" Text='<%# Bind("id_orden") %>' />
                                            <br />
                                            fecha:
                                            <asp:TextBox ID="fechaTextBox" runat="server" Text='<%# Bind("fecha") %>' />
                                            <br />
                                            fecha_autorizacion:
                                            <asp:TextBox ID="fecha_autorizacionTextBox" runat="server" Text='<%# Bind("fecha_autorizacion") %>' />
                                            <br />
                                            descripcion:
                                            <asp:TextBox ID="descripcionTextBox" runat="server" Text='<%# Bind("descripcion") %>' />
                                            <br />
                                            fecha_pago:
                                            <asp:TextBox ID="fecha_pagoTextBox" runat="server" Text='<%# Bind("fecha_pago") %>' />
                                            <br />
                                            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insertar" />
                                            &nbsp;&nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <table class="auto-style1">
                                                <tr>
                                                    <td>ID ORDEN</td>
                                                    <td>
                                                        <asp:Label ID="id_ordenLabel" runat="server" Text='<%# Eval("id_orden") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>FECHA</td>
                                                    <td>
                                                        <asp:Label ID="fechaLabel" runat="server" Text='<%# Bind("fecha") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>FECHA AUTORIZACION</td>
                                                    <td>
                                                        <asp:Label ID="fecha_autorizacionLabel" runat="server" Text='<%# Bind("fecha_autorizacion") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>SECTOR</td>
                                                    <td>
                                                        <asp:Label ID="descripcionLabel" runat="server" Text='<%# Bind("descripcion") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>FECHA PAGO</td>
                                                    <td>
                                                        <asp:Label ID="fecha_pagoLabel" runat="server" Text='<%# Bind("fecha_pago") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>RUC</td>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("ruc_proveedor") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Nro Factura</td>
                                                    <td>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("nro_factura") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                            <asp:Button ID="Button1" runat="server" CommandName="Edit" CssClass="boton" Text="Procesar Orden" />
                                            &nbsp;<asp:Button ID="Button2" runat="server" CausesValidation="False" CommandName="Cancel" CssClass="boton" Text="Cancelar" />
                                            <br />
                                        </ItemTemplate>
                                    </asp:FormView>
                                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT
O.id_orden,
O.fecha,
O.fecha_autorizacion,
S.descripcion,
O.fecha_pago,
O.ruc_proveedor,
O.nro_factura
FROM
dbo.Orden AS O
INNER JOIN dbo.Sector AS S ON O.id_sector = S.id_sector
where O.id_orden = @id_orden"
                                        UpdateCommand="usp_OrdenProcesar" UpdateCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="HiddenField1" Name="id_orden" PropertyName="Value" />
                                        </SelectParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="id_orden" Type="Int64" />
                                            <asp:Parameter Name="fecha" Type="DateTime" />
                                            <asp:Parameter Name="fecha_autorizacion" Type="DateTime" />
                                            <asp:Parameter Name="descripcion" Type="String" />
                                            <asp:Parameter Name="fecha_pago" DbType="Date" />
                                            <asp:Parameter Name="ruc_proveedor" Type="String" />
                                            <asp:Parameter Name="nro_factura" Type="String" />
                                            <asp:CookieParameter CookieName="idUsuario" Name="id_usuario_procesado" Type="Int32" />
                                        </UpdateParameters>
                                    </asp:SqlDataSource>
                                                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                    <br />
                            </asp:View>
                            <br />
                            <br />
                        </asp:MultiView>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                       
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">

                        <table class="auto-style1">
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>

                                    <uc2:wucReporte ID="wucReporte1" runat="server" />
                                    <br />
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:cuMensajeBox ID="cuMensajeBox1" runat="server" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

