﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.master" Culture="Auto" UICulture="Auto" AutoEventWireup="false" CodeFile="ConsultarOrden.aspx.vb" Inherits="Operacion_ConsultarOrden" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<%@ Register Src="../control/Calendario.ascx" TagName="Calendario" TagPrefix="uc1" %>

<%@ Register Src="../control/wucReporte.ascx" TagName="wucReporte" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style6 {
        }

        .auto-style7 {
            width: 145px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlBusquedas" runat="server" CssClass="conPadding05">
                <div style="padding-top: 5px; width: 500px">
                    <asp:Button ID="cmdNuevo" runat="server" Text="Nuevo" Height="22px" Width="100px" CssClass="boton" PostBackUrl="~/Operacion/abm_orden.aspx?ID=0" />
                    &nbsp;<asp:Button ID="cmdEnviarExcel" runat="server" Height="22px" Text="Enviar Excel" Width="100px" CssClass="boton" Visible="False" />
                    &nbsp;<asp:Button ID="cmdEnviarPdf" runat="server" Text="Enviar PDF" Height="22px" Width="100px" CssClass="boton" Visible="False" />
                    &nbsp;<asp:Button ID="cmdEnviarPdf0" runat="server" CssClass="boton" Height="22px" Text="Refrescar Datos" Visible="False" Width="124px" />
                </div>
                <table class="auto-style1" width="500px">
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table class="auto-style1">
                                <tr>
                                    <td class="auto-style7">FECHA INICIO</td>
                                    <td>
                                        <uc1:Calendario ID="txtFechaInicio" runat="server" Requerido="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style7">FECHA FIN</td>
                                    <td>
                                        <uc1:Calendario ID="txtFechaFin" runat="server" Requerido="True" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style6" colspan="2">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="id_sector" DataSourceID="SqlDataSector" AllowPaging="True" AllowSorting="True" Caption="DATOS DEL MES ACTUAL">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="id_sector" HeaderText="id_sector" SortExpression="id_sector" />
                                    <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion" />
                                    <asp:BoundField DataField="limite" DataFormatString="{0:N0}" HeaderText="limite" SortExpression="limite">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="extension" DataFormatString="{0:N0}" HeaderText="extension" SortExpression="extension">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="monto_autorizado" DataFormatString="{0:N0}" HeaderText="monto_autorizado" SortExpression="monto_autorizado">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="monto_pendiente" DataFormatString="{0:N0}" HeaderText="monto_pendiente" SortExpression="monto_pendiente">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="saldo" DataFormatString="{0:N0}" HeaderText="saldo" SortExpression="saldo">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerTemplate>
                                    DATOS DEL MES ACTUAL....
                                </PagerTemplate>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSector" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CONSULTAR_SALDO_MES" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:CookieParameter CookieName="idUsuario" Name="id_usuario" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
            <div style="padding-top: 5px; padding-bottom: 5px">
                <div>
                    <div class="cuadroTitulo">
                        <asp:Literal ID="Literal2" runat="server" Text="ORDEN DE COMPRA"></asp:Literal>
                    </div>
                    <asp:GridView ID="grvDatos" runat="server" AutoGenerateColumns="False" DataKeyNames="id_orden" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataGrilla" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Adjuntar Presupuesto">
                                <ItemTemplate>
                                    <asp:HyperLink ID="btnPresupuesto" runat="server" ToolTip="Adjuntar Presupuesto" ImageUrl="~/App_Themes/css/img/standar/agregar.png" NavigateUrl='<%# Eval("id_orden", "AddPresupuesto.aspx?ID={0}")%>' Visible='<%# Eval("Modificar") %>'></asp:HyperLink>
                                    <%-- <asp:ImageButton ID="btnPresupuesto" runat="server" CommandName="Adjuntar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/css/img/standar/agregar.png" ToolTip="Adjuntar Presupuesto" />--%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="10px" />
                            </asp:TemplateField>
<%--                            <asp:TemplateField HeaderText="Visualizar" Visible="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnVisualizar" runat="server" CommandName="Visualizar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/img/estandar/buscar03.png" ToolTip="Visualizar Orden" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                            </asp:TemplateField>--%>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink ID="btnEditar" runat="server" ToolTip="Editar Registro" ImageUrl="~/App_Themes/css/img/standar/update.png" NavigateUrl='<%# Eval("id_orden", "abm_orden.aspx?ID={0}") %>' Text="mmm" Visible='<%# Eval("Modificar") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEliminr" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument='<%# Eval("id_orden") %>' ImageUrl="~/App_Themes/css/img/standar/recicle.png" ToolTip="Elimiar Registro" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" Visible='<%# Eval("Eliminar") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="30px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="id_orden" SortExpression="id_orden" HeaderText="id_orden" ReadOnly="True">
                                <ItemStyle Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="fecha" HeaderText="fecha" SortExpression="fecha" DataFormatString="{0:d}">
                                <ItemStyle Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="id_sector" HeaderText="id_sector" SortExpression="id_sector">
                                <ItemStyle Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="descripcion" HeaderText="Sector" SortExpression="descripcion" >
                            <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="monto" HeaderText="monto" SortExpression="monto" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" Width="70px" />
                            </asp:BoundField>

                            <asp:BoundField DataField="Presupuesto" HeaderText="Presupuesto" SortExpression="Presupuesto">
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:BoundField>

                        </Columns>
                        <EmptyDataTemplate>
                            NO EXISTE ORDEN DE COMPRA..
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataGrilla" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CONSULTA_ORDEN_GRID" DeleteCommand="usp_OrdenDelete" DeleteCommandType="StoredProcedure" SelectCommandType="StoredProcedure">
                        <DeleteParameters>
                            <asp:Parameter Name="id_orden" Type="Int64" />
                            <asp:CookieParameter CookieName="IdUsuario" Name="add_usuario" Type="Int32" />
                        </DeleteParameters>
                        <SelectParameters>
                            <asp:ControlParameter ControlID="GridView1" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtFechaInicio" DbType="Date" Name="fecha_inicio" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtFechaFin" DbType="Date" DefaultValue="" Name="fecha_fin" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                    <br />

                    <div class="cuadroTitulo">
                        <asp:Literal ID="Literal1" runat="server" Text="ORDEN DE COMPRA APROBADO POR ADMINISTRACION"></asp:Literal>
                    </div>
                    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_orden" DataSourceID="SqlDataSourceCheck" Width="1035px">
                        <Columns>
                            <asp:TemplateField HeaderText="Visualizar" Visible="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnVisualizar" runat="server" CommandName="Visualizar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/img/estandar/buscar03.png" ToolTip="Visualizar Orden" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" />
                            <asp:BoundField DataField="fecha" HeaderText="fecha" SortExpression="fecha" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="id_sector" HeaderText="id_sector" SortExpression="id_sector" />
                            <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion" />
                            <asp:BoundField DataField="monto" HeaderText="monto" SortExpression="monto" DataFormatString="{0:N0}" />
                            <asp:BoundField DataField="fecha_check" HeaderText="fecha_check" SortExpression="fecha_check" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSourceCheck" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CONSULTA_ORDEN_GRID_CHECK" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="GridView1" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtFechaInicio" DbType="Date" Name="fecha_inicio" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtFechaFin" DbType="Date" Name="fecha_fin" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                    <br />
                    <div class="cuadroTitulo">
                        <asp:Literal ID="Literal5" runat="server" Text="ORDEN DE COMPRA PUESTO COMO PENDIENTE POR GERENCIA"></asp:Literal>
                    </div>
                    <asp:GridView ID="grillaPendiente" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_orden" DataSourceID="SqlDataSourcePendiente" Width="1099px">
                        <Columns>
                            <asp:TemplateField HeaderText="Visualizar" Visible="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnVisualizar" runat="server" CommandName="Visualizar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/img/estandar/buscar03.png" ToolTip="Visualizar Orden" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="fecha" DataFormatString="{0:d}" HeaderText="fecha" SortExpression="fecha">
                                <ItemStyle Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="id_sector" HeaderText="id_sector" SortExpression="id_sector" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="descripcion" HeaderText="Sector" SortExpression="descripcion">
                                <ItemStyle Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="monto" DataFormatString="{0:N0}" HeaderText="monto" SortExpression="monto">
                                <ItemStyle HorizontalAlign="Right" Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="obs_gerencia" HeaderText="obs_gerencia" SortExpression="obs_gerencia" />
                        </Columns>
                        <EmptyDataTemplate>
                            NO EXISTE ORDEN DE COMPRA PUESTO COMO PENDIENTE
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSourcePendiente" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CONSULTA_ORDEN_GRID_PENDIENTE" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="GridView1" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtFechaInicio" DbType="Date" Name="fecha_inicio" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtFechaFin" DbType="Date" Name="fecha_fin" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                    <br />

                    <div class="cuadroTitulo">
                        <asp:Literal ID="Literal3" runat="server" Text="ORDEN DE COMPRA APROBADO POR GERENCIA"></asp:Literal>
                    </div>
                    <asp:GridView ID="GridView3" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_orden" DataSourceID="SqlDataSourceAutorizado" Width="1099px">
                        <Columns>
<%--                            <asp:TemplateField HeaderText="Visualizar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnVisualizar" runat="server" CommandName="Visualizar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/img/estandar/buscar03.png" ToolTip="Visualizar Orden" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="DATOS DE FACTURA" Visible="False">
                                <ItemTemplate>
                                  <asp:HyperLink ID="btnEditar" runat="server" ToolTip="Editar Registro" ImageUrl="~/App_Themes/img/estandar/info09.png" NavigateUrl='<%# Eval("id_orden", "OrdenCompraFactura.aspx?ID={0}")%>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" />
                            <asp:BoundField DataField="fecha" HeaderText="fecha" SortExpression="fecha" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="id_sector" HeaderText="id_sector" SortExpression="id_sector" />
                            <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion" />
                            <asp:BoundField DataField="monto" HeaderText="monto" SortExpression="monto" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="fecha_autorizacion" HeaderText="fecha_autorizacion" SortExpression="fecha_autorizacion" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSourceAutorizado" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CONSULTA_ORDEN_GRID_AUTORIAZADO" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="GridView1" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtFechaInicio" DbType="Date" Name="fecha_inicio" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtFechaFin" DbType="Date" Name="fecha_fin" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <br />
                    <div class="cuadroTitulo">
                        <asp:Literal ID="Literal4" runat="server" Text="ORDEN DE COMPRA RECHAZADO"></asp:Literal>
                    </div>
                    <asp:GridView ID="GridView4" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_orden" DataSourceID="SqlDataSourceRechazado" Width="1099px">
                        <Columns>
                            <asp:TemplateField HeaderText="Visualizar" Visible="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnVisualizar" runat="server" CommandName="Visualizar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/img/estandar/buscar03.png" ToolTip="Visualizar Orden" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="fecha" HeaderText="fecha" SortExpression="fecha" DataFormatString="{0:d}">
                                <ItemStyle HorizontalAlign="Right" Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="id_sector" HeaderText="id_sector" SortExpression="id_sector" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion">
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="monto" HeaderText="monto" SortExpression="monto" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="fecha_rechazado" HeaderText="fecha_rechazado" SortExpression="fecha_rechazado" DataFormatString="{0:d}">
                                <ItemStyle Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="obs_gerencia" HeaderText="obs_gerencia" SortExpression="obs_gerencia" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSourceRechazado" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CONSULTA_ORDEN_GRID_RECHAZADO" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="GridView1" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtFechaInicio" DbType="Date" Name="fecha_inicio" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtFechaFin" DbType="Date" Name="fecha_fin" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <br />
                    <br />
                    <br />
                </div>

            </div>
            <uc2:wucReporte ID="wucReporte1" runat="server" />
            <asp:HiddenField ID="hdfError" runat="server" />
            <asp:HiddenField ID="Usuario" runat="server" />
            <uc1:cuMensajeBox ID="cuMensajeBox" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdEnviarExcel" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

