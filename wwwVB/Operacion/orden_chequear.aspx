﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="orden_chequear.aspx.vb" Inherits="Operacion_orden_chequear" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<%@ Register Src="../control/wucReporte.ascx" TagName="wucReporte" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 68px;
        }

        .auto-style3 {
            height: 19px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
<img alt="" class="style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="auto-style1">
                <tr>
                    <td>
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style2">Sector</td>
                                <td>
                                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSector" DataTextField="descripcion" DataValueField="id_sector" Height="24px" Width="272px">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSector" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CBO_SECTOR" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="GrillaDatos" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_orden,id_sector" DataSourceID="SqlDataSource1" Width="1114px">
                            <Columns>
                                <asp:TemplateField HeaderText="Ver Presupuesto">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="btnPresupuesto" runat="server" ToolTip="Descargar Presupuesto" ImageUrl="~/App_Themes/css/img/standar/descargar.png" NavigateUrl='<%# Eval("id_orden", "VerPresupuesto.aspx?ID={0}")%>'></asp:HyperLink>
                                        <%-- <asp:ImageButton ID="btnPresupuesto" runat="server" CommandName="Adjuntar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/css/img/standar/agregar.png" ToolTip="Adjuntar Presupuesto" />--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Visualizar">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnVisualizar" runat="server" CommandName="Visualizar" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/img/estandar/buscar03.png" ToolTip="Visualizar Orden" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Autorizar">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnAutorizar" runat="server" CommandName="Autorizar" OnClientClick="return confirm('Confirmar que el chequeo Financiero!!!?');" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/img/estandar/actualizar01.png" ToolTip="Pendiente" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" >
                                <ItemStyle Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="fecha" HeaderText="fecha" SortExpression="fecha" DataFormatString="{0:d}" >
                                <ItemStyle Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="id_sector" HeaderText="id_sector" ReadOnly="True" SortExpression="id_sector" >
                                <ItemStyle Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="sector" HeaderText="sector" SortExpression="sector" >
                                <ItemStyle Width="80px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="monto" HeaderText="monto" SortExpression="monto" DataFormatString="{0:N0}">
                                    <ItemStyle HorizontalAlign="Right" Width="70px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="saldo_actual" DataFormatString="{0:N0}" HeaderText="saldo_actual" SortExpression="saldo_actual">
                                <ItemStyle HorizontalAlign="Right" Width="70px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="observacion" HeaderText="observacion" SortExpression="observacion" />
                            </Columns>
                            <EmptyDataTemplate>
                                SELECCIONE OTRO SECTOR.....<br />
                                &nbsp;<br />
                                NO EXISTE ORDEN DE COMPRA PENDIENTE DE CHECK....<br />
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="GRID_CHEQUEAR_ORDEN" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="DropDownList1" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <br />
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style3">
                                    <asp:GridView ID="GrillaDatos0" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_orden,id_sector" DataSourceID="SqlDataSource2" Width="1114px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Visualizar">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnVisualizar0" runat="server" CommandArgument='<%# Eval("id_orden")%>' CommandName="Visualizar" ImageUrl="~/App_Themes/img/estandar/buscar03.png" ToolTip="Visualizar Orden" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cancelar">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnCancelar" runat="server" CommandArgument='<%# Eval("id_orden")%>' CommandName="Cancelar" ImageUrl="~/App_Themes/img/estandar/eliminar02.png" OnClientClick="return confirm('Confirmar desea cancelar chequeo!!!?');" ToolTip="Cancelar" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" />
                                            <asp:BoundField DataField="fecha" DataFormatString="{0:d}" HeaderText="fecha" SortExpression="fecha" />
                                            <asp:BoundField DataField="id_sector" HeaderText="id_sector" ReadOnly="True" SortExpression="id_sector" />
                                            <asp:BoundField DataField="sector" HeaderText="sector" SortExpression="sector" />
                                            <asp:BoundField DataField="observacion" HeaderText="observacion" SortExpression="observacion" />
                                            <asp:BoundField DataField="monto" DataFormatString="{0:N0}" HeaderText="monto" SortExpression="monto">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="fecha_check" HeaderText="fecha_check" SortExpression="fecha_check">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            NO EXISTE ORDEN DE COMPRA CHECK....
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="GRID_CHEQUEAR_ORDEN_CANCELAR" SelectCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="DropDownList1" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                                <td class="auto-style3"></td>
                            </tr>
                            <tr>
                                <td>
                                    <uc2:wucReporte ID="wucReporte1" runat="server" />
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:cuMensajeBox ID="cuMensajeBox1" runat="server" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

