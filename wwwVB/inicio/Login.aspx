﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="inicio_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title></title>
    <link href="../App_Themes/Login/bootstrap.css" rel="stylesheet" />
    <link href="../App_Themes/Login/metisMenu.css" rel="stylesheet" />
    <link href="../App_Themes/Login/sb-admin-2.css" rel="stylesheet" />
    <link href="../App_Themes/Login/morris.css" rel="stylesheet" />
    <link href="../App_Themes/Login/font-awesome.min.css" rel="stylesheet" />
<%--    <link href="../App_Themes/sb-admin/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../App_Themes/sb-admin/vendor/metisMenu/metisMenu.css" rel="stylesheet" />
    <link href="../App_Themes/sb-admin/dist/css/sb-admin-2.css" rel="stylesheet" />
    <link href="../App_Themes/sb-admin/vendor/morrisjs/morris.css" rel="stylesheet" />
    <link href="../App_Themes/sb-admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" />--%>
</head>
<body>
    <form role="form" runat="server">


        <div class="container">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div style="margin: auto; width: 100px; height: 120px;">
                            <img src="../App_Themes/Imagen/logo.png" height="120" style="align-content: center;" />
                        </div>
                        <div class="alert alert-dismissible alert-info" style="text-align: center;">
                            <h3 class="panel-title">ORDEN DE COMPRA 2019</h3>
                            <h3 class="panel-title">Ingreso al Sistema </h3>
                        </div>
                        <%--                        <div class="panel-heading" style="text-align: center;">
                            <h3 class="panel-title">Ingreso al Sistema</h3>
                        </div>--%>
                        <div class="panel-body">
                            <fieldset>
                                <div class="form-group">
                                    <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" placeholder="Usuario"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" placeholder="Password" TextMode="Password"></asp:TextBox>
                                </div>
                               <%-- <div class="checkbox">
                                    <label>
                                        <asp:CheckBox ID="chkGuardarPass" runat="server" Text="Recordar Password" />
                                        <%--<input name="remember" type="checkbox" value="Remember Me">Remember Me         
                                    </label>
                                </div>--%>
                                <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn btn-lg btn-success btn-block" />
                                <div style="margin-top:10px;">
                                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
