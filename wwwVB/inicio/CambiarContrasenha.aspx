﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="CambiarContrasenha.aspx.vb" Inherits="inicio_CambiarContrasenha" %>

<%@ Register src="../control/cuMensajeBox.ascx" tagname="cuMensajeBox" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="auto-style1" src="../App_Themes/Imagen/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="block">
                    <div class="block-content collapse in">
                        <div class="container-fluid">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">Login  : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtLogin" runat="server" Enabled="false" MaxLength="10" CssClass="input-group disabled"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Contraseña Actual : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtPassAnterior" runat="server" MaxLength="20" CssClass="input-group" TextMode="Password"  placeholder="PASSWORD ACTUAL"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPassw" ErrorMessage="*" ></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Nueva Contraseña : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtPassw" runat="server" MaxLength="20" placeholder="NUEVO PASSWORD" CssClass="input-group" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassw" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Repetir Contraseña : </label>
                                    <div class="controls">    
                                        <asp:TextBox ID="txtPassw2" runat="server" MaxLength="20"  placeholder="REPETIR NUEVO PASSWORD" CssClass="input-group" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPassw" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassw" ControlToValidate="txtPassw2" ErrorMessage="No coincide la contraseña"></asp:CompareValidator>
                                        
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <asp:Button ID="cmdGuardar" runat="server" Text="Guardar" class="btn btn-primary" />
                                    <asp:Button ID="cmdCancelar" runat="server" Text="Cancelar" CausesValidation="False" class="btn" PostBackUrl="~/Default.aspx" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    
                    <uc1:cuMensajeBox ID="cuMensajeBox" runat="server" />
                    
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

