﻿
Imports CapaMedio.DAO

Partial Class inicio_CambiarContrasenha
    Inherits PaginaBase

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Try
            Dim dao As DAO_USUARIO = New DAO_USUARIO()
            dao.Cambiar_Password(Convert.ToInt16(Request.Cookies("idUsuario").Value), txtPassAnterior.Text, txtPassw.Text)
            Response.Redirect(cmdCancelar.PostBackUrl.ToString)

        Catch ex As Exception
            MensajeError(ex.Message())
        End Try
    End Sub

    'Private Sub inicio_CambiarContrasenha_Load(sender As Object, e As EventArgs) Handles Me.Load
    '    Dim dao As DAO_USUARIO = New DAO_USUARIO()
    '    If Page.IsPostBack = False Then
    '        Cargar_Pagina()
    '        'If sMens = "" Then
    '        '    Dim cooBuscar As HttpCookie = Request.Cookies("Buscar")
    '        '    If Not cooBuscar Is Nothing Then
    '        '        txtBuscar.Text = cooBuscar.Value.ToString
    '        '        Response.Cookies("Buscar").Value = ""
    '        '    End If
    '        '    Usuario.Value = Request.Cookies("idUsuario").Value
    '        '    Response.Cookies("PaginaAnterior").Value = "~" + sPagina
    '        '    CType(Master.FindControl("lblTitulo"), Label).Text = "MANTENIMIENTO DE USUARIO"
    '        'Else
    '        '    Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
    '        'End If
    '        txtLogin.Text = "sdsdsds"
    '        txtPassw.Text = "dsdsdsd"
    '    End If
    'End Sub
    Private Sub Cargar_Pagina()
        Dim dao As DAO_USUARIO = New DAO_USUARIO()
        Dim us = dao.getUsuario(Convert.ToInt16(Request.Cookies("idUsuario").Value))
        txtLogin.Text = us.log_usuario

    End Sub
    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox.tipoMensaje = 2
        cuMensajeBox.Mensaje = sMensaje
        cuMensajeBox.show()
    End Sub

    Private Sub inicio_CambiarContrasenha_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Cargar_Pagina()
            CType(Master.FindControl("lblTitulo"), Label).Text = "CAMBIAR CONTRASEÑA"

            'txtLogin.Text = "sdsdsds"
            'txtPassw.Text = "dsdsdsd"
            'txtPassw2.Text = "dsdsdsd"

        End If
    End Sub
End Class
