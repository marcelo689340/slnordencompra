﻿
Imports CapaMedio.DAO

Partial Class inicio_Login
    Inherits System.Web.UI.Page


    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        lblMensaje.Text = ""
        'Dim dao As DAO_USUARIO = New DAO_USUARIO()
        'Dim usu As Usuario
        'Try
        '    usu = dao.Validar_Ingreso(txtUsuario.Text, txtPassword.Text)
        '    FormsAuthentication.RedirectFromLoginPage(txtUsuario.Text, False)
        'Catch ex As Exception
        '    lblMensaje.Text = ex.Message.ToString()
        'End Try
        Dim dao As DAO_USUARIO = New DAO_USUARIO()
        Dim id_user As Integer = 0
        Try
            id_user = dao.Validar_Usuario(txtUsuario.Text, txtPassword.Text)
            If id_user >= 0 Then
                FormsAuthentication.RedirectFromLoginPage(txtUsuario.Text, False)
                Session("usuario") = id_user.ToString()
                Response.Cookies("idUsuario").Value = id_user.ToString()

            End If
        Catch ex As Exception
            lblMensaje.Text = ex.Message.ToString()
        End Try




    End Sub
End Class
