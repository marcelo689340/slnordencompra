﻿Imports CapaMedio.DAO
Partial Class MpInicio
    Inherits System.Web.UI.MasterPage
    'Private db As DataSkintecDataContext = cConexion.getConnectionDirecta
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If (Page.IsPostBack = False) Then
            lblSistemaVersion.Text = ConfigurationManager.AppSettings("Version").ToString
            Response.Cookies("PaginaAnterior").Value = "~/Default.aspx"
        End If
    End Sub

    Protected Sub cmdEntrar_Click(sender As Object, e As EventArgs) Handles cmdEntrar.Click
        Dim dao As DAO_USUARIO = New DAO_USUARIO()
        Dim id_user As Integer = 0
        Try
            id_user = dao.Validar_Usuario(txtLogin.Text, txtPassword.Text)
            If id_user <> 0 Then
                FormsAuthentication.RedirectFromLoginPage(txtLogin.Text, False)
                Response.Cookies("idUsuario").Value = id_user.ToString()

            End If
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            cuMensajeBox.tipoMensaje = 2
            cuMensajeBox.Mensaje = sMensaje
            cuMensajeBox.show()
        End Try

    End Sub

End Class

