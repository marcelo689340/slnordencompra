﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports System.Data.SqlClient
Imports System.Drawing
Imports System.IO

Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Imports CapaMedio.DAO

Partial Class MuestraImagenDocumentoPresupuesto
    Inherits System.Web.UI.Page


    Private imagen As Byte()
    Private ImagenS As New MemoryStream
    Private Width As Integer
    Private Height As Integer
    Private shadowSize As Integer
    Private Bitmap As System.Drawing.Bitmap
    Private ImgFormat As System.Drawing.Imaging.ImageFormat
    Private Img As System.Drawing.Image
    Private baseMap As Bitmap
    Private top As Integer
    Private left As Integer

    Sub CargarImagen()

        ImgFormat = ImageFormat.Jpeg : Response.ContentType = "image/jpeg"  ' Default=jpeg '


        Dim id_orden As Long = Request("id")
        Dim item As Integer = Request("item")
        'Dim dao As DAO_ORDEN = New DAO_ORDEN()
        'Dim pre = dao.Presupuesto(id_orden, item)
        'Session("imagen") = pre.presupuesto


        'Recuperamos el paramento con el id de imagen
        Dim codImagen As String = Request("id")
        Dim strcnn As String = ConfigurationManager.ConnectionStrings("bd_Orden").ConnectionString
        Dim cnn As New SqlConnection(strcnn)

        Dim cmd As String = "Select presupuesto From viewPresupuesto where id_orden=@id and item = @item"

        Dim comm As New SqlCommand(cmd, cnn)
        comm.Parameters.Add(New SqlParameter("@id", SqlDbType.Decimal)).Value = id_orden
        comm.Parameters.Add(New SqlParameter("@item", SqlDbType.Int)).Value = item
        Try
            'Dim dao As DAO_ORDEN = New DAO_ORDEN()
            ' Dim pre = dao.Presupuesto(id_orden, item)
            cnn.Open()

            Try
                ' Recuperamos la imagen de la Base de datos
                'imagen = pre.presupuesto.ToArray()

                'Recuperamos la imagen de la Base de datos
                imagen = DirectCast(comm.ExecuteScalar(), Byte())

                Dim imageStream As New MemoryStream(imagen)
                ImagenS = imageStream

                'determina si el archivo es imagen o pdf
                Dim sinsql As String
                Dim obj As New ObtieneDataTable
                Dim dt As New DataTable

                sinsql = "Select archivo_nombre From viewPresupuesto where id_orden= " & id_orden & " and item = " & item
                dt = obj.DevuelveDatos(sinsql, "bd_Orden")


                'If (pre.archivo_nombre.ToString Like "*.pdf*") Then
                '    Dim conv As New System.Text.ASCIIEncoding

                '    Response.Buffer = True
                '    Response.ContentType = "application/pdf"
                '    Response.BinaryWrite(imagen)
                '    Exit Sub
                'End If

                If dt.Rows.Count > 0 And Not IsNothing(dt) Then
                    If dt.Rows(0)(0).ToString Like "*.pdf*" Then
                        Dim conv As New System.Text.ASCIIEncoding

                        Response.Buffer = True
                        Response.ContentType = "application/pdf"
                        Response.BinaryWrite(imagen)
                        Response.Flush()
                        Response.End()
                        Exit Sub
                    End If

                End If


                'Mostramos la imagen en la página directamente
                CreateGraphic()
                baseMap.Save(Response.OutputStream, ImageFormat.Jpeg)
                baseMap.Dispose()

            Catch
                Response.Clear()

                Response.Write("Error: No existe la imagen")

            End Try
        Catch err As SqlException
            Response.Clear()

            Response.Write("Error:" & err.Message.ToString())
        Finally


            cnn.Close()
        End Try


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarImagen()
    End Sub


    Sub CreateGraphic()

        Dim sTexto As String
        Dim i As Integer
        Dim iBAnd As Integer
        iBAnd = 0
        Img = System.Drawing.Image.FromStream(ImagenS)

        Dim iStep As Integer
        Dim iFuente As Integer
        Dim letterBrush As SolidBrush = New SolidBrush(Color.FromArgb(50, 255, 255, 255))
        Dim shadowBrush As SolidBrush = New SolidBrush(Color.FromArgb(50, 0, 0, 0))


        Width = Img.Width
        Height = Img.Height
        iFuente = (Height / 100) * 5
        iStep = Height / 6
        Dim fontTitle As Font = New Font("tahoma", iFuente, FontStyle.Bold)
        baseMap = New Bitmap(Width, Height)
        Dim myGraphic As Graphics = Graphics.FromImage(baseMap)
        With myGraphic
            .DrawImage(Img, 0, 0, Width, Height)
            .SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias
            For i = 0 To Height Step iStep
                Select Case iBAnd
                    Case 0
                        sTexto = "Uso  RRHH - CTBM "
                    Case 1
                        sTexto = HttpContext.Current.User.Identity.Name.ToString + "    " + HttpContext.Current.User.Identity.Name.ToString + "    " + HttpContext.Current.User.Identity.Name.ToString
                    Case 2
                        sTexto = "      " + HttpContext.Current.User.Identity.Name.ToString + "    " + HttpContext.Current.User.Identity.Name.ToString + "    " + HttpContext.Current.User.Identity.Name.ToString
                    Case 3
                        sTexto = " CTBM     " + HttpContext.Current.User.Identity.Name.ToString + "    " + HttpContext.Current.User.Identity.Name.ToString + "    " + HttpContext.Current.User.Identity.Name.ToString
                    Case Else
                        sTexto = "    RRHH - CTBM " + "    " + HttpContext.Current.User.Identity.Name.ToString + "    " + HttpContext.Current.User.Identity.Name.ToString
                End Select

                .DrawString(sTexto, fontTitle, shadowBrush, 5, i)
                .DrawString(sTexto, fontTitle, letterBrush, 7, i + 2)

                iBAnd = iBAnd + 1
            Next
        End With
        Img.Dispose()

    End Sub
End Class

