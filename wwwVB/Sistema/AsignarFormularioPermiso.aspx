﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.master" Culture="Auto" UICulture="Auto" AutoEventWireup="false" CodeFile="AsignarFormularioPermiso.aspx.vb" Inherits="Sistema_AsignarFormularioPermiso" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style4 {
            width: 121px;
        }

        .auto-style6 {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="block">
                    <div class="container-fluid" style="margin-top: 10px !important">
                        <div class="container-fluid">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label btn-block">PERMISO :</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtDescripcion" runat="server" Enabled="false" CssClass="input-group"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">ASIGNAR PERMISOS  </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label btn-block">MODULO :</label>
                            <div class="controls">
                                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataModulo" DataTextField="Descripcion" DataValueField="MenuId" Height="22px" Width="294px" AutoPostBack="True" CausesValidation="True">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataModulo" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT [MenuId]
      ,[Descripcion]
      ,[posicion]
  FROM [vwMenu]
  order by  posicion,  [MenuId] asc"></asp:SqlDataSource>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <asp:Button ID="cmdVolver" runat="server" Text="Volver" CssClass="btn" CausesValidation="False" TabIndex="10" />
                        <asp:Button ID="cmdGuardar" runat="server" CausesValidation="False" CssClass="btn btn-primary" TabIndex="10" Text="Guardar" Visible="False" />
                    </div>
                    <div class="control-group">
                        <asp:GridView ID="grvDatos" runat="server" AutoGenerateColumns="False" Caption="" CaptionAlign="Left">
                            <Columns>
                                <asp:BoundField DataField="MenuId" HeaderText="MenuId" SortExpression="MenuId" ReadOnly="True"></asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion" ReadOnly="True"></asp:BoundField>
                                <asp:TemplateField HeaderText="Consultar" SortExpression="Consultar">
                                    <EditItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Consultar") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkConsultar" runat="server" Checked='<%# Bind("Consultar") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Insertar" SortExpression="Insertar">
                                    <EditItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("Insertar") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkInsertar" runat="server" Checked='<%# Bind("Insertar") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Eliminar" SortExpression="Eliminar">
                                    <EditItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("Eliminar") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkEliminar" runat="server" Checked='<%# Bind("Eliminar") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Modificar" SortExpression="Modificar">
                                    <EditItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("Modificar") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkModificar" runat="server" Checked='<%# Bind("Modificar") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <br />
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:HiddenField ID="hdfError" runat="server" />
                        <asp:HiddenField ID="Usuario" runat="server" />
                        <uc1:cuMensajeBox ID="cuMensajeBox" runat="server" />
                    </div>
                </div>
            </div>




            <%--<asp:Panel ID="pnlBusquedas" runat="server" DefaultButton="cmdBuscar" CssClass="conPadding05">--%>


            <%--      </asp:Panel>--%>
            <div style="padding-top: 5px; padding-bottom: 5px">
                <asp:Panel ID="pnlGrilla" runat="server" Visible="true" Width="500px">
                    <div>
                        <table class="auto-style1">
                            <tr>
                                <%--                                <asp:Panel ID="pntOpcionGrilla" runat="server" Height="16px">
                                    <asp:Literal ID="ltlListado" runat="server" Text="Listado de Usuarios"></asp:Literal>
                                    <asp:Literal ID="ltlInformacion" runat="server"></asp:Literal>
                                    <asp:RequiredFieldValidator ID="rfvCantFila" runat="server" ControlToValidate="txtCantFila" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtCantFila" runat="server" Height="16px" Width="20px"></asp:TextBox>
                                    <asp:Button ID="Button1" runat="server" Height="24px" Text="Button" Width="31px" />
                                </asp:Panel>--%>
                            </tr>
                            <tr>
                                <td class="auto-style4">

                                    <br />
                                    <br />
                                </td>

                            </tr>
                        </table>
                    </div>

                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="cmdEnviarExcel" />--%>
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

