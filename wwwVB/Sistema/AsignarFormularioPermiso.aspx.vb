﻿
Imports System.Data
Imports CapaMedio
Imports CapaMedio.DAO

Partial Class Sistema_AsignarFormularioPermiso : Inherits PaginaBase
    'Inherits System.Web.UI.Page
    ''Inherits PaginaBase
    Dim dt As DataTable

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If
            'If Not cooBuscar Is Nothing Then
            '    'txtBuscar.Text = cooBuscar.Value.ToString
            '    Cargar_Grilla()
            'End If
            Usuario.Value = Request.Cookies("idUsuario").Value
            Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            CType(Master.FindControl("lblTitulo"), Label).Text = "PERMISO - ASIGNAR PERMISO"
            Dim lqs = (From C In db._Permiso Where C.id_permiso = Request.QueryString("ID") Select C).Single
            txtDescripcion.Text = lqs.descripcion.ToString
        End If
    End Sub

    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox.tipoMensaje = 2
        cuMensajeBox.Mensaje = sMensaje
        cuMensajeBox.show()
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
        Try
            Dim lqs = db.CONSULTAR_DETALLE_PERMISO(Request.QueryString("ID"), DropDownList1.SelectedValue.ToString)
            grvDatos.DataSource = lqs.ToList()
        Catch ex As Exception
            grvDatos.DataSource = Nothing
        End Try
        grvDatos.DataBind()
    End Sub

    Protected Sub grvDatos_DataBound(sender As Object, e As EventArgs) Handles grvDatos.DataBound
        If grvDatos.Rows.Count > 0 Then
            cmdGuardar.Visible = True
        Else
            cmdGuardar.Visible = False
        End If
    End Sub

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Dim oItem As _Permiso_Detalle = New _Permiso_Detalle()
        Dim lst As List(Of _Permiso_Detalle) = New List(Of _Permiso_Detalle)()
        Dim bConsultar As Boolean = False
        Dim chkConsultar As CheckBox
        Dim chkInsertar As CheckBox
        Dim chkEliminar As CheckBox
        Dim ChkModificar As CheckBox
        For Each row As GridViewRow In grvDatos.Rows
            chkConsultar = CType(row.FindControl("chkConsultar"), CheckBox)
            chkInsertar = CType(row.FindControl("chkInsertar"), CheckBox)
            chkEliminar = CType(row.FindControl("chkEliminar"), CheckBox)
            ChkModificar = CType(row.FindControl("ChkModificar"), CheckBox)
            If chkConsultar.Checked = False And (chkEliminar.Checked = True Or ChkModificar.Checked = True Or chkInsertar.Checked = True) Then
                chkConsultar.Checked = True
            End If
            oItem.id_permiso = Convert.ToInt16(Request.QueryString("ID"))
            oItem.MenuId = Convert.ToInt16(row.Cells(0).Text)
            oItem.Consultar = chkConsultar.Checked
            oItem.Insertar = chkInsertar.Checked
            oItem.Modificar = ChkModificar.Checked
            oItem.Eliminar = chkEliminar.Checked
            oItem.add_usuario = Convert.ToInt16(Request.Cookies("idUsuario").Value)
            lst.Add(oItem)
            oItem = New _Permiso_Detalle()
        Next
        Dim dao As DAO__PERMISO_DETALLE = New DAO__PERMISO_DETALLE()
        Try
            dao.Actualizar(lst)
            MensajeError("Datos Guardado con exito")
        Catch ex As Exception
            MensajeError(ex.Message)
        End Try
    End Sub
End Class
