﻿Imports CapaMedio
Imports CapaMedio.DAO

Partial Class Sistema_Sector_Afecta
    Inherits PaginaBase

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Dim oItem As Sector_Compra
        Dim lst As List(Of Sector_Compra) = New List(Of Sector_Compra)
        Dim chkSeleccion As CheckBox
        For Each row As GridViewRow In GridView1.Rows
            oItem = New Sector_Compra()
            chkSeleccion = CType(row.FindControl("chkSeleccionar"), CheckBox)
            oItem.id_sector = Request.QueryString("ID")
            oItem.id_sector_compra = row.Cells(0).Text
            oItem.estado = chkSeleccion.Checked ' IIf(chkSeleccion.Checked = True, True, False)
            lst.Add(oItem)
        Next
        Dim dao As DAO_SECTOR = New DAO_SECTOR()
        Try
            dao.Actualizar_Sector_Compra(lst)
            MensajeError("Datos Guardado con exito")
        Catch ex As Exception
            MensajeError(ex.Message)
        End Try


    End Sub
    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox1.tipoMensaje = 2
        cuMensajeBox1.Mensaje = sMensaje
        cuMensajeBox1.show()
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If
            CType(Master.FindControl("lblTitulo"), Label).Text = "SELECCIONAR SECTOR AL QUE PUEDE REALIZARLE COMPRAS"


            Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            'If Request.QueryString("ID") <> "0" Then
            '    Datos_bd()
            '    CType(Master.FindControl("lblTitulo"), Label).Text = "MODIFICAR ORDEN DE COMPRA : " + Request.QueryString("ID").ToString()
            '    'Cargar_Datos(Convert.ToInt16(Request.QueryString("ID")))
            'Else
            '    cmdGuardar.Visible = False
            '    CType(Master.FindControl("lblTitulo"), Label).Text = "NUEVA ORDEN DE COMPRA"
            'End If
        End If

    End Sub

    'Protected Sub cuMensajeBox1_DespuesdeMostrar(sender As Object, e As EventArgs) Handles cuMensajeBox1.DespuesdeMostrar
    '    Response.Redirect(cmdVolver.PostBackUrl)
    'End Sub
End Class
