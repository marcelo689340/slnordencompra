﻿<%@ Page Title="CAMBIO DE PASSWORD" Language="VB" MasterPageFile="~/MpPrincipal.master" AutoEventWireup="false" CodeFile="CambiarPassword.aspx.vb" Inherits="CambiarPassword" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>


<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 128px;
            height: 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="style1" 
            <img src="Imagen/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="FrameCarga" style="padding: 10px;">
                <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
                DataSourceID="SqlDataSource1" Height="76px" Width="486px" 
                DefaultMode="Edit">
                <Fields>
                    <asp:BoundField DataField="log_usuario" HeaderText="Usuario" ReadOnly="True" 
                        SortExpression="Usuario" >
                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Password" SortExpression="Password">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("contrasenha") %>' 
                                MaxLength="15" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="TextBox1" ErrorMessage="*"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                ControlToCompare="TextBox1" ControlToValidate="TextBox2" 
                                ErrorMessage="No coincide la contraseña"></asp:CompareValidator>
                            <br />
                            <asp:TextBox ID="TextBox2" runat="server" MaxLength="15" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ControlToValidate="TextBox2" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("contrasenha")%>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle VerticalAlign="Top" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:Button ID="Button1" runat="server" CausesValidation="True" 
                                CommandName="Update"  Text="Actualizar" CssClass="boton" />
                            &nbsp;<asp:Button ID="Button2" runat="server" CausesValidation="False" 
                                CommandName="Cancel" Text="Cancelar" CssClass="boton" PostBackUrl="~/Default.aspx" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Button ID="Button1" runat="server" CausesValidation="False" 
                                CommandName="Edit" Text="Editar" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>
                <HeaderTemplate>
                    CAMBIAR CONTRASEÑA
                </HeaderTemplate>
            </asp:DetailsView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT log_usuario
      ,contrasenha
  FROM Usuario
  where id_usuario = @id_usuario" UpdateCommand="update Usuario set contrasenha = @contrasenha where id_usuario = @id_usuario">
                <SelectParameters>
                    <asp:CookieParameter CookieName="idUsuario" Name="id_usuario" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="contrasenha" Type="String" />
                    <asp:CookieParameter CookieName="idUsuario" Name="id_usuario" />
                </UpdateParameters>
            </asp:SqlDataSource>
            </div>
            <asp:HiddenField ID="HiddenField1" runat="server" />
<br />
<br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

