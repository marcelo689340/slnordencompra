﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.master" Culture="Auto" UICulture="Auto" AutoEventWireup="false" CodeFile="ABMPermiso.aspx.vb" Inherits="Sistema_ABMPermiso" %>

<%@ Register Src="~/control/cuMensajeBox.ascx" TagPrefix="uc1" TagName="cuMensajeBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img src="../App_Themes/css/img/standar/ajax-loader-barra.gif" alt="" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="block">
                    <div class="block-content collapse in">
                        <div class="container-fluid">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">Codigo : </label>
                                    <div class="controls">
                                        <asp:Label ID="lblCodigo" runat="server" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Descripcion : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtDescripcion" runat="server" TabIndex="2" MaxLength="50" CssClass="input-group"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDescripcion" runat="server" ControlToValidate="txtDescripcion" ErrorMessage="*" CssClass="validador_error"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Activo : </label>
                                    <div class="controls">
                                        <asp:CheckBox ID="chkActivo" runat="server" TabIndex="7" />
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <asp:Button ID="cmdGuardar" runat="server" Text="Guardar" TabIndex="9" CssClass="btn btn-primary" />
                                    <asp:Button ID="cmdVolver" runat="server" CausesValidation="False" CssClass="btn" Text="Volver" TabIndex="10" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <uc1:cuMensajeBox runat="server" ID="cuMensajeBox" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

