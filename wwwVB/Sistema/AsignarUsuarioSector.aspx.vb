﻿Imports CapaMedio
Imports CapaMedio.DAO
Partial Class Sistema_AsignarUsuarioSector
    Inherits PaginaBase

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If
            'If Not cooBuscar Is Nothing Then
            '    'txtBuscar.Text = cooBuscar.Value.ToString
            '    Cargar_Grilla()
            'End If
            Usuario.Value = Request.Cookies("idUsuario").Value
            Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            CType(Master.FindControl("lblTitulo"), Label).Text = "USUARIO - ASIGNAR SECTOR"
            Dim lqs = (From C In db.Usuario Where C.id_usuario = Request.QueryString("ID") Select C).Single
            txtDescripcion.Text = lqs.log_usuario.ToString

            Try
                Dim lqs2 = db.CONSULTAR_USUARIO_SECTOR(Convert.ToInt16(Request.QueryString("ID")))
                grvDatos.DataSource = lqs2.ToList()
            Catch ex As Exception
                grvDatos.DataSource = Nothing
            End Try
            grvDatos.DataBind()
        End If

    End Sub
    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox.tipoMensaje = 2
        cuMensajeBox.Mensaje = sMensaje
        cuMensajeBox.show()
    End Sub
    Protected Sub grvDatos_DataBound(sender As Object, e As EventArgs) Handles grvDatos.DataBound
        If grvDatos.Rows.Count > 0 Then
            cmdGuardar.Visible = True
        Else
            cmdGuardar.Visible = False
        End If
    End Sub
    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click

        Dim oItem As Sector_Usuario = New Sector_Usuario()
        Dim ChkOk As CheckBox



        Dim lst As List(Of Sector_Usuario) = New List(Of Sector_Usuario)()


        For Each row As GridViewRow In grvDatos.Rows
            ChkOk = CType(row.FindControl("ChkOk"), CheckBox)
            oItem.id_usuario = Convert.ToInt16(Request.QueryString("ID"))
            oItem.id_sector = Convert.ToInt16(row.Cells(0).Text)
            oItem.estado = ChkOk.Checked
            oItem.add_usuario = Convert.ToInt16(Request.Cookies("idUsuario").Value)

            lst.Add(oItem)
            oItem = New Sector_Usuario()
            'If ChkOk.Checked = True Then
            'Dim lqs = (From A In db.Sector_Usuario Where A.id_sector = oItem.id_sector And A.id_usuario = oItem.id_usuario Select A).SingleOrDefault()
            'If (lqs Is Nothing) Then
            '    dao.Insertar(oItem)
            'Else
            '    dao.Actualizar(oItem)
            'End If
        Next

        Try
            Dim dao As DAO_SECTOR_USUARIO = New DAO_SECTOR_USUARIO()
            DAO.Actualizar(lst)
            MensajeError("Datos Guardado con exito")
        Catch ex As Exception
            MensajeError(ex.Message)
        End Try
        Response.Redirect(cmdVolver.PostBackUrl.ToString)

    End Sub
End Class
