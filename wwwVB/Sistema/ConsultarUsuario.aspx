﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.master" Culture="Auto" UICulture="Auto" AutoEventWireup="false" CodeFile="ConsultarUsuario.aspx.vb" Inherits="Sistema_ConsultarUsuario" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="container-fluid">
                    <div class="form-horizontal" style="margin-top: 15px !important;">
                        <div class="control-group">
                            <label class="control-label">Usuario :</label>
                            <%--<asp:Label ID="Label3" runat="server" Text="Usuario    : " CssClass="control-label col-md-1 col-lg-1 col-xs-1"></asp:Label>--%>
                            <div class="controls">
                                <asp:TextBox ID="txtBuscar" runat="server" CssClass="input-group"></asp:TextBox>
                                <asp:Button ID="cmdBuscar" runat="server" Text="Buscar" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                    <div class="control-group form-actions">
                        <div class="controls">
                            <asp:Button ID="cmdNuevo" runat="server" Text="Nuevo" CssClass="btn btn-primary" PostBackUrl="~/Sistema/abm_usuario.aspx?ID=0" />
                            <asp:Button ID="cmdEnviarExcel" runat="server" Text="Enviar Excel" CssClass="btn btn-primary" Visible="False" />
                            <asp:Button ID="cmdEnviarPdf" runat="server" Text="Enviar PDF" CssClass="btn btn-primary" Visible="False" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="col-md-12">

                        <asp:GridView ID="grvDatos" runat="server" AutoGenerateColumns="False" DataKeyNames="id_usuario" AllowPaging="True" DataSourceID="SqlDataGrilla" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="btnEditar" runat="server" ToolTip="Editar Registro" ImageUrl="~/App_Themes/css/img/standar/update.png" NavigateUrl='<%# Eval("id_usuario", "abm_usuario.aspx?ID={0}")%>' Text=""></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminr" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument='<%# Eval("id_usuario")%>' ImageUrl="~/App_Themes/css/img/standar/recicle.png" Text="Eliminar" ToolTip="Elimiar Registro" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="id_usuario" SortExpression="id_usuario">
                                    <EditItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("id_usuario") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("id_usuario") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="log_usuario" SortExpression="log_usuario">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("log_usuario") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("log_usuario") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="nombre" SortExpression="nombre">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("nombre") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("nombre") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="apellido" SortExpression="apellido">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("apellido") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("apellido") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="email" SortExpression="email">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("email") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="celular" SortExpression="celular">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("celular") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label7" runat="server" Text='<%# Bind("celular") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ACTIVO" SortExpression="estado">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("estado") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("estado") %>' Enabled="False" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="fecha_utl_mod" SortExpression="fecha_utl_mod" HeaderText="fecha_utl_mod"></asp:BoundField>
                                <asp:TemplateField HeaderText="ASIGNAR SECTOR">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="btnAsignarSector" runat="server" ToolTip="Asignar Sector" ImageUrl="~/App_Themes/css/img/standar/deposito.png" NavigateUrl='<%# Eval("id_usuario", "AsignarUsuarioSector.aspx?ID={0}")%>' Text=""></asp:HyperLink>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ASIGNAR PERMISO">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="btnAsignarPermiso" runat="server" ToolTip="Asignar Permiso" ImageUrl="~/App_Themes/css/img/standar/permisos.png" NavigateUrl='<%# Eval("id_usuario", "AsignarPermisoUsuario.aspx?ID={0}")%>' Text=""></asp:HyperLink>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="add_usuario" HeaderText="add_usuario" SortExpression="add_usuario" />--%>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataGrilla" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT id_usuario
      ,log_usuario
      ,contrasenha
      ,nombre
      ,apellido
      ,email
      ,celular
      ,estado
      ,fecha_utl_mod
      ,add_usuario
  FROM bd_Orden.dbo.Usuario
  where id_usuario like @filtro
      or log_usuario like '%' + @filtro + '%' 
      or nombre like '%' + @filtro + '%'  
      or apellido like '%' + @filtro + '%'  "
                            DeleteCommand="usp_UsuarioDelete" DeleteCommandType="StoredProcedure">
                            <DeleteParameters>
                                <asp:Parameter Name="id_usuario" Type="Int32" />
                                <asp:ControlParameter ControlID="Usuario" Name="add_usuario" PropertyName="Value" Type="Int32" DefaultValue="0" />
                            </DeleteParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="txtBuscar" Name="filtro" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:HiddenField ID="hdfError" runat="server" />
                        <asp:HiddenField ID="Usuario" runat="server" />
                        <uc1:cuMensajeBox ID="cuMensajeBox" runat="server" />

                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdEnviarExcel" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

