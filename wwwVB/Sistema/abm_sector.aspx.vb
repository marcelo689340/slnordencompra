﻿Imports CapaMedio.DAO
Imports CapaMedio
Partial Class Sistema_abm_sector
    Inherits PaginaBase

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Dim lqs As Sector = New Sector()
        lqs.id_sector = Convert.ToInt16(Request.QueryString("ID"))
        lqs.descripcion = txtDescripcion.Text
        lqs.Limite = txtLimite.Text
        lqs.estado = chbActivo.Checked
        lqs.add_usuario = Request.Cookies("idUsuario").Value
        Try
            Dim dao As DAO_SECTOR = New DAO_SECTOR()
            If Request.QueryString("ID") <> "0" Then
                dao.Actualizar(lqs)
            Else
                dao.Insertar(lqs)
            End If
            Response.Redirect(cmdCancelar.PostBackUrl.ToString)
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If

            Me.cmdCancelar.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            If Val(Request.QueryString("ID")) <> "0" Then
                '   CType(Master.FindControl("lblTitulo"), Label).Text = "MODIFICAR SECTOR CODIGO : " + Request.QueryString("ID").ToString()
                CType(Master.FindControl("lblTitulo"), Label).Text = "MODIFICAR SECTOR"
                Cargar_Datos(Convert.ToInt16(Request.QueryString("ID")))
            Else
                CType(Master.FindControl("lblTitulo"), Label).Text = "NUEVO SECTOR"
            End If
        End If
    End Sub

    Private Sub Cargar_Datos(ByVal id As Integer)
        Dim dao As DAO_SECTOR = New DAO_SECTOR()
        Dim lqs = dao.setSector(id)
        txtDescripcion.Text = lqs.descripcion
        txtLimite.Text = IIf(lqs.Limite Is Nothing, 0, lqs.Limite)
        chbActivo.Checked = lqs.estado

    End Sub

End Class
