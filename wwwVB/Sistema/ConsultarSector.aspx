﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.master" Culture="Auto" UICulture="Auto" AutoEventWireup="false" CodeFile="ConsultarSector.aspx.vb" Inherits="Sistema_ConsultarSector" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 61px;
        }

        .auto-style4 {
            width: 121px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="container-fluid">
                    <div class="form-horizontal" style="margin-top: 15px !important;">
                        <div class="control-group">
                            <label class="control-label">Sector :</label>
                            <%--<asp:Label ID="Label3" runat="server" Text="Usuario    : " CssClass="control-label col-md-1 col-lg-1 col-xs-1"></asp:Label>--%>
                            <div class="controls">
                                <asp:TextBox ID="txtBuscar" runat="server" CssClass="input-group"></asp:TextBox>
                                <asp:Button ID="cmdBuscar" runat="server" Text="Buscar" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                    <div class="control-group form-actions">
                        <div class="controls">
                            <asp:Button ID="cmdNuevo" runat="server" Text="Nuevo" CssClass="btn btn-primary" PostBackUrl="~/Sistema/abm_sector.aspx?ID=0" />
                            <asp:Button ID="cmdEnviarExcel" runat="server" Text="Enviar Excel" CssClass="btn btn-primary" Visible="False" />
                            <asp:Button ID="cmdEnviarPdf" runat="server" Text="Enviar PDF" CssClass="btn btn-primary" Visible="False" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="col-md-12">
                        <asp:GridView ID="grvDatos" runat="server" AutoGenerateColumns="False" DataKeyNames="id_sector" AllowPaging="True" DataSourceID="SqlDataGrilla" AllowSorting="True" Width="1250px">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="btnEditar" runat="server" ToolTip="Editar Registro" ImageUrl="~/App_Themes/css/img/standar/update.png" NavigateUrl='<%# Eval("id_sector", "abm_sector.aspx?ID={0}")%>' Text=""></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminr" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument='<%# Eval("id_sector")%>' ImageUrl="~/App_Themes/css/img/standar/recicle.png" ToolTip="Elimiar Registro" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="id_sector" SortExpression="id_sector" HeaderText="id_sector" ReadOnly="True"></asp:BoundField>
                                <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion" />
                                <asp:BoundField DataField="limite" DataFormatString="{0:N0}" HeaderText="Limite Mensual" SortExpression="limite" />
                                <asp:CheckBoxField DataField="estado" HeaderText="estado" />
                                <asp:BoundField DataField="fecha_ult_mod" HeaderText="fecha_ult_mod" SortExpression="fecha_ult_mod" />
                                <asp:TemplateField HeaderText="Seleccionar Sector">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="btn" runat="server" ToolTip="Asignar Sector" ImageUrl="~/App_Themes/css/img/standar/estrella08.png" NavigateUrl='<%# Eval("id_sector", "Sector_Afecta.aspx?ID={0}")%>' Text="mmm"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataGrilla" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT id_sector 
      ,descripcion 
      ,limite 
      ,estado 
      ,fecha_ult_mod 
  FROM Sector 
  where id_sector like @filtro or descripcion like '%' + @filtro + '%'"
                            DeleteCommand="usp_SectorDelete" DeleteCommandType="StoredProcedure">
                            <DeleteParameters>
                                <asp:Parameter Name="id_sector" Type="Int32" />
                                <asp:ControlParameter ControlID="Usuario" Name="add_usuario" PropertyName="Value" Type="Int32" DefaultValue="0" />
                            </DeleteParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="txtBuscar" Name="filtro" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:HiddenField ID="hdfError" runat="server" />
                        <asp:HiddenField ID="Usuario" runat="server" />
                        <uc1:cuMensajeBox ID="cuMensajeBox" runat="server" />
                    </div>
                </div>
            </div>


        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdEnviarExcel" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

