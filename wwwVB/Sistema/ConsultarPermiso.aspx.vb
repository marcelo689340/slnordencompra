﻿
Imports System.Data

Partial Class Sistema_ConsultarPermiso : Inherits PaginaBase
    'Inherits System.Web.UI.Page
    ''Inherits PaginaBase
    Dim dt As DataTable


    Protected Sub cmdBuscar_Click(sender As Object, e As EventArgs) Handles cmdBuscar.Click
        Cargar_Grilla()
        Cargar_Cookies()
    End Sub
    Protected Sub Cargar_Cookies()
        Response.Cookies("Buscar").Value = txtBuscar.Text
        Response.Cookies("Buscar").Expires = DateTime.Now.AddMinutes(5)
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim sPagina As String = "/Sistema/ConsultarPermiso.aspx"
            Dim sMens As String = Validar_Ingreso(sPagina, cmdNuevo, grvDatos)
            If sMens = "" Then
                Dim cooBuscar As HttpCookie = Request.Cookies("Buscar")
                If Not cooBuscar Is Nothing Then
                    txtBuscar.Text = cooBuscar.Value.ToString
                    Cargar_Grilla()
                End If
                Usuario.Value = Request.Cookies("idUsuario").Value
                Response.Cookies("PaginaAnterior").Value = "~/Sistema/ConsultarPermiso.aspx"
                CType(Master.FindControl("lblTitulo"), Label).Text = "MANTENIMIENTO DE PERMISO"
            Else
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
            End If
        End If
    End Sub
    Private Sub Cargar_Grilla()
        'pnlGrilla.Visible = True
    End Sub
    Protected Sub cmdEnviarExcel_Click(sender As Object, e As EventArgs) Handles cmdEnviarExcel.Click
        ExportarTodaGrilla("Permiso.xls", grvDatos, "LISTADO DE PERMISO")
    End Sub
    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox.tipoMensaje = 2
        cuMensajeBox.Mensaje = sMensaje
        cuMensajeBox.show()
    End Sub
    Protected Sub grvDatos_DataBound(sender As Object, e As EventArgs) Handles grvDatos.DataBound
        If grvDatos.Rows.Count > 0 Then
            Activar_Boton(True)
        Else
            Activar_Boton(False)
        End If
    End Sub
    Private Sub Activar_Boton(ByVal estado As Boolean)
        cmdEnviarExcel.Visible = estado
        cmdEnviarPdf.Visible = estado
    End Sub
    Protected Sub SqlDataGrilla_Deleted(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataGrilla.Deleted
        Dim sMensaje As String
        If Not (e.Exception Is Nothing) Then
            sMensaje = e.Exception.Message.ToString
            MensajeError(sMensaje)
            e.ExceptionHandled = True
            Me.hdfError.Value = sMensaje
        End If
    End Sub
End Class
