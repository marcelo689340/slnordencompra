﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="abm_usuario.aspx.vb" Inherits="Sistema_abm_usuario" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 128px;
            height: 15px;
        }

        .auto-style2 {
            width: 100%;
        }

        .auto-style3 {
            width: 213px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="auto-style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="block">
                    <div class="block-content collapse in">
                        <div class="container-fluid">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">Login : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtLogin" runat="server" CssClass="input-group"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validador_error" ControlToValidate="txtLogin"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Contraseña : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtPassword" runat="server" MaxLength="50" CssClass="input-group"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validador_error" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Nombre : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtNombre" runat="server" MaxLength="50" CssClass="input-group"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="validador_error" ControlToValidate="txtNombre"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Apellido : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtApellido" runat="server" MaxLength="50" CssClass="input-group"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" CssClass="validador_error" ControlToValidate="txtApellido"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Email : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" CssClass="input-group"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Celular : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtCelular" runat="server" MaxLength="50" CssClass="input-group"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Activo : </label>
                                    <div class="controls">
                                        <asp:CheckBox ID="chbActivo" runat="server" Text=" " Checked="True" />
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <asp:Button ID="cmdGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" />
                                    <asp:Button ID="cmdCancelar" runat="server" Text="Cancelar" CausesValidation="False" CssClass="btn" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <uc1:cuMensajeBox ID="cuMensajeBox1" runat="server" />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

