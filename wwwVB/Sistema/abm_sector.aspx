﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="abm_sector.aspx.vb" Inherits="Sistema_abm_sector" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<%@ Register Src="../control/textNumero.ascx" TagName="textNumero" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="auto-style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="block">
                    <div class="block-content collapse in">
                        <div class="container-fluid">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">Descripcion : </label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtDescripcion" runat="server" CssClass="input-group"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validador_error" ControlToValidate="txtDescripcion"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Limite : </label>
                                    <div class="controls">
                                        <uc2:textNumero ID="txtLimite" runat="server" Decimales="0" MaxValue="999999999999" MinValue="1" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Activo : </label>
                                    <div class="controls">
                                        <asp:CheckBox ID="chbActivo" runat="server" Checked="True" Text=" " />
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <asp:Button ID="cmdGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" />
                                    <asp:Button ID="cmdCancelar" runat="server" Text="Cancelar" CausesValidation="False" CssClass="btn" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <uc1:cuMensajeBox ID="cuMensajeBox1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

