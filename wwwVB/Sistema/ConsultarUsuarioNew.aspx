﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.master" Culture="Auto" UICulture ="Auto" AutoEventWireup="false" CodeFile="ConsultarUsuarioNew.aspx.vb" Inherits="Sistema_ConsultarUsuarioNew" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 61px;
        }

        .auto-style4 {
            width: 121px;
        }
        .auto-style5 {
            width: 265px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                                        <div style="padding-top: 5px; width: 500px">
                    <asp:Button ID="cmdNuevo" runat="server" Text="Nuevo" Height="22px" Width="100px" CssClass="boton" PostBackUrl="~/Sistema/abm_usuario.aspx?ID=0" />
                    &nbsp;<asp:Button ID="cmdEnviarExcel" runat="server" Height="22px" Text="Enviar Excel" Width="100px" CssClass="boton" Visible="False" />
                    &nbsp;<asp:Button ID="cmdEnviarPdf" runat="server" Text="Enviar PDF" Height="22px" Width="100px" CssClass="boton" Visible="False" />
                    &nbsp;
                </div>
                <table class="auto-style1" width="500px">
                    <tr>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style2">Usuario</td>
                        <td>
                            <asp:TextBox ID="txtBuscar" runat="server" Height="20px" Width="275px"></asp:TextBox>
                            &nbsp;<asp:Button ID="cmdBuscar" runat="server" Text="Buscar" CssClass="boton" />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            
            <div style="padding-top: 5px; padding-bottom: 5px">

                    <div>
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style4">
                                    <asp:GridView ID="grvDatos" runat="server" AutoGenerateColumns="False" DataKeyNames="id_usuario" AllowPaging="True" DataSourceID="SqlDataGrilla" AllowSorting="True">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="btnEditar" runat="server" ToolTip="Editar Registro" ImageUrl="~/App_Themes/css/img/standar/update.png" NavigateUrl='<%# Eval("id_usuario", "abm_usuario.aspx?ID={0}")%>' Text=""></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnEliminr" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument='<%# Eval("id_usuario")%>' ImageUrl="~/App_Themes/css/img/standar/recicle.png" ToolTip="Elimiar Registro" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" />
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="id_usuario" SortExpression="id_usuario">
                                                <EditItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("id_usuario") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("id_usuario") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="log_usuario" SortExpression="log_usuario">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("log_usuario") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("log_usuario") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="nombre" SortExpression="nombre">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("nombre") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("nombre") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="apellido" SortExpression="apellido">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("apellido") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("apellido") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="email" SortExpression="email">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("email") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="celular" SortExpression="celular">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("celular") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("celular") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ACTIVO" SortExpression="estado">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("estado") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("estado") %>' Enabled="False" />
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="fecha_utl_mod" SortExpression="fecha_utl_mod">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("fecha_utl_mod") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("fecha_utl_mod") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ASIGNAR SECTOR">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="btnAsignarSector" runat="server" ToolTip="Asignar Sector" ImageUrl="~/App_Themes/css/img/standar/deposito.png" NavigateUrl='<%# Eval("id_usuario", "AsignarUsuarioSector.aspx?ID={0}")%>' Text=""></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ASIGNAR PERMISO">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="btnAsignarPermiso" runat="server" ToolTip="Asignar Permiso" ImageUrl="~/App_Themes/css/img/standar/permisos.png" NavigateUrl='<%# Eval("id_usuario", "AsignarPermisoUsuario.aspx?ID={0}")%>' Text=""></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <%--<asp:BoundField DataField="add_usuario" HeaderText="add_usuario" SortExpression="add_usuario" />--%>

                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataGrilla" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT id_usuario
      ,log_usuario
      ,contrasenha
      ,nombre
      ,apellido
      ,email
      ,celular
      ,estado
      ,fecha_utl_mod
      ,add_usuario
  FROM bd_Orden.dbo.Usuario
  where id_usuario like @filtro
      or log_usuario like '%' + @filtro + '%' 
      or nombre like '%' + @filtro + '%'  
      or apellido like '%' + @filtro + '%'  " DeleteCommand="usp_UsuarioDelete" DeleteCommandType="StoredProcedure">
                                        <DeleteParameters>
                                            <asp:Parameter Name="id_usuario" Type="Int32" />
                                            <asp:ControlParameter ControlID="Usuario" Name="add_usuario" PropertyName="Value" Type="Int32" DefaultValue="0" />
                                        </DeleteParameters>
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="txtBuscar" Name="filtro" PropertyName="Text" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                    <asp:HiddenField ID="hdfError" runat="server" />
                                    <asp:HiddenField ID="Usuario" runat="server" />
                                    <uc1:cuMensajeBox ID="cuMensajeBox" runat="server" />
                                </td>

                            </tr>
                        </table>
                    </div>


            </div>
                    </asp:View>
                    <br />
                    <asp:View ID="View2" runat="server">
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <br />
                        <br />
                    </asp:View>
                </asp:MultiView>
                <br />

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdEnviarExcel" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

