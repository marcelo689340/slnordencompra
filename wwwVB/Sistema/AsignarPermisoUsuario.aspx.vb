﻿Imports CapaMedio
Imports CapaMedio.DAO
Partial Class Sistema_AsignarPermisoUsuario
    Inherits PaginaBase

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If
            'If Not cooBuscar Is Nothing Then
            '    'txtBuscar.Text = cooBuscar.Value.ToString
            '    Cargar_Grilla()
            'End If
            Usuario.Value = Request.Cookies("idUsuario").Value
            Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            CType(Master.FindControl("lblTitulo"), Label).Text = "USUARIO - ASIGNAR PERMISO"
            Dim lqs = (From C In db.Usuario Where C.id_usuario = Request.QueryString("ID") Select C).Single
            txtDescripcion.Text = lqs.log_usuario.ToString

            Try
                Dim lqs2 = db.CONSULTAR_PERMISO_USUARIO(Convert.ToInt16(Request.QueryString("ID")))
                grvDatos.DataSource = lqs2.ToList()
            Catch ex As Exception
                grvDatos.DataSource = Nothing
            End Try
            grvDatos.DataBind()





            'Dim UsuarioPermiso As cUsuario = New cUsuario()
            'Dim Acceso As cPermiso = New cPermiso
            'If UsuarioPermiso.Obtener_Permiso("/Mantenimiento/ConsultarAplicacion.aspx") = True Then
            '    Acceso = UsuarioPermiso.Nivel_Permiso
            '    If Acceso.pModificar = "N" Then
            '        Me.grvDatos.Columns(0).Visible = False
            '    End If
            '    If Acceso.pEliminar = "N" Then
            '        Me.grvDatos.Columns(1).Visible = False
            '    End If
            '    If Acceso.pInsertar = "N" Then
            '        Me.cmdNuevo.Visible = False
            '    End If
            '    If Acceso.pInsertar = "N" And Acceso.pModificar = "N" Then
            '        cmdNuevo.Visible = False
            '    End If
            '    If Acceso.pSuper = "N" Then
            '        Me.grvDatos.Columns(5).Visible = False
            '        Me.grvDatos.Columns(6).Visible = False
            '    End If
            'Else
            '    Response.Redirect("~/ErrorPagina.aspx?Mensaje=No posee permisos sobre la pagina")
            'End If
        End If

    End Sub
    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox.tipoMensaje = 2
        cuMensajeBox.Mensaje = sMensaje
        cuMensajeBox.show()
    End Sub
    Protected Sub grvDatos_DataBound(sender As Object, e As EventArgs) Handles grvDatos.DataBound
        If grvDatos.Rows.Count > 0 Then
            cmdGuardar.Visible = True
        Else
            cmdGuardar.Visible = False
        End If
    End Sub
    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click

        Dim oItem As Usuario_Permiso = New Usuario_Permiso()
        Dim lst As List(Of Usuario_Permiso) = New List(Of Usuario_Permiso)()
        Dim ChkOk As CheckBox

        For Each row As GridViewRow In grvDatos.Rows
            ChkOk = CType(row.FindControl("ChkOk"), CheckBox)
            oItem.id_usuario = Convert.ToInt16(Request.QueryString("ID"))
            oItem.id_permiso = Convert.ToInt16(row.Cells(0).Text)
            oItem.estado = ChkOk.Checked
            oItem.add_usuario = Convert.ToInt16(Request.Cookies("idUsuario").Value)
            lst.Add(oItem)

            'If ChkOk.Checked = True Then
            'Dim lqs = (From A In db.Usuario_Permiso Where A.id_permiso = oItem.id_permiso And A.id_usuario = oItem.id_usuario Select A).SingleOrDefault()
            'If (lqs Is Nothing) Then
            '    'db.Dispose()
            '    dao.Insertar(oItem)
            'Else
            '    ' db.Dispose()
            '    dao.Actualizar_Usuario(oItem)
            'End If
            oItem = New Usuario_Permiso()
        Next
        Dim dao As DAO_USUARIO_PERMISO = New DAO_USUARIO_PERMISO()
        dao.Actualizar(lst)
        Response.Redirect(cmdVolver.PostBackUrl.ToString)

    End Sub
End Class
