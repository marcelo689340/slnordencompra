﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.master" Culture="Auto" UICulture="Auto" AutoEventWireup="false" CodeFile="ConsultarPermiso.aspx.vb" Inherits="Sistema_ConsultarPermiso" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="container-fluid">
                    <div class="form-horizontal" style="margin-top: 15px !important;">
                        <div class="control-group">
                            <label class="control-label">Permiso :</label>
                            <%--<asp:Label ID="Label3" runat="server" Text="Usuario    : " CssClass="control-label col-md-1 col-lg-1 col-xs-1"></asp:Label>--%>
                            <div class="controls">
                                <asp:TextBox ID="txtBuscar" runat="server" CssClass="input-group"></asp:TextBox>
                                <asp:Button ID="cmdBuscar" runat="server" Text="Buscar" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                    <div class="control-group form-actions">
                        <div class="controls">
                            <asp:Button ID="cmdNuevo" runat="server" Text="Nuevo" CssClass="btn btn-primary" PostBackUrl="~/Sistema/ABMPermiso.aspx?ID=0" />
                            <asp:Button ID="cmdEnviarExcel" runat="server" Text="Enviar Excel" CssClass="btn btn-primary" Visible="False" />
                            <asp:Button ID="cmdEnviarPdf" runat="server" Text="Enviar PDF" CssClass="btn btn-primary" Visible="False" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="col-md-12">
                        <asp:GridView ID="grvDatos" runat="server" AutoGenerateColumns="False" DataKeyNames="id_permiso" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataGrilla">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="btnEditar" runat="server" ToolTip="Editar Registro" ImageUrl="~/App_Themes/css/img/standar/update.png" NavigateUrl='<%# Eval("id_permiso", "ABMPermiso.aspx?ID={0}")%>' Text="mmm"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminr" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument='<%# Eval("id_permiso")%>' ImageUrl="~/App_Themes/css/img/standar/recicle.png" Text="Eliminar" ToolTip="Elimiar Registro" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="id_permiso" SortExpression="id_permiso" HeaderText="id_permiso" ReadOnly="True"></asp:BoundField>
                                <asp:BoundField DataField="descripcion" SortExpression="descripcion" HeaderText="descripcion"></asp:BoundField>
                                <asp:CheckBoxField DataField="estado" HeaderText="estado" SortExpression="estado" />
                                <asp:TemplateField HeaderText="ASIGNAR FORMULARIOS">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="btnAsignarUsuario" runat="server" ToolTip="Asignar Usuario" ImageUrl="~/App_Themes/css/img/standar/usuario.png" NavigateUrl='<%# Eval("id_permiso", "AsignarFormularioPermiso.aspx?ID={0}")%>' Text=""></asp:HyperLink>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataGrilla" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" DeleteCommand="usp_PermisoDelete" SelectCommand="SELECT [id_permiso]
      ,[descripcion]
      ,[estado]
  FROM [_Permiso]
  where [id_permiso] like @filtro or [descripcion] like '%' + @filtro + '%'"
                            DeleteCommandType="StoredProcedure">
                            <DeleteParameters>
                                <asp:Parameter Name="id_permiso" Type="Int32" />
                                <asp:ControlParameter ControlID="Usuario" Name="add_usuario" PropertyName="Value" Type="Int32" />
                            </DeleteParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="txtBuscar" Name="filtro" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:HiddenField ID="hdfError" runat="server" />
                        <asp:HiddenField ID="Usuario" runat="server" />
                        <uc1:cuMensajeBox ID="cuMensajeBox" runat="server" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdEnviarExcel" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

