﻿Imports CapaMedio.DAO
Imports CapaMedio
Partial Class Sistema_abm_usuario
    Inherits PaginaBase

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Dim lqs As Usuario = New Usuario()
        lqs.id_usuario = Convert.ToInt16(Request.QueryString("ID"))
        lqs.log_usuario = txtLogin.Text
        lqs.contrasenha = txtPassword.Text
        lqs.nombre = txtNombre.Text
        lqs.apellido = txtApellido.Text
        lqs.email = txtEmail.Text
        lqs.celular = txtCelular.Text
        lqs.estado = chbActivo.Checked
        lqs.add_usuario = Request.Cookies("idUsuario").Value
        Try
            Dim dao As DAO_USUARIO = New DAO_USUARIO()
            If Request.QueryString("ID") <> "0" Then
                dao.Actualizar(lqs)
            Else
                dao.Insertar(lqs)
            End If
            Response.Redirect(cmdCancelar.PostBackUrl.ToString)
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If

            Me.cmdCancelar.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            If Request.QueryString("ID") <> "0" Then
                CType(Master.FindControl("lblTitulo"), Label).Text = "MODIFICAR USUARIO CODIGO : " + Request.QueryString("ID").ToString()
                Cargar_Datos(Convert.ToInt16(Request.QueryString("ID")))
            Else
                CType(Master.FindControl("lblTitulo"), Label).Text = "NUEVO USUARIO"
            End If
        End If
    End Sub

    Private Sub Cargar_Datos(ByVal id As Integer)
        Dim dao As DAO_USUARIO = New DAO_USUARIO()
        Dim lqs = dao.GetUsuario(id)
        lqs.id_usuario = id
        txtLogin.Text = lqs.log_usuario
        txtPassword.Text = lqs.contrasenha
        txtNombre.Text = lqs.nombre
        txtApellido.Text = lqs.apellido
        txtEmail.Text = lqs.email
        txtCelular.Text = lqs.celular
        chbActivo.Checked = lqs.estado
    End Sub

End Class
