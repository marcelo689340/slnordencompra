﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.master" Culture="Auto" UICulture ="Auto" AutoEventWireup="false" CodeFile="AsignarPermisoUsuario2.aspx.vb" Inherits="Sistema_AsignarPermisoUsuario2" %>

<%@ Register Src="~/control/cuMensajeBox.ascx" TagPrefix="uc1" TagName="cuMensajeBox" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style11 {
        }
        .auto-style13 {
            width: 143px;
        }
        .auto-style14 {
            width: 429px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <div class="cuadroTitulo">
                    <asp:Literal ID="ltDatos" runat="server" Text="Usuario"></asp:Literal>
                </div>
                <div class="conMargin10">
                    <div style="float: left; padding-bottom: 5px; width: 411px;">
                        <fieldset id="fsBuscarRuc" runat="server" class="fieldSet01">
                            <legend>
                                <asp:Literal ID="ltTituloFsDatos" runat="server" Text="Datos"></asp:Literal>
                            </legend>
                        
                        <div class="conPadding10">

                            <table class="tabla">
                                <tr style="height:29px">
                                    <td class="auto-style13">Usuario</td>
                                    <td class="auto-style14">
                                        <asp:Label ID="lblUsuario" runat="server" Text="0"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="height:29px">
                                    <td class="auto-style13">Permiso</td>
                                    <td class="auto-style14">
                                        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="descripcion" DataValueField="id_permiso" Height="22px" Width="259px">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT [id_permiso]
      ,[descripcion]
  FROM [_Permiso]
  where [estado]=1 or [id_permiso] in(Select [id_permiso] from Sucursal_Usuario where id_sucursal_usuario = @ID)">
                                            <SelectParameters>
                                                <asp:QueryStringParameter Name="ID" QueryStringField="ID" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style13">Activo</td>
                                    <td class="auto-style14">
                                        <asp:CheckBox ID="chkActivo" runat="server" TabIndex="7" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style11" colspan="2">
                                        <asp:Button ID="cmdVolver" runat="server" CausesValidation="False" CssClass="boton" Text="Volver" TabIndex="10" />
                                        &nbsp;<asp:Button ID="cmdGuardar" runat="server" CssClass="boton" Text="Guardar" TabIndex="9" />
                                    </td>
                                </tr>
                            </table>
                            <uc1:cuMensajeBox runat="server" ID="cuMensajeBox" />
                        </div>
                            </fieldset>
                    </div>
                </div>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

