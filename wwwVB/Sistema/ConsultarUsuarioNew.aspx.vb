﻿
Imports System.Data

Partial Class Sistema_ConsultarUsuarioNew : Inherits PaginaBase
    'Inherits System.Web.UI.Page
    ''Inherits PaginaBase
    Dim dt As DataTable


    Protected Sub cmdBuscar_Click(sender As Object, e As EventArgs) Handles cmdBuscar.Click
        'pnlGrilla.Visible = True
        Response.Cookies("Buscar").Value = txtBuscar.Text
    End Sub


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim sPagina As String = "/Sistema/ConsultarUsuario.aspx"
            Dim sMens As String = Validar_Ingreso(sPagina, cmdNuevo, grvDatos)
            If sMens = "" Then
                Dim cooBuscar As HttpCookie = Request.Cookies("Buscar")
                If Not cooBuscar Is Nothing Then
                    txtBuscar.Text = cooBuscar.Value.ToString
                    'Response.Cookies("Buscar").Value = ""
                End If
                Usuario.Value = Request.Cookies("idUsuario").Value
                Response.Cookies("PaginaAnterior").Value = "~" + sPagina
                CType(Master.FindControl("lblTitulo"), Label).Text = "MANTENIMIENTO DE USUARIO"
            Else
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
            End If
        End If
    End Sub
    Protected Sub cmdEnviarExcel_Click(sender As Object, e As EventArgs) Handles cmdEnviarExcel.Click
        ExportarTodaGrilla("Usuario.xls", grvDatos, "LISTADO DE USUARIO")
    End Sub

    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox.tipoMensaje = 2
        cuMensajeBox.Mensaje = sMensaje
        cuMensajeBox.show()
    End Sub

    Protected Sub grvDatos_DataBound(sender As Object, e As EventArgs) Handles grvDatos.DataBound
        If grvDatos.Rows.Count > 0 Then
            Activar_Boton(True)
        Else
            Activar_Boton(False)
        End If
    End Sub
    Private Sub Activar_Boton(ByVal estado As Boolean)
        cmdEnviarExcel.Visible = estado
        cmdEnviarPdf.Visible = estado
    End Sub
    Protected Sub SqlDataGrilla_Deleted(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataGrilla.Deleted
        Dim sMensaje As String
        If Not (e.Exception Is Nothing) Then
            sMensaje = e.Exception.Message.ToString
            MensajeError(sMensaje)
            e.ExceptionHandled = True
            Me.hdfError.Value = sMensaje
        End If
    End Sub
End Class
