﻿Imports CapaMedio
Imports CapaMedio.DAO
Partial Class Sistema_AsignarPermisoUsuario2 : Inherits PaginaBase

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        ' Dim UsuPer As U
        Dim dao As DAO_USUARIO_PERMISO = New DAO_USUARIO_PERMISO()
        Dim obj As Usuario_Permiso = New Usuario_Permiso()
        obj.id_usuario = Convert.ToInt16(Request.QueryString("ID"))
        obj.id_permiso = DropDownList1.SelectedValue.ToString()
        obj.estado = chkActivo.Checked
        obj.add_usuario = Request.Cookies("idUsuario").Value
        'Dim lqs = (From A In db.Usuario_Permiso Where A.id_permiso = obj.id_permiso And A.id_usuario = obj.id_usuario).SingleOrDefault()
        Dim lqs = (From A In db.Usuario_Permiso Where A.id_usuario = obj.id_usuario).SingleOrDefault()
        If (lqs Is Nothing) Then
            dao.Insertar(obj)
        Else
            dao.Actualizar_Usuario(obj)
        End If
        Response.Redirect(cmdVolver.PostBackUrl.ToString)

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If
            CType(Master.FindControl("lblTitulo"), Label).Text = ""
            Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            Cargar_Datos()

        End If
    End Sub





    Protected Sub Cargar_Datos()
        Dim Aux As Usuario = New Usuario()
        Dim dao As DAO_USUARIO = New DAO_USUARIO()
        Aux = dao.GetUsuario(Convert.ToInt16(Request.QueryString("ID")))
        lblUsuario.Text = Aux.log_usuario
        Dim Aux2 As Usuario_Permiso = New Usuario_Permiso()
        Dim dao2 As DAO_USUARIO_PERMISO = New DAO_USUARIO_PERMISO()
        Aux2 = dao2.setUsuario_Permiso(Convert.ToInt16(Request.QueryString("ID")))
        DropDownList1.SelectedValue = Aux2.id_permiso

    End Sub

    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox.tipoMensaje = 2
        cuMensajeBox.Mensaje = sMensaje
        cuMensajeBox.show()
    End Sub

End Class
