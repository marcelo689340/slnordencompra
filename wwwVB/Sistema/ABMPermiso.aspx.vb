﻿Imports CapaMedio
Imports CapaMedio.DAO
Partial Class Sistema_ABMPermiso : Inherits PaginaBase

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Dim obj As _Permiso = New _Permiso()
        Dim dao As DAO__PERMISO = New DAO__PERMISO()
        obj.id_permiso = Request.QueryString("ID")
        obj.descripcion = txtDescripcion.Text
        obj.estado = chkActivo.Checked
        obj.add_usuario = Request.Cookies("idUsuario").Value

        Try
            If Request.QueryString("ID") <> "0" Then
                dao.Actualizar(obj)
            Else
                dao.Insertar(obj)
            End If
            Response.Redirect(cmdVolver.PostBackUrl.ToString)
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            MensajeError(ex.Message.ToString)
        End Try

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If
            CType(Master.FindControl("lblTitulo"), Label).Text = ""
            Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            If Request.QueryString("ID") <> "0" Then
                CType(Master.FindControl("lblTitulo"), Label).Text = "MODIFICAR PERMISO"
                Cargar_Datos()
            Else
                CType(Master.FindControl("lblTitulo"), Label).Text = "NUEVO PERMISO"
                chkActivo.Checked = True
            End If
        End If
    End Sub

    'Protected Sub Agregar_Nuevo()
    '    Try
    '        Dim iID As Integer
    '        Try
    '            iID = (From A In dbAud._Permiso_Aud Select A.id_permiso).Max()
    '        Catch ex As Exception
    '            iID = 0
    '        End Try
    '        iID = iID + 1
    '        Dim lqs As New CapaMedio._Permiso
    '        lqs.id_permiso = iID
    '        lqs.descripcion = txtDescripcion.Text.ToUpper
    '        lqs.estado = chkActivo.Checked
    '        lqs.id_user = Request.Cookies("idUsuario").Value
    '        db._Permiso.InsertOnSubmit(lqs)
    '        db.SubmitChanges()
    '        iID = lqs.id_permiso
    '        ''cmdVolver.OnClientClick = True
    '        Response.Redirect(cmdVolver.PostBackUrl.ToString)
    '    Catch ex As Exception
    '        Dim sMensaje As String = ex.Message.ToString
    '        MensajeError(ex.Message.ToString)
    '    End Try
    'End Sub
    'Protected Sub Modificar_Datos()

    '    Try
    '        Dim lqs = (From U In db._Permiso Where U.id_permiso = Request.QueryString("ID") _
    '             Select U).Single
    '        lblCodigo.Text = lqs.id_permiso
    '        lqs.descripcion = txtDescripcion.Text.ToUpper
    '        lqs.estado = chkActivo.Checked
    '        lqs.id_user = Request.Cookies("idUsuario").Value
    '        db.SubmitChanges()
    '        Response.Redirect(cmdVolver.PostBackUrl.ToString)
    '    Catch ex As Exception
    '        MensajeError(ex.Message.ToString)
    '    End Try
    'End Sub


    Protected Sub Cargar_Datos()

        Dim obj As _Permiso = New _Permiso()
        Dim dao As DAO__PERMISO = New DAO__PERMISO()
        Try
            obj = dao.set_Permiso(Convert.ToInt16(Request.QueryString("ID")))
            lblCodigo.Text = obj.id_permiso
            txtDescripcion.Text = obj.descripcion
            chkActivo.Checked = obj.estado
        Catch ex As Exception
            MensajeError(ex.Message.ToString)
        End Try
    End Sub

    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox.tipoMensaje = 2
        cuMensajeBox.Mensaje = sMensaje
        cuMensajeBox.show()
    End Sub

End Class
