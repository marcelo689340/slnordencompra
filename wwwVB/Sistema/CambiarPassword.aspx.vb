﻿
Partial Class CambiarPassword
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            CType(Master.FindControl("lblTitulo"), Label).Text = "CAMBIAR CONTRASEÑA"
            Me.HiddenField1.Value = HttpContext.Current.User.Identity.Name
        End If
    End Sub

    Protected Sub ControlarError_Opercion_Base(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDataSource1.Updated
        Dim CodigoScript As New CodigoScript
        Dim sMensaje As String
        If Not (e.Exception Is Nothing) Then
            sMensaje = ""
            sMensaje = sMensaje & "SE HA PRODUCIDO UN ERROR DURANTE LA INSERCION;  Descripcion :  "
            sMensaje = sMensaje & e.Exception.Message.ToString
            CodigoScript.Mensaje(Me.Page, sMensaje)
            e.ExceptionHandled = True
        Else
            sMensaje = ""
            Response.Redirect("~/Default.aspx")

        End If
    End Sub

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs)
        Response.Redirect("~/Default.aspx")
    End Sub

    Protected Sub Button2_Click(sender As Object, e As System.EventArgs)
        Response.Redirect("~/Index.aspx")
    End Sub


End Class
