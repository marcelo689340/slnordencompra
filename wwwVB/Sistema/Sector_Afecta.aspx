﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="Sector_Afecta.aspx.vb" Inherits="Sistema_Sector_Afecta" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="auto-style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="block">
                    <div class="block-content collapse in">
                        <div class="container-fluid">
                            <div class="control-group">
                                <div class="controls">
                                    <asp:FormView ID="FormView1" runat="server" DataKeyNames="id_sector" DataSourceID="SqlDataSource1" Height="56px" Width="188px">
                                        <EditItemTemplate>
                                            id_sector:
                                <asp:Label ID="id_sectorLabel1" runat="server" Text='<%# Eval("id_sector") %>' />
                                            <br />
                                            descripcion:
                                <asp:TextBox ID="descripcionTextBox" runat="server" Text='<%# Bind("descripcion") %>' />
                                            <br />
                                            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Actualizar" />
                                            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                                        </EditItemTemplate>
                                        <InsertItemTemplate>
                                            id_sector:
                                <asp:TextBox ID="id_sectorTextBox" runat="server" Text='<%# Bind("id_sector") %>' />
                                            <br />
                                            descripcion:
                                <asp:TextBox ID="descripcionTextBox" runat="server" Text='<%# Bind("descripcion") %>' />
                                            <br />
                                            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insertar" />
                                            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                                        </InsertItemTemplate>
                                        <ItemTemplate>
                                            <div class="form-horizontal">
                                                <div class="control-group">
                                                    <label class="control-label">ID SECTOR : </label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtid_sector" runat="server" CssClass="input-group" Text='<%# Eval("id_sector") %>' Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">DESCRIPCION: </label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtdescripcion" runat="server" CssClass="input-group" Text='<%# Bind("descripcion") %>' Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:FormView>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT [id_sector]
      ,[descripcion]
  FROM Sector
  where id_sector = @id_sector">
                                        <SelectParameters>
                                            <asp:QueryStringParameter Name="id_sector" QueryStringField="ID" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </div>
                            <div class="form-actions">
                                <asp:Button ID="cmdVolver" runat="server" CausesValidation="False" CssClass="btn" Text="Volver" />
                                <asp:Button ID="cmdGuardar" runat="server" CssClass="btn btn-primary" Text="Guardar" ValidationGroup="Cabecera" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">SELECCIONAR SECTOR AL QUE PUEDE REALIZAR COMPRA  </div>
                    </div>

                    <div class="control-group">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2">
                            <Columns>
                                <asp:BoundField DataField="id_sector" HeaderText="id_sector" ReadOnly="True" SortExpression="id_sector" />
                                <asp:BoundField DataField="descripcion" HeaderText="descripcion" ReadOnly="True" SortExpression="descripcion" />
                                <asp:TemplateField HeaderText="Activo" SortExpression="Activo">
                                    <EditItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Activo") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSeleccionar" runat="server" Checked='<%# Bind("Activo") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="CONSULTA_SECTOR_COMPRA" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="id_sector" QueryStringField="ID" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </div>
            </div>
            <uc1:cuMensajeBox ID="cuMensajeBox1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

