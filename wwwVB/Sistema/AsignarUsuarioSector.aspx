﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="AsignarUsuarioSector.aspx.vb" Inherits="Sistema_AsignarUsuarioSector" %>

<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img src="../App_Themes/css/img/standar/ajax-loader-barra.gif" alt="" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="block">
                    <div class="container-fluid" style="margin-top: 10px !important">
                        <div class="container-fluid">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label btn-block">USUARIO :</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtDescripcion" runat="server" Enabled="false" CssClass="input-group"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <asp:Button ID="cmdVolver" runat="server" Text="Volver" CssClass="btn" CausesValidation="False" TabIndex="10" />
                        <asp:Button ID="cmdGuardar" runat="server" CausesValidation="False" CssClass="btn btn-primary" TabIndex="10" Text="Guardar" Visible="False" />

                    </div>
                </div>

                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">ASIGNAR SECTOR  </div>
                    </div>

                    <div class="control-group">
                        <asp:GridView ID="grvDatos" runat="server" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="id_sector" HeaderText="id_sector" ReadOnly="True" SortExpression="id_sector" />
                                <asp:BoundField DataField="descripcion" HeaderText="descripcion" ReadOnly="True" SortExpression="descripcion" />
                                <%--<asp:BoundField DataField="OK" HeaderText="OK" ReadOnly="True" SortExpression="OK" />--%>
                                <asp:TemplateField HeaderText="SELECCIONAR">
                                    <EditItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("OK")%>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkOk" runat="server" Checked='<%# Bind("OK")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="Usuario" runat="server" />
                        <uc1:cuMensajeBox ID="cuMensajeBox" runat="server" />
                    </div>
                </div>
            </div>


        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

