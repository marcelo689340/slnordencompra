﻿Imports System.Data
Imports System.Data.Linq
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports CapaMedio.DAO

Partial Class CajaAhorro_Caja_Ahorro_Firma
    Inherits PaginaBase

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try
            Dim imagen As Byte()

            If IsNothing(Session("imagen")) Then
                ' MensajeTiempo(Page, "No existe imagen cargada", 3000, 0)
                Exit Sub
            End If

            If txtDescripcion.Text.Trim = "" Then txtDescripcion.Text = lblDatos.Text

            imagen = Session("imagen")
            Dim binaryObj As New Binary(imagen)

            Dim sArchivo As String = lblDatos.Text

            Dim dao As DAO_ORDEN = New DAO_ORDEN()
            dao.Insertar_Presupuesto(1, lblDatos.Text, "", 1, binaryObj.ToArray())



            'db.proc_Caja_Ahorro_FirmaInsert(txtCajaAhorro.Text, txtDescripcion.Text, lblDatos.Text, binaryObj)

            'MensajeTiempo(Page, "Guardado con exito", 3000, OpcionMensaje.vbSuccess)
            SqlDatosFirmas.DataBind()
            grillaFirmas.DataBind()

            txtDescripcion.Text = ""
            FileUpload1.Visible = True
            Limpiar_Foto()
            MultiView1.ActiveViewIndex = -1
        Catch ex As Exception
            'MensajeTiempo(Page, ex.Message.ToString, 3000, OpcionMensaje.vbError)
        End Try
    End Sub

    Private Sub CajaAhorro_Caja_Ahorro_Firma_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try


            If Request.Form("__eventtarget") = "fileUpload1" Then
                If FileUpload1.HasFile Then

                    If FileUpload1.FileBytes.Length > 5000000 Then
                        ''MensajeTiempo(Page, "El tamaño del archivo, supera el maximo permitido", 4000, OpcionMensaje.vbError)
                        Exit Sub
                    End If

                    Dim path As String = Server.MapPath("~/temporales/")
                    path = path + "borrar_" + HttpContext.Current.User.Identity.Name.ToString + ".jpg"
                    Dim arrImage As Byte()
                    arrImage = FileUpload1.FileBytes



                    Dim binaryObj As New Binary(arrImage), Foto As Byte()
                    Dim redime As New ImagenClase

                    Foto = DirectCast(binaryObj.ToArray, Byte())
                    If arrImage.Length > 1000000 Then
                        Foto = redime.ImagenCambiarTamanho(Foto, 800)
                    End If
                    File.WriteAllBytes(path, Foto)
                    Session("imagen") = Foto

                    If Not IsNothing(Session("imagen")) Then
                        Me.Foto.ImageUrl = "~/verfoto.aspx"

                        'btnGuardarFoto.Visible = False

                        lblDatos.Text = FileUpload1.FileName
                        FileUpload1.Visible = False

                    End If
                End If
            End If

            If Page.IsPostBack = False Then
                ScriptManager1.RegisterPostBackControl(grillaFirmas)

                ''  Dim permi As New Permiso
                'permi.Pantalla = "cajaahorro/caja_ahorro_firma.aspx"
                'permi.Usuario = HttpContext.Current.User.Identity.Name.ToString
                'If permi.PermisoEjecucion = False Then Response.Redirect("~/errorpagina.aspx?mensaje=Error, no posee permisos para ejecutar la pagina")
                ''If permi.PermisoInsertar = False Then btnInsertarFirma.Visible = False
                'If permi.PermisoEliminar = False Then grillaFirmas.Columns(0).Visible = False

                'Dim sinSQL As String
                'Dim dt As New Data.DataTable
                'Dim obj As New ModuloPrincipal
                'sinSQL = "select Cah_Numero, Tca_Descripcion, "
                'sinSQL = sinSQL + " (select stuff((select ',' + Cas_Cod_Socio + '; ' + Cas_Nombre + ' ' + Cas_Apellido "
                'sinSQL = sinSQL + " From CAJA_AHORRO_SOCIO "
                'sinSQL = sinSQL + " where Cas_Caja_Ahorro = Cah_Numero "
                'sinSQL = sinSQL + " for XML path('')), 1,1, '')) as Socios_Desc "
                'sinSQL = sinSQL + " From CAJA_AHORRO  "
                'sinSQL = sinSQL + " inner join TIPO_CAJA_AHORRO "
                'sinSQL = sinSQL + " On Tca_Codigo = Cah_TipoCaja "
                'sinSQL = sinSQL + " where Cah_Numero = '" + Request.QueryString("id") + "'"
                'dt = obj.DevuelveDatos(sinSQL, "cnnGestion")
                'If dt.Rows.Count > 0 And Not IsNothing(dt) Then
                '    txtCajaAhorro.Text = dt.Rows(0)("cah_numero")
                '    txtTipoCaja.Text = dt.Rows(0)("tca_descripcion")
                '    txtSocios.Text = dt.Rows(0)("Socios_Desc")
                'End If
                Limpiar_Foto()
                Timer1.Enabled = True
            End If
        Catch ex As Exception
            'MensajeTiempo(Page, ex.Message.ToString, 0, OpcionMensaje.vbError)
        End Try

    End Sub

    Sub Limpiar_Foto()
        Session("imagen") = Nothing
        Foto.ImageUrl = ""
    End Sub

    Private Sub FileUpload1_Init(sender As Object, e As EventArgs) Handles FileUpload1.Init
        FileUpload1.Attributes.Add("onchange", "fileUpload1()")
    End Sub

    Function LeerArchivo() As Byte()

        Dim path As String
        path = Server.MapPath("~/temporales/")

        'open file from the disk (file path is the path to the file to be opened)
        Dim sArchivo As String = path & "borrar_" & HttpContext.Current.User.Identity.Name.ToString + ".jpg"
        Dim memStream As New MemoryStream()
        Using fileStream As FileStream = File.OpenRead(sArchivo)
            'create new MemoryStream object
            memStream.SetLength(fileStream.Length)
            'read file to MemoryStream
            fileStream.Read(memStream.GetBuffer(), 0, CInt(Fix(fileStream.Length)))
        End Using

        Return memStream.ToArray

    End Function

    Protected Sub btnInsertarFirma_Click(sender As Object, e As EventArgs) Handles btnInsertarFirma.Click
        MultiView1.ActiveViewIndex = 0
        Limpiar_Foto()
    End Sub
    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Session("imagen") = Nothing
        MultiView1.ActiveViewIndex = -1
        lblDatos.Text = ""
        txtDescripcion.Text = ""
        FileUpload1.Visible = True
        Limpiar_Foto()
    End Sub
    Protected Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False
        MultiView1.ActiveViewIndex = -1
    End Sub
    Protected Sub grillaFirmas_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grillaFirmas.RowCommand
        If e.CommandName = "Ver" Then


            Dim strcnn As String = ConfigurationManager.ConnectionStrings("cnnGestion").ConnectionString
            Dim cnn As New SqlConnection(strcnn)
            Dim codImagen As Double
            codImagen = Convert.ToDouble(e.CommandArgument)

            Dim cmd As String = "Select doc_imagen From documento_archivos where doc_id=@id"

            Dim comm As New SqlCommand(cmd, cnn)
            comm.Parameters.Add(New SqlParameter("@id", SqlDbType.Int)).Value = codImagen

            Try

                cnn.Open()
                Dim imagen As Byte(), imagenS As New MemoryStream
                'Recuperamos la imagen de la Base de datos
                imagen = DirectCast(comm.ExecuteScalar(), Byte())


                Session("imagen") = imagen
                Foto.ImageUrl = "~/documentos/MuestraImagenDocumento.aspx?id=" & e.CommandArgument

                Foto.DataBind()


                MultiView1.ActiveViewIndex = -1

            Catch ex As Exception
                ' MensajeTiempo(Page, ex.Message.ToString, 0, OpcionMensaje.vbError)
            End Try

        End If
        If e.CommandName = "Eliminar" Then
            Try

                'Dim sinSQL As String, obj As New ModuloPrincipal
                'sinSQL = "delete from documento_Archivos where doc_id = " & e.CommandArgument
                'obj.DevuelveDatos(sinSQL, "cnnGestion")
                'sinSQL = "delete from caja_ahorro_firma where caf_imagen_id = " & e.CommandArgument
                'obj.DevuelveDatos(sinSQL, "cnnGestion")
                'SqlDatosFirmas.DataBind()
                'grillaFirmas.DataBind()

                '  MensajeTiempo(Page, "Eliminado con exito", 3000, OpcionMensaje.vbSuccess)
                MultiView1.ActiveViewIndex = -1
            Catch ex As Exception
                'MensajeTiempo(Page, ex.Message.ToString, 0, OpcionMensaje.vbError)
            End Try

        End If
    End Sub
    Protected Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Response.Redirect("~/cajaahorro/cajaahorro.aspx")
    End Sub
End Class
