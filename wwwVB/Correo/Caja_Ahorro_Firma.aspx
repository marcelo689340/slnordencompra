﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="Caja_Ahorro_Firma.aspx.vb" Inherits="CajaAhorro_Caja_Ahorro_Firma" %>

<%--<%@ Register src="../controles/BarraProgreso.ascx" tagname="BarraProgreso" tagprefix="uc1" %>

<%@ Register src="../controles/CuSinInformacion.ascx" tagname="CuSinInformacion" tagprefix="uc2" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <script type="text/javascript">
        $(document).ready(function () {
            
        });

			function fileUpload1() {
			    __doPostBack('fileUpload1', '');
			    return (true);
			}
		</script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
   <%--         <uc1:BarraProgreso ID="BarraProgreso1" runat="server" />--%>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="cuadroLineaAlrededor" style="max-width:1000px">
                <div class="cuadroTitulo">Caja de Ahorro - Firmas<asp:Timer ID="Timer1" runat="server" Interval="100">
                    </asp:Timer>
                </div>
                <div class="cuadroCuerpo">
                    <div class="row">
                        <div class="col s1">Caja Ahorro:</div>
                        <div class="col s4">
                            <asp:TextBox ID="txtCajaAhorro" runat="server" Enabled="False"></asp:TextBox></div>
                        <div class="col s1">Tipo Caja:</div>
                        <div class="col s3">
                            <asp:TextBox ID="txtTipoCaja" runat="server" Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s1">Socios:</div>
                        <div class="col s10"><asp:TextBox ID="txtSocios" runat="server" Width="450px" Enabled="False"></asp:TextBox></div>
                    </div>
                    <div class="row">
                        <div class="col s6">
                            <asp:Button ID="btnInsertarFirma" runat="server" Text="Insertar" />
                            <asp:Button ID="btnVolver" runat="server" Text="Volver" />

<%--                            <AbiBoton:AbiBoton ID="btnInsertarFirma" runat="server" 
                                cssclass="botonctbm agregar" SkinID="botonctbm agregar" Text="Insertar" CausesValidation="false"/>
                            <AbiBoton:AbiBoton ID="btnVolver" runat="server" CausesValidation="false" 
                                cssclass="botonctbm volver" SkinID="botonctbm volver" Text="Volver" />--%>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <asp:GridView ID="grillaFirmas" runat="server" AutoGenerateColumns="False" DataKeyNames="id_orden,item" DataSourceID="SqlDatosFirmas" Width="450px">
                                <Columns>
                                    <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="item" HeaderText="item" SortExpression="item" ReadOnly="True">
                                    </asp:BoundField>
                                    <asp:CheckBoxField DataField="aprobado" HeaderText="aprobado" SortExpression="aprobado" />
                                    <asp:BoundField DataField="add_usuario" HeaderText="add_usuario" SortExpression="add_usuario" />
                                    <asp:BoundField DataField="fecha_ult_mod" HeaderText="fecha_ult_mod" SortExpression="fecha_ult_mod" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <%--<uc2:CuSinInformacion ID="CuSinInformacion1" runat="server" mensajeError="No existen detalles cargados" requerido="False" />--%>
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDatosFirmas" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT [id_orden]
      ,[item]
      ,[presupuesto]
      ,[aprobado]
      ,[add_usuario]
      ,[fecha_ult_mod]
  FROM [bd_Orden_Imagen].[dbo].[Presupuesto]
">
                            </asp:SqlDataSource>
                        </div>
                    </div>
                     
                        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                            <asp:View ID="View1" runat="server">
                                <div class="row">
                                    <div class="col s8">
                                        <asp:FileUpload ID="FileUpload1" Width="290px" runat="server"></asp:FileUpload>
                                        <asp:Label ID="lblDatos" runat="server" Text=""></asp:Label>
                                      </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s2">Descripcion:</div>
                                        <div class="col s4">
                                            <asp:TextBox ID="txtDescripcion" Width="90%" runat="server"></asp:TextBox>
                                        </div>
                                    <div class="col s5">
                                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" />
                                        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" />
                                      <%--  <AbiBoton:AbiBoton ID="btnGuardar" runat="server" CssClass="botonctbm guardar" SkinID="botonctbm guardar" Text="Guardar" />
                                        <AbiBoton:AbiBoton ID="btnCancelar" runat="server" CssClass="botonctbm cancelar" SkinID="botonctbm cancelar" Text="Cancelar" />--%>

                                    </div>
                                </div>
                            </asp:View>
                        </asp:MultiView>
                        
               
                    <div class="row">
                        <div class="col s12">
                            <%--<div style="width: 800px; height: 400px; overflow: scroll">--%>
                                <asp:Image ID="Foto" Style="max-width:600px" runat="server" Width="16px" />
                          <%--  </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

