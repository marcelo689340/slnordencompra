﻿Imports System.IO
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class Proceso
    Inherits PaginaBase

    Dim v_codServicio As String
    Dim v_tipoTrx As String
    Dim v_usuario As String
    Dim v_password As String
    Dim v_nroFactura As String
    Dim v_importe As String
    Dim v_moneda As String
    Dim v_medioPago As String
    Dim v_codTransaccion As String

    Private Sub Proceso_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim objeto As New Object
        Dim resp As New respCorta
        Dim pedido As New Pedido

        Try



            Dim intvar As Integer
            intvar = Request.TotalBytes

            Dim InStream As Stream
            Dim iCounter, longitud, iRead As Integer
            Dim OutString As String = ""

            InStream = Request.InputStream
            Longitud = CInt(InStream.Length)
            Dim ByteArray(longitud) As Byte
            Trace.Write("Len", longitud)
            iRead = InStream.Read(ByteArray, 0, longitud)

            If longitud > 0 Then

                For iCounter = 0 To longitud - 1

                    OutString = OutString & Chr(ByteArray(iCounter))

                Next iCounter


                'Deserializamos la respuesta del servidor
                Dim root As JObject = JObject.Parse(OutString)
                Dim iNroMovimiento As Double

                v_codServicio = root("codServicio")
                v_tipoTrx = root("tipoTrx")
                v_usuario = root("usuario")
                v_password = root("password")
                v_nroFactura = root("nroFactura")
                v_importe = root("importe")
                v_moneda = root("moneda")
                v_medioPago = root("medioPago")
                v_codTransaccion = root("codTransaccion")

                Dim Valido As Integer

                dbApi.proc_Verifica_Contrasenha(v_password, v_usuario, Valido)

                If Valido = 0 Then
                    resp.codRetorno = "999"
                    resp.desRetorno = "Usuario y Contraseña No valida"
                    EnviarJSON(resp)
                End If

                If IsNothing(v_codTransaccion) Then
                    v_codTransaccion = "SIN TRANSACCION"
                End If

                'If Val(Left(v_importe, Len(v_importe) - 2)) <= 5000 Then
                '    resp.codRetorno = "999"
                '    resp.desRetorno = "Monto Menor a 5000"
                '    EnviarJSON(resp)
                'End If

                If Not IsNothing(v_nroFactura) And Not IsNothing(v_codTransaccion) Then

                        pedido.nroFactura = v_nroFactura
                        pedido.importe = Left(v_importe, Len(v_importe) - 2)
                        pedido.moneda = v_moneda
                        pedido.medioPago = v_medioPago
                        pedido.codTransaccion = v_codTransaccion

                    dbApi.proc_InsertaPago_Copavic(v_nroFactura, pedido.importe, iNroMovimiento)
                    pedido.MovimientoNro = iNroMovimiento
                        pedido.codRetorno = "000"
                    pedido.desRetorno = "APROBADO"
                    pedido.Modificacion = Date.Now
                    dbApi.Pedido.InsertOnSubmit(pedido)
                        dbApi.SubmitChanges()
                        resp.codRetorno = "000" : resp.desRetorno = "APROBADO"
                        objeto = resp
                    End If
                End If
        Catch ex As Exception
            dbApi.proc_Log_Sistema(1, Nothing, Nothing, "DOCUMENTA", "PEDIDO " & v_codTransaccion, "DOCUMENTA", Nothing)
            If ex.Message.ToString Like "*El importe no debe superar al monto de la cuota*" Then
                resp.codRetorno = "021" : resp.desRetorno = "El importe no debe superar al monto de la cuota"
            Else
                resp.codRetorno = "020" : resp.desRetorno = "Error en API"
            End If

            objeto = resp

        End Try
        EnviarJSON(objeto)
    End Sub
End Class
