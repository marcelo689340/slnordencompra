﻿
<System.ComponentModel.DefaultBindingProperty("Text")> _
Partial Class Calendario
    Inherits System.Web.UI.UserControl
    Private m_Max As String = "31/12/2100"
    Private m_Min As String = "01/01/1900"
    Private m_Value As String = ""
    Private m_Actual As String = ""
    Private m_CurrentFecha As String = ""
    Private m_ValidationGroup As String = ""
    Private m_Enabled As Boolean = True
    Private m_Requerido As Boolean = True
    Public Event ValueChanged As System.EventHandler

    Public Property MinValue() As String
        Get
            Return m_Min
        End Get
        Set(ByVal MinValue As String)
            m_Min = MinValue
            If Requerido = True Then
                RangeValidator1.MinimumValue = MinValue
            End If
        End Set
    End Property

    Public Property MaxValue() As String
        Get
            Return m_Max
        End Get
        Set(ByVal MaxValue As String)
            m_Max = MaxValue
            If Requerido = True Then
                RangeValidator1.MaximumValue = MaxValue
            End If
        End Set
    End Property


    Public Property ValidationGroup() As String
        Get
            Return m_ValidationGroup
        End Get
        Set(ByVal ValidationGroup As String)
            m_ValidationGroup = ValidationGroup
            mskValidadortxtFecha.ValidationGroup = ValidationGroup
            RequiredFieldValidator1.ValidationGroup = ValidationGroup
        End Set
    End Property

    Public Property Text() As String
        Get
            If m_Value = "" Then
                m_Value = Me.txtFecha.Text
            End If
            Return m_Value
        End Get
        Set(ByVal Text As String)
            m_Value = Text
            Me.txtFecha.Text = String.Format("{0:d}", Text)
        End Set
    End Property

    Public Sub SetFocus(ByVal sender As Object, e As System.EventArgs)
        Dim sm As ScriptManager
        sm = ScriptManager.GetCurrent(Me.Page)
        sm.SetFocus(txtFecha.ClientID)
    End Sub

    Protected Sub txtFEcha_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFecha.TextChanged
        Me.Text = txtFecha.Text
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.txtFecha.Text = Me.Text.ToString
            m_Value = Text.ToString
            Me.mskValidadortxtFecha.ValidationGroup = ValidationGroup
            Me.RangeValidator1.ValidationGroup = ValidationGroup
            MinValue = m_Min
            MaxValue = m_Max
            
        End If
    End Sub

    Sub New()

    End Sub

    Public Property Enabled() As String
        Get
            Return m_Enabled
        End Get
        Set(ByVal Enabled As String)
            m_Enabled = Enabled
            Me.txtFecha.Enabled = Enabled

        End Set
    End Property

    Public Property Requerido() As Boolean
        Get
            Return m_Requerido
        End Get
        Set(ByVal Requerido As Boolean)
            m_Requerido = Requerido
            If Requerido = False Then
                RequiredFieldValidator1.Visible = False
                RangeValidator1.Visible = False

            Else
                RequiredFieldValidator1.Visible = True
                RangeValidator1.Visible = True
                mskValidadortxtFecha_ValidatorCalloutExtender.Enabled = False
            End If
        End Set
    End Property

End Class
