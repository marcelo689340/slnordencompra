﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="textNumero.ascx.vb" Inherits="controles_textNumero" %>

<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
<%--<script type="text/javascript" src="../js/jquery-1.11.2.min.js"></script>--%>
<script type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../js/jquery.number.js"></script>

<script type="text/javascript"> //language="JavaScript">


    function puntos(donde, decimales) {
        $(donde).number(true, decimales, ',', '.')
    }

</script>
<asp:TextBox ID="txtctbmNro" runat="server" Width="180px">0</asp:TextBox>
<asp:PlaceHolder ID="PlaceHolder1" runat="server">
    <asp:RangeValidator ID="RangeValidator1" runat="server"
        ControlToValidate="txtctbmNro" ErrorMessage="*" Type="Currency" CssClass="validador_error"></asp:RangeValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
        ControlToValidate="txtctbmNro" ErrorMessage="No Cargo ningun valor" Text="*" CssClass="validador_error"></asp:RequiredFieldValidator>
    <aspAjax:ValidatorCalloutExtender
        runat="Server"
        ID="PNRequerido"
        TargetControlID="RequiredFieldValidator1"
        CloseImageUrl="close.gif" CssClass="customCalloutStyle"  />
</asp:PlaceHolder>


