﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BuscarTablaNew.ascx.vb" Inherits="control_BuscarTablaNewBckp" %>
<style type="text/css">
    table {
        border-style: none;
        border-color: inherit;
        border-width: medium;
        border-collapse: collapse;
        width: 222px;
        height: 22px;
    }

    td {
        padding: 0;
    }

    .auto-style1 {
        height: 48px;
    }

    .auto-style2 {
        height: 48px;
        width: 227px;
    }

    #paginacion {
        margin 0px;
        border: 1px solid #CCC;
        background-color: #E0E0E0;
        padding: 5px;
        overflow: hidden;
        width: 634px;
    }

    .derecha {
        float: right;
    }

    .izquierda {
        float: left;
        padding: 0px;
        width: 569px;
    }
</style>
<script type="text/javascript">
    function clickButton(e, buttonid) {
        //click on the button an enter key  
        var evt = e ? e : window.event;
        var bt = document.getElementById(buttonid);
        if (bt) {
            if (evt.keyCode == 13) {
                bt.click();
                return false;
            }
        }
    }
</script>
<asp:PlaceHolder ID="PlaceHolder2" runat="server">
    <div style="width: 149px; height: 22px; margin: 0px; padding: 0px;">
        <asp:UpdatePanel ID="upCarga" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <%--<div id="paginacion">--%>
                <div class="izquierda">
                    <asp:TextBox ID="txtBuscarCabecera" runat="server"></asp:TextBox>
                    <asp:ImageButton ID="imbVaciar" runat="server" CausesValidation="False" ImageUrl="~/App_Themes/css/img/standar/B_RECYCLE.png" ToolTip="Eliminar Seleccion" Width="16px" Visible="False" />
                    <asp:ImageButton ID="imbBuscarCabecera" runat="server" CausesValidation="False" ImageUrl="~/App_Themes/css/img/standar/search.png" Style="height: 16px" ToolTip="Buscar" Width="16px" />
                    <asp:ImageButton ID="imbAgregar" runat="server" CausesValidation="False" ImageUrl="~/App_Themes/css/img/standar/agregar01.png" ToolTip="Buscar" Width="16px" Visible="False" />
                    <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txtBuscarCabecera" ErrorMessage="****" CssClass="validador_error"></asp:RequiredFieldValidator>
                    <asp:Literal ID="ltlDescripcion" runat="server"></asp:Literal>
                </div>
                </div>

            <%--</div>--%>

                <asp:PlaceHolder ID="PlaceHolder1" runat="server">

                    <aspAjax:ModalPopupExtender ID="mdlPopup" runat="server"
                        BackgroundCssClass="modalBackground" CancelControlID="divCerrar"
                        PopupControlID="pnlPanelNew" TargetControlID="btnShowPopup" Drag="True">
                    </aspAjax:ModalPopupExtender>
                    <asp:SqlDataSource ID="SqldeGrilla" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>"
                        SelectCommand="SELECT [id_articulo],[id_lista_precio],[art_descripcion],[listaprecio],[monto],[moneda] FROM [vwPrecio_Articulo] where [id_articulo] like @filtro or [art_descripcion] like '%' + @filtro + '%'">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="filtro" Name="filtro"
                                PropertyName="Value" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:HiddenField ID="filtro" runat="server" />
                    <%--Style="display: none"--%>
                    <asp:Panel ID="pnlPanelNew" runat="server" CssClass="modalPopup" Font-Italic="False" Style="display: none"
                        ScrollBars="Auto" Width="800px" Height="493px">
                        <div id="divCerrar" style="display: none"></div>
                        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
                        <div class="modalPopup titulomodal">
                            BUSCAR TABLA
                     <div class="modalPopup titulomodalderecho">
                         <asp:ImageButton ID="imgCerrar" runat="server"
                             ImageUrl="~/App_Themes/css/img/standar/crossIcon.png" CausesValidation="False" DescriptionUrl="Cerrar" />
                     </div>
                        </div>
                        Buscar&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtBuscar" runat="server" Width="250px"></asp:TextBox>
                        <asp:ImageButton ID="imgBuscarModal" runat="server" ImageUrl="~/App_Themes/css/img/standar/search.png" Width="20px" CausesValidation="False" Height="20px" />
                        <div class="body">

                            <asp:GridView ID="Grilla" runat="server" AllowPaging="True" Width="100%"
                                DataSourceID="SqldeGrilla" GridLines="Horizontal"
                                AllowSorting="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No existen registros con los filtros deseados...
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </asp:PlaceHolder>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="imgCerrar" />
                <asp:AsyncPostBackTrigger ControlID="imgBuscarModal" />

                <%-- <asp:AsyncPostBackTrigger EventName ="Show" />--%>
                <%--  <asp:PostBackTrigger ControlID="imbBuscarCabecera" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:PlaceHolder>

