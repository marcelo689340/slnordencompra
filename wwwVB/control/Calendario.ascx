﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Calendario" CodeFile="Calendario.ascx.vb" %>
<%--<div style="width: 160px;max-height:26px;margin-top:-5px">--%>
<div class="form-inline" style="max-height:30px;display:inline-block">
    <asp:TextBox ID="txtFecha" runat="server" Width="90px" Style="vertical-align:super" placeholder="Fecha"></asp:TextBox>
    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Cargar Fecha" ControlToValidate="txtFecha" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator1">
        </ajaxToolkit:ValidatorCalloutExtender>
        <ajaxToolkit:MaskedEditValidator ID="mskValidadortxtFecha" runat="server" ControlExtender="mskEdittxtFecha"
            ControlToValidate="txtFecha" ErrorMessage="*" 
            IsValidEmpty="true" EmptyValueMessage="*" SetFocusOnError="True" InvalidValueMessage="*" Display="Dynamic" Text="*"></ajaxToolkit:MaskedEditValidator>
        <ajaxToolkit:ValidatorCalloutExtender ID="mskValidadortxtFecha_ValidatorCalloutExtender" runat="server"
            TargetControlID="mskValidadortxtFecha">
        </ajaxToolkit:ValidatorCalloutExtender>
         <ajaxToolkit:CalendarExtender ID="CalendartxtFecha" runat="server" CssClass="MyCalendar"
        Format="dd/MM/yyyy" PopupPosition="BottomLeft" TargetControlID="txtFecha">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskEdittxtFecha" runat="server"
          Mask="99/99/9999" MaskType="Date"
         TargetControlID="txtFecha">
    </ajaxToolkit:MaskedEditExtender>
        
<%--     
      <ajaxToolkit:CalendarExtender ID="CalendartxtFecha" runat="server" CssClass="MyCalendar"
        Format="dd/MM/yyyy" PopupButtonID="btntxtFecha" PopupPosition="right" TargetControlID="txtFecha">
    </ajaxToolkit:CalendarExtender>

    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" AcceptNegative="Left"
        DisplayMoney="Left" ErrorTooltipEnabled="True" InputDirection="RightToLeft" Mask="99/99/9999"
        MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
        OnInvalidCssClass="MaskedEditError" TargetControlID="txtFecha">
    </ajaxToolkit:MaskedEditExtender>--%>

            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtFecha" ErrorMessage="Fecha invalida o fuera de rango" Type="Date" Text="*"></asp:RangeValidator>
        <ajaxToolkit:ValidatorCalloutExtender ID="RangeValidator1_ValidatorCalloutExtender" runat="server" TargetControlID="RangeValidator1">
        </ajaxToolkit:ValidatorCalloutExtender>
    </asp:PlaceHolder>

</div>

