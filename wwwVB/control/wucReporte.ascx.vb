﻿
Partial Class controles_wucReporte
    Inherits UserControl
    Private m_pagina As String = ""
    Private m_ControlError As String = ""

    Public Property Pagina() As String
        Get
            If m_pagina = "" Then
                m_pagina = HiControl.Value
            End If
            Return m_pagina
        End Get
        Set(ByVal Pagina As String)
            m_pagina = Pagina
            HiControl.Value = Pagina
        End Set
    End Property

    Sub New()

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            m_pagina = Pagina.ToString
            'frame1.Attributes.Add("src", Pagina)
        End If
    End Sub

    Public Sub Show(sPagina As String)
        Pagina = sPagina
        frame1.Attributes.Add("src", Pagina)
        If UCase(sPagina) Like "*SALIDA=PANTALLA*" Or InStr(UCase(sPagina), "SALIDA=") = 0 Then
            Me.mdplPrint.Show()
        End If

    End Sub

    Public Event DespuesdeMostrar As EventHandler
    Public Sub DespuesdeMostrar_(ByVal sender As Object, e As System.EventArgs)
        RaiseEvent DespuesdeMostrar(sender, e)
    End Sub

    Protected Sub btnCerrar_Click(sender As Object, e As System.EventArgs) Handles ImageButton1.Click
        Me.mdplPrint.Hide()
        DespuesdeMostrar_(sender, e)
    End Sub


End Class
