﻿

<%@ Control Language="VB" AutoEventWireup="false" Inherits="BuscarDatos" Codefile="BuscarDatos.ascx.vb" %>


<style type="text/css">
    .cuadro
    {
        width: 250px;
    }

        .body
        {   
            padding: 1px;
            
        }
  .tablaEstilo1
{
  padding: 0px;  
}

.tablaEstilo1 td
{
   padding: 0px;
   text-align: left;
}

.tablaEstilo1 th
{
    padding: 0px;
    text-align: left;
    background-color: #C1E0FF;
    font-family: 'Trebuchet MS';
}

    .auto-style1 {
        margin-top: 0px;
    }

    </style>
    
    <asp:UpdatePanel ID="UpdConrolBuscar" runat="server">
    <ContentTemplate><asp:panel runat="server" ID="pnlBuscar">
  <div style="width:400px;max-width:900px;max-height:24px;margin-left:0px">
                        <table class="estiloTabla1" border="0">
                            <tr style="vertical-align:top">
                                <td style="vertical-align:text-top;margin-left:1px!important">
                                  <asp:TextBox ID="txtID" runat="server" style="display:none" Enabled="False" Width="0px"></asp:TextBox>
                                    <asp:TextBox ID="txtBuscarCara" style="margin-left:-3px;margin-top:-2px" 
                                         runat="server" 
                                        AutoPostBack="True" CssClass="textoMayuscula" 
                                        Width="150px" Height="16px"></asp:TextBox>
                                    <aspAjax:TextBoxWatermarkExtender ID="txtBuscarCara_TextBoxWatermarkExtender" runat="server" TargetControlID="txtBuscarCara" WatermarkCssClass="watermarkCSS" WatermarkText="Texto a Buscar">
                                    </aspAjax:TextBoxWatermarkExtender>
                                    <aspAjax:AutoCompleteExtender ID="txtBuscarCara_AutoCompleteExtender" runat="server" 
                                        CompletionInterval="100" 
                                        CompletionListCssClass="clstBuscadorCssClass01" 
                                        CompletionListHighlightedItemCssClass="liBuscadorTextoH01" 
                                        CompletionListItemCssClass="liBuscadorTexto01" 
                                        CompletionSetCount="10" 
                                        EnableCaching="false" 
                                        FirstRowSelected="true" 
                                        MinimumPrefixLength="3" 
                                        ServiceMethod="BuscarDatos" 
                                        ServicePath="~/webserviceDatos.asmx" 
                                        TargetControlID="txtBuscarCara" UseContextKey="true" />
                                    
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbVaciar" runat="server" style="margin-top:-3px;margin-left:-3px;width:20px" CausesValidation="False" ImageUrl="~/App_Themes/img/estandar/reciclado02.png" ToolTip="Eliminar Seleccion"  Visible="False" />
                                    <asp:ImageButton ID="imbAgregar" runat="server" style="margin-top:-3px;margin-left:-3px" CausesValidation="False" ImageUrl="~/App_Themes/img/estandar/agregar01.png" ToolTip="Agregar Registro"  Visible="False" />
                                </td>
                                <td style="vertical-align:top">
                                    <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txtBuscarCara" Display="Dynamic" ErrorMessage="Debe cargar datos para busqueda" SetFocusOnError="True">**</asp:RequiredFieldValidator>
                                    
                                </td>
                                <td style="vertical-align:top">
                                    <asp:RequiredFieldValidator ID="rfv0" runat="server" ControlToValidate="txtID" Display="Dynamic" ErrorMessage="Debe cargar datos para busqueda">**</asp:RequiredFieldValidator>
                                    <aspAjax:ValidatorCalloutExtender ID="rfv_ValidatorCalloutExtenderBuscarDatos" runat="server" TargetControlID="rfv">
                                    </aspAjax:ValidatorCalloutExtender>
                             
                                </td>
                            </tr>
                        </table>

        
        
        <asp:HiddenField ID="HiRequerido" runat="server" />
        <asp:HiddenField ID="HiCampo" runat="server" />
      
                        <asp:HiddenField ID="hiAnho" runat="server" />
      
                        <asp:HiddenField ID="hiMostrarSelect" runat="server" />
                        <asp:HiddenField ID="hiSelectAutocomplete" runat="server" />
      
  </div>
</asp:panel>
      </ContentTemplate>
</asp:UpdatePanel>





