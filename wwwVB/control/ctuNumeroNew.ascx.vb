﻿Imports System.ComponentModel
<System.ComponentModel.DefaultBindingProperty("Text")> _
Partial Class control_ctuNumeroNew
    Inherits System.Web.UI.UserControl
    '<Browsable(True), Category("Mi Propiedad"), DefaultValue("0"), Description("")>
    'Public Property MinValue() As String
    '    Get
    '        Return ViewState("Min")
    '    End Get
    '    Set(ByVal MinValue As String)
    '        ViewState("Min") = MinValue
    '        'Me.txtNumero.MinValue = MinValue
    '        rvRango.MinimumValue = MinValue
    '    End Set
    'End Property
    '<Browsable(True), Category("Mi Propiedad"), DefaultValue("0"), Description("Validator Group")>
    'Public Property MaxValue() As String
    '    Get
    '        Return ViewState("Max")
    '    End Get
    '    Set(ByVal MaxValue As String)
    '        ViewState("Max") = MaxValue
    '        'Me.txtNumero.MaxValue = MaxValue
    '        rvRango.MaximumValue = MaxValue
    '    End Set
    'End Property
    '<Browsable(True), Category("Mi Propiedad"), DefaultValue("270"), Description("")>
    'Public Property Width() As String
    '    Get
    '        Return ViewState("Width")
    '    End Get
    '    Set(ByVal Width As String)
    '        ViewState("Width") = Width
    '        'Me.txtNumero.MaxValue = MaxValue
    '        Me.txtNumero.Width = Width
    '    End Set
    'End Property
    '<Browsable(True), Category("Mi Propiedad"), DefaultValue("0"), Description("")>
    'Public Property Decimales() As String
    '    Get
    '        Return ViewState("Decimales")
    '    End Get
    '    Set(ByVal Decimales As String)
    '        ViewState("Decimales") = Decimales
    '        'Me.txtNumero.MinValue = MinValue
    '        'Me.txtNumero.cantidadDecimales = Decimales
    '    End Set

    'End Property
    '<Browsable(True), Category("Mi Propiedad"), DefaultValue(""), Description("Validator Group")>
    'Public Property ValidationGroup() As String
    '    Get
    '        Return ViewState("ValidationGroup")
    '    End Get
    '    Set(ByVal ValidationGroup As String)
    '        ViewState("ValidationGroup") = ValidationGroup
    '        'Me.AbiTextBoxNumerico1.MinValue = MinValue
    '        Me.rvRango.ValidationGroup = ValidationGroup
    '        Me.rfv.ValidationGroup = ValidationGroup
    '    End Set

    'End Property
    '<Browsable(True), Category("Mi Propiedad"), DefaultValue("True"), Description("Validator Group")>
    'Public Property Enabled() As Boolean
    '    Get
    '        Return Convert.ToBoolean(ViewState("Enabled"))
    '    End Get
    '    Set(ByVal Enabled As Boolean)
    '        ViewState("Enabled") = Convert.ToString(Enabled)
    '        Me.txtNumero.Enabled = Enabled
    '    End Set
    'End Property

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        'Dim sComando As String
        Dim Decimales As String = "2"

        'sComando = "puntos(this, " & Decimales & ")"
        'txtNumero.Attributes.Add("onfocus", sComando)
        txtNumero.Attributes.Add("onkeyup", "Thousand(this,this.value.charAt(this.value.length-1));")

    End Sub
End Class
