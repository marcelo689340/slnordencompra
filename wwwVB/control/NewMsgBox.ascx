﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewMsgBox.ascx.vb" Inherits="control_NewMsgBox" %>
<div style="width: 20px; height: 20px;">
    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <aspAjax:ModalPopupExtender ID="mpeNroComp" runat="server" Enabled="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPanel" TargetControlID="btnShowPopup"
            CancelControlID="btnAceptarMensajeNormal">
        </aspAjax:ModalPopupExtender>
        <asp:UpdatePanel ID="upCarga" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlPanel" runat="server" class="conZindex501" Style="display: none">
                    <div id="divCerrar" style="display: none"></div>
                    <div class="modalPopup titulomodal" style="background-color: red; color: white;">
                        MSGBOX
                     <div class="modalPopup titulomodalderecho" style="display: none">
                         <asp:ImageButton ID="imgCerrar" runat="server" ImageUrl="~/App_Themes/css/img/standar/crossIcon.png" CausesValidation="False" DescriptionUrl="Cerrar" />
                     </div>
                    </div>
                    <div class="cuadro02 conPadding05">
                        <div style="margin: 20px 0px 20px 0px;">
                            <asp:Label ID="lblMensajeNormal" CssClass="mensajes03" runat="server"></asp:Label>
                        </div>
                        <div class="conPaddingTop05 conPaddingBottom05" style="text-align: center;">
                            <asp:Button ID="btnAceptarMensajeNormal" runat="server" Text="Aceptar" CssClass="boton06" CausesValidation="false" />
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnAceptarMensajeNormal" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:PlaceHolder>
</div>
