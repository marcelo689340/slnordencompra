﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="controles_wucReporte" CodeFile="wucReporte.ascx.vb" %>
<style type="text/css">
    .style1 {
        width: 128px;
        height: 15px;
    }
</style>
<asp:HiddenField ID="HiControl" runat="server" />
<asp:UpdatePanel ID="udpReporte" runat="server">
    <ContentTemplate>
        <aspAjax:ModalPopupExtender ID="mdplPrint" runat="server"
            BackgroundCssClass="modalBackground" CancelControlID="divCerrar"
            PopupControlID="PanelPrint" TargetControlID="btnShowPopup">
        </aspAjax:ModalPopupExtender>
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <div id="divCerrar" style="display: none"></div>
        <asp:Panel ID="PanelPrint" Style="display: none" CssClass="modalPopup" runat="server" Height="95%" Width="95%">
            <div class="modalPopup titulomodal">
                VISTA DE IMPRESION<div class="modalPopup titulomodalderecho">
                    <asp:ImageButton ID="ImageButton1" runat="server"
                        ImageUrl="~/App_Themes/css/img/standar/crossIcon.png" CausesValidation="False" DescriptionUrl="Cerrar" ValidationGroup="xaxa" />
                </div>
            </div>
            <iframe id="frame1" runat="server"
                style="width: 95%; height: 95%; position: relative"
                frameborder="0"></iframe>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
