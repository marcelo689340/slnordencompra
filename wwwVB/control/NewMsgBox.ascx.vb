﻿Imports System.ComponentModel

Partial Class control_NewMsgBox
    Inherits System.Web.UI.UserControl

    <Browsable(True), Category("Datos"), DefaultValue("a"), Description("")>
    Public Property MensajeError() As String
        Get
            Dim C As String = CStr(ViewState("Text"))
            If C Is Nothing Then C = String.Empty
            Return C
        End Get
        Set(ByVal value As String)
            ViewState("DataText") = value
            lblMensajeNormal.Text = value
        End Set
    End Property
    Public Sub Show()
        mpeNroComp.Show()
    End Sub
    Public Event DespuesdeMostrar As EventHandler
    Public Sub DespuesdeMostrar_(ByVal sender As Object, e As System.EventArgs)
        RaiseEvent DespuesdeMostrar(sender, e)
    End Sub
#Region "EVENTO DE CONTROLES"

    Public Event CapturarDatosCliente As EventHandler
    Public Event EnviarCliente As EventHandler
    Public Sub CapturarDatosClienteClick(ByVal sender As Object, e As System.EventArgs)
        RaiseEvent CapturarDatosCliente(sender, e)
    End Sub
    Public Sub EnviarClienteClick(ByVal sender As Object, e As System.EventArgs)
        RaiseEvent EnviarCliente(sender, e)
    End Sub
#End Region

End Class
