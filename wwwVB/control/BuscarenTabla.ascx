﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="BuscarenTabla" CodeFile="BuscarenTabla.ascx.vb" %>


<style type="text/css">
    .cuadro {
        width: 250px;
    }

    .body {
        padding: 1px;
    }
    .margen{
        margin-left:0px;
    }
    .Invisible {}
</style>
<asp:PlaceHolder ID="PlaceHolder1" runat="server">
<asp:UpdatePanel ID="UpdConrolBuscar" runat="server">
    <ContentTemplate>
        <div style="width: 400px; max-width: 900px; margin-left: -3px">
            <table border="0" style="margin-left: 0px">
                <tr>
                    <td valign="top">
                        <asp:TextBox ID="txtID" runat="server" CssClass="Invisible" Enabled="False" Width="0px"></asp:TextBox>
                        <asp:TextBox ID="txtBuscarCara" runat="server" AutoPostBack="True" CssClass="textoMayuscula margen " Width="107px"></asp:TextBox>
                        <aspAjax:AutoCompleteExtender ID="txtBuscarCara_AutoCompleteExtender" runat="server" CompletionInterval="100" 
                            CompletionListCssClass="clstBuscadorCssClass01" CompletionListHighlightedItemCssClass="liBuscadorTextoH01" 
                            CompletionListItemCssClass="liBuscadorTexto01" CompletionSetCount="10" EnableCaching="false" FirstRowSelected="true" 
                            MinimumPrefixLength="3" ServiceMethod="BuscarDatos" ServicePath="../webserviceDatos.asmx" 
                            TargetControlID="txtBuscarCara" UseContextKey="true" />
                        <asp:DetailsView ID="DetailsView1" runat="server" CellPadding="2" CellSpacing="2" DataSourceID="sqldeForm">
                        </asp:DetailsView>
                    </td>
                    <td valign="top">
                        <asp:ImageButton ID="imbVaciar" runat="server" CausesValidation="False" ImageUrl="~/App_Themes/css/img/standar/B_RECYCLE.png" ToolTip="Eliminar Seleccion" Width="16px" Visible="False" />
                    </td>
                    <td valign="top">
                        <asp:ImageButton ID="imbBuscar1" runat="server" CausesValidation="False" ImageUrl="~/App_Themes/css/img/standar/search.png" Style="height: 16px" ToolTip="Buscar" Width="16px" />
                    </td>
                    <td valign="top">
                        <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txtID" ErrorMessage="****" CssClass="validador_error"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <asp:SqlDataSource ID="sqldeForm" runat="server"
                ConnectionString="<%$ ConnectionStrings:bd_Orden %>"
                ProviderName="<%$ ConnectionStrings:bd_Orden.ProviderName %>"
                SelectCommand="SELECT vw_wClientes.NUMERO FROM vw_wClientes LEFT OUTER JOIN vw_wVendedor ON vw_wClientes.VENDEDOR = vw_wVendedor.Cod LEFT OUTER JOIN vw_wClientes_Saldos ON vw_wClientes.NUMERO = vw_wClientes_Saldos.CLIENTE WHERE (vw_wClientes.NUMERO LIKE @FILTRO) ">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtID" Name="FILTRO" PropertyName="Text" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqldeGrilla" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>"
                SelectCommand="SELECT vw_wClientes.NUMERO, vw_wClientes.RAZONSOC, vw_wClientes.CUIT, vw_wClientes.IMPCREDITO, ISNULL(vw_wClientes_Saldos.SALDOML, 0) AS SALDOML, vw_wClientes.CONDVTA FROM vw_wClientes LEFT OUTER JOIN vw_wClientes_Saldos ON vw_wClientes.NUMERO = vw_wClientes_Saldos.CLIENTE WHERE (vw_wClientes.NUMERO LIKE '%' + @FILTRO + '%')">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtBuscar" Name="FILTRO"
                        PropertyName="Text" />

                </SelectParameters>
            </asp:SqlDataSource>

            <asp:Panel ID="pnlPanel" runat="server" CssClass="modalPopup" Font-Italic="False"
                ScrollBars="Auto" Style="display: none" Width="700px" Height="80%">
                <div id="divCerrar" style="display: none"></div>
                <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
                <div class="modalPopup titulomodal">
                    BUSCAR TABLA
                     <div class="modalPopup titulomodalderecho">
                            <asp:ImageButton ID="imgCerrar" runat="server"
                                      ImageUrl="~/App_Themes/css/img/standar/crossIcon.png" CausesValidation="False" DescriptionUrl="Cerrar" />
                     </div>
                </div>
                Buscar&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtBuscar" runat="server" Width="250px"></asp:TextBox>
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/App_Themes/css/img/standar/search.png" Width="20px" CausesValidation="False" />
                    <div class="body">
                        <asp:GridView ID="Grilla" runat="server" AllowPaging="True"
                            DataSourceID="SqldeGrilla" OnSelectedIndexChanged="GrillaCliente_SelectedIndexChanged"
                            PageSize="10" GridLines="Horizontal"
                            AllowSorting="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                            </Columns>
                            <EmptyDataTemplate>
                                No existen registros con los filtros deseados...
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
            </asp:Panel>

            <asp:HiddenField ID="HiRequerido" runat="server" />
            <asp:HiddenField ID="HiCampo" runat="server" />
            <asp:HiddenField ID="hdfID" runat="server" />
            <aspAjax:ModalPopupExtender ID="mdlPopup" runat="server"
                BackgroundCssClass="modalBackground" CancelControlID="divCerrar"
                PopupControlID="pnlPanel" TargetControlID="btnShowPopup" Drag="True">
            </aspAjax:ModalPopupExtender>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:PlaceHolder>






