﻿Imports System.ComponentModel

<System.ComponentModel.DefaultBindingProperty("DataText")> _
Partial Class control_BuscarTablaNewBckp
    Inherits System.Web.UI.UserControl
#Region "Propiedades"
    <Browsable(True), Category("Mi Propiedad"), DefaultValue("~/App_Themes/css/img/standar/agregar01.png"), Description("MostrarBotonAgregar")>
    Public Property ImagenURLBotonAgregar() As String
        Get
            Dim img As String = CStr(ViewState("ImagenURLBotonAgregar"))
            img = IIf(img Is Nothing, "~/App_Themes/css/img/standar/agregar01.png", CStr(ViewState("ImagenURLBotonAgregar")))
            Return Convert.ToBoolean(ViewState("ImagenURLBotonAgregar"))
        End Get
        Set(ByVal ImagenURLBotonAgregar As String)
            ViewState("ImagenURLBotonAgregar") = ImagenURLBotonAgregar
            Me.imbAgregar.ImageUrl = ImagenURLBotonAgregar
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue("False"), Description("MostrarBotonAgregar")>
    Public Property MostrarBotonAgregar() As Boolean
        Get
            Return Convert.ToBoolean(ViewState("MostrarBotonAgregar"))
        End Get
        Set(ByVal MostrarBotonAgregar As Boolean)
            ViewState("MostrarBotonAgregar") = Convert.ToString(MostrarBotonAgregar)
            Me.imbAgregar.Visible = MostrarBotonAgregar
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue(""), Description("Tamaños del panel")>
    Public Property Height() As String
        Get
            ViewState("Height") = Convert.ToString(pnlPanelNew.Height)
            Return ViewState("Height")
        End Get
        Set(ByVal Height As String)
            ViewState("Height") = Height
            pnlPanelNew.Height = Height
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue("100px"), Description("Tamaños Textbox")>
    Public Property Width_textbox() As String
        Get
            ViewState("Width_textbox") = Convert.ToString(txtBuscarCabecera.Width)
            Return ViewState("Width_textbox")
        End Get
        Set(ByVal Width_textbox As String)
            ViewState("Width_textbox") = Width_textbox
            txtBuscarCabecera.Width = Width_textbox
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue(""), Description("Tamaños del panel")>
    Public Property Width() As String
        Get
            ViewState("width") = Convert.ToString(pnlPanelNew.Width)
            Return ViewState("width")
        End Get
        Set(ByVal Width As String)
            ViewState("width") = Width
            pnlPanelNew.Width = Width
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue(""), Description("Validator Group")>
    Public Property ValidationGroup() As String
        Get
            Return ViewState("ConnectionString")
        End Get
        Set(ByVal ValidationGroup As String)
            ViewState("ConnectionString") = ValidationGroup
            rfv.ValidationGroup = ValidationGroup
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue("bd_Orden"), Description("Tamaños del panel")>
    Public Property ConnectionString() As String
        Get
            Return ViewState("ConnectionString")
        End Get
        Set(ByVal ConnectionString As String)
            ViewState("ConnectionString") = ConnectionString
            Dim strConexion As String
            Dim connectionStrings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings
            strConexion = connectionStrings(ConnectionString).ToString()
            Me.SqldeGrilla.ConnectionString = strConexion
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue("True"), Description("Tamaños del panel")>
    Public Property ShowBotonBuscar() As Boolean
        Get
            Return Convert.ToBoolean(ViewState("ShowBotonBuscar"))
        End Get
        Set(ByVal ShowBotonBuscar As Boolean)
            ViewState("ShowBotonBuscar") = Convert.ToString(ShowBotonBuscar)
            Me.imgBuscarModal.Visible = ShowBotonBuscar
            Me.imbBuscarCabecera.Visible = ShowBotonBuscar
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue("True"), Description("Tamaños del panel")>
    Public Property Enabled() As Boolean
        Get
            If IsNothing(ViewState("Enabled")) Then ViewState("Enabled") = "True"
            Return Convert.ToBoolean(ViewState("Enabled"))
        End Get
        Set(ByVal Enabled As Boolean)
            ViewState("Enabled") = Convert.ToString(Enabled)
            'RequiredFieldValidator11.ValidationGroup = ValidationGroup
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue("False"), Description("Tamaños del panel")>
    Public Property Mostrar_Descripcion() As Boolean
        Get
            Return Convert.ToBoolean(ViewState("Mostrar_Descripcion"))
        End Get
        Set(ByVal Mostrar_Descripcion As Boolean)
            ViewState("Mostrar_Descripcion") = Convert.ToString(Mostrar_Descripcion)
            Me.ltlDescripcion.Visible = Mostrar_Descripcion
            'RequiredFieldValidator11.ValidationGroup = ValidationGroup
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue("False"), Description("Texto Requerido")>
    Public Property EnviaParametro() As Boolean
        Get
            Return Convert.ToBoolean(ViewState("EnviaParametro"))
        End Get
        Set(ByVal EnviaParametro As Boolean)
            ViewState("EnviaParametro") = Convert.ToString(EnviaParametro)
            rfv.Enabled = EnviaParametro
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue("True"), Description("Texto Requerido")>
    Public Property Requerido() As Boolean
        Get
            Return Convert.ToBoolean(ViewState("Requerido"))
        End Get
        Set(ByVal Requerido As Boolean)
            ViewState("Requerido") = Convert.ToString(Requerido)
            rfv.Enabled = Requerido
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue(""), Description("")>
    Public Property Grilla_Select() As String
        Get
            Dim C As String = CStr(ViewState("Grilla_Select"))
            If C Is Nothing Then C = String.Empty
            Return C
        End Get
        Set(ByVal Grilla_Select As String)
            ViewState("Grilla_Select") = Grilla_Select
            'SqldeGrilla.SelectCommand = Grilla_Select
        End Set
    End Property
    <Browsable(True), Category("Mi Propiedad"), DefaultValue(""), Description("")>
    Public Property Mostrar_Select() As String
        Get
            Dim C As String = CStr(ViewState("Mostrar_Select"))
            If C Is Nothing Then C = String.Empty
            Return C
        End Get
        Set(ByVal Mostrar_Select As String)
            ViewState("Mostrar_Select") = Mostrar_Select
            SqldeGrilla.SelectCommand = Mostrar_Select
        End Set
    End Property
    <Browsable(True), Category("Datos"), DefaultValue(""), Description("")>
    Public Property DataText() As String
        Get
            Dim C As String = CStr(ViewState("DataText"))
            'If C Is Nothing Then C = String.Empty
            If C Is Nothing Then C = "0"
            Return C
        End Get
        Set(ByVal DataText As String)
            ViewState("DataText") = DataText
            filtro.Value = DataText
            Me.txtBuscarCabecera.Text = DataText
            If DataText = "" Then
                Me.ltlDescripcion.Text = String.Empty
                Me.txtBuscarCabecera.Enabled = True
                Me.imbBuscarCabecera.Visible = True
                Me.imbAgregar.Visible = MostrarBotonAgregar
                Me.imbVaciar.Visible = False
            End If
            'ltlDescripcion.Text = String.Empty
            'If txtBuscarCabecera.Text = "" Then
            '    ltlDescripcion.Text = String.Empty
            '    Cambiar_Estado_Boton_Cabecera(True)
            'End If
        End Set
    End Property
#End Region

#Region "Evento Privada"
    Public Sub Inicializar_ViewState()
        Consultar_Codigo()
    End Sub
    Protected Sub Cambiar_Estado_Boton_Cabecera(ByVal Estado As Boolean)
        If Estado = False Then
            txtBuscarCabecera.Enabled = False
            Me.imbBuscarCabecera.Visible = False
            Me.imbAgregar.Visible = False
            Me.imbVaciar.Visible = True
        Else
            'txtBuscarCabecera.Text = ""
            Me.DataText = ""
            ltlDescripcion.Text = String.Empty
            txtBuscarCabecera.Enabled = True
            Me.imbBuscarCabecera.Visible = True
            Me.imbAgregar.Visible = MostrarBotonAgregar
            Me.imbVaciar.Visible = False
        End If
    End Sub
    Protected Sub Cargar_Parametro(ByVal row As GridViewRow)
        If IsNothing(row) = False Then
            Me.DataText = row.Cells(1).Text
            Me.ltlDescripcion.Text = row.Cells(2).Text
            If EnviaParametro = True Then
                Dim iCant As Integer = row.Cells.Count - 1
                Dim Parametro As New List(Of String)
                Dim iColum As Integer = 0
                While iColum < (iCant)
                    If (row.Cells(iColum + 1).Text) = "" Then
                        Try
                            Dim chk_Publicar As CheckBox = DirectCast(row.Cells(iColum + 1).Controls(0), CheckBox)
                            Parametro.Add(Convert.ToString(chk_Publicar.Checked))
                        Catch ex As Exception

                        End Try
                    Else
                        Parametro.Add(row.Cells(iColum + 1).Text)
                    End If
                    iColum = iColum + 1
                End While
                Dim cParametro As New cParametro(Parametro)
                EnviarParametro(Me, cParametro)
            End If
            txtBuscar.Text = ""
            Cambiar_Estado_Boton_Cabecera(False)
        End If
    End Sub
    Protected Sub Consultar_Filtro(ByRef pGrilla As GridView)
        If Grilla.Rows.Count = 1 Then
            Cargar_Parametro(pGrilla.Rows(0))
            Cambiar_Estado_Boton_Cabecera(False)
            If Requerido = True Then
                Me.rfv.Enabled = False
            End If
            filtro.Value = ""
            txtBuscar.Text = ""
        Else
            mdlPopup.Show()
        End If
    End Sub
#End Region
#Region ""
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Inicializar_ViewState()
            'Refresh_Filtro()
            txtBuscarCabecera.Attributes.Add("onkeypress", "return clickButton(event,'" + imbBuscarCabecera.ClientID + "')")
            txtBuscar.Attributes.Add("onkeypress", "return clickButton(event,'" + imgBuscarModal.ClientID + "')")


        Else
            txtBuscarCabecera.Attributes.Add("onkeypress", "return clickButton(event,'" + imbBuscarCabecera.ClientID + "')")
            txtBuscar.Attributes.Add("onkeypress", "return clickButton(event,'" + imgBuscarModal.ClientID + "')")
        End If
        'If Enabled = False Then
        '    Me.txtBuscar.Enabled = False
        '    Me.imbAgregar.Enabled = False
        '    Me.imbVaciar.Enabled = False
        '    Me.imbBuscarCabecera.Enabled = False
        '    txtBuscar.Enabled = False
        'End If
        Me.ltlDescripcion.Visible = Mostrar_Descripcion
    End Sub
    Protected Sub Grilla_PageIndexChanged(sender As Object, e As EventArgs) Handles Grilla.PageIndexChanged
        mdlPopup.Show()
    End Sub
    Protected Sub Grilla_Sorted(sender As Object, e As EventArgs) Handles Grilla.Sorted
        mdlPopup.Show()
    End Sub
    Protected Sub imbBuscarCabecera_Click(sender As Object, e As ImageClickEventArgs) Handles imbBuscarCabecera.Click
        Refresh_Filtro()
        cmdVarios(sender, e)
    End Sub
    Private Sub Refresh_Filtro()
        filtro.Value = txtBuscarCabecera.Text
        If Consultar_Codigo() = False Then
            txtBuscar.Text = filtro.Value
            Consultar_like()
        End If
    End Sub
    Protected Function Consultar_like() As Boolean
        Dim resultado As Boolean = True
        Try
            SqldeGrilla.SelectCommand = Grilla_Select
            Grilla.DataBind()
            Consultar_Filtro(Grilla)
        Catch ex As Exception
            resultado = False
        End Try
        Return resultado
    End Function

    Protected Function Consultar_Codigo() As Boolean
        Dim resultado As Boolean
        Try
            SqldeGrilla.SelectCommand = Mostrar_Select
            Grilla.DataBind()
            resultado = Consultar_Filtro_New(Grilla)
        Catch ex As Exception
            resultado = False
        End Try
        Return resultado
    End Function
    Protected Function Consultar_Filtro_New(ByRef pGrilla As GridView) As Boolean
        If Grilla.Rows.Count = 1 Then
            Cargar_Parametro(pGrilla.Rows(0))
            Cambiar_Estado_Boton_Cabecera(False)
            If Requerido = True Then
                Me.rfv.Enabled = False
            End If
            filtro.Value = ""
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub imbVaciar_Click(sender As Object, e As ImageClickEventArgs) Handles imbVaciar.Click
        If Enabled = True Then LimpiarDato(sender, e)
    End Sub
    Protected Sub imgBuscarModal_Click(sender As Object, e As ImageClickEventArgs) Handles imgBuscarModal.Click
        filtro.Value = txtBuscar.Text
        Consultar_like()
        Consultar_Filtro(Grilla)
        cmdVarios(sender, e)
    End Sub
    Protected Sub Grilla_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles Grilla.SelectedIndexChanging
        Dim row As GridViewRow = Grilla.Rows(e.NewSelectedIndex)
        Cargar_Parametro(row)
    End Sub
    Protected Sub imgCerrar_Click(sender As Object, e As ImageClickEventArgs) Handles imgCerrar.Click
        cmdVarios(sender, e)
    End Sub
    Protected Sub imbAgregar_Click(sender As Object, e As ImageClickEventArgs) Handles imbAgregar.Click
        cmdAgregar(sender, e)
    End Sub
#End Region

#Region "EVENTO DE CONTROLES"
    Public Event cmdAgregar_Click As EventHandler
    Public Event cmdVarios_Click As EventHandler
    Public Event EnviarParametroClick As EventHandler
    Public Event LimpiarClick As EventHandler
    Public Event EnviarDatoClick As EventHandler
    Public Sub EnviarDatos(ByVal sender As Object, e As System.EventArgs)
        RaiseEvent EnviarDatoClick(sender, e)
    End Sub
    Public Sub EnviarParametro(ByVal sender As Object, ByVal e As cParametro)
        RaiseEvent EnviarParametroClick(sender, e)
    End Sub
    Public Sub LimpiarDato(ByVal sender As Object, e As System.EventArgs)
        Cambiar_Estado_Boton_Cabecera(True)
        Me.rfv.Enabled = Enabled
        RaiseEvent LimpiarClick(sender, e)
    End Sub
    Public Sub cmdVarios(ByVal sender As Object, e As System.EventArgs)
        Dim cPar As New cParametro()
        RaiseEvent cmdVarios_Click(sender, e)
    End Sub
    Public Sub cmdAgregar(ByVal sender As Object, e As System.EventArgs)
        Dim cPar As New cParametro()
        RaiseEvent cmdAgregar_Click(sender, e)
    End Sub
#End Region


End Class
