﻿
<ComponentModel.DefaultBindingProperty("Text")>
Public Class cuMensajeBox
    Inherits UserControl
    Dim _tipoMensaje As Byte = listaTipoMensaje.alerta
    Dim _color As String = listaColores.rojo
    Dim _redireccionar As String = String.Empty
    Dim _mensaje_value As String = ""



    Public Sub show()
        Select Case _tipoMensaje
            Case listaTipoMensaje.informacion
                lblMensajeNormal.Style.Add("background-image", "../Imagen/estandar/info09.png")
            Case listaTipoMensaje.exclamacion
                lblMensajeNormal.Style.Add("background-image", "Imagen/estandar/info01.png")
            Case listaTipoMensaje.e_error
                lblMensajeNormal.Style.Add("background-image", "Imagen/estandar/eliminar02.png")
            Case listaTipoMensaje.interrogacion
                lblMensajeNormal.Style.Add("background-image", "Imagen/estandar/info08.png")
            Case listaTipoMensaje.alerta
                lblMensajeNormal.Style.Add("background-image", "Imagen/estandar/alerta01.gif")
        End Select
        lblMensajeNormal.Text = _mensaje_value
        lblMensajeNormal.Style.Add("color", _color)
        If Not (Trim(_redireccionar) = "") Then
            ltJs.Text = "<script type=\""text/javascript\" > "" + " function redireccionar() {  window.location.href = \" + _redireccionar + " \ "" }</script>"
            mpeMensajeNormal.OnOkScript = "redireccionar()"
        End If
        mpeMensajeNormal.Show()
    End Sub

    Public Property redireccionar() As String
        Get
            Return _redireccionar
        End Get
        Set(ByVal Value As String)
            _redireccionar = Value
        End Set
    End Property

    Public ReadOnly Property ClienteID() As String
        Get
            Return lblMensajeNormal.ClientID
        End Get

    End Property

    Public ReadOnly Property behaviorID() As String
        Get
            Return mpeMensajeNormal.BehaviorID
        End Get

    End Property

    Public Property Mensaje() As String
        Get
            Return _mensaje_value
        End Get
        Set(ByVal Value As String)
            _mensaje_value = Value
            If _mensaje_value <> "" Then
                lblMensajeNormal.Text = _mensaje_value
            End If
        End Set
    End Property

    Public Property tipoMensaje() As Byte
        Get
            Return _tipoMensaje
        End Get
        Set(ByVal Value As Byte)
            _tipoMensaje = Value
        End Set
    End Property

    Public Property Color() As String
        Get
            Return _color
        End Get
        Set(ByVal Value As String)
            _color = Value
        End Set
    End Property

    Private Class listaTipoMensaje
        Public Const informacion As Byte = 1
        Public Const e_error As Byte = 2
        Public Const interrogacion As Byte = 3
        Public Const exclamacion As Byte = 4
        Public Const alerta As Byte = 5
    End Class

    Public Class listaColores
        Public Shared rojo As String = "#FF3300"
        Public Shared verde As String = "#00FF00"
        Public Shared azul As String = "#0000FF"
    End Class

End Class


