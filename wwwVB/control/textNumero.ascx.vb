﻿<System.ComponentModel.DefaultBindingProperty("Text")> _
Partial Class controles_textNumero
    Inherits System.Web.UI.UserControl
    Private m_Value As String = ""
    Private m_Max As Double = 0
    Private m_Min As Double = 0
    Private m_tamanho As Integer = 70
    Private m_Decimales As Integer = 0
    Private m_Enabled As Boolean
    Private m_ValidationGroup As String = ""
    Public Event Pagina_Recargada(ByVal sender As Object, ByVal e As EventArgs)

    Protected Sub Pagina_Refresca(ByVal sender As Object, ByVal e As EventArgs)
        RaiseEvent Pagina_Recargada(sender, EventArgs.Empty)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim Formato As String

            Formato = "{0:N0}"

            Formato = IIf(Decimales > 0, Replace(Formato, "N0", "N2"), Formato)

            If Not IsNothing(Text) Then

                If Decimales = 0 Then
                    Text = Replace(Text, ".", "")
                Else
                    Text = Replace(Text, ".", ",")
                End If

                If Decimales = 0 then
                    Me.txtctbmNro.Text = String.Format(Formato, Convert.ToDecimal(Text))
                Else
                    txtctbmNro.Text = Replace(Text, ".", ",")
                End If
            Else
                txtctbmNro.Text = ""
            End If

            m_Value = Me.Text.ToString

            If MaxValue = 0 And MinValue = 0 Then
                RangeValidator1.Visible = False
            Else
                RangeValidator1.Visible = True
                Me.RangeValidator1.MaximumValue = MaxValue
                Me.RangeValidator1.MinimumValue = MinValue

            End If
            Me.txtctbmNro.Width = Width

        End If
    End Sub

    Public Property Text() As String
        Get
            If m_Value = "" Then
                m_Value = Replace(Me.txtctbmNro.Text, ".", "")

            End If
            Dim Valor As String
            Valor = m_Value
            Valor = Replace(Valor, ".", "")
            Valor = Replace(Valor, ",", ".")
            Return Valor
        End Get
        Set(ByVal Value As String)
            Dim Formato As String
            Dim Monto As Double

            If Value = "" Then
                Value = "0"
            End If

            Formato = "{0:N0}"

            Formato = IIf(Decimales > 0, Replace(Formato, "N0", "N2"), Formato)

            Monto = 0
            If Value Like "*.*" Then
                Monto = Replace(Value, ".", "")
                Value = String.Format(Formato, Monto)
            Else
                Monto = Value
            End If

            m_Value = Monto
            Me.txtctbmNro.Text = String.Format(Formato, Monto)

        End Set
    End Property

    Public Property Value() As String
        Get
            Value = Replace(Text, ".", "")
            Return Value
        End Get
        Set(value As String)

        End Set
    End Property


    'Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtctbmNro.TextChanged
    '    Me.Text = Me.txtctbmNro.Value
    'End Sub

    Public Property MinValue() As String
        Get
            Return m_Min
        End Get
        Set(ByVal MinValue As String)
            m_Min = MinValue
            'Me.txtctbmNro.MinValue = MinValue
            RangeValidator1.MinimumValue = MinValue
        End Set
    End Property

    Public Property MaxValue() As String
        Get
            Return m_Max
        End Get
        Set(ByVal MaxValue As String)
            m_Max = MaxValue
            'Me.txtctbmNro.MaxValue = MaxValue
            RangeValidator1.MaximumValue = MaxValue
        End Set
    End Property

    Public Property Width() As String
        Get
            Return m_tamanho
        End Get
        Set(ByVal Width As String)
            m_tamanho = Width
            'Me.txtctbmNro.MaxValue = MaxValue
            Me.txtctbmNro.Width = Width
        End Set
    End Property

    Public Property Decimales() As String
        Get
            Return m_Decimales
        End Get
        Set(ByVal Decimales As String)
            m_Decimales = Decimales
            'Me.txtctbmNro.MinValue = MinValue
            'Me.txtctbmNro.cantidadDecimales = Decimales
        End Set

    End Property

    Public Property ValidationGroup() As String
        Get
            Return m_ValidationGroup
        End Get
        Set(ByVal ValidationGroup As String)
            m_ValidationGroup = ValidationGroup
            'Me.AbiTextBoxNumerico1.MinValue = MinValue
            Me.RangeValidator1.ValidationGroup = ValidationGroup
            Me.RequiredFieldValidator1.ValidationGroup = ValidationGroup
        End Set

    End Property
    Public Property Enabled() As String
        Get
            Return m_Enabled
        End Get
        Set(ByVal Enabled As String)
            m_Enabled = Enabled
            Me.txtctbmNro.Enabled = Enabled
        End Set
    End Property

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim sComando As String

        sComando = "puntos(this, " & Decimales & ")"
        txtctbmNro.Attributes.Add("onfocus", sComando)
    End Sub
End Class



