﻿Imports System.Web.Services

<System.ComponentModel.DefaultBindingProperty("DataText")>
Partial Class BuscarDatos
    Inherits System.Web.UI.UserControl
    Private m_Campo As String = ""
    Private m_Value As String = ""
    Private m_Mostrar_Select As String = ""
    Private m_Select_AutoComplete As String = ""
    Private m_AutoCompleteColumna As Integer = -1
    'Private m_DataKeysNames As String = ""
    Private m_ConnectionString As String = "bd_Orden"
    Private m_ValidationGroup As String = ""
    Private m_Requerido As Boolean = True
    Private m_Enabled As Boolean = True
    Private m_width_Muestra As String = "0"
    Private m_width As String = ""
    Private m_ShowBotonBuscar As Boolean = False
    Private m_height As String = ""
    Private m_MostrarDescentrado As Boolean = False
    Private m_TextoWatermark As String = "Texto a Buscar"
    Private m_Anho As Integer = 2000
    Private m_BotonAgregar As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If ConnectionString = "" Then
                Me.ConnectionString = m_ConnectionString
            End If

            m_Mostrar_Select = Mostrar_Select
            m_Select_AutoComplete = Select_AutoComplete
            'asigna el valor del select autocomplete
            m_AutoCompleteColumna = AutoCompleteColumna

            'activa la consulta que se utilizara en el autocomplete
            HiCampo.Value = Campo
            m_width_Muestra = Width_Muestra

            Me.txtID.Text = Me.DataText.ToString
            m_Value = Me.DataText.ToString


            If Requerido = True Then
            End If
            If Enabled = False Then

                Me.imbVaciar.Enabled = False

                txtBuscarCara.Enabled = False
            End If
            m_MostrarDescentrado = MostrarDescentrado
        End If
    End Sub

    Public Property Mostrar_Select() As String
        Get
            Return hiMostrarSelect.Value
        End Get
        Set(ByVal Mostrar_Select As String)
            hiMostrarSelect.Value = Mostrar_Select

        End Set
    End Property
    Public Property Select_AutoComplete() As String
        Get
            Return hiSelectAutocomplete.Value
        End Get
        Set(ByVal Select_AutoComplete As String)
            Dim encrip As New Encriptar
            Dim sTexto As String = System.Configuration.ConfigurationManager.AppSettings("Versiontexto").ToString()
            hiSelectAutocomplete.Value = encrip.AES_Encrypt(Select_AutoComplete, sTexto)
            Me.txtBuscarCara_AutoCompleteExtender.ContextKey = encrip.AES_Encrypt(Select_AutoComplete + "//" + ConnectionString, sTexto)
        End Set
    End Property

    Public Property DataText() As String
        Get
            If m_Value = "" Then
                m_Value = Me.txtID.Text
            End If
            Return m_Value
        End Get
        Set(ByVal DataText As String)
            m_Value = DataText
            Me.txtID.Text = DataText
            Obtiene_Descripcion()
            SeleccionDato(Nothing, Nothing)
        End Set
    End Property
    Public Event SeleccionClick As EventHandler
    Public Sub SeleccionDato(ByVal sender As Object, e As System.EventArgs)
        Dim sm As ScriptManager
        sm = ScriptManager.GetCurrent(Me.Page)

        If Me.imbVaciar.Visible = True Then sm.SetFocus(imbVaciar.ClientID)
        If Me.txtBuscarCara.Enabled = True Then sm.SetFocus(txtBuscarCara.ClientID)
        RaiseEvent SeleccionClick(sender, e)
    End Sub

    Public Sub SetFocus(ByVal sender As Object, e As System.EventArgs)
        Dim sm As ScriptManager
        sm = ScriptManager.GetCurrent(Me.Page)
        If Me.imbVaciar.Visible = True Then sm.SetFocus(imbVaciar.ClientID)
        If Me.txtBuscarCara.Visible = True Then sm.SetFocus(txtBuscarCara.ClientID)
        If DataText = "" Then sm.SetFocus(txtBuscarCara.ClientID)
    End Sub




    Public Property TextoWatermark() As String
        Get
            Return m_TextoWatermark
        End Get
        Set(ByVal value As String)
            txtBuscarCara_TextBoxWatermarkExtender.WatermarkText = value
            m_TextoWatermark = value
        End Set
    End Property

    Public Property ConnectionString() As String
        Get
            Return m_ConnectionString
        End Get
        Set(ByVal ConnectionString As String)
            m_ConnectionString = ConnectionString
            Dim strConexion As String
            Dim connectionStrings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings
            strConexion = connectionStrings(ConnectionString).ToString()


        End Set
    End Property

    Protected Sub imbVaciar_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imbVaciar.Click
        Me.txtID.Text = vbNullString
        imbVaciar.Visible = False
        txtBuscarCara.Visible = True
        txtBuscarCara.Enabled = True
        txtBuscarCara.Text = ""
        txtBuscarCara.ToolTip = ""
        txtID.Text = ""

        If BotonAgregar Then
            imbAgregar.Visible = True
        End If

        SeleccionDato(sender, e)

    End Sub
    Public Property Requerido() As Boolean
        Get
            Return m_Requerido
        End Get
        Set(ByVal Requerido As Boolean)
            m_Requerido = Requerido
            If Requerido = True Then

                rfv.Enabled = True
                rfv0.Enabled = True

            Else
                rfv.Enabled = False
                rfv0.Enabled = False
            End If
        End Set
    End Property

    Public Property BotonAgregar() As Boolean
        Get
            Return m_BotonAgregar
        End Get
        Set(value As Boolean)
            imbAgregar.Visible = value
            m_BotonAgregar = value
        End Set
    End Property

    Public Property Campo() As String
        Get
            Return m_Campo
        End Get
        Set(ByVal Campo As String)
            m_Campo = Campo
            HiCampo.Value = Campo
        End Set
    End Property
    Public Property MostrarDescentrado() As Boolean
        Get
            Return m_MostrarDescentrado
        End Get
        Set(ByVal MostrarDescentrado As Boolean)
            m_MostrarDescentrado = MostrarDescentrado
        End Set
    End Property
    Public Property Enabled() As Boolean
        Get
            Return m_Enabled
        End Get
        Set(ByVal Enabled As Boolean)
            m_Enabled = Enabled
            Dim iche As Boolean

            iche = Enabled


            Me.imbVaciar.Enabled = iche

            txtBuscarCara.Enabled = iche

        End Set
    End Property

    Public Property Width_Muestra() As String
        Get
            Return m_width_Muestra
        End Get
        Set(ByVal Width_Muestra As String)
            m_width_Muestra = Width_Muestra
            If Width_Muestra > 0 Then
                txtBuscarCara.Width = Width_Muestra

            End If
        End Set
    End Property
    Public Property ValidationGroup() As String
        Get
            Return m_ValidationGroup
        End Get
        Set(ByVal ValidationGroup As String)
            m_ValidationGroup = ValidationGroup
            rfv.ValidationGroup = ValidationGroup
            rfv0.ValidationGroup = ValidationGroup
        End Set
    End Property
    Public Property Width() As String
        Get
            m_width = 9999
            Return m_width
        End Get
        Set(ByVal Width As String)
            m_width = Width

        End Set
    End Property
    Public Property Height() As String
        Get
            m_height = 999
            Return m_height
        End Get
        Set(ByVal Height As String)
            m_height = Height

        End Set
    End Property
    Public Property AutoCompleteColumna() As Integer
        Get
            Return m_AutoCompleteColumna
        End Get
        Set(ByVal AutoCompleteColumna As Integer)
            m_AutoCompleteColumna = AutoCompleteColumna
            'RequiredFieldValidator11.ValidationGroup = ValidationGroup
        End Set
    End Property

    Protected Sub txtBuscarCara_TextChanged(sender As Object, e As System.EventArgs) Handles txtBuscarCara.TextChanged
        If Trim("SSS") <> "" Then
            Dim sTexto As String, iCAnt As Integer
            If Not txtBuscarCara.Text Like "*;*" Then
                Dim sinSQL As String, dt As New Data.DataTable, obj As New ModuloPrincipal
                sinSQL = Me.Select_AutoComplete
                sinSQL = Replace(UCase(sinSQL), UCase("@filtro"), txtBuscarCara.Text)
                dt = obj.DevuelveDatos(sinSQL, Me.ConnectionString)
                If dt.Rows.Count > 0 And Not IsNothing(dt) Then
                    sTexto = dt.Rows(0)(1)
                End If
            Else
                sTexto = txtBuscarCara.Text
            End If


            Dim sCodigo As String() = sTexto.Split(New Char() {"(", ";", ")"})
            If sCodigo.Length > 1 Then
                txtBuscarCara.Text = ""
                iCAnt = sCodigo.Length
                txtID.Text = Trim(sCodigo(iCAnt - 1))

                Obtiene_Descripcion()

                SeleccionDato(Nothing, Nothing)
            Else
                Dim sinSQL As String, dt As New Data.DataTable, obj As New ModuloPrincipal
                sinSQL = Me.Select_AutoComplete
                sinSQL = Replace(UCase(sinSQL), UCase("@filtro"), txtBuscarCara.Text)
                dt = obj.DevuelveDatos(sinSQL, Me.ConnectionString)

            End If
        Else

        End If
    End Sub

    Sub Obtiene_Descripcion()
        Dim sinsql As String, dt As New Data.DataTable
        Dim obj As New ModuloPrincipal

        sinsql = Replace(UCase(hiMostrarSelect.Value), UCase("@filtro"), "'" + txtID.Text + "'")
        dt = obj.DevuelveDatos(sinsql, ConnectionString)

        If Not IsNothing(dt) And dt.Rows.Count > 0 Then
            txtBuscarCara.Text = dt.Rows(0)(0)
            txtBuscarCara.Enabled = False
            txtBuscarCara.ToolTip = dt.Rows(0)(0) + " ;" + txtID.Text
            imbVaciar.Visible = True
            '          txtBuscarCara.CssClass = "CuentaContableTitulo"
            If Width_Muestra > 0 Then
                txtBuscarCara.Width = Width_Muestra
            Else
                txtBuscarCara.Width = 150
            End If
            imbAgregar.Visible = False
        Else
            txtID.Text = ""
            txtBuscarCara.Text = ""
            txtBuscarCara.ToolTip = ""
            txtBuscarCara.Enabled = True
            imbVaciar.Visible = False
            If Width_Muestra > 0 Then
                txtBuscarCara.Width = Width_Muestra
            Else
                txtBuscarCara.Width = 150
            End If

            If BotonAgregar = True Then
                imbAgregar.Visible = True
            End If
        End If
    End Sub

    Public Event BotonAgregar_Click As EventHandler
    Protected Sub imbAgregar_Click(sender As Object, e As ImageClickEventArgs) Handles imbAgregar.Click
        RaiseEvent BotonAgregar_Click(sender, e)
    End Sub


    Public Property Text() As String
        Get
            Dim sTExto As String
            If txtBuscarCara.Enabled = False Then
                sTExto = txtBuscarCara.Text
            Else
                sTExto = ""
            End If

            Return sTExto
        End Get
        Set(ByVal DataText As String)

        End Set
    End Property


End Class