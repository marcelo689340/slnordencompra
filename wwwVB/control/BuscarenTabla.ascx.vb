﻿Imports System.Web.Services

<System.ComponentModel.DefaultBindingProperty("DataText")> _
Partial Class BuscarenTabla
    Inherits System.Web.UI.UserControl

    Private m_Campo As String = ""
    Private m_Value As String = ""
    Private m_Grilla_Select As String = ""
    Private m_Mostrar_Select As String = ""
    Private m_Select_AutoComplete As String = ""
    Private m_AutoCompleteColumna As Integer = -1
    'Private m_DataKeysNames As String = ""
    Private m_ConnectionString As String = "bd_Orden"
    Private m_ValidationGroup As String = ""
    Private m_Requerido As Boolean = True
    Private m_Enabled As Boolean = True
    Private m_width_Muestra As String = "0"
    Private m_width As String = ""
    Private m_ShowBotonBuscar As Boolean = True
    Private m_height As String = ""
    Private m_MostrarDescentrado As Boolean = False


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then

            If ConnectionString = "" Then
                Me.ConnectionString = m_ConnectionString

            End If

            m_Grilla_Select = Grilla_Select
            m_Mostrar_Select = Mostrar_Select
            Me.SqldeGrilla.SelectCommand = Grilla_Select
            Me.sqldeForm.SelectCommand = Mostrar_Select

            'asigna el valor del select autocomplete
            m_Select_AutoComplete = Select_AutoComplete
            m_AutoCompleteColumna = AutoCompleteColumna

            'activa la consulta que se utilizara en el autocomplete
            txtBuscarCara_AutoCompleteExtender.ContextKey = Select_AutoComplete + "//" + ConnectionString

            'determina si posee una consulta para el autocomplete en caso que NO la DESACTIVA
            If Len(Trim(Select_AutoComplete)) = 0 Or IsNothing(Select_AutoComplete) Then
                txtBuscarCara_AutoCompleteExtender.Enabled = False
            Else
                txtBuscarCara_AutoCompleteExtender.Enabled = True

            End If

            m_ShowBotonBuscar = ShowBotonBuscar

            HiCampo.Value = Campo

            m_width_Muestra = Width_Muestra
            Me.DetailsView1.Width = Width_Muestra

            Me.txtID.Text = Me.DataText.ToString
            m_Value = Me.DataText.ToString


            Me.DetailsView1.DataBind()
            Me.Grilla.DataBind()

            If DetailsView1.Rows.Count = 1 Then
                txtBuscarCara.Visible = False
            End If

            If Requerido = True Then

            End If

            If Enabled = False Then
                Me.txtBuscar.Enabled = False
                Me.ImageButton1.Enabled = False
                Me.imbVaciar.Enabled = False
                Me.imbBuscar1.Enabled = False
                txtBuscarCara.Enabled = False
            End If

            m_MostrarDescentrado = MostrarDescentrado

        End If
    End Sub


    Public Property DataText() As String
        Get
            If m_Value = "" Then
                m_Value = Me.txtID.Text
            End If
            Return m_Value
        End Get
        Set(ByVal DataText As String)
            m_Value = DataText
            Me.txtID.Text = DataText

        End Set
    End Property


    Public Event SeleccionClick As EventHandler
    Public Event LimpiarClick As EventHandler
    Public Sub SeleccionDato(ByVal sender As Object, e As System.EventArgs)
        RaiseEvent SeleccionClick(sender, e)
    End Sub
    Public Sub LimpiarDato(ByVal sender As Object, e As System.EventArgs)
        RaiseEvent LimpiarClick(sender, e)
    End Sub

  

    'Public Event Refresca As EventHandler
    'Public Sub RefescaDatos(ByVal sender As Object, e As System.EventArgs)
    '    RaiseEvent Refresca(sender, e)
    'End Sub

    Protected Sub GrillaCliente_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grilla.SelectedIndexChanged

        If txtBuscar.Text = "" Then
            Exit Sub
        End If
        txtID.Text = Grilla.SelectedRow.Cells(1).Text
        txtBuscar.Text = ""
        Me.Grilla.DataBind()
        Me.DetailsView1.DataBind()
        Me.Grilla.SelectedIndex = -1
        If DetailsView1.Rows.Count = 1 Then
            txtBuscarCara.Visible = False
        End If
        SeleccionDato(sender, e)

    End Sub

    Public Property Grilla_Select() As String
        Get
            Return m_Grilla_Select
        End Get
        Set(ByVal Grilla_Select As String)
            m_Grilla_Select = Grilla_Select
            SqldeGrilla.SelectCommand = Grilla_Select
        End Set
    End Property

    Public Property Mostrar_Select() As String
        Get
            Return m_Mostrar_Select
        End Get
        Set(ByVal Mostrar_Select As String)
            m_Mostrar_Select = Mostrar_Select
            sqldeForm.SelectCommand = Mostrar_Select
        End Set
    End Property

    Public Property Select_AutoComplete() As String
        Get
            Return m_Select_AutoComplete
        End Get
        Set(ByVal Select_AutoComplete As String)
            m_Select_AutoComplete = Select_AutoComplete
            Me.txtBuscarCara_AutoCompleteExtender.ContextKey = Select_AutoComplete
        End Set
    End Property


    Public Property ConnectionString() As String
        Get
            Return m_ConnectionString
        End Get
        Set(ByVal ConnectionString As String)
            m_ConnectionString = ConnectionString

            Dim strConexion As String
            Dim connectionStrings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings

            strConexion = connectionStrings(ConnectionString).ToString()

            Me.sqldeForm.ConnectionString = strConexion
            Me.SqldeGrilla.ConnectionString = strConexion
        End Set
    End Property

    Protected Sub Grilla_PageIndexChanged(sender As Object, e As EventArgs) Handles Grilla.PageIndexChanged
        mdlPopup.Show()
        'RefescaDatos(sender, e)
    End Sub

    Protected Sub Grilla_Sorted(sender As Object, e As EventArgs) Handles Grilla.Sorted
        mdlPopup.Show()
        'RefescaDatos(sender, e)
    End Sub

    Protected Sub ImageButton1_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If txtBuscar.Text = "" Then
            Exit Sub
        End If


        Grilla.DataBind()
        If Grilla.Rows.Count = 1 Then
            txtID.Text = Grilla.Rows(0).Cells(1).Text
            txtBuscar.Text = ""

            SeleccionDato(sender, e)

            Me.DetailsView1.DataBind()
            Me.Grilla.DataBind()
            Me.Grilla.SelectedIndex = -1
        Else
            mdlPopup.Show()

        End If

    End Sub

    Protected Sub Button2_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imbBuscar1.Click
        txtBuscar.Text = txtBuscarCara.Text
        txtBuscarCara.Text = ""
        'If txtBuscar.Text = "" Then
        '    Exit Sub
        'End If

        'txtID.Text = ""
        Grilla.DataBind()
        If Grilla.Rows.Count = 1 Then
            txtID.Text = Grilla.Rows(0).Cells(1).Text
            txtBuscar.Text = ""

            SeleccionDato(sender, e)

            Me.DetailsView1.DataBind()
            Me.Grilla.DataBind()
            Me.Grilla.SelectedIndex = -1
        Else
            If MostrarDescentrado = True Then
                mdlPopup.X = 0
                mdlPopup.Y = 0
            End If
            mdlPopup.Show()
            'RefescaDatos(sender, e)
        End If

    End Sub

    Protected Sub imbVaciar_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imbVaciar.Click
        ''Me.txtID.Text = vbNullString
        Me.hdfID.Value = vbNullString
        txtBuscarCara.Visible = True
        txtID.Text = ""        
        Me.DetailsView1.DataBind()
        SeleccionDato(sender, e)
    End Sub

    Public Property Requerido() As Boolean
        Get
            Return m_Requerido
        End Get
        Set(ByVal Requerido As Boolean)
            m_Requerido = Requerido
            If Requerido = True Then
                'Dim rfv As RequiredFieldValidator = New RequiredFieldValidator()
                'rfv.ControlToValidate = Me.txtID.ID
                'HiCampo.Value = Campo
                'rfv.ErrorMessage = "Error no ha seleccionado"
                'rfv.ValidationGroup = ValidationGroup
                'rfv.Text = HiCampo.Value
                'Me.PlaceHolder1.Controls.Add(rfv)
                rfv.enabled = True
            Else
                rfv.enabled = False

            End If
        End Set
    End Property

    Public Property Campo() As String
        Get
            Return m_Campo
        End Get
        Set(ByVal Campo As String)
            m_Campo = Campo
            HiCampo.Value = Campo
        End Set
    End Property

    Public Property MostrarDescentrado() As Boolean
        Get
            Return m_MostrarDescentrado
        End Get
        Set(ByVal MostrarDescentrado As Boolean)
            m_MostrarDescentrado = MostrarDescentrado

        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return m_Enabled
        End Get
        Set(ByVal Enabled As Boolean)
            m_Enabled = Enabled
            'RequiredFieldValidator11.ValidationGroup = ValidationGroup
        End Set
    End Property

    Public Property ShowBotonBuscar() As Boolean
        Get
            Return m_ShowBotonBuscar
        End Get
        Set(ByVal ShowBotonBuscar As Boolean)
            m_ShowBotonBuscar = ShowBotonBuscar
            Me.ImageButton1.Visible = ShowBotonBuscar

            Me.imbBuscar1.Visible = ShowBotonBuscar
        End Set
    End Property

    Public Property Width_Muestra() As String
        Get
            Return m_width_Muestra
        End Get
        Set(ByVal Width_Muestra As String)
            m_width_Muestra = Width_Muestra
            If Width_Muestra > 0 Then
                txtBuscarCara.Width = Width_Muestra
                txtBuscar.Width = Width_Muestra
            End If
        End Set
    End Property

    Public Property ValidationGroup() As String
        Get
            Return m_ValidationGroup
        End Get
        Set(ByVal ValidationGroup As String)
            m_ValidationGroup = ValidationGroup
            rfv.ValidationGroup = ValidationGroup
        End Set
    End Property


    Public Property Width() As String
        Get
            m_width = Convert.ToString(pnlPanel.Width)
            Return m_width
        End Get
        Set(ByVal Width As String)
            m_width = Width
            pnlPanel.Width = Width
        End Set
    End Property

    Public Property Height() As String
        Get
            m_height = Convert.ToString(pnlPanel.Height)
            Return m_height
        End Get
        Set(ByVal Height As String)
            m_height = Height
            pnlPanel.Height = Height
        End Set
    End Property

    Public Property AutoCompleteColumna() As Integer
        Get
            Return m_AutoCompleteColumna
        End Get
        Set(ByVal AutoCompleteColumna As Integer)
            m_AutoCompleteColumna = AutoCompleteColumna
            'RequiredFieldValidator11.ValidationGroup = ValidationGroup
        End Set
    End Property

    Protected Sub DetailsView1_DataBound(sender As Object, e As System.EventArgs) Handles DetailsView1.DataBound
        Try

            If DetailsView1.Rows.Count >= 1 Then
                For i = 0 To DetailsView1.Rows.Count - 1
                    DetailsView1.Rows(i).Cells(1).Wrap = False
                    DetailsView1.Rows(i).Cells(0).Font.Bold = True

                Next
                imbVaciar.Visible = True
            Else
                imbVaciar.Visible = False
            End If

            If DetailsView1.Rows.Count = 1 Then
                txtBuscarCara.Visible = False
            Else
                txtBuscarCara.Visible = True
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub txtBuscarCara_TextChanged(sender As Object, e As System.EventArgs) Handles txtBuscarCara.TextChanged
        If Trim(Select_AutoComplete) <> "" Then
            Dim sTexto As String
            sTexto = txtBuscarCara.Text

            Dim sCodigo As String() = sTexto.Split(New Char() {"(", ";", ")"})
            If sCodigo.Count > 1 Then
                txtBuscarCara.Text = ""

                If AutoCompleteColumna > -1 Then
                    txtID.Text = Trim(sCodigo(AutoCompleteColumna))
                Else
                    txtID.Text = Trim(sCodigo(sCodigo.Count - 1))
                End If

                txtBuscar.Text = ""

                Me.sqldeForm.DataBind()
                Me.DetailsView1.DataBind()
                Me.Grilla.DataBind()
                Me.Grilla.SelectedIndex = -1

                '------------ NUEVO-------------- EN PRUEBA
                If DetailsView1.Rows.Count = 1 Then
                    txtBuscarCara.Visible = False
                End If

                SeleccionDato(Nothing, Nothing)
            End If
        Else

            txtBuscar.Text = txtBuscarCara.Text
            SqldeGrilla.DataBind()
            Grilla.DataBind()

            If Grilla.Rows.Count = 1 Then
                txtID.Text = Replace(Replace(Grilla.Rows(0).Cells(1).Text, ".", ""), ",", "")
                sqldeForm.DataBind()
                Me.DetailsView1.DataBind()
                txtBuscarCara.Text = ""
                txtBuscar.Text = ""
                SeleccionDato(sender, e)
            Else
                mdlPopup.Show()
            End If


        End If

    End Sub
End Class

