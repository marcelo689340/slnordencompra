﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports System.Data.SqlClient
Imports System.Drawing
Imports System.IO

Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D

Partial Class VerFoto
    Inherits PaginaBase

    Sub CargarImagen()
        Try


            Try

                Dim ImagenS As New MemoryStream
                Dim Width As Integer
                Dim Height As Integer

                Dim Bitmap As System.Drawing.Bitmap
                Dim ImgFormat As System.Drawing.Imaging.ImageFormat
                Dim Img As System.Drawing.Image
                Dim baseMap As Bitmap

                Dim imagen As Byte()


                imagen = Session("imagen")

                Dim imageStream As New MemoryStream(imagen)
                ImagenS = imageStream

                Img = System.Drawing.Image.FromStream(ImagenS)
                ImgFormat = ImageFormat.Jpeg : Response.ContentType = "image/jpeg"  ' Default=jpeg '

                Width = Img.Width
                Height = Img.Height

                Dim relacion As Double, ancho As Integer, alto As Integer
                If Request.QueryString("ancho") <> "" Then

                    ancho = Val(Request.QueryString("ancho"))
                    relacion = ancho * 100 / Width
                    Width = Request.QueryString("ancho")
                    Height = Height * relacion / 100

                End If



                baseMap = New Bitmap(Width, Height)
                Dim myGraphic As Graphics = Graphics.FromImage(baseMap)

                myGraphic.DrawImage(Img, 0, 0, Width, Height)

                Img.Dispose()

                baseMap.Save(Response.OutputStream, ImageFormat.Jpeg)
                baseMap.Dispose()

            Catch
                Response.Clear()

                Response.Write("Error: No existe la imagen")

            End Try
        Catch err As SqlException
            Response.Clear()

            Response.Write("Error:" & err.Message.ToString())
        Finally


        End Try


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CargarImagen()

    End Sub


End Class

