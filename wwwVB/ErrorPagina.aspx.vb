﻿Public Class ErrorPagina
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            lblMensaje.Text = Request.QueryString("Mensaje").ToString
        End If
    End Sub

End Class