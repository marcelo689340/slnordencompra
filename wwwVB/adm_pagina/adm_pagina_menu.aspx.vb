﻿Imports CapaMedio.DAO
Imports CapaMedio

Partial Class adm_pagina_adm_pagina_menu : Inherits PaginaBase
    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Dim obj As _Menu = New _Menu()
        Dim dao As DAO__MENU = New DAO__MENU()

        obj.MenuId = Request.QueryString("ID")
        obj.Descripcion = txtDescripcion.Text
        obj.Url = txtUrl.Text
        obj.PadreId = cboPadre.SelectedValue
        obj.UsuarioCreacion = Request.Cookies("idUsuario").Value
        obj.UsuarioModificacion = Request.Cookies("idUsuario").Value
        obj.Habilitado = Convert.ToInt64(IIf(chkActivo.Checked = True, 1, 0))

        Try
            If Request.QueryString("ID") <> "0" Then
                dao.Actualizar(obj)
            Else
                dao.Insertar(obj)
            End If
            Response.Redirect(cmdVolver.PostBackUrl.ToString)
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            MensajeError(ex.Message.ToString)
        End Try

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If
            CType(Master.FindControl("lblTitulo"), Label).Text = ""
            Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            If Request.QueryString("ID") <> "0" Then
                ltDatos.Text = "MODIFICAR PAGINA MENU"
                Cargar_Datos()
            Else
                ltDatos.Text = "NUEVO PAGINA MENU"
                chkActivo.Checked = True
            End If
        End If
    End Sub

    Protected Sub Cargar_Datos()
        Dim obj As _Menu = New _Menu()
        Dim dao As DAO__MENU = New DAO__MENU()
        Try
            obj = dao.get_Menu(Convert.ToInt16(Request.QueryString("ID")))
            cboPadre.SelectedValue = obj.PadreId
            lblCodigo.Text = obj.MenuId
            txtDescripcion.Text = obj.Descripcion
            txtUrl.Text = obj.Url
            chkActivo.Checked = obj.Habilitado
        Catch ex As Exception
            MensajeError(ex.Message.ToString)
        End Try
    End Sub

    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox1.tipoMensaje = 2
        cuMensajeBox1.Mensaje = sMensaje
        cuMensajeBox1.show()
    End Sub

End Class
