﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="adm_menu.aspx.vb" Inherits="adm_pagina_adm_menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 128px;
            height: 15px;
        }

        .auto-style2 {
            width: 68%;
        }

        .auto-style3 {
            width: 95px;
        }

        .auto-style4 {
            width: 95px;
            height: 22px;
        }

        .auto-style5 {
            height: 22px;
        }

        .auto-style6 {
            height: 22px;
            width: 267px;
        }

        .auto-style7 {
            width: 267px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="auto-style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="auto-style2">
                <tr>
                    <td>
                        <table class="auto-style2">
                            <tr>
                                <td class="auto-style4">Menu Padre</td>
                                <td class="auto-style6">
                                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataGrupoMenu" DataTextField="Descripcion" DataValueField="MenuId" Height="20px" Width="268px">
                                    </asp:DropDownList>
                                </td>
                                <td class="auto-style5">
                                    <asp:Button ID="cmdNuevo" runat="server" CausesValidation="False" CssClass="boton" Height="22px" PostBackUrl="~/adm_pagina/adm_grupo_menu.aspx?ID=0" Text="Agregar Nuevo Grupo" Width="156px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style3">&nbsp;</td>
                                <td class="auto-style7">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style3">&nbsp;</td>
                                <td class="auto-style7">
                                    <asp:Button ID="cmdNuevo0" runat="server" CausesValidation="False" CssClass="boton" Height="22px" PostBackUrl="~/adm_pagina/adm_pagina_menu.aspx?ID=0" Text="Agregar Nuevo Pagina" Width="156px" />
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="MenuId" DataSourceID="SqlDataGrilla" Width="497px">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="btnEditar" runat="server" ToolTip="Editar Registro" ImageUrl="~/App_Themes/css/img/standar/update.png" NavigateUrl='<%# Eval("MenuId", "adm_pagina_menu.aspx?ID={0}")%>' Text="mmm" />
                                    </ItemTemplate>
                                    <ItemStyle Width="70px" />
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminr" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument='<%# Eval("MenuId")%>' ImageUrl="~/App_Themes/css/img/standar/recicle.png" Text="Eliminar" ToolTip="Elimiar Registro" OnClientClick="return confirm('¿Esta seguro de eliminar este registro?');"  />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="MenuId" HeaderText="MenuId" InsertVisible="False" ReadOnly="True" SortExpression="MenuId" />
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion" />
                                <asp:BoundField DataField="Url" HeaderText="Url" SortExpression="Url" />
                                <asp:BoundField DataField="Habiitado" HeaderText="Habiitado" ReadOnly="True" SortExpression="Habiitado" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataGrilla" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" DeleteCommand="usp__MenuDelete" DeleteCommandType="StoredProcedure" SelectCommand="SELECT MenuId
      ,Descripcion     
     ,Url
      , CASE WHEN Habilitado = 1 THEN 'SI' ELSE 'NO' END AS Habiitado
  FROM  _Menu  
  where Url is not null and PadreId = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="MenuId" Type="Int32" />
                            </DeleteParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="DropDownList1" Name="id" PropertyName="SelectedValue" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:SqlDataSource ID="SqlDataGrupoMenu" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT MenuId
      ,Descripcion     
  FROM  _Menu  
  where Url is null and Habilitado = 1"></asp:SqlDataSource>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

