﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="adm_pagina_menu.aspx.vb" Inherits="adm_pagina_adm_pagina_menu" %>

<%@ Register src="../control/cuMensajeBox.ascx" tagname="cuMensajeBox" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 128px;
            height: 15px;
        }

        .auto-style2 {
            width: 63%;
        }

        .auto-style4 {
            width: 113px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="auto-style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="auto-style2">
                <tr>
                    <td>
                        <div>
                            <div class="cuadroTitulo">
                                <asp:Literal ID="ltDatos" runat="server" Text="Usuario"></asp:Literal>
                            </div>
                            <div class="conMargin10">
                                <div style="float: left; padding-bottom: 5px; width: 411px;">
                                    <fieldset id="fsBuscarRuc" runat="server" class="fieldSet01">
                                        <legend>
                                            <asp:Literal ID="ltTituloFsDatos" runat="server" Text="Datos"></asp:Literal>
                                        </legend>

                                        <div class="conPadding10">

                                            <table class="auto-style2">
                                                <tr>
                                                    <td class="auto-style4">Codigo</td>
                                                    <td>
                                                        <asp:Label ID="lblCodigo" runat="server" Text="0"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style4">Padre</td>
                                                    <td>
                                                        <asp:DropDownList ID="cboPadre" runat="server" DataSourceID="SqlDataPadre" DataTextField="Descripcion" DataValueField="MenuId" Height="16px" Width="215px">
                                                        </asp:DropDownList>
                                                        <asp:SqlDataSource ID="SqlDataPadre" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT 0 as MenuId
      ,'NINGUNO' as Descripcion  
UNION         
SELECT MenuId
      ,Descripcion     
  FROM  _Menu  
  where Url is null and Habilitado = 1 "></asp:SqlDataSource>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style4">Titulo</td>
                                                    <td>
                                                        <asp:TextBox ID="txtDescripcion" runat="server" Width="450px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="rfv" ErrorMessage="*" ControlToValidate="txtDescripcion"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style4">Pagina</td>
                                                    <td>
                                                        <asp:TextBox ID="txtUrl" runat="server" Width="450px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUrl" CssClass="rfv" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style4">Habilitado</td>
                                                    <td>
                                                        <asp:CheckBox ID="chkActivo" runat="server" Checked="True" TabIndex="7" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style4">&nbsp;</td>
                                                    <td>
                                                        <asp:Button ID="cmdVolver" runat="server" CausesValidation="False" CssClass="boton" TabIndex="10" Text="Volver" Width="71px" />
                                                        <asp:Button ID="cmdGuardar" runat="server" CssClass="boton" TabIndex="9" Text="Guardar" />
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                        </div>


                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <uc1:cuMensajeBox ID="cuMensajeBox1" runat="server" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

