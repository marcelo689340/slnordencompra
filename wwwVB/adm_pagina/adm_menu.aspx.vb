﻿
Partial Class adm_pagina_adm_menu : Inherits PaginaBase
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim sPagina As String = "/adm_pagina/adm_menu.aspx"

            Dim sMens As String = Validar_Ingreso(sPagina)
            If sMens = "" Then
                GetCookies()
                Response.Cookies("PaginaAnterior").Value = "~" + sPagina
                CType(Master.FindControl("lblTitulo"), Label).Text = "MANTENIMIENTO DE MENU"
            Else
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
            End If
        End If

    End Sub

    Private Sub SetCookies()
        Cookies(Me, Me.DropDownList1.GetType.Name) = DropDownList1.SelectedValue
    End Sub

    Private Sub GetCookies()
        If Cookies(Me, Me.DropDownList1.GetType.Name) IsNot Nothing Then
            DropDownList1.SelectedValue = Cookies(Me, Me.DropDownList1.GetType.Name)
        End If
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
        SetCookies()
    End Sub
End Class
