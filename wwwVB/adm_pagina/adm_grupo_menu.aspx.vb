﻿Imports CapaMedio.DAO
Imports CapaMedio

Partial Class adm_pagina_adm_grupo_menu : Inherits PaginaBase
    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        Dim obj As _Menu = New _Menu()
        Dim dao As DAO__MENU = New DAO__MENU()

        obj.MenuId = Request.QueryString("ID")
        obj.Descripcion = txtDescripcion.Text
        obj.UsuarioCreacion = Request.Cookies("idUsuario").Value
        obj.UsuarioModificacion = Request.Cookies("idUsuario").Value
        obj.Habilitado = Convert.ToUInt64(IIf(chkActivo.Checked = True, 1, 0))

        Try
            If Request.QueryString("ID") <> "0" Then
                dao.Actualizar(obj)
            Else
                dao.Insertar(obj)
            End If
            Response.Redirect(cmdVolver.PostBackUrl.ToString)
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            MensajeError(ex.Message.ToString)
        End Try

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If (Request.UrlReferrer Is Nothing) Then
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=Error, no accedio a la pagina como corresponde")
            End If
            CType(Master.FindControl("lblTitulo"), Label).Text = ""
            Me.cmdVolver.PostBackUrl = Request.Cookies("PaginaAnterior").Value
            If Request.QueryString("ID") <> "0" Then
                ltDatos.Text = "MODIFICAR GRUPO MENU"
                Cargar_Datos()
            Else
                ltDatos.Text = "NUEVO GRUPO MENU"
                chkActivo.Checked = True
            End If
        End If
    End Sub

    'Protected Sub Agregar_Nuevo()
    '    Try
    '        Dim iID As Integer
    '        Try
    '            iID = (From A In dbAud._Permiso_Aud Select A.id_permiso).Max()
    '        Catch ex As Exception
    '            iID = 0
    '        End Try
    '        iID = iID + 1
    '        Dim lqs As New CapaMedio._Permiso
    '        lqs.id_permiso = iID
    '        lqs.descripcion = txtDescripcion.Text.ToUpper
    '        lqs.estado = chkActivo.Checked
    '        lqs.id_user = Request.Cookies("idUsuario").Value
    '        db._Permiso.InsertOnSubmit(lqs)
    '        db.SubmitChanges()
    '        iID = lqs.id_permiso
    '        ''cmdVolver.OnClientClick = True
    '        Response.Redirect(cmdVolver.PostBackUrl.ToString)
    '    Catch ex As Exception
    '        Dim sMensaje As String = ex.Message.ToString
    '        MensajeError(ex.Message.ToString)
    '    End Try
    'End Sub
    'Protected Sub Modificar_Datos()

    '    Try
    '        Dim lqs = (From U In db._Permiso Where U.id_permiso = Request.QueryString("ID") _
    '             Select U).Single
    '        lblCodigo.Text = lqs.id_permiso
    '        lqs.descripcion = txtDescripcion.Text.ToUpper
    '        lqs.estado = chkActivo.Checked
    '        lqs.id_user = Request.Cookies("idUsuario").Value
    '        db.SubmitChanges()
    '        Response.Redirect(cmdVolver.PostBackUrl.ToString)
    '    Catch ex As Exception
    '        MensajeError(ex.Message.ToString)
    '    End Try
    'End Sub


    Protected Sub Cargar_Datos()



        Dim obj As _Menu = New _Menu()
        Dim dao As DAO__MENU = New DAO__MENU()
        Try


            obj = dao.get_Menu(Convert.ToInt16(Request.QueryString("ID")))

            lblCodigo.Text = obj.MenuId
            txtDescripcion.Text = obj.Descripcion
            chkActivo.Checked = obj.Habilitado
        Catch ex As Exception
            MensajeError(ex.Message.ToString)
        End Try
    End Sub

    Protected Sub MensajeError(sMensaje As String)
        cuMensajeBox1.tipoMensaje = 2
        cuMensajeBox1.Mensaje = sMensaje
        cuMensajeBox1.show()
    End Sub

End Class
