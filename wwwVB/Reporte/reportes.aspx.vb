﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data


Partial Class reportes
    Inherits System.Web.UI.Page
    Dim report As New ReportDocument

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Call Muestra_Informe()
        Else
            Call Muestra_Informe_Paginacion()
        End If
    End Sub
    Protected Sub Page_Render(ByVal sender As Object, ByVal e As EventArgs)
        Page.ClientScript.RegisterForEventValidation("1")
    End Sub
    Sub Muestra_Informe()
        Dim sReporte As String = ""
        Try

            'DETERMINA SI EL NAVEGADOR ES UN MOBIL
            Dim uAg As String = Request.ServerVariables("HTTP_USER_AGENT")
            Dim regEx As New Regex("android|iphone|ipad|ipod|blackberry|symbianos", RegexOptions.IgnoreCase)
            Dim isMobile As Boolean = regEx.IsMatch(uAg)

            Dim ds As New Data.DataSet

            ds = Session("rs")
            Session("rs") = Nothing


            'Dim sReporteSufijo As String, obj As New ModuloPrincipal, sSufijo As String
            Dim sReporteSufijo As String
            'sSufijo = obj.ReporteSufijo
            sReporte = Request.QueryString("Reporte")
            sReporte = Replace(sReporte, "\", "/")
            ' sreporte = Replace(sreporte, ".rpt", "")

            sReporteSufijo = sreporte
            'sReporteSufijo = sreporte + "_" + sSufijo + ".rpt"

            If System.IO.File.Exists(Server.MapPath(".") + "\" + sReporteSufijo) Then
                sreporte = sReporteSufijo
            Else
                sreporte = sreporte + ".rpt"
            End If

            'If System.IO.File.Exists(Server.MapPath(".") + "\" + sReporteSufijo) Then
            '    sreporte = sReporteSufijo
            'Else
            '    sreporte = sreporte + ".rpt"
            'End If



            report = ReportFactory.GetReport(report.GetType())

            report.Load(Server.MapPath(".") + "\" + sreporte)
            report.SetDataSource(ds)

            
            If Trim(Request.QueryString("Salida")) = "" Or UCase(Request.QueryString("Salida")) = "PANTALLA" _
                Or UCase(Request.QueryString("Salida")) = "PANTALLACR" Then

                'determina si el reporte es un cheque o es un mobil....
                If UCase(sreporte) Like "*CHEQUE*" Or isMobile Or Trim(UCase(Request.QueryString("Salida"))) = "PANTALLACR" Then

                    CrystalReportViewer1.ReportSource = report



                    Session("report_") = report

                    Exit Sub
                Else
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Reportes")

                End If


            End If

            Dim sNombreArchivo As String
            sNombreArchivo = Request.QueryString("NombreArchivo")
            sNombreArchivo = sNombreArchivo + String.Format("{0:yyyyMMdd_hhmmss}", Date.Now)
            If UCase(Request.QueryString("Salida")) = "WORD" Then
                sNombreArchivo = sNombreArchivo + ".doc"
                report.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, False, sNombreArchivo)
            End If

            If UCase(Request.QueryString("Salida")) = "EXCEL" Then
                sNombreArchivo = sNombreArchivo + ".xls"
                report.ExportToHttpResponse(ExportFormatType.ExcelRecord, Response, False, sNombreArchivo)
            End If

            Exit Sub
        Catch ex As Exception
            'MsgBox("Ha ocurrido el siguiente Error: " & Err.Number & " " & vbCrLf & ex.Message)
            'Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + ex.Message)
            If Not ex.Message.ToString Like "*anulado*" Then

                lblError.Text = lblError.Text + "   " + ex.Message.ToString + "    " + Server.MapPath(sreporte)
            End If
        End Try


    End Sub


    Protected Sub Muestra_Informe_Paginacion()

        Try

            'If Not IsNothing(ViewState("reporte")) Then

            report = Session("report_")

            CrystalReportViewer1.ReportSource = report


            'End If


        Catch ex As Exception
            'MsgBox("Ha ocurrido el siguiente Error: " & vbCrLf & ex.Message)
            '            Response.Redirect("ErrorPagina.aspx?Mensaje=" + ex.Message)

        End Try



    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'Me.CrystalReportSource1.Dispose()
        'Me.CrystalReportViewer1.Dispose()
        'report.Close()
        'report.Dispose()

        'Session("rs") = Nothing
    End Sub

    Protected Sub CrystalReportViewer1_Unload(sender As Object, e As EventArgs) Handles CrystalReportViewer1.Unload
        'Me.CrystalReportSource1.Dispose()
        'Me.CrystalReportViewer1.Dispose()
        'report.Close()
        'report.Dispose()
    End Sub
End Class
