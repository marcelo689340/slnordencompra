﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="rptReporteOrdenCompraEstado.aspx.vb" Inherits="Reporte_rptReporteOrdenCompraEstado" %>

<%@ Register Src="../control/Calendario.ascx" TagName="Calendario" TagPrefix="uc1" %>
<%@ Register Src="../control/wucReporte.ascx" TagName="wucReporte" TagPrefix="uc2" %>
<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 550px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="container-fluid">
                <div class="block">
                    <div class="block-content collapse in">
                        <div class="container-fluid">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">Estado de Orden Compra : </label>
                                    <div class="controls">
                                    <asp:DropDownList ID="cboSector" runat="server" DataTextField="descripcion" DataValueField="id_sector" Height="27px" Width="323px">
                                        <asp:ListItem>CHEQUEADO</asp:ListItem>
                                        <asp:ListItem>AUTORIZADO</asp:ListItem>
                                        <asp:ListItem>PENDIENTE</asp:ListItem>
                                        <asp:ListItem>RECHAZADO</asp:ListItem>
                                    </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Fecha Inicio : </label>
                                    <div class="controls">
                                        <uc1:Calendario ID="txtFechaInicio" runat="server" Requerido="True" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Fecha Fin : </label>
                                    <div class="controls">
                                        <uc1:Calendario ID="txtFechaFin" runat="server" Requerido="True" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Text="Mostrar" />
                               <%-- <asp:Button ID="cmdEnviarExcel" runat="server" CssClass="btn btn-primary" Text="Enviar Excel" Visible="False" />--%>
                            </div>
                        </div>
                    </div>
                </div>
                <uc2:wucReporte ID="wucReporte1" runat="server" />
                <uc3:cuMensajeBox ID="cuMensajeBox1" runat="server" />
            </div>




            <%--            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <table class="auto-style1">
                            <tr>
                                <td>Estado de Orden Compra</td>
                                <td>
                                    <asp:DropDownList ID="cboSector" runat="server" DataTextField="descripcion" DataValueField="id_sector" Height="27px" Width="323px">
                                        <asp:ListItem>CHEQUEADO</asp:ListItem>
                                        <asp:ListItem>AUTORIZADO</asp:ListItem>
                                        <asp:ListItem>PENDIENTE</asp:ListItem>
                                        <asp:ListItem>RECHAZADO</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Fecha Inicio</td>
                                <td>
                                    <uc1:Calendario ID="txtFechaInicio" runat="server"  Requerido="True" />
                                </td>
                            </tr>
                            <tr>
                                <td>Fecha Fin</td>
                                <td>
                                    <uc1:Calendario ID="txtFechaFin" runat="server" Requerido="True" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Button ID="Button1" runat="server" Text="MOSTRAR" Width="91px" CssClass="boton" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <uc2:wucReporte ID="wucReporte1" runat="server" />
                        <uc3:cuMensajeBox ID="cuMensajeBox1" runat="server" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

