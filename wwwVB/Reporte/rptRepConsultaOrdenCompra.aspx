﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MpPrincipal.Master" AutoEventWireup="false" CodeFile="rptRepConsultaOrdenCompra.aspx.vb" Inherits="Reporte_rptRepConsultaOrdenCompra" %>

<%@ Register Src="../control/Calendario.ascx" TagName="Calendario" TagPrefix="uc1" %>
<%@ Register Src="../control/wucReporte.ascx" TagName="wucReporte" TagPrefix="uc2" %>
<%@ Register Src="../control/cuMensajeBox.ascx" TagName="cuMensajeBox" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <img alt="" class="style1" src="../App_Themes/css/img/standar/ajax-loader-barra.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="block">
                    <div class="block-content collapse in">
                        <div class="container-fluid">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">SECTOR : </label>
                                    <div class="controls">
                                        <asp:DropDownList ID="cboSector" runat="server" DataSourceID="SqlDataSource1" DataTextField="descripcion" DataValueField="id_sector" Height="27px" Width="323px" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" SelectCommand="SELECT id_sector
      ,descripcion
  FROM viewCboSector
"></asp:SqlDataSource>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Fecha Inicio : </label>
                                    <div class="controls">
                                        <uc1:Calendario ID="txtFechaInicio" runat="server" Requerido="True" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Fecha Fin : </label>
                                    <div class="controls">
                                        <uc1:Calendario ID="txtFechaFin" runat="server" Requerido="True" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Text="Refrescar Grilla" />
                                <asp:Button ID="cmdEnviarExcel" runat="server" CssClass="btn btn-primary" Text="Enviar Excel" Visible="False" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="controls">
                    <asp:GridView ID="grvDatos" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_orden" DataSourceID="SqlDataGrilla" >
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Imprimir" CommandArgument='<%# Eval("id_orden")%>' ImageUrl="~/App_Themes/css/img/standar/impresora.png" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="id_orden" HeaderText="id_orden" ReadOnly="True" SortExpression="id_orden" />
                            <asp:BoundField DataField="fecha" DataFormatString="{0:d}" HeaderText="fecha" SortExpression="fecha" />
                            <asp:BoundField DataField="id_sector" HeaderText="id_sector" SortExpression="id_sector" />
                            <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion" />
                            <asp:BoundField DataField="monto" DataFormatString="{0:N0}" HeaderText="monto" SortExpression="monto">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="fecha_autorizacion" DataFormatString="{0:d}" HeaderText="fecha_autorizacion" SortExpression="fecha_autorizacion" />
                            <asp:BoundField DataField="fecha_procesado" DataFormatString="{0:d}" HeaderText="fecha_procesado" SortExpression="fecha_procesado" />
                            <asp:BoundField DataField="fecha_rechazado" DataFormatString="{0:d}" HeaderText="fecha_rechazado" SortExpression="fecha_rechazado" />
                            <asp:CheckBoxField DataField="pendiente" HeaderText="pendiente" SortExpression="pendiente" />
                            <asp:BoundField DataField="id_usuario" HeaderText="id_usuario" SortExpression="id_usuario" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataGrilla" runat="server" ConnectionString="<%$ ConnectionStrings:bd_Orden %>" DeleteCommand="usp_OrdenDelete" DeleteCommandType="StoredProcedure" SelectCommand="REP_CONSULTA_ORDEN_GRID" SelectCommandType="StoredProcedure">
                        <DeleteParameters>
                            <asp:Parameter Name="id_orden" Type="Int64" />
                            <asp:CookieParameter CookieName="IdUsuario" Name="add_usuario" Type="Int32" />
                        </DeleteParameters>
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cboSector" Name="id_sector" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtFechaInicio" DbType="Date" Name="fecha_inicio" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtFechaFin" DbType="Date" DefaultValue="" Name="fecha_fin" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
            <uc2:wucReporte ID="wucReporte1" runat="server" />
            <uc3:cuMensajeBox ID="cuMensajeBox1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdEnviarExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

