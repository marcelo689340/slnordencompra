﻿Imports CapaMedio.DAO

Partial Class Reporte_rptReporteOrdenCompraEstado

    Inherits PaginaBase

    Protected Sub Visualizar_Report(ByVal dt As Data.DataTable)
        Dim ds As New Data.DataSet
        ds.Tables.Add(dt)
        ds.Tables(0).TableName = "REPORTE_ORDEN_COMPRA"
        Session("rs") = ds
        Dim sReporte As String = ""
        Select Case cboSector.SelectedValue
            Case "AUTORIZADO"
                sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompraAutorizado.rpt"
            Case "PENDIENTE"
                sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompraPendiente.rpt"
            Case "RECHAZADO"
                sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompraRechazado.rpt"
            Case "CHEQUEADO"
                sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompraCheck.rpt"
                ' sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompraChequeado.rpt"

        End Select

        wucReporte1.Show(sReporte)
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim dao As DAO_ORDEN = New DAO_ORDEN()

        Try
            Dim dt As New Data.DataTable
            dt = dao.repOrdenCompraEstado(cboSector.SelectedValue, txtFechaInicio.Text, txtFechaFin.Text)

            If dt.Rows.Count = 0 Then
                Mostrar_Error("NO EXISTE DATOS..")
            Else

                Visualizar_Report(dt)

            End If
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            Mostrar_Error(sMensaje)
            'cuMensajeBox1.tipoMensaje = 2
            'cuMensajeBox1.Mensaje = sMensaje
            'cuMensajeBox1.show()
        End Try
    End Sub
    Private Sub Mostrar_Error(sError As String)
        Dim sMensaje As String = sError
        cuMensajeBox1.tipoMensaje = 2
        cuMensajeBox1.Mensaje = sMensaje
        cuMensajeBox1.show()
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim sPagina As String = "/Reporte/rptReporteOrdenCompraEstado.aspx"
            Dim sMens As String = Validar_Ingreso(sPagina)
            If sMens = "" Then
                Dim cooBuscar As HttpCookie = Request.Cookies("Buscar")
                If Not cooBuscar Is Nothing Then
                    'txtBuscar.Text = cooBuscar.Value.ToString
                    'Cargar_Grilla()
                End If
                ' Usuario.Value = Request.Cookies("idUsuario").Value
                Response.Cookies("PaginaAnterior").Value = "~" + sPagina
                CType(Master.FindControl("lblTitulo"), Label).Text = "REPORTE DE ORDEN DE COMPRAS POR ESTADO"
                GetCookies()
            Else
                Response.Redirect("~/ErrorPagina.aspx?Mensaje=" + sMens, False)
            End If
        End If
    End Sub

    Private Sub SetCookies()
        Cookies(Me, Me.cboSector.GetType.Name) = cboSector.SelectedValue
        Cookies(Me, Me.txtFechaFin.GetType.Name) = txtFechaFin.Text
        Cookies(Me, Me.txtFechaInicio.GetType.Name) = txtFechaInicio.Text
    End Sub

    Private Sub GetCookies()
        If Cookies(Me, Me.cboSector.GetType.Name) IsNot Nothing Then
            cboSector.SelectedValue = Cookies(Me, Me.cboSector.GetType.Name)
        End If

        If Cookies(Me, Me.txtFechaFin.GetType.Name) IsNot Nothing Then
            txtFechaFin.Text = Cookies(Me, Me.txtFechaFin.GetType.Name)
        End If

        If Cookies(Me, Me.txtFechaInicio.GetType.Name) IsNot Nothing Then
            txtFechaInicio.Text = Cookies(Me, Me.txtFechaInicio.GetType.Name)
        End If
    End Sub

End Class
