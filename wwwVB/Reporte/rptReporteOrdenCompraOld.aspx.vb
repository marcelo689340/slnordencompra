﻿Imports CapaMedio.DAO

Partial Class Reporte_rptReporteOrdenCompraOld
    Inherits PaginaBase

    Protected Sub Visualizar_Report(ByVal dt As Data.DataTable)
        Dim ds As New Data.DataSet
        ds.Tables.Add(dt)
        ds.Tables(0).TableName = "viewOrden"
        Session("rs") = ds
        Dim sReporte As String
        sReporte = "../reportes.aspx?reporte=Reportes/rptOrdenCompraSector.rpt"
        wucReporte1.Show(sReporte)
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim dao As DAO_ORDEN = New DAO_ORDEN()

        Try
            Dim dt As New Data.DataTable
            dt = dao.repOrdenCompra(cboSector.SelectedValue, txtFechaInicio.Text, txtFechaFin.Text)
            Visualizar_Report(dt)
        Catch ex As Exception
            Dim sMensaje As String = ex.Message.ToString
            cuMensajeBox1.tipoMensaje = 2
            cuMensajeBox1.Mensaje = sMensaje
            cuMensajeBox1.show()
        End Try
        'Dim dao As DAO_RETIRO_EFECTIVO = New DAO_RETIRO_EFECTIVO()
        'Try
        '    Dim dt As New Data.DataTable
        '    Dim id_retiro = dao.Insertar(Convert.ToInt16(Request.QueryString("juego").ToString()), Convert.ToInt16(Request.QueryString("mesa").ToString()), Convert.ToInt16(Request.Cookies("idUsuario").Value))
        '    dt = dao.SetRetiro(id_retiro)
        '    Visualizar_Report(dt)
        '    cmdGuardar.Enabled = False
        'Catch ex As Exception
        '    Dim sMensaje As String = ex.Message.ToString
        '    cuMensajeBox1.tipoMensaje = 2
        '    cuMensajeBox1.Mensaje = sMensaje
        '    cuMensajeBox1.show()
        'End Try
    End Sub
End Class
